package com.csci6545.evoting.test;

import java.io.IOException;
import java.math.BigInteger;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import com.csci6545.evoting.be.Ballot;
import com.csci6545.evoting.be.BcBallotFactor;
import com.csci6545.evoting.be.Counter;
import com.csci6545.evoting.be.Election;
import com.csci6545.evoting.be.Message;
import com.csci6545.evoting.be.TicketBB;
import com.csci6545.evoting.be.TicketElection;
import com.csci6545.evoting.be.User;
import com.csci6545.evoting.be.Voter;
import com.csci6545.evoting.bl.KeyDistribution;
import com.csci6545.evoting.bl.MgrBallot;
import com.csci6545.evoting.bl.MgrClientConnection;
import com.csci6545.evoting.bl.MgrConfidentiality;
import com.csci6545.evoting.bl.MgrConnectionCounter;
import com.csci6545.evoting.bl.MgrElection;
import com.csci6545.evoting.bl.MgrIntegrity;
import com.csci6545.evoting.bl.MgrMessage;
import com.csci6545.evoting.bl.MgrSSL;
import com.csci6545.evoting.bl.MgrTicket;
import com.csci6545.evoting.bl.MsgHandler;
import com.csci6545.evoting.dl.LoadConfig;

public class TestDrive {

	public static void main(String[] args) throws NoSuchAlgorithmException, ParseException, InvalidKeyException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, NoSuchProviderException, InvalidKeySpecException, UnknownHostException, IOException {
		// Start Load Config
		//LoadConfig.loadDBConfig();
		LoadConfig.loadServerConfig();
		LoadConfig.loadClientConfig();
		//LoadConfig.loadCounterConfig();
		// End Load Config
		// TODO Auto-generated method stub
		KeyPair kp = KeyDistribution.GenerateKeyPairs();
		byte [] key = MgrConfidentiality.generateSecreteKey();
		byte [] iv = MgrConfidentiality.getRandomIV();
		byte [] bcBallot = MgrConfidentiality.encryptAESBallot(new Ballot(1,1).getBytes(),  key, iv);
		byte [] supSig = MgrIntegrity.sign(KeyDistribution.GetPrivateKey(MgrSSL.getServerKeypairFromKeyStore()), bcBallot);
		byte [] pre_hybrid = MgrMessage.submitBcBallotMsg(bcBallot, supSig, 1);
		byte [] hybrid = MgrConfidentiality.hybridEncryption(pre_hybrid, MgrSSL.getCounterPublicKeyFromTrustStore().getEncoded(), iv);
		byte [] msg = MgrMessage.submitHybridMsg(hybrid, iv);
		
		Socket ss = new Socket(LoadConfig.COUNTER_IP_ADDRESS, LoadConfig.COUNTER_PORT);
		MgrMessage.sendMsg(ss, msg);
		
		byte [] bb = MgrMessage.recvMsg(ss);
		Message reply = MgrMessage.recoverCounterAckMsg(bb);
		System.out.println(reply.getDetail());
		//Message hybridCipher = MgrMessage.recoverSubmitHybridMsg(msg); 
		
		/*byte [] msgBytes =  MgrConfidentiality.hybridDecryption(hybridCipher.getHybrid(),
				KeyDistribution.GetPrivateKey(MgrSSL.getCounterKeypairFromKeyStore()).getEncoded(), hybridCipher.getIvBcBallot().toByteArray());
		
		System.out.println(MgrMessage.getMessgaeType(msgBytes));*/
	}

}
