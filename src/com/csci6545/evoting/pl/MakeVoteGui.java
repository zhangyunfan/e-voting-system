package com.csci6545.evoting.pl;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.border.TitledBorder;

import com.csci6545.evoting.be.Election;
import com.csci6545.evoting.be.User;
import com.csci6545.evoting.be.Voter;
import com.csci6545.evoting.bl.MgrElection;

public class MakeVoteGui {
	
	static JComboBox electionList = null;
	JLabel electionLable = null;
	JButton submitBtn = null;
	private JFrame frame = null;
	private static Voter voter;
	public static List<Election> elections;
	private VoterGui voterGui;
	public static JPanel candidatePanel = null;
	public static long electionID;
	public static Map candidates = null;
	public static JRadioButton candidateRad;
	
	public MakeVoteGui() {
		getFrame().setContentPane(mainPanel());
		getFrame().addWindowListener(new VoterListner());
		getFrame().setLocation(150, 180);
		getFrame().setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		getFrame().pack();
		getFrame().setVisible(true);
		getFrame().setResizable(false);
	}

	public static void createAndShow(final Voter user, final List<Election> electionList) {
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				voter = user;
				elections = electionList;
				MakeVoteGui makeVoteGui = new MakeVoteGui();

			}
		};
		SwingUtilities.invokeLater(runnable);
	}

	public JPanel mainPanel() {
		JPanel panel = new JPanel();
		panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		panel.add(electonPanel());
		panel.add(getCandidatePanel());
		return panel;

	}

	public JPanel electonPanel() {
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		TitledBorder border = BorderFactory
				.createTitledBorder("ELECTION INFORMATION");
		border.setTitleJustification(TitledBorder.CENTER);
		panel.setPreferredSize(new Dimension(300, 100));
		panel.setBorder(border);
		panel.add(getElectionLable());
		panel.add(getElectionList());

		return panel;
	}

	public JPanel getCandidatePanel() {
		if(candidatePanel == null){
			candidatePanel = new JPanel();
			candidatePanel.setLayout(new BoxLayout(candidatePanel, BoxLayout.Y_AXIS));
			TitledBorder border = BorderFactory
					.createTitledBorder("CANDIDATES INFORMATION");
			border.setTitleJustification(TitledBorder.CENTER);
			candidatePanel.setBorder(border);
			candidatePanel.setPreferredSize(new Dimension(300, 300));
			candidatePanel.add(getSubmitBtn());
		}
		
		return candidatePanel;
	}
	
	public JFrame getFrame() {
		if (frame == null) {
			frame = new JFrame(ElectionRegistrationGui.ELECTION + " - " + VoterGui.fullName.toUpperCase());

		}
		return frame;
	}


	public JLabel getElectionLable() {
		if (electionLable == null) {
			electionLable = new JLabel("Select Election ID: ");
		}
		return electionLable;
	}

	public JComboBox getElectionList() {
		if (electionList == null) {
			//electionList = new JComboBox(MgrElection.getElectionIdsForVoter(
			//		VoterGui.voter).toArray());
			
			// Poom Fixed it
			List<Long> electList = new ArrayList();
			System.out.println("size "+elections.size());
			for(int i = 0; i < elections.size(); i++){
				//elecList[i] = elections.get(i).getId();
				if(!elections.isEmpty())
					electList.add(elections.get(i).getId());
			}
			electionList = new JComboBox(electList.toArray());
			electionList.addActionListener(new MakeVoteListner(this));
			electionList.setActionCommand(MakeVoteListner.SELECT_ELECTION);
			
		}
		return electionList;
	}

	public JButton getSubmitBtn() {
		if (submitBtn == null) {
			submitBtn = new JButton("SUBMIT");
			submitBtn
					.addActionListener(new MakeVoteListner(this));
			submitBtn.setActionCommand(MakeVoteListner.SUBMIT_VOTE);
		}
		return submitBtn;
	}

}
