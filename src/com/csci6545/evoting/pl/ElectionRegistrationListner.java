package com.csci6545.evoting.pl;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.Socket;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import com.csci6545.evoting.be.Message;
import com.csci6545.evoting.bl.MgrAudit;
import com.csci6545.evoting.bl.MgrMessage;
import com.csci6545.evoting.bl.MgrSSL;
import com.csci6545.evoting.bl.MsgHandler;

public class ElectionRegistrationListner implements ActionListener {

	ElectionRegistrationGui electionGui;
	
	public ElectionRegistrationListner(
			ElectionRegistrationGui electionRegistrationGui) {
		this.electionGui = electionRegistrationGui;
	
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String command = e.getActionCommand();
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		if(command.equals(ClientListner.REGISTER)){
			Socket ss = MgrSSL.initSSLClientToServerSocket();
			byte [] msg = MgrMessage.registerForElectionMsg((Long)ElectionRegistrationGui.getElectionList().getSelectedItem(),VoterGui.voter.getTicketBB());
			MgrMessage.sendMsg(ss, msg);
			//
			System.out.println(new String(msg));
			byte[] bb = MgrMessage.recvMsg(ss);

			//Handle Msg
			Message recvAckRegElection= MsgHandler.voterHandleMessage(MgrMessage.getMessgaeType(bb),bb,ss);
			VoterGui.voter.setTicketBB(recvAckRegElection.getTicketBB());
			if(!VoterGui.voter.getTicketBB().getRole().equals("failed"))
			{
				if(recvAckRegElection.getDetail().equals("succeeded"))
				{
					JOptionPane.showMessageDialog(frame, "You have successfully registered for the election");
					
					electionGui.getFrame().dispose();
				}
				else
				{
					int dialogButton1 = JOptionPane.WARNING_MESSAGE;
					// failed authentication
					JOptionPane
							.showMessageDialog(
									frame,
									recvAckRegElection.getDetail(),
									"Election Registration", dialogButton1);
				}
			}
			else
			{
				int dialogButton1 = JOptionPane.WARNING_MESSAGE;
				JOptionPane
				.showMessageDialog(
						frame,
						"Ticket Expired!!",
						"Error", dialogButton1);
				VoterListner.voter.getFrame().dispose();
				ClientGui.createAndShowGUI();
			}
		}

	}

}
