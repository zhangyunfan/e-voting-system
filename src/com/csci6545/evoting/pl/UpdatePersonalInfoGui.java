/* Autheor: Alemberhan Getahun
 * Date: 11/25/2013 */
package com.csci6545.evoting.pl;

import java.awt.Component;
import java.awt.FlowLayout;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.TitledBorder;

import com.csci6545.evoting.be.Election;
import com.csci6545.evoting.be.User;
import com.csci6545.evoting.be.Voter;

public class UpdatePersonalInfoGui {
	JLabel fnameLable = null;
	JLabel lnameLable = null;
	JLabel dobLable = null;
	JLabel emailLable = null;
	JTextField fnameText = null;
	JTextField lnameText = null;
	JTextField dobText = null;
	JTextField emailText = null;
	JButton submit = null;
	private static JFrame frame = null;
	
	public static String fullName = null;
	public static Voter voter;
	
	public UpdatePersonalInfoGui()
	{
		getFrame().setContentPane(mainPanel());
		getFrame().addWindowListener(new VoterListner());
		getFrame().setLocation(150, 180);
		getFrame().setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		getFrame().pack();
		getFrame().setVisible(true);
		getFrame().setResizable(false);
	}
	
	public static void createAndShow(final Voter user)
	{
		voter = user;
		Runnable runnable = new Runnable() {
			
			@Override
			public void run() {
				UpdatePersonalInfoGui.setFullName(user.getfName(), user.getlName());
				UpdatePersonalInfoGui voter = new UpdatePersonalInfoGui();
				
				
			}
		};
		SwingUtilities.invokeLater(runnable);
		
		
	}
	
	//
	
	public JFrame getFrame() {
		if(frame == null){
			frame = new JFrame( "UPDATE PERSONAL INFO - " + UpdatePersonalInfoGui.fullName.toUpperCase());
			
		}
		return frame;
	}
	
	public JPanel mainPanel()
	{
		JPanel panel = new JPanel();
		panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		panel.add(updatePanel());
		return panel;
	}


	
	public JPanel updatePanel()
	{
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		panel.add(getFnamePanel());
		panel.add(getLnamePanel());
		//panel.add(getDobPanel());
		panel.add(getEmailPanel());
		panel.add(getSubmit());
		return panel;
		
	}
	private JPanel getFnamePanel() {
		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout(FlowLayout.LEFT));
		panel.add(getFnameLable());
		panel.add(getFnameText());
		return panel;
	}
	
	private JPanel getLnamePanel() {
		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout(FlowLayout.LEFT));
		panel.add(getLnameLable());
		panel.add(getLnameText());
		return panel;
	}
	
	private JPanel getDobPanel() {
		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout(FlowLayout.LEFT));
		panel.add(getDobLable());
		panel.add(getDobText());
		return panel;
	}
	
	private JPanel getEmailPanel() {
		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout(FlowLayout.LEFT));
		panel.add(getEmailLable());
		panel.add(getEmailText());
		return panel;
	}

	public static void setFullName(String fname, String lname) {
		UpdatePersonalInfoGui.fullName = fname + " " + lname;
	}

	public JLabel getFnameLable() {
		if(fnameLable == null){
			fnameLable = new JLabel("FIRST NAME:");
		}
		return fnameLable;
	}

	public JLabel getLnameLable() {
		if(lnameLable == null){
			lnameLable = new JLabel("LAST NAME:");
		}
		return lnameLable;
	}

	public JLabel getDobLable() {
		if(dobLable == null){
			dobLable = new JLabel("DATE OF BIRTH (YYYY-MM-DD):");
		}
		return dobLable;
	}

	public JLabel getEmailLable() {
		if(emailLable == null){
			emailLable = new JLabel("EMAIL:");
		}
		return emailLable;
	}

	public JTextField getFnameText() {
		if(fnameText == null){
			fnameText = new JTextField(voter.getfName());
		}
		return fnameText;
	}

	public JTextField getLnameText() {
		if(lnameText == null){
			lnameText = new JTextField(voter.getlName());
		}
		return lnameText;
	}

	public JTextField getDobText() {
		if(dobText == null){
			//Format formatter = new SimpleDateFormat("yyyy-MM-dd");
			//String s = formatter.format(voter.getDob());
			dobText = new JTextField(13);
		}
		return dobText;
	}

	public JTextField getEmailText() {
		if(emailText == null){
			emailText = new JTextField(voter.getEmail());
			
		}
		return emailText;
	}
	
	public JButton getSubmit(){
		if(submit == null){
			submit = new JButton("SUBMIT");
			submit.addActionListener(new UpdatePersonalInfoListner(this));
			submit.setActionCommand(UpdatePersonalInfoListner.UPDATE_PERSONAL);
		}
		return submit;
	}
	
	
}
