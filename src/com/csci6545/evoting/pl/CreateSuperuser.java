package com.csci6545.evoting.pl;

import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.Scanner;

import com.csci6545.evoting.be.User;
import com.csci6545.evoting.bl.MgrPassword;
import com.csci6545.evoting.dl.LoadConfig;
import com.csci6545.evoting.dl.MgrCounter;
import com.csci6545.evoting.dl.MgrSuperuser;

public class CreateSuperuser {
	public static void main(String[] args) throws NoSuchAlgorithmException, SQLException
	{
		LoadConfig.loadServerDBConfig();
		System.out.println("#####     Create Super User    #####");
		Scanner inputk = new Scanner(System.in);
		String username = null;
		try {
			do
			{
				System.out.println("Please enter username: ");
				username = inputk.nextLine();
			}
			while(MgrSuperuser.existsSuper(username));
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		System.out.print("Please enter valid email address: ");
		String email = inputk.nextLine();
		if(MgrSuperuser.createSuper(username, "temp", email) == 1)
		{
			System.out.println("Creating superuser, please wait....");			 
			if(MgrPassword.generateSuperRandomPassword(username, "Welcome to E-voting System", 
					 "Your username is: "+username+" Your one time password is" ))
			{
				System.out.println("Succeeded, please get your one time pasword from your email");
			}
			
		}
	}
}
