package com.csci6545.evoting.pl;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.Socket;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;

import com.csci6545.evoting.be.Ballot;
import com.csci6545.evoting.be.BcBallotFactor;
import com.csci6545.evoting.be.Election;
import com.csci6545.evoting.be.Message;
import com.csci6545.evoting.bl.MgrBallot;
import com.csci6545.evoting.bl.MgrMessage;
import com.csci6545.evoting.bl.MgrResult;
import com.csci6545.evoting.bl.MgrSSL;
import com.csci6545.evoting.bl.MsgHandler;

public class CheckResultListner implements ActionListener {

	public static final String CHECK_RESULT = "check result";
	CheckResultGui gui;
	public static long id;
	public CheckResultListner(CheckResultGui gui) {
		this.gui = gui;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JFrame frame = new JFrame();
		String command = e.getActionCommand();
		Election selectedElection = new Election();
		if(command.equals(CheckResultListner.CHECK_RESULT)){
			//check result
			id = 	(Long) gui.getElectionList().getSelectedItem();
			CheckResultGui.electionID = id;
			CheckResultGui.resultArea.setText(null);
			//System.out.println(gui.electionID);
			for(int i=0;i<CheckResultGui.elections.size();i++)
			{
				if(CheckResultGui.elections.get(i).getId() == id)
				{
					selectedElection = CheckResultGui.elections.get(i);
					break;
				}
			}
			System.out.println(id);
			
			// Getting Result below
			byte [] msg = MgrMessage.checkBcBallotFactorMsg("CheckResult", VoterGui.voter.getTicketBB(), id);
			Socket s = MgrSSL.initSSLClientToServerSocket();
			MgrMessage.sendMsg(s, msg);
			
			byte [] bb = MgrMessage.recvMsg(s);
			Message m = MsgHandler.voterHandleMessage(MgrMessage.getMessgaeType(bb), bb, s);
			VoterGui.voter.setTicketBB(m.getTicketBB());
			if(!VoterGui.voter.getTicketBB().getRole().equals("failed"))
			{
				if(m.getDetail().equals("succeeded"))
				{
					//show result
					displayResult(m.getBcArray(),selectedElection);
					verifyResult(m.getBcArray(),selectedElection);
				}
				else
				{
					int dialogButton1 = JOptionPane.WARNING_MESSAGE;
					// failed authentication
					JOptionPane
							.showMessageDialog(
									frame,
									m.getDetail(),
									"Election Registration", dialogButton1);
				}
			}
			else{
				int dialogButton1 = JOptionPane.WARNING_MESSAGE;
				JOptionPane
				.showMessageDialog(
						frame,
						"Ticket Expired!!",
						"Error", dialogButton1);
				gui.getFrame().dispose();
				VoterListner.voter.getFrame().dispose();
				ClientGui.createAndShowGUI();
			}
		}

	}
	static void displayResult(BcBallotFactor [] bcFactorArr, Election selectedElection)
	{
		int [] result = MgrResult.getResult(bcFactorArr,selectedElection);
		System.out.println(result.length);
		Set mapSet = (Set) selectedElection.getCandidate().entrySet();
		Iterator iterate = mapSet.iterator();
		Iterator it = selectedElection.getCandidate().entrySet().iterator();
		CheckResultGui.resultArea.append("Election result: "+selectedElection.getName()+"\n");
		CheckResultGui.resultArea.append("Election ID: "+selectedElection.getId()+"\n");
		CheckResultGui.resultArea.append("\n\n");
		while(iterate.hasNext()){
			Map.Entry cadidate = (Map.Entry) iterate.next();
			long candidateId = Integer.parseInt(cadidate.getKey().toString().replace(" ", ""));		
			
			Map.Entry pairs = (Map.Entry)it.next();
			System.out.println((String) pairs.getValue()+": "+result[(int)candidateId]);
			CheckResultGui.resultArea.append((String) pairs.getValue()+": "+result[(int)candidateId] +"\n");
		}
		CheckResultGui.resultArea.append("\n\n");
		
	}
	static void verifyResult(BcBallotFactor [] bcFactorArr, Election selectedElection)
	{
		boolean foundElection = false;
		Set mapSet = (Set) VoterGui.voter.getMapElectionEntry().entrySet();
		Iterator iterate = mapSet.iterator();
		while(iterate.hasNext()){
			Map.Entry electionEntry = (Map.Entry) iterate.next();
			long electionId = Integer.parseInt(electionEntry.getKey().toString().replace(" ", ""));		
			if(electionId == id)
			{			
				foundElection = true;
				break;
			}
		}
		if(foundElection)
		{
			int entry = VoterGui.voter.getMapElectionEntry().get(id);
			byte [] myBallot = VoterGui.voter.getBallotMap().get(id);
			Ballot myBl = MgrBallot.recoverBallot(myBallot);
			System.out.println(myBl.getcandidateID());
			if(MgrResult.verifyMyBallot(bcFactorArr, entry, myBallot)){
				CheckResultGui.resultArea.append("Your ballot: "+
			selectedElection.getCandidate().get(myBl.getcandidateID()+"")+" (entry: "+entry+")");
			}	
		}
		else{
			CheckResultGui.resultArea.append("You cannot verify your ballot result in this election");
		}

	}

}
