package com.csci6545.evoting.pl;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.net.Socket;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;

import org.bouncycastle.util.Arrays;






import com.csci6545.evoting.be.Counter;
import com.csci6545.evoting.be.Election;
import com.csci6545.evoting.be.Message;
import com.csci6545.evoting.be.Supervisor;
import com.csci6545.evoting.be.Title;
import com.csci6545.evoting.be.User;
import com.csci6545.evoting.bl.MgrElection;
import com.csci6545.evoting.bl.MgrMessage;
import com.csci6545.evoting.bl.MgrSSL;
import com.csci6545.evoting.bl.MsgHandler;
import com.csci6545.evoting.dl.MgrTitle;


public class SupervisorListner extends WindowAdapter implements ActionListener {

	public static Supervisor supervisor = null;
	public static SupervisorGui superGui = null;
	private JFrame frame = null;
	public SupervisorListner(SupervisorGui supervisorGui) {
		this.superGui = supervisorGui;
	}

	public SupervisorListner() {
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		String command = e.getActionCommand();
		System.out.println(command);
		
		 if(command.equals(SupervisorGui.MANAGE_VOTER)){
			//manage user action
			
		} else if(command.equals(SupervisorGui.SIGN_OUT)){
			try {
				MgrMessage.sendMsg(SupervisorGui.socket, MgrMessage.logOutMsg(SupervisorGui.supervisor.getTicketBB()));
			} catch (Exception ex) {
				// TODO Auto-generated catch block
				ex.printStackTrace();
			}
			superGui.getMainFrame().dispose();
		} else if (command.equals(ElectionCreationGui.CREATE)){
			//create an election in the database and send the election id
			if(!(ElectionCreationGui.nameField.getText().toString().equals("")))
				{
				List<String> temp = ElectionCreationGui.titleList.getSelectedValuesList();
				List<Title> selectedTitle = new ArrayList<Title>();
				if(temp.size() > 0)
				{
					for(int i=0;i<temp.size();i++)
					{
						Title t = new Title();
						t.setTitleName(temp.get(i));
						t.setPermission(0);
						selectedTitle.add(t);
					}
					List<String> tempInclude = ElectionCreationGui.includeList.getSelectedValuesList();
					List<String> tempExclude = ElectionCreationGui.excludeList.getSelectedValuesList();
					
					List<User> includedUser = getIncludedUser(tempInclude);
					if(includedUser.size()==0)
					{
						includedUser = getDummyAllowedUser();
					}
					List<User> ExcludedUser = getExcludedUser(tempExclude);
					if(ExcludedUser.size()==0){
						ExcludedUser = getDummyDeniedUser();
					}
					Map<String,String> candidateMap = getSelectedCandidate(ElectionCreationGui.candidateList.getSelectedValuesList());
					System.out.println(ElectionCreationGui.candidateList.getSelectedValuesList().size());
					
					if(candidateMap.size() > 1)
					{
						Timestamp startDate = new Timestamp(ElectionCreationGui.startDate.getDate().getTime());
						Timestamp endDate = new Timestamp(ElectionCreationGui.endDate.getDate().getTime());
						Election newElection = MgrElection.createElection(0L, ElectionCreationGui.nameField.getText().toString(), 
								startDate, endDate, candidateMap, getDummyCounter());
						byte [] msg = MgrMessage.createElectionMsg(VoterGui.voter.getUserId(), 
								newElection, VoterGui.voter.getTicketBB(), 
								includedUser, ExcludedUser, selectedTitle);
						//System.out.println(MgrElection.recoverElection(newElection.getBytes()).getCandidate().get("0"));
						Socket s = MgrSSL.initSSLClientToServerSocket();
						
						MgrMessage.sendMsg(s, msg);
						
						byte [] bb = MgrMessage.recvMsg(s);
						Message m = MsgHandler.supHandleMsg(MgrMessage.getMessgaeType(bb), bb, s);
						VoterGui.voter.setTicketBB(m.getTicketBB());
						if(m.getDetail().equals("succeeded"))
						{
							JOptionPane.showMessageDialog(frame, "You have successfully created an election");
						}
						else
						{
							int dialogButton1 = JOptionPane.WARNING_MESSAGE;
							// failed authentication
							JOptionPane
									.showMessageDialog(
											frame,
											m.getDetail(),
											"Election Creation", dialogButton1);
						}
					}
					else
					{
						int dialogButton1 = JOptionPane.WARNING_MESSAGE;
						// failed authentication
						JOptionPane
								.showMessageDialog(
										frame,
										"You must select at lease 2 candidates",
										"Election Creation", dialogButton1);
					}
					
				}
				else	
				{
					int dialogButton1 = JOptionPane.WARNING_MESSAGE;
					// failed authentication
					JOptionPane
							.showMessageDialog(
									frame,
									"You must select at least one eligible title",
									"Election Creation", dialogButton1);
				}
			}
			else
			{
				int dialogButton1 = JOptionPane.WARNING_MESSAGE;
				// failed authentication
				JOptionPane
						.showMessageDialog(
								frame,
								"You must enter election name",
								"Election Creation", dialogButton1);
			}
		}
		
	}
	
	public List<User> getDummyAllowedUser()
	{
		List<User> dummy= new ArrayList<User>();
		User du = new User();
		du.setUserId("unknown");
		dummy.add(du);
		return dummy;
	}
	
	public List<User> getDummyDeniedUser()
	{
		List<User> dummy= new ArrayList<User>();
		User du = new User();
		du.setUserId(VoterGui.voter.getUserId());
		dummy.add(du);
		return dummy;
	}
	
	public Counter getDummyCounter()
	{
		Counter dummy= new Counter();
		dummy.setfName("unknown");
		dummy.setlName("unknown");
		dummy.setUserId("unknown");
		return dummy;
	}

	public Map<String, String> getSelectedCandidate(List<String> selected)
	{
		Map<String, String> cand= new HashMap<String,String>();
		for(int i = 0;i < selected.size();i++)
		{
			int startIndex = selected.get(i).indexOf("[");
			String username = selected.get(i).substring(startIndex+1,selected.get(i).length()-1);
			cand.put(i+"", username);
		}
		return cand;
	}
	
	public List<User> getIncludedUser(List<String> selected)
	{
		List<User> include= new ArrayList<User>();
		for(int i = 0;i < selected.size();i++)
		{
			int startIndex = selected.get(i).indexOf("[");
			String username = selected.get(i).substring(startIndex+1,selected.get(i).length()-1);
			User iu = new User();
			iu.setUserId(username);
			include.add(iu);
		}
		return include;
	}
	public List<User> getExcludedUser(List<String> selected)
	{
		List<User> exclude= new ArrayList<User>();
		for(int i = 0;i < selected.size();i++)
		{
			int startIndex = selected.get(i).indexOf("[");
			String username = selected.get(i).substring(startIndex+1,selected.get(i).length()-1);
			User iu = new User();
			iu.setUserId(username);
			exclude.add(iu);
		}
		User du = new User();
		du.setUserId(VoterGui.voter.getUserId());
		exclude.add(du);
		return exclude;
	}
}
