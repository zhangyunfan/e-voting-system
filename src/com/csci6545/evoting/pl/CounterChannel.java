package com.csci6545.evoting.pl;

import java.awt.Container;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

import com.csci6545.evoting.bl.KeyDistribution;
import com.csci6545.evoting.bl.MgrCounterChannel;
import com.csci6545.evoting.bl.MgrIntegrity;
import com.csci6545.evoting.bl.MgrMessage;
import com.csci6545.evoting.bl.MgrSSL;
import com.csci6545.evoting.bl.MgrSystemProperties;
import com.csci6545.evoting.bl.MsgHandler;
import com.csci6545.evoting.bl.MgrClientConnection;
import com.csci6545.evoting.dl.LoadConfig;
import com.sun.corba.se.impl.oa.poa.ActiveObjectMap.Key;
public class CounterChannel {

	static JTextArea consol = null;
	JFrame mainFrame = null;
	static String passKeystore;	
	public JTextArea getConsol() {
		if(consol == null){
			consol = new JTextArea(30, 70);
		}
		return consol;
	}

	public static void appendText(String text) {
		String newline = "\n";
		consol.append(text+newline);
	}

	public JFrame getMainFrame() {
		if(mainFrame == null){
			mainFrame = new JFrame("COUNTER CHANNEL");
		}
		return mainFrame;
	}



	public CounterChannel()
	{
		
		getMainFrame().setContentPane((Container) getConsolPanel());
		
		//create and setup the window
		getMainFrame().addWindowListener(new ClientListner());
		getMainFrame().setLocation(150, 180);
		getMainFrame().setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		//Display the window
		getMainFrame().pack();
		getMainFrame().setVisible(true);
		getMainFrame().setResizable(false);
	}
	
	

	private Object getConsolPanel() {
		JPanel panel = new JPanel();
		panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		panel.add(getConsol());
		return panel;
	}



	public static void main(String[] args)
	{
		//passKeystore = args[0];
		LoadConfig.loadServerConfig();
		LoadConfig.loadServerDBConfig();
		CounterChannel.createAndShowGUI();
	}
	
	public static void createAndShowGUI()
	{
		Runnable createAndShow = new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				
				JFrame.setDefaultLookAndFeelDecorated(true);
				CounterChannel gui = new CounterChannel();
				
			}
		};
		SwingUtilities.invokeLater(createAndShow);
		ServerSocket ss=null;
		try {
			KeyPair kp = MgrSSL.getServerKeypairFromKeyStore();
            ss = MgrSSL.initSSLServerCounterChannelSocket();
            while (true) {
            	new MgrCounterChannel(ss.accept(), KeyDistribution.GetPrivateKey(kp),
            			KeyDistribution.GetPublicKey(kp), MgrIntegrity.getHmacKey(),
            			MgrSSL.getCounterPublicKeyFromTrustStore(),LoadConfig.COUNTER_IP_ADDRESS, LoadConfig.COUNTER_PORT).start();
            }
        } catch (Exception e) {
            System.out.println(e);
            
        }  
	}
}
