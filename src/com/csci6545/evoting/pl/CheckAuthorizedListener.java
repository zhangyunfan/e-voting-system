package com.csci6545.evoting.pl;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.Socket;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;

import com.csci6545.evoting.be.Ballot;
import com.csci6545.evoting.be.BcBallotFactor;
import com.csci6545.evoting.be.Election;
import com.csci6545.evoting.be.Message;
import com.csci6545.evoting.bl.MgrBallot;
import com.csci6545.evoting.bl.MgrMessage;
import com.csci6545.evoting.bl.MgrResult;
import com.csci6545.evoting.bl.MgrSSL;
import com.csci6545.evoting.bl.MsgHandler;

public class CheckAuthorizedListener implements ActionListener {

	public static final String CHECK_AUTHORIZED = "check authorized";
	CheckAuthorized gui;
	public static long id;
	public CheckAuthorizedListener(CheckAuthorized gui) {
		this.gui = gui;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JFrame frame = new JFrame();
		String command = e.getActionCommand();
		Election selectedElection = new Election();
		if(command.equals(CheckAuthorizedListener.CHECK_AUTHORIZED)){
			//check result
			id = 	(Long) gui.getElectionList().getSelectedItem();
			CheckAuthorized.electionID = id;
			CheckAuthorized.resultArea.setText(null);
			//System.out.println(gui.electionID);
			for(int i=0;i<CheckAuthorized.elections.size();i++)
			{
				if(CheckAuthorized.elections.get(i).getId() == id)
				{
					selectedElection = CheckAuthorized.elections.get(i);
					break;
				}
			}
			System.out.println(id);
			
			// Getting Result below
			byte [] msg = MgrMessage.checkAuthorizedBallotMsg(VoterGui.voter.getTicketBB(), id);
			Socket s = MgrSSL.initSSLClientToServerSocket();
			MgrMessage.sendMsg(s, msg);
			
			byte [] bb = MgrMessage.recvMsg(s);
			Message m = MsgHandler.voterHandleMessage(MgrMessage.getMessgaeType(bb), bb, s);
			VoterGui.voter.setTicketBB(m.getTicketBB());
			if(!VoterGui.voter.getTicketBB().getRole().equals("failed"))
			{
				if(m.getDetail().equals("succeeded"))
				{
					//show result
					CheckAuthorized.resultArea.append("Election result: "+selectedElection.getName()+"\n");
					CheckAuthorized.resultArea.append("Election ID: "+selectedElection.getId()+"\n");		
					if(m.getAllAuthorizedUserBallot().size()>1)
					{
						CheckAuthorized.resultArea.append(m.getAllAuthorizedUserBallot().size()+" ballots have been authorized \n");
						CheckAuthorized.resultArea.append("\n");
						/*for(int i=0;i<m.getAllAuthorizedUserBallot().size();i++)
						{
							CheckAuthorized.resultArea.append(m.getAllAuthorizedUserBallot().get(i) +"\n");
						}*/
					}
					else
					{
						int dialogButton1 = JOptionPane.WARNING_MESSAGE;
						// failed authentication
						JOptionPane
								.showMessageDialog(
										frame,
										"The authorized ballots are not ready to be published",
										"Check Authorized Ballots", dialogButton1);
					}
				}
				else
				{
					int dialogButton1 = JOptionPane.WARNING_MESSAGE;
					// failed authentication
					JOptionPane
							.showMessageDialog(
									frame,
									m.getDetail(),
									"Check Authorized Ballots", dialogButton1);
				}
			}
			else{
				int dialogButton1 = JOptionPane.WARNING_MESSAGE;
				JOptionPane
				.showMessageDialog(
						frame,
						"Ticket Expired!!",
						"Error", dialogButton1);
				gui.getFrame().dispose();
				VoterListner.voter.getFrame().dispose();
				ClientGui.createAndShowGUI();
			}
		}

	}	

}
