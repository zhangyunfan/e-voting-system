package com.csci6545.evoting.pl;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.border.TitledBorder;

import com.csci6545.evoting.be.Election;
import com.csci6545.evoting.be.User;
import com.csci6545.evoting.be.Voter;
import com.csci6545.evoting.dl.MgrElection;

public class VerifyGui {
	public static String VERIFY = "VERIFY BALLOTS";
	static JComboBox electionList = null;
	JLabel electionLable = null;
	JButton verifyBtn = null;
	private JFrame frame = null;
	JTextArea electionArea = null;
	public static JScrollPane sbrConsol = null;
	private static Voter voter;
	private static List<Election> elections;
	private VoterGui voterGui;

	public VerifyGui() {
		getFrame().setContentPane(mainPanel());
		getFrame().addWindowListener(new VoterListner());
		getFrame().setLocation(150, 180);
		getFrame().setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		getFrame().pack();
		getFrame().setVisible(true);
		getFrame().setResizable(false);
	}

	public static void createAndShow(final Voter user, final List<Election> electionList) {
		
		Runnable runnable = new Runnable() {
			
			@Override
			public void run() {
				voter = user;
				elections = electionList;
				VerifyGui electionRegister = new VerifyGui();

			}
		};
		SwingUtilities.invokeLater(runnable);
	}


	public JFrame getFrame() {
		if (frame == null) {
			frame = new JFrame(VerifyGui.VERIFY + " - " + VoterGui.fullName.toUpperCase());

		}
		return frame;
	}


	public JLabel getElectionLable() {
		if (electionLable == null) {
			electionLable = new JLabel("Select Election ID: ");
		}
		return electionLable;
	}

	public static JComboBox getElectionList() {
		if (electionList == null) {
			electionList = new JComboBox(voter.getElectionIdList().toArray(new Long[voter.getElectionIdList().size()]));			
		}
		return electionList;
	}

	public JButton getVerifyBtn() {
		if (verifyBtn == null) {
			verifyBtn = new JButton(ClientListner.VERIFY_BALLOT);
			verifyBtn
					.addActionListener(new VerifyListener(this));
			verifyBtn.setActionCommand(ClientListner.VERIFY_BALLOT);
		}
		return verifyBtn;
	}

	public JScrollPane getElectionArea() {
		if (sbrConsol == null) {
			electionArea = new JTextArea(20, 30);
			electionArea.setLineWrap(true);
			electionArea.setText("");
			if (VoterGui.elections != null) {
				Iterator it = VoterGui.elections.iterator();
				while (it.hasNext()) {
					Election election = (Election) it.next();
					electionArea.append("ID: "
							+ String.valueOf(election.getId()) + "\n");
					electionArea.append("Name: " + election.getName() + "\n");
					electionArea.append("Start Date: "
							+ String.valueOf(election.getStart()) + "\n");
					electionArea.append("End Date: "
							+ String.valueOf(election.getEnd()) + "\n");
					electionArea
							.append("*****************************************************"
									+ "\n");

				}

			}
		}
		electionArea.setEditable(false);
		
		sbrConsol = new JScrollPane(electionArea);
		sbrConsol.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

		return sbrConsol;
	}

	public JPanel mainPanel() {
		JPanel panel = new JPanel();
		panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		panel.add(registrationPanel());
		panel.add(electonPanel());
		return panel;

	}

	public JPanel registrationPanel() {
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		TitledBorder border = BorderFactory
				.createTitledBorder("BALLOT VERIFICATION");
		border.setTitleJustification(TitledBorder.CENTER);
		panel.setPreferredSize(new Dimension(300, 100));
		panel.setBorder(border);
		panel.add(getElectionLable());
		panel.add(getElectionList());
		panel.add(getVerifyBtn());

		return panel;
	}

	public JPanel electonPanel() {
		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout());
		TitledBorder border = BorderFactory
				.createTitledBorder("ELECTION INFORMATION");
		border.setTitleJustification(TitledBorder.CENTER);
		panel.setBorder(border);
		panel.add(getElectionArea());
		return panel;
	}

}
