package com.csci6545.evoting.pl;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.Socket;
import java.security.KeyPair;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import com.csci6545.evoting.be.Message;
import com.csci6545.evoting.bl.KeyDistribution;
import com.csci6545.evoting.bl.MgrConfidentiality;
import com.csci6545.evoting.bl.MgrMessage;
import com.csci6545.evoting.bl.MgrPassword;
import com.csci6545.evoting.bl.MgrSSL;
import com.csci6545.evoting.bl.MsgHandler;

public class PasswordResetListner implements ActionListener {

	public static final String RESET_PASSWORD = "reset password";
	ResetPasswordGui client = null;


	public PasswordResetListner() {
	}

	public PasswordResetListner(ResetPasswordGui client) {
		this.client = client;
		}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		String command = e.getActionCommand();
		
		if(command.equals(RESET_PASSWORD)){
			passwordResetActionPerformed();
		}

	}

	private void passwordResetActionPerformed() {
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		int dialogButton = JOptionPane.WARNING_MESSAGE;
		//check the new password entered are identical
		if(!client.getPasswordText().getText().equals(client.getPasswordText1().getText())){
			
			System.out.println("the password is not identical");
			JOptionPane
					.showMessageDialog(
							frame,
							"the password is not identical",
							"Update Password", dialogButton);
			client.getPasswordText().setText("");
			client.getPasswordText1().setText("");
		}
		else if (MgrPassword.isStrong(client.getPasswordText().getText()))
		{
			KeyPair kp = null;
			int result = 0;
			try
			{
				kp = KeyDistribution.GenerateKeyPairs();
				byte [] msg = MgrMessage.updatePasswordMsg(ResetPasswordGui.username, 
					client.getPasswordText().getText(), kp.getPublic());
				Socket s = MgrSSL.initSSLClientToServerSocket();
				MgrMessage.sendMsg(s , msg);
				byte[] bb = MgrMessage.recvMsg(s );
				Message recvAckUpdatePwd = MsgHandler.userHandleMsg(MgrMessage.getMessgaeType(bb),bb,s);
				
				if (recvAckUpdatePwd.getDetail().equals("succeeded")) {
					// generate key
					// code comes here
					// successful key generation message to Voter
					try {
						byte [] hashPwd = MgrConfidentiality.generatePasswordHash(client.getPasswordText().getText());
						byte [] keyPin = MgrConfidentiality.getSecretKey(hashPwd);
						byte [] keyIv = MgrConfidentiality.getIV(hashPwd);
						byte [] encryptedPrivKey = MgrConfidentiality.encryptAESBallot(KeyDistribution.GetPrivateKey(kp).getEncoded(), keyPin, keyIv);
						MgrConfidentiality.byteToFile(encryptedPrivKey, "./resources/"+ResetPasswordGui.username+".bin");
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					JOptionPane
							.showConfirmDialog(
									frame,
									"You have successfully updated password, please sign in again",
									"Update Password", dialogButton);
					client.getMainFrame().dispose();
				}
				else
				{
					// failed authentication
					JOptionPane
							.showMessageDialog(
									frame,
									"Update password failed",
									"Update Password", dialogButton);
					client.getPasswordText().setText("");
					client.getPasswordText1().setText("");
				}
			}
			catch(Exception e)
			{
				
			}
		}
		else
		{
			// failed authentication
			JOptionPane
					.showMessageDialog(
							frame,
							"Password is too weak",
							"Update Password", dialogButton);
			client.getPasswordText().setText("");
			client.getPasswordText1().setText("");
		}
		
	}

}
