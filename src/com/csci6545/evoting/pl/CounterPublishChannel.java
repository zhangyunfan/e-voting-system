package com.csci6545.evoting.pl;

import java.io.Console;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.text.ParseException;
import java.util.List;
import java.util.Scanner;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import com.csci6545.evoting.be.BcBallotFactor;
import com.csci6545.evoting.be.Message;
import com.csci6545.evoting.be.TicketBB;
import com.csci6545.evoting.bl.KeyDistribution;
import com.csci6545.evoting.bl.MgrMessage;
import com.csci6545.evoting.bl.MgrPassword;
import com.csci6545.evoting.bl.MgrSSL;
import com.csci6545.evoting.dl.LoadConfig;
import com.csci6545.evoting.dl.MgrCounter;

public class CounterPublishChannel {
	public static void main(String[] args) throws NoSuchAlgorithmException,
			ParseException, InvalidKeyException, NoSuchPaddingException,
			InvalidAlgorithmParameterException, IllegalBlockSizeException,
			BadPaddingException, NoSuchProviderException,
			InvalidKeySpecException {
		// Start Load Config
		LoadConfig.loadCounterConfig();
		LoadConfig.loadCounterDBConfig();
		// End Load Config

		Scanner inputk = new Scanner(System.in);		
		String cmd = "";		
		System.out.println("Welcome to counter publishing channel");
		/*System.out.print("username: ");
		String username = inputk.nextLine();
		System.out.print("password: ");
		String password = inputk.nextLine();*/
		Console c = System.console();
        if (c == null) {
            System.err.println("No console.");
            System.exit(1);
        }

        String username = c.readLine("Enter your username: ");
        char [] password = c.readPassword("Enter your password: ");
		TicketBB ticket = signIn(username, new String(password), inputk);		
		//TicketBB ticket = signIn(username, password, inputk);
		if(ticket != null && !ticket.getRole().equals("failed"))
		{
			do {
				System.out.println("To check Ballot pool (0) ");
				System.out.println("To publish BcBallot enter (1) ");
				System.out.println("To publish Result enter (2) ");
				System.out.println("To quit enter (q) ");
				cmd = inputk.nextLine();
				if(cmd.equals("0"))
				{
					getStatus();
				}
				else if (cmd.equals("1")) {
					System.out.println("Enter election ID: ");
					Long electionId = Long.parseLong(inputk.nextLine());
					ticket = publishAllBcBallot(electionId, ticket);
				} else if (cmd.equals("2")) {
					System.out.println("Enter election ID: ");
					Long electionId = Long.parseLong(inputk.nextLine());
					ticket = publishAllBallot(electionId, ticket);
				} else if (cmd.equals("q")){
					System.out.println("Bye!");
				}
				System.out.println("-------------------------------------------------------------");
			} while (!cmd.equals("q") && !ticket.getRole().equals("failed"));
		}
		else
		{
			inputk.close();
		}
	}

	static TicketBB publishAllBcBallot(long electionId, TicketBB ticket) {
		try {
			List<BcBallotFactor> listBcFactor = MgrCounter
					.getAllBcBallot(electionId);
			if (!listBcFactor.isEmpty()) {
				Socket s = MgrSSL.initSSLCounterToServerSocket();
				byte[] msg = MgrMessage.ackRequestBcBallotFactorMsg(
						"AckRequestBcFactor", electionId, listBcFactor.size(),
						listBcFactor.toArray(new BcBallotFactor[listBcFactor
								.size()]), "succeeded", ticket);
				System.out.println("publishing " + listBcFactor.size()
						+ " BcBallot(s) for election: " + electionId);
				MgrMessage.sendMsg(s, msg);
				byte [] bb  = MgrMessage.recvMsg(s);
				Message m = MgrMessage.recoverReplyAckRequestBcBallotFactorMsg(bb);
				return m.getTicketBB();
			} else {
				System.out.println("No new BcBallot");
				return ticket;
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return null;
	}

	static TicketBB publishAllBallot(long electionId, TicketBB ticket) {
		try {
			List<BcBallotFactor> listBcFactor = MgrCounter
					.getAllBallot(electionId);
			if (!listBcFactor.isEmpty()) {
				Socket s = MgrSSL.initSSLCounterToServerSocket();

				byte[] msg = MgrMessage.ackRequestBcBallotFactorMsg(
						"AckRequestResult", electionId, listBcFactor.size(),
						listBcFactor.toArray(new BcBallotFactor[listBcFactor
								.size()]), "succeeded", ticket);
				System.out.println("publishing " + listBcFactor.size()
						+ " Ballot(s) for election: " + electionId);
				MgrMessage.sendMsg(s, msg);
				byte [] bb  = MgrMessage.recvMsg(s);
				Message m = MgrMessage.recoverReplyAckRequestBcBallotFactorMsg(bb);
				return m.getTicketBB();
			} else {
				System.out.println("No new ballot");
				return ticket;
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return null;
	}
	
	static void getStatus() {
		try {
			List<BcBallotFactor> listBcFactor = MgrCounter.getBcBallotStatus();
			List<BcBallotFactor> listBallotFactor = MgrCounter.getBallotStatus();
			if (!listBcFactor.isEmpty()) {
				System.out.println("You can publish BcBallots for the following election(s)");
				for(int i=0;i<listBcFactor.size();i++)
				{
					System.out.println("Election ID: "+listBcFactor.get(i).getElectionId());
				}
			} else {
				System.out.println("No new BcBallot");
			}
			if (!listBallotFactor.isEmpty())
			{
				System.out.println("You can publish result for the following election(s)");
				for(int i=0;i<listBallotFactor.size();i++)
				{
					System.out.println("Election ID: "+listBallotFactor.get(i).getElectionId());
				}
			}
			else
			{
				System.out.println("No new Result");
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	static TicketBB signIn(String username, String password, Scanner inputk) {
		Socket s = MgrSSL.initSSLCounterToServerSocket();
		byte [] msg = MgrMessage.counterLoginMsg(username, password);
		MgrMessage.sendMsg(s, msg);
		
		byte [] bb = MgrMessage.recvMsg(s);
		Message m = MgrMessage.recoverAckCounterLoginMsg(bb);
		if(m.getDetail().equals("succeeded")){
			return m.getTicketBB();
		}
		else if (m.getDetail().equals("otp"))
		{
			s = MgrSSL.initSSLCounterToServerSocket();
			try {
				//String pwd = "";
				//String rePwd = "";
				char [] pwd = null;
				char [] repwd = null;
				Console c = System.console();
		        if (c == null) {
		            System.err.println("No console.");
		            System.exit(1);
		        }
				do
				{
					 pwd = c.readPassword("Enter your new password: ");
						repwd = c.readPassword("Re Enter your new password: ");
						//System.out.print("new password: ");
						//pwd = inputk.nextLine();
						//System.out.print("re enter new password: ");
						//rePwd = inputk.nextLine();
						if(!new String(pwd).equals(new String(repwd)))
						{
							System.err.println("Password mismatched");
						}
						else if(!MgrPassword.isStrong(new String(pwd)))
						{
							System.err.println("Password is too weak");
						}
					} while(!new String(pwd).equals(new String(repwd)) || !MgrPassword.isStrong(new String(pwd)));
				msg = MgrMessage.updatePasswordMsg(username, new String(pwd), KeyDistribution.GetPublicKey(KeyDistribution.GenerateKeyPairs()));
				MgrMessage.sendMsg(s, msg);
				bb = MgrMessage.recvMsg(s);
				m = MgrMessage.recoverAckUpdatePasswordMsg(bb);
				if(m.getDetail().equals("succeeded"))
				{
					System.out.println("Update Password Succeeded, please login again");					
				}
				else
				{
					System.out.println("Update Password Failed");	
				}
				return m.getTicketBB();
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else {
			System.out.println(m.getDetail());
			return m.getTicketBB();
		}
		return null;
	}
}
