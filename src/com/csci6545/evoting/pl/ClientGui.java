/* Autheor: Alemberhan Getahun
 * Date: 11/01/2013 */
package com.csci6545.evoting.pl;

import java.awt.Component;
import java.awt.FlowLayout;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.net.SocketFactory;
import javax.net.ssl.SSLSocketFactory;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXDatePicker;

import com.csci6545.evoting.bl.MgrSSL;
import com.csci6545.evoting.bl.MgrSystemProperties;
import com.csci6545.evoting.dl.LoadConfig;

public class ClientGui {

	private static final String PATH_TO_TRUSTSOTRE = "./resources/trustEvotingServer.jks";
	private static final String PWD_OF_TRUSTSOTRE = "123456";
	static final String FRAME_TITLE = "ELECTRONIC VOTING SYSTEM";

	private static final String KEG_GENEATE = "GENERATE KEY";
	private static JFrame mainFrame = null;
	private static JPanel newUserPanel = null;
	private static JPanel existingUserPanel = null;
	private JPanel mainPanel = null;

	private static JLabel userNameL = null;
	private static JLabel passwordL = null;
	private static JLabel newUserNameL = null;
	private static JLabel newPasswordL = null;
	
	private static JLabel dobL = null;
	public static JXDatePicker dobT = null;
	public static JTextField userNameText = null;
	private static JPasswordField passwordText = null;
	public static JTextField newUserNameText = null;
	public static JPasswordField newPasswordText = null;
	private static JPasswordField pinText = null;
	public static JPasswordField keyPinText = null;
	private static JButton keyButton = null;
	private static JButton resetPassword = null;
	private static JButton signIn = null;

	public ClientGui() {
		mainPanel = populateMainPanel(getNewUserPanel(), getExistingUserPanel());
		getMainFrame().setContentPane(mainPanel);

		// create and setup the window
		getMainFrame().addWindowListener(new ClientListner());
		getMainFrame().setLocation(150, 180);
		getMainFrame().setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Display the window
		getMainFrame().pack();
		getMainFrame().setVisible(true);
		getMainFrame().setResizable(false);
	}

	public static void main(String[] args) {
		LoadConfig.loadClientConfig();
		//LoadConfig.loadCounterConfig();
		ClientGui.createAndShowGUI();
	}

	public static void createAndShowGUI() {
		Runnable createAndShow = new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				JFrame.setDefaultLookAndFeelDecorated(true);
				ClientGui gui = new ClientGui();
				MgrSystemProperties.initClientSystemProperty(
						PATH_TO_TRUSTSOTRE, PWD_OF_TRUSTSOTRE);// poom

			}
		};
		SwingUtilities.invokeLater(createAndShow);
	}

	private JPanel populateMainPanel(JPanel newUserPanel,
			JPanel existingUserPanel) {
		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout());
		panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		panel.add(newUserPanel);
		panel.add(existingUserPanel);
		return panel;
	}

	public JFrame getMainFrame() {
		if (mainFrame == null) {
			mainFrame = new JFrame(ClientGui.FRAME_TITLE);
		}
		return mainFrame;
	}

	public JPanel getNewUserPanel() {
		if (newUserPanel == null) {
			newUserPanel = new JPanel();
			TitledBorder border = BorderFactory
					.createTitledBorder("KEY GENERATION");
			border.setTitleJustification(TitledBorder.CENTER);
			newUserPanel.setBorder(border);
			newUserPanel
					.setLayout(new BoxLayout(newUserPanel, BoxLayout.Y_AXIS));
			newUserPanel.add(newUserPanel());
			newUserPanel.add(newPassPanel());
			newUserPanel.add(dobPanel());
			newUserPanel.add(getKeyButton());

		}
		return newUserPanel;
	}

	public JPanel getExistingUserPanel() {
		if (existingUserPanel == null) {
			existingUserPanel = new JPanel();
			TitledBorder border = BorderFactory
					.createTitledBorder("LOGIN TO VOTING SYSTEM");
			border.setTitleJustification(TitledBorder.CENTER);
			existingUserPanel.setBorder(border);
			existingUserPanel.setLayout(new BoxLayout(existingUserPanel,
					BoxLayout.Y_AXIS));
			existingUserPanel.add(userPanel());
			existingUserPanel.add(passPanel());
			existingUserPanel.add(signResetPanel());

		}
		return existingUserPanel;
	}

	private JPanel signResetPanel() {
		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout(FlowLayout.CENTER));
		panel.add(getSignIn());
		panel.add(getResetPassword());
		return panel;
	}

	public JButton getResetPassword() {
		if (resetPassword == null) {
			resetPassword = new JButton("RESET PASSWORD");
			resetPassword.addActionListener(new ClientListner(this));
			resetPassword.setActionCommand(ClientListner.RESET_PASSWORD);
		}
		return resetPassword;
	}

	public static JLabel getUserNameL() {
		if (userNameL == null) {
			userNameL = new JLabel("USER ID  ");
		}
		return userNameL;
	}

	public static JLabel getPasswordL() {
		if (passwordL == null) {
			passwordL = new JLabel("PASSWORD");
		}
		return passwordL;
	}

	public JTextField getUserNameText() {
		if (userNameText == null) {
			userNameText = new JTextField(25);
			userNameText.addKeyListener(new ClientListner(this));
		}
		return userNameText;
	}

	public JPasswordField getPasswordText() {
		if (passwordText == null) {
			passwordText = new JPasswordField(25);
			passwordText.addKeyListener(new ClientListner(this));
		}
		return passwordText;
	}

	public JButton getSignIn() {
		if (signIn == null) {
			signIn = new JButton("SIGN IN");
			signIn.addActionListener(new ClientListner(this));
			signIn.setActionCommand(ClientListner.SIGNIN);
		}
		return signIn;
	}

	public JPanel userPanel() {
		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout());
		panel.add(getUserNameL());
		panel.add(getUserNameText());
		return panel;
	}

	public JPanel passPanel() {
		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout());
		panel.add(getPasswordL());
		panel.add(getPasswordText());
		return panel;
	}

	

	public JPanel dobPanel() {
		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout());
		panel.add(getDobL());
		panel.add(getDobT());
		return panel;
	}

	

	public static JPasswordField getPinText() {
		if (pinText == null) {
			pinText = new JPasswordField(10);

		}
		return pinText;
	}

	public JButton getKeyButton() {
		if (keyButton == null) {
			keyButton = new JButton(ClientGui.KEG_GENEATE);
			keyButton.addActionListener(new ClientListner(this));
			keyButton.setActionCommand(ClientListner.KEY);
		}
		return keyButton;
	}

	public static JLabel getDobL() {
		if (dobL == null) {
			dobL = new JLabel("DOB");
		}
		return dobL;
	}

	public JXDatePicker getDobT() {
		if (dobT == null) {
			dobT = new JXDatePicker();
			dobT.setDate(Calendar.getInstance().getTime());
			dobT.setFormats(new SimpleDateFormat("yyyy-MM-dd"));
		}
		return dobT;
	}

	public JPanel getMainPanel() {
		return mainPanel;
	}

	public static JLabel getNewUserNameL() {
		if (newUserNameL == null) {
			newUserNameL = new JLabel("USER ID  ");
		}
		return newUserNameL;
	}

	public static JLabel getNewPasswordL() {
		if (newPasswordL == null) {
			newPasswordL = new JLabel("PASSWORD");
		}
		return newPasswordL;
	}

	public JTextField getNewUserNameText() {
		if (newUserNameText == null) {
			newUserNameText = new JTextField(25);
		}
		return newUserNameText;
	}

	public JPasswordField getNewPasswordText() {
		if (newPasswordText == null) {
			newPasswordText = new JPasswordField(25);
		}
		return newPasswordText;
	}

	public JPanel newUserPanel() {
		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout());
		panel.add(getNewUserNameL());
		panel.add(getNewUserNameText());
		return panel;
	}

	public JPanel newPassPanel() {
		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout());
		panel.add(getNewPasswordL());
		panel.add(getNewPasswordText());
		return panel;
	}

	

	public static JPasswordField getKeyPinText() {
		if(keyPinText == null){
			keyPinText = new JPasswordField(10);
		}
		return keyPinText;
	}

	
	
}
