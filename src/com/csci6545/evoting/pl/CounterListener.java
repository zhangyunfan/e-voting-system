package com.csci6545.evoting.pl;

import java.net.ServerSocket;
import java.net.Socket;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.text.ParseException;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import com.csci6545.evoting.be.BcBallotFactor;
import com.csci6545.evoting.bl.KeyDistribution;
import com.csci6545.evoting.bl.MgrConnectionCounter;
import com.csci6545.evoting.bl.MgrMessage;
import com.csci6545.evoting.bl.MgrSSL;
import com.csci6545.evoting.dl.LoadConfig;
import com.csci6545.evoting.dl.MgrCounter;

public class CounterListener {
	public static void main(String[] args) throws NoSuchAlgorithmException, ParseException, InvalidKeyException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, NoSuchProviderException, InvalidKeySpecException {
		// Start Load Config
		//LoadConfig.loadDBConfig();
		//LoadConfig.loadServerConfig();
		//LoadConfig.loadClientConfig();
		LoadConfig.loadCounterConfig();
		LoadConfig.loadCounterDBConfig();
		// End Load Config
		// TODO Auto-generated method stub
		
		ServerSocket clientSocket=null;
		Socket s = null;
		try {
			KeyPair kp = MgrSSL.getCounterKeypairFromKeyStore();
			//clientSocket = new ServerSocket(LoadConfig.COUNTER_PORT);
			clientSocket = MgrSSL.initSSLCounterSocket();
			/*MgrMessage.sendMsg(serverSock, MgrMessage.loginMsg("poom", "1234567", KeyDistribution.GetPrivateKey(kp)));
            byte [] bb = MgrMessage.recvMsg(serverSock);
            Message m = MgrMessage.recoverAckLoginMsg(bb);
			System.out.println(m.getDetail());*/
			while (true) {
            	new MgrConnectionCounter(clientSocket.accept(), KeyDistribution.GetPrivateKey(kp)).start();
            }

        } catch (Exception e) {
            System.out.println(e);
            
        }  
	}
}
