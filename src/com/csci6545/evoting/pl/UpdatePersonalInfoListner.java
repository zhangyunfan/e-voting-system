/* Autheor: Alemberhan Getahun 
 * Date: 11/01/2013 */
package com.csci6545.evoting.pl;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.Socket;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import com.csci6545.evoting.be.Message;
import com.csci6545.evoting.bl.MgrMessage;
import com.csci6545.evoting.bl.MgrSSL;
import com.csci6545.evoting.bl.MsgHandler;

public class UpdatePersonalInfoListner implements ActionListener {
	public static final String UPDATE_PERSONAL = "update personal info";
	UpdatePersonalInfoGui gui;

	public UpdatePersonalInfoListner(UpdatePersonalInfoGui gui) {
		this.gui = gui;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		String command = e.getActionCommand();
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		if(command.equals(UpdatePersonalInfoListner.UPDATE_PERSONAL)){
			// update personal information
			
			VoterGui.voter.setfName(gui.fnameText.getText().toString());
			VoterGui.voter.setlName(gui.lnameText.getText().toString());
			VoterGui.voter.setEmail(gui.emailText.getText().toString());
			byte [] msg = MgrMessage.updatePersonalInfoMsg(VoterGui.voter.getUserId(), 
					VoterGui.voter.getfName(), VoterGui.voter.getlName(), 
					VoterGui.voter.getEmail(), VoterGui.voter.getTicketBB());
			
			Socket s = MgrSSL.initSSLClientToServerSocket();
			MgrMessage.sendMsg(s, msg);
			
			byte [] bb = MgrMessage.recvMsg(s);
			Message m = MsgHandler.userHandleMsg(MgrMessage.getMessgaeType(bb), bb, s);
			VoterGui.voter.setTicketBB(m.getTicketBB());
			if(!VoterGui.voter.getTicketBB().getRole().equals("failed"))
			{
				if(m.getDetail().equals("succeeded")){
					JOptionPane.showMessageDialog(frame, "You have successfully updated your personal infoamtion");
				}
				else
				{
					int dialogButton1 = JOptionPane.WARNING_MESSAGE;
					JOptionPane
					.showMessageDialog(
							frame,
							m.getDetail(),
							"Error", dialogButton1);
	
				}
				gui.getFrame().dispose();
			}
			else
			{
				int dialogButton1 = JOptionPane.WARNING_MESSAGE;
				JOptionPane
				.showMessageDialog(
						frame,
						"Ticket Expired!!",
						"Error", dialogButton1);
				VoterListner.voter.getFrame().dispose();
				gui.getFrame().dispose();
				ClientGui.createAndShowGUI();
			}
		}

	}

}
