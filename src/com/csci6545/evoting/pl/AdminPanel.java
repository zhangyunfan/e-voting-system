package com.csci6545.evoting.pl;

import java.io.Console;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

import com.csci6545.evoting.be.Title;
import com.csci6545.evoting.be.User;
import com.csci6545.evoting.bl.MgrConfidentiality;
import com.csci6545.evoting.bl.MgrPassword;
import com.csci6545.evoting.dl.LoadConfig;
import com.csci6545.evoting.dl.MgrCounter;
import com.csci6545.evoting.dl.MgrSuperuser;
import com.csci6545.evoting.dl.MgrTitle;
import com.csci6545.evoting.dl.MgrUser;

public class AdminPanel {
	public static void main(String[] args) throws NoSuchAlgorithmException, SQLException{
		LoadConfig.loadServerDBConfig();
		Scanner inputk = new Scanner(System.in);
		String cmd = "";
		System.out.println("Welcome to admin panel...");
		//System.out.print("username: ");
		//String username = inputk.nextLine();
		//System.out.print("password: ");
		//String password = inputk.nextLine();
		Console c = System.console();
        if (c == null) {
            System.err.println("No console.");
            System.exit(1);
        }

        String username = c.readLine("Enter your username: ");
        char [] password = c.readPassword("Enter your password: ");
		if(signIn(username, new String(password), inputk))
		//if(signIn(username, password, inputk))
		{
			do
			{
				System.out.println("To add new counter ENTER (0)");
				System.out.println("To add new user ENTER (1)");
				System.out.println("To add new title ENTER (2)");
				System.out.println("To update user title ENTER (3)");
				System.out.println("To update title permission ENTER (4)");
				System.out.println("To reset user password ENTER (5)");
				System.out.println("To reset counter password ENTER (6)");
				System.out.println("To quit ENTER (q)");
				cmd = inputk.nextLine();
				if(cmd.equals("0"))
					addNewCounter(inputk);
				else if(cmd.equals("1"))
					addNewUser(inputk);
				else if(cmd.equals("2"))
					addTitle(inputk);
				else if(cmd.equals("3"))
					updateUserTitle(inputk);
				else if(cmd.equals("4"))
					updateTitlePermission(inputk);
				else if(cmd.equals("5"))
					resetUserPassword(inputk);
				else if(cmd.equals("6"))
					resetCounterPassword(inputk);
				else if (cmd.equals("q"))
					System.out.println("Bye!");
				System.out.println("___________________________________________________");
			}while(!cmd.equals("q"));
		}
		inputk.close();
	}
	static void addTitle(Scanner inputk)
	{
		System.out.println("Welcome to job title creation");
		//Scanner inputk = new Scanner(System.in);
		int permission = 0;
		String titleName = null;
		String temp = null;
		do
		{
			System.out.println("Please enter new job title: ");
			titleName = inputk.nextLine().toUpperCase();
		}
		while(MgrTitle.existsTitle(titleName));
		
		do
		{
			System.out.print("Is this title eligible to create election? (Y/N):");
			temp = inputk.nextLine().toLowerCase();
		}
		while(!temp.equals("y") && !temp.equals("n"));	
		if(temp.equals("y"))
		{
			permission = 4;
		}
		
		do
		{
			System.out.print("Is this title eligible to be a candidate? (Y/N):");
			temp = inputk.nextLine().toLowerCase();
		}
		while(!temp.equals("y") && !temp.equals("n"));	
		if(temp.equals("y"))
		{
			permission = permission+2;
		}
		
		do
		{
			System.out.print("Is this title eligible to cast vote? (Y/N):");
			temp = inputk.nextLine().toLowerCase();
		}
		while(!temp.equals("y") && !temp.equals("n"));
		if(temp.equals("y"))
		{
			permission = permission+1;
		}
		
		if(MgrTitle.createTitle(titleName, permission))
		{
			System.out.println("Create Title Succeeded");
		}	
	}
	static void addNewCounter(Scanner inputk) {
	 	System.out.println("Welcome to user account creation");
		//Scanner inputk = new Scanner(System.in);
		String username = null;
		try {
			do
			{
				System.out.println("Please enter username: ");
				username = inputk.nextLine();
			}
			while(MgrCounter.existsCounter(username));
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		System.out.print("Please enter valid email address: ");
		String email = inputk.nextLine();
		if(MgrCounter.createCounter(username, "temp", email) == 1)
		{
			System.out.println("Creating Counter, please wait....");			 
			if(MgrPassword.generateCounterRandomPassword(username, "Welcome to E-voting System", 
					 "Your username is: "+username+" Your one time password is" ))
			{
				System.out.println("Succeeded");
			}
			
		}
	}
	static void addNewUser(Scanner inputk) {
		 	System.out.println("Welcome to user account creation");
			User user = new User();
			//Scanner inputk = new Scanner(System.in);
			String username = null;
			int titleId = 0;
			try {
				do
				{
					System.out.println("Please enter username: ");
					username = inputk.nextLine();
				}
				while(MgrUser.existsUser(username));
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			System.out.print("Please enter first name: ");
			String fName = inputk.nextLine();
			System.out.print("Please enter Last name: ");
			String lName = inputk.nextLine();
			System.out.print("Please enter date of birth(mm/dd/yyyy): ");
			String dob = inputk.nextLine();
			System.out.print("Please select Title ID: \n");
			List<Title> listTitle = new ArrayList<Title>();
			try {
				listTitle = MgrTitle.getAllTitle();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			for(int i=0;i<listTitle.size();i++)
			{
				System.out.print(listTitle.get(i).getTitleName()+" ");
				System.out.println("("+listTitle.get(i).getTitleId()+")");
			}
			titleId = Integer.parseInt(inputk.nextLine());
			System.out.print("Please enter valid email address: ");
			String email = inputk.nextLine();
			SimpleDateFormat format =  new SimpleDateFormat("MM/dd/yyyy", Locale.US);		 
			user.setfName(fName);
			user.setlName(lName);
			try {
				user.setDob(format.parse(dob));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 user.setPassword("12345678");
			 user.setUserId(username);
			 user.setEmail(email);
			 if(MgrUser.createUser(user, titleId) == 1)
			 {
				 System.out.println("Creating User, please wait....");
			 
				 KeyPair kp = null;
				try {
					kp = MgrConfidentiality.generateRSAKeyPair();
				} catch (NoSuchAlgorithmException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				 PublicKey publicKey = kp.getPublic();
				 MgrUser.setUserPublicKey(user,publicKey.getEncoded());
				 byte[] returnByte = MgrUser.getUserPublicKey(user);
				 for(int i=0;i<returnByte.length;i++){
					 if(returnByte[i]!=publicKey.getEncoded()[i]){
						 System.out.println("Failed");
					 }
				 }
				 
				 if(MgrPassword.generateRandomPassword(username, "Welcome to E-voting System", 
						 "Your username is: "+username+" Your one time password is" ))
				 {
						
				 }
				 System.out.println("Succeeded");
			 }
		 }
	
	static void updateTitlePermission(Scanner inputk) {
		//Scanner inputk = new Scanner(System.in);
		List<Title> listTitle = new ArrayList<Title>();
		int permission = 0;
		String temp = "";
		try {
			listTitle = MgrTitle.getAllTitle();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		for(int i=0;i<listTitle.size();i++)
		{
			System.out.print(listTitle.get(i).getTitleName()+" ");
			System.out.println("("+listTitle.get(i).getTitleId()+")");
		}
		int titleId = Integer.parseInt(inputk.nextLine());
		do
		{
			System.out.print("Is this title eligible to create election? (Y/N):");
			temp = inputk.nextLine().toLowerCase();
		}
		while(!temp.equals("y") && !temp.equals("n"));	
		if(temp.equals("y"))
		{
			permission = 4;
		}
		
		do
		{
			System.out.print("Is this title eligible to be a candidate? (Y/N):");
			temp = inputk.nextLine().toLowerCase();
		}
		while(!temp.equals("y") && !temp.equals("n"));	
		if(temp.equals("y"))
		{
			permission = permission+2;
		}
		
		do
		{
			System.out.print("Is this title eligible to cast vote? (Y/N):");
			temp = inputk.nextLine().toLowerCase();
		}
		while(!temp.equals("y") && !temp.equals("n"));
		if(temp.equals("y"))
		{
			permission = permission+1;
		}
		
		if(MgrTitle.updateTitlePermission(permission, titleId))
		{
			System.out.println("succeeded");
		}	
		
		
	}
	static void updateUserTitle(Scanner inputk) {
		String username = "";
		//Scanner inputk = new Scanner(System.in);
		try {
			do
			{
				System.out.println("Please enter username: ");
				username = inputk.nextLine();
			}
			while(!MgrUser.existsUser(username));
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		List<Title> listTitle = new ArrayList<Title>();
		try {
			listTitle = MgrTitle.getAllTitle();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		for(int i=0;i<listTitle.size();i++)
		{
			System.out.print(listTitle.get(i).getTitleName()+" ");
			System.out.println("("+listTitle.get(i).getTitleId()+")");
		}
		int titleId = Integer.parseInt(inputk.nextLine());
		if(MgrUser.updateUserTitle(username, titleId)){
			System.out.println("succeeded");
		}
		
	}
	static void resetUserPassword(Scanner inputk)
	{
		String username = "";
		try {
			do
			{
				System.out.println("Please enter username: ");
				username = inputk.nextLine();
			}
			while(!MgrUser.existsUser(username));
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if(MgrPassword.generateRandomPassword(username, "Reset Password", "Your one time password is" ))
		{
			System.out.println("Done");
		}
	}
	
	static void resetCounterPassword(Scanner inputk)
	{
		String username = "";
		try {
			do
			{
				System.out.println("Please enter username: ");
				username = inputk.nextLine();
			}
			while(!MgrCounter.existsCounter(username));
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if(MgrPassword.generateCounterRandomPassword(username, "Reset Password", "Your one time password is" ))
		{
			System.out.println("Done");
		}
	}
	
	static Boolean signIn(String username, String password, Scanner inputk)
	{
		User superuser = MgrSuperuser.passwordAuthentcateSuper(username, password);
		if(superuser == null)
		{
			int attempts = MgrSuperuser.getSuperLoginAttempts(username);
			if (attempts < 3) {
				MgrSuperuser.setSuperLoginAttempts(username, ++attempts);
				System.out.println("Your username or password is incorrect");
			}
			else
			{
				System.out.println("Your account has been locked. Please get one time password from your email");
				MgrPassword.generateSuperRandomPassword(username, "Reset Password", "One time password");
			}
			return false;
		}
		else if(superuser.getIsOtp() == 1)
		{
			MgrSuperuser.setSuperLoginAttempts(username, 0);
			char [] pwd = null;
			char [] repwd = null;
			Console c = System.console();
	        if (c == null) {
	            System.err.println("No console.");
	            System.exit(1);
	        }
			//String pwd = "";
			//String rePwd = "";
			do
			{
				    pwd = c.readPassword("Enter your new password: ");
					repwd = c.readPassword("Re Enter your new password: ");
					//System.out.print("new password: ");
					//pwd = inputk.nextLine();
					//System.out.print("re enter new password: ");
					//rePwd = inputk.nextLine();
					if(!new String(pwd).equals(new String(repwd)))
					{
						System.err.println("Password mismatched");
					}
					else if(!MgrPassword.isStrong(new String(pwd)))
					{
						System.err.println("Password is too weak");
					}
				} while(!new String(pwd).equals(new String(repwd)) || !MgrPassword.isStrong(new String(pwd)));	
				if (MgrSuperuser.setSuperPassword(username, new String(pwd), 0)) {
				System.out.println("Your password is updated, please login again");
			} 
			return false;
		}
		else if(superuser.getLoginAttempts() < 3)
		{
			MgrSuperuser.setSuperLoginAttempts(username, 0);
			return true;
		}
		return false;
	}
}
