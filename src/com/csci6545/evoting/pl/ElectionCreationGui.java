package com.csci6545.evoting.pl;

import java.awt.Component;
import java.awt.Container;
import java.awt.FlowLayout;
import java.io.IOException;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXDatePicker;

import com.csci6545.evoting.be.Candidate;
import com.csci6545.evoting.be.Message;
import com.csci6545.evoting.be.Supervisor;
import com.csci6545.evoting.be.Title;
import com.csci6545.evoting.be.User;
import com.csci6545.evoting.be.Voter;
import com.csci6545.evoting.bl.MgrMessage;
import com.csci6545.evoting.bl.MgrSSL;
import com.csci6545.evoting.bl.MsgHandler;

public class ElectionCreationGui {

	public static final String TITLE = "ELECTION CREATION FORM";
	public static final String CREATE = "CREATE";
	JLabel nameLable = null;
	static JTextField nameField = null;
	JLabel startLable = null;
	JLabel endLable = null;
	static JXDatePicker startDate = null;
	static JXDatePicker endDate = null;
	JButton createBut = null;
	JLabel candiateLable = null;
	JLabel titleLable = null;
	JLabel includeLable = null;
	JLabel excludeLable = null;
	static JList<String> candidateList = null;
	static JList<String> titleList = null;
	static JList<String> includeList = null;
	static JList<String> excludeList = null;

	private JFrame frame = null;
	private static Voter supervisor;
	public static User[] candidateArray;
	public static Title[] titles;

	public ElectionCreationGui() {
		getFrame().setContentPane(mainPanel());
		getFrame().addWindowListener(new VoterListner());
		getFrame().setLocation(150, 180);
		getFrame().setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		getFrame().pack();
		getFrame().setVisible(true);
		getFrame().setResizable(false);
	}

	public JPanel mainPanel() {
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		panel.add(upperPanel());
		panel.add(lowerPanel());
		return panel;
	}

	public JPanel upperPanel() {
		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout());
		panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		panel.add(leftPanel());
		panel.add(rightPanel());
		return panel;
	}

	public JPanel lowerPanel() {
		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout(FlowLayout.CENTER));
		panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		panel.add(getCreateBut());
		return panel;
	}

	public JPanel leftPanel() {
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		panel.add(electionPanel());
		panel.add(candidatePanel());
		panel.add(startPanel());
		panel.add(endPanel());

		return panel;

	}

	public JPanel rightPanel() {
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		panel.add(titlePanel());
		panel.add(includePanel());
		panel.add(excludePanel());

		return panel;

	}

	private Component excludePanel() {
		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout(FlowLayout.LEFT));

		panel.add(getExcludeLable());
		panel.add(getExcludeList());
		return panel;
	}

	private Component includePanel() {
		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout(FlowLayout.LEFT));

		panel.add(getIncludeLable());
		panel.add(getIncludeList());
		return panel;
	}

	private JPanel titlePanel() {
		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout(FlowLayout.LEFT));

		panel.add(getTitleLable());
		panel.add(getTitleList());
		return panel;
	}

	public static void createAndShow(Voter sup, User[] candidates,
			Title[] titleArray) {
		supervisor = sup;
		candidateArray = candidates;
		titles = titleArray;
		Runnable runnable = new Runnable() {

			@Override
			public void run() {

				ElectionCreationGui eGui = new ElectionCreationGui();

			}
		};
		SwingUtilities.invokeLater(runnable);
	}

	public JLabel getNameLable() {
		if (nameLable == null) {
			nameLable = new JLabel("ELECTION NAME: ");
		}
		return nameLable;
	}

	public JTextField getNameField() {
		if (nameField == null) {
			nameField = new JTextField(20);
		}
		return nameField;
	}

	public JLabel getStartLable() {
		if (startLable == null) {
			startLable = new JLabel("START DATE: ");
		}
		return startLable;
	}

	public JLabel getEndLable() {
		if (endLable == null) {
			endLable = new JLabel("END DATE: ");
		}
		return endLable;
	}

	public static JXDatePicker getStartDate() {
		if (startDate == null) {
			startDate = new JXDatePicker();
			startDate.setDate(Calendar.getInstance().getTime());
			startDate.setFormats(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
		}
		return startDate;
	}

	public static JXDatePicker getEndDate() {
		if (endDate == null) {
			endDate = new JXDatePicker();
			endDate.setDate(Calendar.getInstance().getTime());
			endDate.setFormats(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
		}
		return endDate;
	}

	public JButton getCreateBut() {
		if (createBut == null) {
			createBut = new JButton("CREATE");
			createBut.addActionListener(new SupervisorListner());
			createBut.setActionCommand(ElectionCreationGui.CREATE);
		}
		return createBut;
	}

	public JFrame getFrame() {
		if (frame == null) {
			frame = new JFrame(ElectionCreationGui.TITLE);
		}
		return frame;
	}

	public JLabel getCandiateLable() {
		if (candiateLable == null) {
			candiateLable = new JLabel("CANDIDATES: ");
		}
		return candiateLable;
	}

	public static JList<String> getCandidateList() {
		if (candidateList == null) {
			// String[] candiates = {"Alem","Poom", "Yunfun"};
			// Get User Information
			// Socket s = MgrSSL.initSSLClientSocket();
			String[] candiates = new String[candidateArray.length];
			for (int i = 0; i < candiates.length; i++) {
				if (candidateArray[i].getPermission() == 2
						|| candidateArray[i].getPermission() == 3
						|| candidateArray[i].getPermission() == 5
						|| candidateArray[i].getPermission() == 7)

					candiates[i] = candidateArray[i].getfName() + " "
							+ candidateArray[i].getlName() + 
							" [" + candidateArray[i].getUserId()+"]";
			}
			candidateList = new JList<String>(candiates);
		}
		return candidateList;
	}

	public JPanel electionPanel() {
		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout(FlowLayout.LEFT));

		panel.add(getNameLable());
		panel.add(getNameField());
		return panel;

	}

	public JPanel candidatePanel() {
		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout(FlowLayout.LEFT));

		panel.add(getCandiateLable());
		panel.add(getCandidateList());
		return panel;

	}

	public JPanel startPanel() {
		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout(FlowLayout.LEFT));

		panel.add(getStartLable());
		panel.add(getStartDate());
		return panel;

	}

	public JPanel endPanel() {
		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout(FlowLayout.LEFT));

		panel.add(getEndLable());
		panel.add(getEndDate());
		return panel;

	}

	public JLabel getTitleLable() {
		if (titleLable == null) {
			titleLable = new JLabel("Select Eligible Title:");
		}
		return titleLable;
	}

	public JLabel getIncludeLable() {
		if (includeLable == null) {
			includeLable = new JLabel("Include User:");
		}
		return includeLable;
	}

	public JLabel getExcludeLable() {
		if (excludeLable == null) {
			excludeLable = new JLabel("Exclude User:");
		}
		return excludeLable;
	}

	public static JList<String> getTitleList() {
		if (titleList == null) {
			String[] titlesStr = new String[titles.length];

			for (int i = 0; i < titles.length; i++) {
				if (titles[i].getPermission() == 1
						|| titles[i].getPermission() == 3
						|| titles[i].getPermission() == 5
						|| titles[i].getPermission() == 7) {
					titlesStr[i] = titles[i].getTitleName();
				}
			}
			titleList = new JList<String>(titlesStr);

		}
		return titleList;
	}

	public static JList<String> getIncludeList() {
		if (includeList == null) {
			String[] includeUsers = new String[candidateArray.length];
			for (int i = 0; i < includeUsers.length; i++) {
				if (candidateArray[i].getPermission() > 0 && !candidateArray[i].getUserId().equals(VoterGui.voter.getUserId()))
					includeUsers[i] = candidateArray[i].getfName() + " "
							+ candidateArray[i].getlName() + 
							" [" + candidateArray[i].getUserId()+"]";
			}
			includeList = new JList<String>(includeUsers);
		}
		return includeList;
	}

	public static JList<String> getExcludeList() {
		if (excludeList == null) {
			String[] excludeUsers = new String[candidateArray.length];
			for (int i = 0; i < excludeUsers.length; i++) {
				if (candidateArray[i].getPermission() > 0 && !candidateArray[i].getUserId().equals(VoterGui.voter.getUserId()))
					excludeUsers[i] = candidateArray[i].getfName() + " "
							+ candidateArray[i].getlName() + 
							" [" + candidateArray[i].getUserId()+"]";
			}
			excludeList = new JList<String>(excludeUsers);
		}
		return excludeList;
	}

}
