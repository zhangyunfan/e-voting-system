/* Autheor: Alemberhan Getahun 
 * Date: 11/01/2013*/
package com.csci6545.evoting.pl;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import com.csci6545.evoting.be.Election;
import com.csci6545.evoting.be.Message;
import com.csci6545.evoting.be.Supervisor;
import com.csci6545.evoting.be.Voter;
import com.csci6545.evoting.bl.KeyDistribution;
import com.csci6545.evoting.bl.MgrAudit;
import com.csci6545.evoting.bl.MgrAuthentication;
import com.csci6545.evoting.bl.MgrConfidentiality;
import com.csci6545.evoting.bl.MgrIntegrity;
import com.csci6545.evoting.bl.MgrMessage;
import com.csci6545.evoting.bl.MgrSSL;
import com.csci6545.evoting.bl.MsgHandler;
import com.csci6545.evoting.dl.MgrUser;

public class ClientListner extends WindowAdapter implements ActionListener,
		KeyListener {

	public static final String RESET_PASSWORD = "reset password";
	public static final String REGISTER_ELECTION = "register election";
	public static final String VOTE = "vote";
	public static final String SIGNIN = "sign in";
	public static final String KEY = "generate key";
	public static final String SIGNOUT = "sign out";
	public static final String REGISTER = "Register";
	public static final String CHECK_RESULT = "check result";
	public static final String UPDATE_PERSONAL = "update personal";
	public static final String SUBMIT = "submit";
	public static final String CREATE_ELECTION = "create election";
	public static final String VERIFY_BALLOT = "verify ballot";
	public static final String CHECK_AUTHORIZED = "check authorized";
	static String[] okOption = { "OK" };

	public static Voter voter = null;
	ClientGui client = null;


	public ClientListner() {
	}

	public ClientListner(ClientGui client) {
		this.client = client;
		}

	public boolean start (){
		return false;
		
	}
	public static Voter getVoter() {
		return voter;
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String command = e.getActionCommand();
		System.out.println(command);
		if (command.equals(ClientListner.SIGNIN)) {
			// signin action
			signInActionPerformed();
		} else if (command.equals(ClientListner.KEY)) {
			// key generation action
			keyGenerateActionPerformed();
		} else if(command.equals(ClientListner.RESET_PASSWORD)){
			resetPasswordActionPerformed();
		}

	}

	private void resetPasswordActionPerformed() {
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		int dialogButton = JOptionPane.WARNING_MESSAGE;
		
		
		String username = client.getUserNameText().getText();
		System.out.println(username);
		if(username.equals(""))
		{
			JOptionPane
			.showMessageDialog(
					frame,
					"Please enter your username",
					"Reset Password", dialogButton);
		}
		else
		{
			byte [] msg = MgrMessage.resetPasswordMsg(username);
			try
			{
				Socket s = MgrSSL.initSSLClientToServerSocket();
				System.out.println(new String(msg));
				MgrMessage.sendMsg(s, msg);
				
				byte[] bb = MgrMessage.recvMsg(s);

				//Handle Msg
				Message recvAckResetPWd = MsgHandler.userHandleMsg(MgrMessage.getMessgaeType(bb),bb,s);
				System.out.println(new String(recvAckResetPWd.ackResetPasswordMsg()));
				if(recvAckResetPWd.getDetail().equals("succeeded"))
				{
					JOptionPane
					.showMessageDialog(
							frame,
							"One time password is already sent to your email "+recvAckResetPWd.getUserEmail(),
							"Reset Password", dialogButton);
				}
			}
			catch(Exception e)
			{
				System.out.println(e);
			}
		}
		
	}

	private void keyGenerateActionPerformed()  {
		// check the voter
		// check voter username ,Password & dob
		//boolean rightUser = false;
		String username = client.getNewUserNameText().getText();
		String password = client.getNewPasswordText().getText();
		Date dob = client.getDobT().getDate();
		
		Message recvKeyGen = null;
		KeyPair kp = null;
		try
		{
			kp = KeyDistribution.GenerateKeyPairs();
			Socket s = MgrSSL.initSSLClientToServerSocket();
			//Send msg
			
			MgrMessage.sendMsg(s, MgrMessage.genKeyMsg(username, password,dob, KeyDistribution.GetPublicKey(kp)));
			// Receive Msg
			byte[] bb = MgrMessage.recvMsg(s);

			//Handle Msg
			recvKeyGen = MsgHandler.userHandleMsg(MgrMessage.getMessgaeType(bb),bb,s);
			System.out.print(recvKeyGen.getDetail());
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	

		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		int dialogButton = JOptionPane.YES_NO_OPTION;
		int result = 0;
		if (recvKeyGen.getDetail().equals("succeeded")) {
			// generate key
			// code comes here
			// successful key generation message to Voter
			try {
				byte [] hashPwd = MgrConfidentiality.generatePasswordHash(password);
				byte [] keyPin = MgrConfidentiality.getSecretKey(hashPwd);
				byte [] keyIv = MgrConfidentiality.getIV(hashPwd);
				byte [] encryptedPrivKey = MgrConfidentiality.encryptAESBallot(KeyDistribution.GetPrivateKey(kp).getEncoded(), keyPin, keyIv);
				MgrConfidentiality.byteToFile(encryptedPrivKey, "./resources/"+username+".bin");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			result = JOptionPane
					.showConfirmDialog(
							frame,
							"You have successfully generated the key!\n Do you want to continue on the Voting window?",
							"KEY GENERATION", dialogButton);
			if (result == JOptionPane.NO_OPTION) {
				client.getMainFrame().dispose();
			}else {
			client.newPasswordText.setText("");
			client.newUserNameText.setText("");
			//client.keyPinText.setText("");
			}

		} else {
			int dialogButton1 = JOptionPane.WARNING_MESSAGE;
			// failed authentication
			JOptionPane
					.showMessageDialog(
							frame,
							recvKeyGen.getDetail(),
							"KEY GENERATION", dialogButton1);
		}

	}

	private void signInActionPerformed() {
		// check voter username & Password

		String username = client.getUserNameText().getText();
		String password = client.getPasswordText().getText();

		//voter = MgrAuthentication.passwordAuthentcateVoter(username,
			//	password);
		Message recvAckLogin = new Message();
		Message recvAckElection = null;
		byte[] privKeyByte = null;
		PrivateKey privateKey = null;

		try
		{
			byte [] hashPwd = MgrConfidentiality.generatePasswordHash(password);
			byte [] keyPin = MgrConfidentiality.getSecretKey(hashPwd);
			byte [] keyIv = MgrConfidentiality.getIV(hashPwd);
			privKeyByte = MgrConfidentiality.getPrivateKey( "./resources/"+username+".bin", keyPin, keyIv);
			
			if(privKeyByte!= null)
        	{        		
        		privateKey = MgrIntegrity.byteToPrivateKey(privKeyByte);    
        		Socket s = MgrSSL.initSSLClientToServerSocket();
        		//Send msg
        		byte [] msg = MgrMessage.loginMsg(username, password, MgrIntegrity.byteToPrivateKey(privKeyByte));
    			MgrMessage.sendMsg(s, msg);
    			
    			System.out.println(new String(msg));
    			
    			// Receive Msg
    			byte[] bb = MgrMessage.recvMsg(s);

    			//Handle Msg
    			recvAckLogin = MsgHandler.userHandleMsg(MgrMessage.getMessgaeType(bb),bb,s);
    			System.out.println(recvAckLogin.getDetail());
        	}
			else
			{
				Socket s = MgrSSL.initSSLClientToServerSocket();
				byte [] msg = MgrMessage.loginMsg(username, password, KeyDistribution.GetPrivateKey(KeyDistribution.GenerateKeyPairs()));
				MgrMessage.sendMsg(s, msg);
				byte[] bb = MgrMessage.recvMsg(s);
				recvAckLogin = MsgHandler.userHandleMsg(MgrMessage.getMessgaeType(bb),bb,s);
			}
 
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		int dialogButton1 = JOptionPane.WARNING_MESSAGE;
		// Login Succeeded
		if (recvAckLogin.getDetail().equals("succeeded")) {

			// If user is a supervisor
			System.out.println("Ticket: "+new String(recvAckLogin.getTicketBB().getBytes()));
			if (recvAckLogin.getUser().getTitle().equals("Supervisor"))
			{
				System.out.println(recvAckLogin.getUser().getTitle());
				Socket s = MgrSSL.initSSLClientToServerSocket();
				MgrMessage.sendMsg(s, MgrMessage.getElectionInfoMsg("getElectionInfo",recvAckLogin.getTicketBB(),username));
				// Receive Msg
				System.out.println(new String(recvAckLogin.getTicketBB().getSigningRequest()));
				byte [] bb = MgrMessage.recvMsg(s);
				//Handle Msg
				recvAckElection = MsgHandler.voterHandleMessage(MgrMessage.getMessgaeType(bb),bb,s);
				Supervisor sup = new Supervisor();
				sup = (Supervisor)recvAckLogin.getUser();
				sup.setUserId(recvAckElection.getTicketBB().getUserId());
				sup.setMyPrivateKey(privateKey);
				sup.setTicketBB(recvAckElection.getTicketBB());
				VoterGui.createAndShow(sup,Arrays.asList(recvAckElection.getElectionArray()));
				client.getMainFrame().dispose();
			}
			// If user is a counter
			else if(recvAckLogin.getUser().getTitle().equals("Counter"))
			{
				// Trigger counterGUI here
			}
			// If user is a voter
			else
			{
				System.out.println(recvAckLogin.getUser().getTitle());
				Socket s = MgrSSL.initSSLClientToServerSocket();
				MgrMessage.sendMsg(s, MgrMessage.getElectionInfoMsg("getElectionInfo",recvAckLogin.getTicketBB(),username));
				// Receive Msg
				byte [] bb = MgrMessage.recvMsg(s);
				//Handle Msg
				recvAckElection = MsgHandler.voterHandleMessage(MgrMessage.getMessgaeType(bb),bb,s);
				System.out.println(new String(recvAckElection.ackElectionInfoMsg()));
				Voter voter = new Voter();
				voter = (Voter)recvAckLogin.getUser();
				voter.setUserId(recvAckElection.getTicketBB().getUserId());
				voter.setMyPrivateKey(privateKey);
				voter.setTicketBB(recvAckElection.getTicketBB());
				VoterGui.createAndShow(voter,Arrays.asList(recvAckElection.getElectionArray()));
				client.getMainFrame().dispose();
			}


		} 
		// Login with OTP
		else if (recvAckLogin.getDetail().equals("otp"))
		{
			//Trigger change password GUI(User, TicketBB) using TicketBB role to distinguish
			//We will gen new key on this GUI too
			client.getPasswordText().setText("");
			ResetPasswordGui.createAndShowGUI(recvAckLogin.getTicketBB().getUserId());
		}
		// Login Failed
		else {
			
			// failed authentication
			JOptionPane
					.showMessageDialog(
							frame,
							recvAckLogin.getDetail(),
							"SIGN IN", dialogButton1);
			client.getPasswordText().setText("");
			client.getUserNameText().setText("");
		}

	}

}
