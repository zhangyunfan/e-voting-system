package com.csci6545.evoting.pl;

import java.awt.FlowLayout;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.TitledBorder;

import com.csci6545.evoting.be.Election;
import com.csci6545.evoting.be.Supervisor;
import com.csci6545.evoting.be.TicketBB;
import com.csci6545.evoting.be.User;
import com.csci6545.evoting.bl.MgrSystemProperties;
import com.csci6545.evoting.dl.MgrElection;

public class SupervisorGui {
	private static final String PATH_TO_TRUSTSOTRE = "./resources/trustEvotingServer.jks";
	private static final String PWD_OF_TRUSTSOTRE = "123456";
	private static final String FRAME_TITLE = "ELECTRONIC VOTING SYSTEM - SUPERVISOR";
    static final String MANAGE_VOTER = "MANAGE VOTER";
	static final String CREATE_ELECTION = "CREATE ELECTION";
	static final String SIGN_OUT = "SIGN OUT";
	private static JFrame mainFrame = null;
	private static JPanel newUserPanel = null;
	private static JPanel existingUserPanel = null;
	private JPanel mainPanel = null;
	private static JTextArea electionText = null;
	private static JButton creatElection = null;
	private static JButton manageVoter = null;
	
	private static JButton signOut = null;
	public static List<Election> elections;
	public static Supervisor supervisor;
	public static Socket socket;
	public SupervisorGui()
	{
		mainPanel = populateMainPanel();
		getMainFrame().setContentPane(mainPanel);
		
		//create and setup the window
		getMainFrame().addWindowListener(new SupervisorListner());
		getMainFrame().setLocation(150, 180);
		getMainFrame().setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		//Display the window
		getMainFrame().setSize(300, 500);
		getMainFrame().pack();
		getMainFrame().setVisible(true);
		getMainFrame().setResizable(false);
	}
	
	

	public static void createAndShowGUI(Supervisor sup, List<Election> list, Socket sock)
	{
		elections = list;
		supervisor = sup;
		socket = sock;
		Runnable createAndShow = new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				JFrame.setDefaultLookAndFeelDecorated(true);
				SupervisorGui gui = new SupervisorGui();
			    MgrSystemProperties.initClientSystemProperty(PATH_TO_TRUSTSOTRE,PWD_OF_TRUSTSOTRE);//poom
				
				
			}
		};
		SwingUtilities.invokeLater(createAndShow);
	}
	
	public JPanel populateMainPanel(){
		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout(FlowLayout.LEFT));
		panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		panel.add(supervisorPanel());
		panel.add(electonPanel());
		return panel;
	}
	
	private JPanel supervisorPanel() {
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		panel.add(getCreatElection());
		panel.add(getManageVoter());
		panel.add(getSignOut());
		return panel;
	}
	
	public JPanel electonPanel (){
		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout());
		TitledBorder border =BorderFactory.createTitledBorder("ELECTION INFORMATION");
		border.setTitleJustification(TitledBorder.CENTER);
		panel.setBorder(border);
		panel.add(getElectionText());
		return panel;
	}
	
	public JFrame getMainFrame() {
		if(mainFrame == null){
			mainFrame = new JFrame(SupervisorGui.FRAME_TITLE);
		}
		return mainFrame;
	}



	public JButton getCreatElection() {
		if(creatElection == null){
			creatElection = new JButton(SupervisorGui.CREATE_ELECTION);
			creatElection.addActionListener(new SupervisorListner(this));
			creatElection.setActionCommand(SupervisorGui.CREATE_ELECTION);
		}
		return creatElection;
	}




	public JButton getManageVoter() {
		if(manageVoter == null){
			manageVoter = new JButton(SupervisorGui.MANAGE_VOTER);
			manageVoter.addActionListener(new SupervisorListner(this));
			manageVoter.setActionCommand(SupervisorGui.MANAGE_VOTER);
		}
		return manageVoter;
	}




	public JButton getSignOut() {
		if(signOut == null){
			signOut = new JButton(SupervisorGui.SIGN_OUT);
			signOut.addActionListener(new SupervisorListner(this));
			signOut.setActionCommand(SupervisorGui.SIGN_OUT);
		}
		return signOut;
	}

	public static JTextArea getElectionText() {
		if(electionText == null){
			electionText = new JTextArea(5,30);
			electionText.setText("");
			if(elections != null){
				Iterator it = elections.iterator();
				while(it.hasNext()){
					Election election = (Election) it.next();
					electionText.append("ID: " +String.valueOf(election.getId()) + "\n");
					electionText.append("Name: " +election.getName() + "\n");
					electionText.append("Start Date: " +String.valueOf(election.getStart()) + "\n");
					electionText.append("End Date: " +String.valueOf(election.getEnd()) + "\n");
					electionText.append("*****************************************************"+"\n");
					
				}
			
			}
		}
		electionText.setEditable(false);
		return electionText;
	}
}
