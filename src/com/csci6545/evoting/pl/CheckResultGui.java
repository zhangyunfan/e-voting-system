/* Autheor: Alemberhan Getahun 
 * Date: 11/01/2013*/
package com.csci6545.evoting.pl;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.border.TitledBorder;

import com.csci6545.evoting.be.Election;
import com.csci6545.evoting.be.Voter;

public class CheckResultGui {

	static JComboBox electionList = null;
	JLabel electionLable = null;
	private JFrame frame = null;
	public static List<Election> elections;
	private VoterGui voterGui;
	public static JPanel resultPanel = null;
	public static JTextArea resultArea = null;
	private static JScrollPane sbrConsol = null;
	private static Voter voter;
	public static long electionID;
	public CheckResultGui() {
		getFrame().setContentPane(mainPanel());
		getFrame().addWindowListener(new VoterListner());
		getFrame().setLocation(150, 180);
		getFrame().setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		getFrame().pack();
		getFrame().setVisible(true);
		getFrame().setResizable(false);
	}

	public static void createAndShow(final Voter user, final List<Election> electionList) {
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				voter = user;
				elections = electionList;
				CheckResultGui checkResultGui = new CheckResultGui();

			}
		};
		SwingUtilities.invokeLater(runnable);
	}

	public JPanel mainPanel() {
		JPanel panel = new JPanel();
		panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		panel.add(electonPanel());
		panel.add(getResultPanel());
		return panel;

	}

	public JPanel electonPanel() {
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		TitledBorder border = BorderFactory
				.createTitledBorder("ELECTION INFORMATION");
		border.setTitleJustification(TitledBorder.CENTER);
		panel.setPreferredSize(new Dimension(300, 100));
		panel.setBorder(border);
		panel.add(getElectionLable());
		panel.add(getElectionList());

		return panel;
	}

	public static JPanel getResultPanel() {
		if(resultPanel == null){
			resultPanel = new JPanel();
			resultPanel.setLayout(new BoxLayout(resultPanel, BoxLayout.Y_AXIS));
			TitledBorder border = BorderFactory
					.createTitledBorder("RESULT INFORMATION");
			border.setTitleJustification(TitledBorder.CENTER);
			resultPanel.setBorder(border);
			resultPanel.setPreferredSize(new Dimension(300, 300));
			resultPanel.add(getResultArea());
		}
		
		return resultPanel;
	}
	
	public JFrame getFrame() {
		if (frame == null) {
			frame = new JFrame("ELECTION RESULT" + " - " + VoterGui.fullName.toUpperCase());

		}
		return frame;
	}

	public JComboBox getElectionList() {
		if (electionList == null) {
			//electionList = new JComboBox(MgrElection.getElectionIdsForVoter(
			//		VoterGui.voter).toArray());
			
			// Poom Fixed it
			List<Long> electList = new ArrayList();
			System.out.println("size "+elections.size());
			for(int i = 0; i < elections.size(); i++){
				//elecList[i] = elections.get(i).getId();
				if(!elections.isEmpty())
					electList.add(elections.get(i).getId());
			}
			electionList = new JComboBox(electList.toArray());
			electionList.addActionListener(new CheckResultListner(this));
			electionList.setActionCommand(CheckResultListner.CHECK_RESULT);
			
		}
		return electionList;
	}

	public JLabel getElectionLable() {
		if (electionLable == null) {
			electionLable = new JLabel("Select Election ID: ");
		}
		return electionLable;
	}


	public static JScrollPane getResultArea() {
		if(sbrConsol == null){
			resultArea = new JTextArea(20, 30);
			resultArea.setLineWrap(true);
			sbrConsol = new JScrollPane(resultArea);
			sbrConsol.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
			resultArea.setEditable(false);
		}
		return sbrConsol;
	}


	
}
