package com.csci6545.evoting.pl;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.Socket;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import com.csci6545.evoting.be.Message;
import com.csci6545.evoting.bl.MgrMessage;
import com.csci6545.evoting.bl.MgrResult;
import com.csci6545.evoting.bl.MgrSSL;
import com.csci6545.evoting.bl.MsgHandler;

public class VerifyListener implements ActionListener {

	VerifyGui verifyGui;
	
	public VerifyListener(
			VerifyGui verifyBallotGui) {
		this.verifyGui = verifyBallotGui;
	
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String command = e.getActionCommand();
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		if(command.equals(ClientListner.VERIFY_BALLOT)){
			try {
				Socket s = MgrSSL.initSSLClientToServerSocket();
				long electionId = (Long)VerifyGui.getElectionList().getSelectedItem();
				byte [] msg = MgrMessage.checkBcBallotFactorMsg("CheckEntry",
						VoterGui.voter.getTicketBB(), electionId);
				MgrMessage.sendMsg(s, msg);
				
				byte [] bb = MgrMessage.recvMsg(s);
				Message m = MsgHandler.voterHandleMessage(MgrMessage.getMessgaeType(bb), bb, s);
				VoterGui.voter.setTicketBB(m.getTicketBB());
				if(m.getDetail().equals("succeeded")){
					
					//if(MgrResult.isVoted(VoterGui.voter, electionId))
					if(VoterGui.voter.getBcBallotMap().get(electionId) != null)
					{
						byte [] bcBallot = VoterGui.voter.getBcBallotMap().get(electionId);
						int entry = MgrResult.getEntry(m.getBcArray(), bcBallot);
						if(entry!=0)
						{
							VoterGui.voter.setMapElectionEntry(MgrResult.storeBcEntry(entry, 
								electionId, VoterGui.voter));
							
							//Send Key to counter
							s = MgrSSL.initSSLClientToCounterSocket(VoterGui.voter.getCounterIPAddress(),
									VoterGui.voter.getCounterPort());
							msg = MgrMessage.submitKeyMsg(VoterGui.voter.getKeyMap().get(electionId), 
									VoterGui.voter.getIvMap().get(electionId), 
									VoterGui.voter.getMapElectionEntry().get(electionId));
							
							MgrMessage.sendMsg(s, msg);
							
							JOptionPane.showMessageDialog(frame, "We found your ballot ("+entry+") \n" +
									"please wait and see the election result");
							
						}
						else
						{
							int dialogButton1 = JOptionPane.WARNING_MESSAGE;
							JOptionPane
							.showMessageDialog(
									frame,
									"Your ballot does not exist or election is not yet over",
									"Verify Ballot", dialogButton1);
						}
					}
					else
					{
						int dialogButton1 = JOptionPane.WARNING_MESSAGE;
						JOptionPane
						.showMessageDialog(
								frame,
								"You have not voted yet",
								"Verify Ballot", dialogButton1);
					}
				}
				else if(m.getDetail().equals("ticket expired!!"))
				{
					int dialogButton1 = JOptionPane.WARNING_MESSAGE;
					JOptionPane
					.showMessageDialog(
							frame,
							m.getDetail(),
							"Verify Ballot", dialogButton1);
					VoterListner.voter.getFrame().dispose();
					ClientGui.createAndShowGUI();
				}
				else
				{
					int dialogButton1 = JOptionPane.WARNING_MESSAGE;
					JOptionPane
					.showMessageDialog(
							frame,
							m.getDetail(),
							"Verify Ballot", dialogButton1);
				}
				
			} catch (Exception ex) {
				// TODO Auto-generated catch block
				ex.printStackTrace();
			}
		}

	}

}
