package com.csci6545.evoting.pl;

import java.awt.Component;
import java.awt.FlowLayout;
import java.net.Socket;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.TitledBorder;

import com.csci6545.evoting.be.User;
import com.csci6545.evoting.bl.MgrSystemProperties;

public class ResetPasswordGui {

	private static JButton submit = null;
	
	private static JLabel passwordL = null;
	private static JLabel passwordL1 = null;
	
	private static JPasswordField passwordText = null;
	private static JPasswordField passwordText1 = null;
	private static JFrame mainFrame = null;
	private JPanel mainPanel = null;
	public static String username;
	public static void createAndShowGUI(String uName) {
		username = uName;
		Runnable createAndShow = new Runnable() {

			@Override
			public void run() {
				JFrame.setDefaultLookAndFeelDecorated(true);
				ResetPasswordGui gui = new ResetPasswordGui();
				// add your code here

			}
		};
		SwingUtilities.invokeLater(createAndShow);
	}
	
	public ResetPasswordGui() {
		mainPanel = populateMainPanel(getPasswordResetPanel());
		getMainFrame().setContentPane(mainPanel);

		// create and setup the window
		getMainFrame().addWindowListener(new ClientListner());
		getMainFrame().setLocation(150, 180);
		getMainFrame().setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		// Display the window
		getMainFrame().pack();
		getMainFrame().setVisible(true);
		getMainFrame().setResizable(false);
	}
	
	private JPanel populateMainPanel(JPanel newUserPanel) {
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		panel.add(newUserPanel);
		return panel;
	}

	public JFrame getMainFrame() {
		if (mainFrame == null) {
			mainFrame = new JFrame(ClientGui.FRAME_TITLE + " - PASSWORD RESET");
		}
		return mainFrame;
	}

	public JPanel getPasswordResetPanel() {
			JPanel panel = new JPanel();
			TitledBorder border = BorderFactory
					.createTitledBorder("PASSWORD RESET");
			border.setTitleJustification(TitledBorder.CENTER);
			panel.setBorder(border);
			panel
					.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
			panel.add(newPasswordPanel());
			panel.add(newPassword2Panel());
			panel.add(getSubmit());
		
		return panel;
	}

	
	private JPanel newPassword2Panel() {
		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout(FlowLayout.LEFT));
		panel.add(getPasswordL1());
		panel.add(getPasswordText1());
		return panel;
	}

	private JPanel newPasswordPanel() {
		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout(FlowLayout.LEFT));
		panel.add(getPasswordL());
		panel.add(getPasswordText());
		return panel;
	}

	public JButton getSubmit() {
		if(submit == null){
			submit = new JButton("SUBMIT");
			submit.addActionListener(new PasswordResetListner(this));
			submit.setActionCommand(PasswordResetListner.RESET_PASSWORD);
		}
		return submit;
	}

	public static JLabel getPasswordL() {
		if(passwordL == null){
			passwordL = new JLabel("New Password: ");
		}
		return passwordL;
	}
	public static JLabel getPasswordL1() {
		if(passwordL1 == null){
			passwordL1 = new JLabel("Re-enter New Password: ");
		}
		return passwordL1;
	}
	
	public static JPasswordField getPasswordText() {
		if(passwordText == null){
			passwordText = new JPasswordField(20);
		}
		return passwordText;
	}
	public static JPasswordField getPasswordText1() {
		if(passwordText1 == null){
			passwordText1 = new JPasswordField(20);
		}
		return passwordText1;
	}
	
	
	
}
