/* Autheor: Alemberhan Getahun 
 * Date: 11/01/2013*/
package com.csci6545.evoting.pl;

import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Panel;
import java.net.Socket;
import java.security.KeyPair;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.border.TitledBorder;

import com.csci6545.evoting.be.Election;
import com.csci6545.evoting.be.Message;
import com.csci6545.evoting.be.TicketBB;
import com.csci6545.evoting.be.User;
import com.csci6545.evoting.be.Voter;
import com.csci6545.evoting.bl.KeyDistribution;
import com.csci6545.evoting.bl.MgrMessage;
import com.csci6545.evoting.bl.MgrSSL;
import com.csci6545.evoting.bl.MsgHandler;
import com.csci6545.evoting.dl.MgrElection;

public class VoterGui {
	private static final String VOTER = "VOTER";
	private static JButton register = null;
	private static JButton vote = null;
	private static JButton checkResult = null;
	private static JButton updatePersonal = null;
	private static JButton createElection = null;
	private static JButton verifyBallot = null;
	private static JButton checkAuthorized= null;
	private static JTextArea electionText = null;
	private static JScrollPane sbrConsol = null;
	private JButton signOut = null;
	private JPanel voterPanel = null;
	private JFrame frame = null;
	public static String fullName = null;
	public static Voter voter;
	public static List<Election> elections;
	public VoterGui()
	{
		getFrame().setContentPane(mainPanel());
		getFrame().addWindowListener(new VoterListner());
		getFrame().setLocation(150, 180);
		getFrame().setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getFrame().pack();
		getFrame().setVisible(true);
		getFrame().setResizable(false);
	}
	
	public static void createAndShow(final Voter user, List<Election> list)
	{
		elections = list;
		voter = user;
		Runnable runnable = new Runnable() {
			
			@Override
			public void run() {
				VoterGui.setFullName(user.getfName(), user.getlName());
				VoterGui voter = new VoterGui();
				
				
			}
		};
		SwingUtilities.invokeLater(runnable);
		
		
	}
	
	
	
	public JFrame getFrame() {
		if(frame == null){
			frame = new JFrame(VoterGui.VOTER + " - " + VoterGui.fullName.toUpperCase());
			
		}
		return frame;
	}
	



	
	public JPanel mainPanel()
	{
		JPanel panel = new JPanel();
		panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		panel.add(getVoterPanel());
		panel.add(electonPanel());
		return panel;
		
	}
	public JPanel getVoterPanel() {
		if(voterPanel == null){
			voterPanel = new JPanel();
			voterPanel.setLayout(new BoxLayout(voterPanel, BoxLayout.Y_AXIS));
			TitledBorder border =BorderFactory.createTitledBorder("VOTER INFORMATION");
			border.setTitleJustification(TitledBorder.CENTER);
			
			voterPanel.setBorder(border);
			if(voter.getPermission() >= 4){
			voterPanel.add(getCreateElection());
			}
			voterPanel.add(getRegister());
			voterPanel.add(getVote());
			voterPanel.add(getVerifyBcBallot());
			voterPanel.add(getCheckResult());
			voterPanel.add(checkAuthorizedBallot());
			voterPanel.add(getUpdatePersonal());
			voterPanel.add(getSignOut());
		}
		return voterPanel;
	}
	private JButton getVerifyBcBallot() {
		if(verifyBallot == null){
			verifyBallot = new JButton("VERIFY MY BALLOTS AND SEND KEYS");
			verifyBallot.addActionListener(new VoterListner(this));
			verifyBallot.setActionCommand(ClientListner.VERIFY_BALLOT);
		}
		return verifyBallot;
	}
	
	private JButton checkAuthorizedBallot() {
		if(checkAuthorized == null){
			checkAuthorized = new JButton("CHECK AUTHORIZED BALLOTS");
			checkAuthorized.addActionListener(new VoterListner(this));
			checkAuthorized.setActionCommand(ClientListner.CHECK_AUTHORIZED);
		}
		return checkAuthorized;
	}
	
	private JButton getCreateElection() {
		if(createElection == null){
			createElection = new JButton("CREATE ELECTION");
			createElection.addActionListener(new VoterListner(this));
			createElection.setActionCommand(ClientListner.CREATE_ELECTION);
		}
		return createElection;
	}

	public JButton getRegister() {
		if(register == null){
			register = new JButton("REGISTER FOR ELECTION");
			register.addActionListener(new VoterListner(this));
			register.setActionCommand(ClientListner.REGISTER_ELECTION);
		}
		return register;
	}
	
	public JButton getCheckResult()
	{
		if(checkResult == null){
			checkResult = new JButton("CHECK RESULT");
			checkResult.addActionListener(new VoterListner(this));
			checkResult.setActionCommand(ClientListner.CHECK_RESULT);
		}
	  return checkResult;	
	}
	
	public JButton getUpdatePersonal(){
		if(updatePersonal == null){
			updatePersonal = new JButton("UPDATE PERSONAL INFROMATION");
			updatePersonal.addActionListener(new VoterListner(this));
			updatePersonal.setActionCommand(ClientListner.UPDATE_PERSONAL);
		}
		return updatePersonal;
	}
	public  JButton getVote() {
		if(vote == null)
		{
			vote = new JButton("MAKE VOTE");
			vote.addActionListener(new VoterListner(this));
			vote.setActionCommand(ClientListner.VOTE);
		}
		return vote;
	}
	
	public JButton getSignOut() {
		if(signOut == null){
			signOut = new JButton("SIGN OUT");
			signOut.addActionListener(new VoterListner(this));
			signOut.setActionCommand(ClientListner.SIGNOUT);
		}
		return signOut;
	}

	public static void setFullName(String fname, String lname) {
		VoterGui.fullName = fname + " " + lname;
	}

	public JPanel electonPanel (){
		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout());
		TitledBorder border =BorderFactory.createTitledBorder("ELECTION INFORMATION");
		border.setTitleJustification(TitledBorder.CENTER);
		panel.setBorder(border);
		panel.add(getElectionText());
		return panel;
	}
	
	
	public static JScrollPane getElectionText() {
		if(sbrConsol == null){
			electionText = new JTextArea(20,30);
			electionText.setLineWrap(true);
			electionText.setText("");
			if(elections != null){
				Iterator it = elections.iterator();
				while(it.hasNext()){
					Election election = (Election) it.next();
					electionText.append("ID: " +String.valueOf(election.getId()) + "\n");
					electionText.append("Name: " +election.getName() + "\n");
					electionText.append("Start Date: " +String.valueOf(election.getStart()) + "\n");
					electionText.append("End Date: " +String.valueOf(election.getEnd()) + "\n");
					electionText.append("*****************************************************"+"\n");
					
				}
			
			}
		}
		electionText.setEditable(false);
		sbrConsol = new JScrollPane(electionText);
		sbrConsol.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

		return sbrConsol;
	}

	public static Voter getVoter() {
		return voter;
	}

	
}
