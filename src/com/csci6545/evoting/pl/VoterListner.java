package com.csci6545.evoting.pl;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.net.Socket;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;









import com.csci6545.evoting.be.Election;
import com.csci6545.evoting.be.Message;
import com.csci6545.evoting.be.Title;
import com.csci6545.evoting.be.User;
import com.csci6545.evoting.be.Voter;
import com.csci6545.evoting.bl.MgrMessage;
import com.csci6545.evoting.bl.MgrResult;
import com.csci6545.evoting.bl.MgrSSL;
import com.csci6545.evoting.bl.MsgHandler;
import com.csci6545.evoting.dl.LoadConfig;
import com.csci6545.evoting.dl.MgrCandidate;
import com.csci6545.evoting.dl.MgrTitle;
import com.csci6545.evoting.dl.MgrUser;

public class VoterListner extends WindowAdapter implements ActionListener {

	public static VoterGui voter = null;
	
	public VoterListner(){}
	
	public VoterListner(VoterGui voterGui){
		voter = voterGui;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		String command = e.getActionCommand();
		System.out.println("Action command: " + command);
		JFrame frame = new JFrame();
		if(command.equals(ClientListner.REGISTER_ELECTION)){
			registerElectonActionPerformed();
		} 
		else if(command.equals(ClientListner.VOTE))
		{
			// create election action
			Socket s = MgrSSL.initSSLClientToServerSocket();
			MgrMessage.sendMsg(s, MgrMessage.getElectionInfoMsg("getRegisteredElections", 
					VoterGui.voter.getTicketBB(), VoterGui.voter.getUserId()));
			// Receive Msg
			byte [] bb = MgrMessage.recvMsg(s);
			//Handle Msg
			Message recvAckRegElection = MsgHandler.voterHandleMessage(MgrMessage.getMessgaeType(bb),bb,s);
			VoterGui.voter.setTicketBB(recvAckRegElection.getTicketBB());
			if (VoterGui.voter.getTicketBB().getRole().equals("failed"))
			{
				int dialogButton1 = JOptionPane.WARNING_MESSAGE;
				JOptionPane
				.showMessageDialog(
						frame,
						"Ticket Expired!!",
						"SIGN IN", dialogButton1);
				voter.getFrame().dispose();
				ClientGui.createAndShowGUI();
				}
			else
			{
				if(recvAckRegElection.getDetail().equals("succeeded"))
					makeVoteActionPerformed(Arrays.asList(recvAckRegElection.getElectionArray()));
				else
				{
					int dialogButton1 = JOptionPane.WARNING_MESSAGE;
					JOptionPane
					.showMessageDialog(
							frame,
							recvAckRegElection.getDetail(),
							"Message", dialogButton1);
				}
			}			
	
		} 
		else if(command.equals(ClientListner.CHECK_RESULT)){
			//check result part
			CheckResultGui.electionList = null;
			Socket s = MgrSSL.initSSLClientToServerSocket();
			MgrMessage.sendMsg(s, MgrMessage.getElectionInfoMsg("getAllElections", 
					VoterGui.voter.getTicketBB(), VoterGui.voter.getUserId()));
			// Receive Msg
			byte [] bb = MgrMessage.recvMsg(s);
			//Handle Msg
			Message recvAckRegElection = MsgHandler.voterHandleMessage(MgrMessage.getMessgaeType(bb),bb,s);
			VoterGui.voter.setTicketBB(recvAckRegElection.getTicketBB());
			if(!VoterGui.voter.getTicketBB().getRole().equals("failed"))
			{
				CheckResultGui.createAndShow(VoterGui.voter, Arrays.asList(recvAckRegElection.getElectionArray()));
			} 
			else{
				int dialogButton1 = JOptionPane.WARNING_MESSAGE;
				JOptionPane
				.showMessageDialog(
						frame,
						"Ticket Expired!!",
						"Error", dialogButton1);
				VoterListner.voter.getFrame().dispose();
				ClientGui.createAndShowGUI();
			}
		}
		else if(command.equals(ClientListner.CHECK_AUTHORIZED)){
			//check result part
			CheckAuthorized.electionList = null;
			Socket s = MgrSSL.initSSLClientToServerSocket();
			MgrMessage.sendMsg(s, MgrMessage.getElectionInfoMsg("getAllRegisteredElections", 
					VoterGui.voter.getTicketBB(), VoterGui.voter.getUserId()));
			// Receive Msg
			byte [] bb = MgrMessage.recvMsg(s);
			//Handle Msg
			Message recvAckRegElection = MsgHandler.voterHandleMessage(MgrMessage.getMessgaeType(bb),bb,s);
			VoterGui.voter.setTicketBB(recvAckRegElection.getTicketBB());
			if(!VoterGui.voter.getTicketBB().getRole().equals("failed"))
			{
				CheckAuthorized.createAndShow(VoterGui.voter, Arrays.asList(recvAckRegElection.getElectionArray()));
			} 
			else{
				int dialogButton1 = JOptionPane.WARNING_MESSAGE;
				JOptionPane
				.showMessageDialog(
						frame,
						"Ticket Expired!!",
						"Error", dialogButton1);
				VoterListner.voter.getFrame().dispose();
				ClientGui.createAndShowGUI();
			}
		}
		else if(command.equals(ClientListner.UPDATE_PERSONAL)){
			// update personal result
			UpdatePersonalInfoGui.createAndShow(VoterGui.getVoter());
		} else if(command.equals(ClientListner.CREATE_ELECTION)){
				//change
				ElectionCreationGui.candidateList = null;
				ElectionCreationGui.includeList = null;
				ElectionCreationGui.excludeList = null;
				ElectionCreationGui.titleList = null;
				ElectionCreationGui.nameField = null;
				Title[] titles = null;
				User[] candidates = null;
				//LoadConfig.loadDBConfig();
				Socket s = null;
				try {
					// Get All Titles
					s = MgrSSL.initSSLClientToServerSocket();
					byte [] msg = MgrMessage.getTitleMsg(VoterGui.voter.getTicketBB());
					MgrMessage.sendMsg(s, msg);
					byte [] bb = MgrMessage.recvMsg(s);
					Message m = MsgHandler.supHandleMsg(MgrMessage.getMessgaeType(bb), bb, s);

					//titles =  MgrTitle.getAllTitle().toArray(new Title[MgrTitle.getAllTitle().size()]);
					titles =  m.getTitle().toArray(new Title[m.getTitle().size()]);
					// Get All Users
					//candidates = MgrUser.getUserInfo().toArray(new User[MgrUser.getUserInfo().size()]);
					s = MgrSSL.initSSLClientToServerSocket();
					msg = MgrMessage.getUserInfoMsg(VoterGui.voter.getTicketBB());
					MgrMessage.sendMsg(s, msg);
					bb = MgrMessage.recvMsg(s);
					m = MsgHandler.supHandleMsg(MgrMessage.getMessgaeType(bb), bb, s);
					VoterGui.voter.setTicketBB(m.getTicketBB());
					candidates = m.getUserArray();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				if(!VoterGui.voter.getTicketBB().getRole().equals("failed"))
				{
					ElectionCreationGui.createAndShow(VoterGui.voter, candidates, titles);
				}
				else{
					int dialogButton1 = JOptionPane.WARNING_MESSAGE;
					JOptionPane
					.showMessageDialog(
							frame,
							"Ticket Expired!!",
							"Error", dialogButton1);
					VoterListner.voter.getFrame().dispose();
					ClientGui.createAndShowGUI();
				}				
				
			} 
		else if(command.equals(ClientListner.VERIFY_BALLOT)){
			VerifyGui.sbrConsol = null;
			if(!VoterGui.voter.getElectionIdList().isEmpty())
			{
				VerifyGui.electionList = null;
				VerifyGui.createAndShow(VoterGui.voter, VoterGui.elections);
			}
			else
			{
				int dialogButton1 = JOptionPane.WARNING_MESSAGE;
				JOptionPane
				.showMessageDialog(
						frame,
						"Please cast your votes first",
						"Ballot Verification", dialogButton1);
			}
		}
		else if(command.equals(ClientListner.SIGNOUT)){
			try {
				Socket s = MgrSSL.initSSLClientToServerSocket();
				MgrMessage.sendMsg(s, MgrMessage.logOutMsg(VoterGui.voter.getTicketBB()));
			} catch (Exception ex) {
				// TODO Auto-generated catch block
				ex.printStackTrace();
			}
			voter.getFrame().dispose();
		}

	}

	private void makeVoteActionPerformed(List<Election> elections) {
		
		MakeVoteGui.electionList = null;
		MakeVoteGui.createAndShow(VoterGui.voter, elections);
		
	}

	private void registerElectonActionPerformed() {
		ElectionRegistrationGui.electionList = null;
		ElectionRegistrationGui.sbrConsol = null;
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Socket s = MgrSSL.initSSLClientToServerSocket();
		MgrMessage.sendMsg(s, MgrMessage.getElectionInfoMsg("getElectionInfo",VoterGui.voter.getTicketBB(),VoterGui.voter.getUserId()));
		// Receive Msg
		byte [] bb = MgrMessage.recvMsg(s);
		//Handle Msg
		Message recvAckElection = MsgHandler.voterHandleMessage(MgrMessage.getMessgaeType(bb),bb,s);
		VoterGui.voter.setTicketBB(recvAckElection.getTicketBB());
		VoterGui.elections = Arrays.asList(recvAckElection.getElectionArray());
		if(!VoterGui.voter.getTicketBB().getRole().equals("failed"))
		{
			ElectionRegistrationGui.createAndShow(VoterGui.voter, VoterGui.elections);
		}
		else{
			int dialogButton1 = JOptionPane.WARNING_MESSAGE;
			JOptionPane
			.showMessageDialog(
					frame,
					"Ticket Expired!!",
					"Error", dialogButton1);
			VoterListner.voter.getFrame().dispose();
			ClientGui.createAndShowGUI();
		}
	}


}
