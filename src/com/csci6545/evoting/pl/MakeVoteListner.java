package com.csci6545.evoting.pl;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.math.BigInteger;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;

import com.csci6545.evoting.be.Ballot;
import com.csci6545.evoting.be.BcBallotFactor;
import com.csci6545.evoting.be.Candidate;
import com.csci6545.evoting.be.Election;
import com.csci6545.evoting.be.Message;
import com.csci6545.evoting.bl.KeyDistribution;
import com.csci6545.evoting.bl.MgrConfidentiality;
import com.csci6545.evoting.bl.MgrIntegrity;
import com.csci6545.evoting.bl.MgrMessage;
import com.csci6545.evoting.bl.MgrSSL;
import com.csci6545.evoting.bl.MgrUser;
import com.csci6545.evoting.bl.MsgHandler;
import com.csci6545.evoting.dl.LoadConfig;
import com.csci6545.evoting.dl.MgrElection;
import com.sun.org.apache.bcel.internal.generic.GETSTATIC;

public class MakeVoteListner implements ActionListener {

	public static final String SELECT_ELECTION = "select election";
	public static final String SUBMIT_VOTE = "submit election";
	public static long id;
	public static JRadioButton [] candidateRad;
	ButtonGroup bg = null;
	MakeVoteGui makeVoteGui;
	Map candidates = null;
	
	public MakeVoteListner(MakeVoteGui makeVoteGui) {
		this.makeVoteGui = makeVoteGui;
	}

	public int findBallotEntry(BcBallotFactor [] bcFactor, byte [] bcBallot)
	{
		return 0;
	}
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		String command = e.getActionCommand();
		
		Election selectedElection = new Election();
		if(command.equals(SELECT_ELECTION)){
			// select election
			
			
			id = 	(Long) makeVoteGui.getElectionList().getSelectedItem();
			MakeVoteGui.electionID = id;
			System.out.println(MakeVoteGui.electionID);
			for(int i=0;i<MakeVoteGui.elections.size();i++)
			{
				if(MakeVoteGui.elections.get(i).getId() == id)
				{
					selectedElection = MakeVoteGui.elections.get(i);
					break;
				}
			}
			
			 candidates = selectedElection.getCandidate();
			 MakeVoteGui.candidates = candidates;
			 candidateRad = new JRadioButton[candidates.size()];
			try {
				//candidates = MgrElection.getElectionCandidates(id);
				bg = new ButtonGroup();
				makeVoteGui.getCandidatePanel().removeAll();
				Iterator it = candidates.entrySet().iterator();
				int i=0;
				while (it.hasNext()) {
					Map.Entry pairs = (Map.Entry)it.next();
					candidateRad[i] = new JRadioButton();
					candidateRad[i].setText((String) pairs.getValue());
					bg.add(candidateRad[i]);
					makeVoteGui.getCandidatePanel().setLayout(new BoxLayout(makeVoteGui.getCandidatePanel(), BoxLayout.Y_AXIS));
					makeVoteGui.getCandidatePanel().add(candidateRad[i]);
					makeVoteGui.getCandidatePanel().add(makeVoteGui.getSubmitBtn());
					
					makeVoteGui.getCandidatePanel().revalidate();
					i++;
				}
	
				
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			
		} else if(command.equals(SUBMIT_VOTE)){
			//submit vote
			String candidateName = null;
			int candidateId = 0;
			for(int i=0;i<candidateRad.length;i++)
			{

				if(candidateRad[i].isSelected()){ 
					
					System.out.println(candidateRad[i].getText());
					candidateName = candidateRad[i].getText();					
					
					Set mapSet = (Set) MakeVoteGui.candidates.entrySet();
					Iterator iterate = mapSet.iterator();
					
					while(iterate.hasNext()){
						Map.Entry cadidate = (Map.Entry) iterate.next();
						if(candidateName.equals(cadidate.getValue())){
							candidateId = Integer.parseInt(cadidate.getKey().toString().replace(" ", ""));
							
						}
					}
					break;
				}	
			}
			System.out.println("eid "+MakeVoteGui.electionID);
			System.out.println("can "+candidateId);
			Ballot ballot = new Ballot(MakeVoteGui.electionID, candidateId);
		
			JFrame frame = new JFrame();
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			try {
				// Prepare BcBallot .......................
				byte [] key = (MgrConfidentiality.generateSecreteKey());
				byte [] iv = (MgrConfidentiality.getRandomIV());
				byte [] bcBallot = MgrConfidentiality.encryptAESBallot(ballot.getBytes(),key, iv);
				//VoterGui.voter.setBcBallot();
				VoterGui.voter.setElectionIdList(MgrUser.storeElectionId(MakeVoteGui.electionID, VoterGui.voter));
				VoterGui.voter.setBcBallotMap(MgrUser.storeVoterBcBallot(MakeVoteGui.electionID, bcBallot, VoterGui.voter));
				VoterGui.voter.setBlindingFactor(MgrIntegrity.getBlindingFactor(MgrSSL.getServerPublicKeyFromTrustStore()).toByteArray());
				VoterGui.voter.setBlindBallot(MgrIntegrity.makeBlind(MgrSSL.getServerPublicKeyFromTrustStore(), 
						new BigInteger(VoterGui.voter.getBlindingFactor()), bcBallot));
				byte [] voterSignature = MgrIntegrity.sign(VoterGui.voter.getMyPrivateKey(), VoterGui.voter.getBlindBallot());
				byte [] msg = MgrMessage.blindBallotMsg(VoterGui.voter.getBlindBallot(),
						voterSignature, VoterGui.voter.getTicketBB(), id);
				
				// Send Blind Ballot............................
				System.out.println(new String(msg));
				Socket s = MgrSSL.initSSLClientToServerSocket();
				MgrMessage.sendMsg(s, msg);
				
				byte[] bb = MgrMessage.recvMsg(s);

				//Handle Msg
				Message recvBlindSignature = MsgHandler.voterHandleMessage(MgrMessage.getMessgaeType(bb),bb,s);
				
				VoterGui.voter.setSupervisorSigOnBcBallot(MgrIntegrity.unBlind(MgrSSL.getServerPublicKeyFromTrustStore(), 
						new BigInteger(VoterGui.voter.getBlindingFactor()), recvBlindSignature.getBlindSignature().toByteArray()));
				VoterGui.voter.setCounterPublicKey(recvBlindSignature.getCounterPublicKey());
				
				VoterGui.voter.setTicketBB(recvBlindSignature.getTicketBB());
				VoterGui.voter.setKeyMap(MgrUser.storeVoterKey(MakeVoteGui.electionID, key, VoterGui.voter));
				VoterGui.voter.setIvMap(MgrUser.storeVoterIv(MakeVoteGui.electionID, iv, VoterGui.voter));
				
				VoterGui.voter.setBallotMap(MgrUser.storeVoterBallot(MakeVoteGui.electionID, ballot.getBytes(), VoterGui.voter));
				
				VoterGui.voter.setCounterIPAddress(recvBlindSignature.getCounterIPAddress());
				VoterGui.voter.setCounterPort(recvBlindSignature.getCounterPort());
				if(!VoterGui.voter.getTicketBB().getRole().equals("failed"))
				{
					if(recvBlindSignature.getDetail().equals("succeeded"))
					{
						// Verify Supervisor Signature...............................
						if(MgrIntegrity.verify(MgrSSL.getServerPublicKeyFromTrustStore(), bcBallot, 
								VoterGui.voter.getSupervisorSigOnBcBallot()))
						{
							msg = MgrMessage.confirmVerifySignatureMsg("succeeded", VoterGui.voter.getTicketBB(), voterSignature);
							System.out.println(new String(VoterGui.voter.getTicketBB().getBytes()));
							System.out.println("Supervisor verify succeeded");
							System.out.println(recvBlindSignature.getCounterIPAddress());
							System.out.println(recvBlindSignature.getCounterPort());
							// Sending BcBallot to Counter....................................
							try {
								//Socket counterSock = new Socket(recvBlindSignature.getCounterIPAddress(),recvBlindSignature.getCounterPort() );
								Socket counterSock = MgrSSL.initSSLClientToCounterSocket(VoterGui.voter.getCounterIPAddress(),VoterGui.voter.getCounterPort());
								byte [] pre_hybrid = MgrMessage.submitBcBallotMsg(bcBallot, VoterGui.voter.getSupervisorSigOnBcBallot(), MakeVoteGui.electionID);						
																			
								MgrMessage.sendMsg(counterSock, pre_hybrid);
								
								JOptionPane.showMessageDialog(frame, "Your ballot is successfully submitted \n " +
										"Please see the result after the election is over");		
								
							} catch (Exception e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							
						}
						else
						{
							int dialogButton1 = JOptionPane.WARNING_MESSAGE;
							// failed authentication
							JOptionPane
									.showMessageDialog(
											frame,
											"Supervisor Signature Failed",
											"Election Registration", dialogButton1);
							msg = MgrMessage.confirmVerifySignatureMsg("failed", VoterGui.voter.getTicketBB(), voterSignature);
						}
						// Send Confirm verify Sup Signature
						System.out.println(new String(msg));
						Socket ss = MgrSSL.initSSLClientToServerSocket();
						MgrMessage.sendMsg(ss, msg);
					}
					else
					{
						int dialogButton1 = JOptionPane.WARNING_MESSAGE;
						// failed authentication
						JOptionPane
								.showMessageDialog(
										frame,
										recvBlindSignature.getDetail(),
										"Election Registration", dialogButton1);
					}
				}
				else
				{
					int dialogButton1 = JOptionPane.WARNING_MESSAGE;
					JOptionPane
					.showMessageDialog(
							frame,
							"Ticket Expired!!",
							"Error", dialogButton1);
					VoterListner.voter.getFrame().dispose();
					ClientGui.createAndShowGUI();
				}
				
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			MakeVoteGui.candidates = null;
			MakeVoteGui.electionList = null;
			makeVoteGui.getCandidatePanel().removeAll();
			MakeVoteGui.electionID = 0;
			makeVoteGui.getFrame().dispose();
		}

	}

}
