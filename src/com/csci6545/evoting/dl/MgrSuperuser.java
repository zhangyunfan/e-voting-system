package com.csci6545.evoting.dl;

import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.csci6545.evoting.be.User;
import com.csci6545.evoting.be.Voter;
import com.csci6545.evoting.bl.MgrBytes;
import com.csci6545.evoting.bl.MgrHash;

public class MgrSuperuser {
	public static boolean existsSuper(String userName) 
			throws SQLException{
		Connection conn = DBManager.getConnection();
		PreparedStatement prep = null;
		ResultSet rs = null;
	    prep = conn.prepareStatement("SELECT * FROM SUPERUSER "
					+ "WHERE USERNAME=?");
		prep.setString(1, userName);
		rs = prep.executeQuery();
		if(rs.next()){
			System.err.println("The username already exists");
			return true;
		}
		return false;

	}

	public static long getIDOfUser(String userName) 
			throws SQLException{
		Connection conn = DBManager.getConnection();
		PreparedStatement prep = null;
		ResultSet rs = null;
	    prep = conn.prepareStatement("SELECT ID,USERNAME FROM SUPERUSER "
					+ "WHERE USERNAME=?");
		prep.setString(1, userName);
		rs = prep.executeQuery();
		if(!rs.next()){
			throw new DBHasNoSuchUserException("No such "
					+ "user in the USER table!");
		}
		return rs.getLong("ID");
		
	}
	/**
	 * 
	 * @param user the User object
	 * @return -1 if user exists, 0 if creation failed, 1 if the creation success
	 */
	 public static int createSuper(String username, String password, String email){
		 int userInserted = 0;
		 Connection conn = DBManager.getConnection();
		 PreparedStatement prep = null;
		 SecureRandom secureRandom = new SecureRandom();
		 byte[] salt;
		 try {
			 if(existsSuper(username)){
				 return 0;
			 }
			prep = conn.prepareStatement("insert into SUPERUSER(USERNAME, PASSWORD, SALT, EMAIL) "
			 		+ "values(?, ?, ?, ?)");
			 salt = new byte[4];
				secureRandom.nextBytes(salt);
			prep.setString(1, username);
			String saltPasswd = MgrHash.generateStringdHash(
					MgrBytes.combineByteArray(salt,password.getBytes()));
			prep.setString(2, saltPasswd);			
			prep.setString(3, MgrBytes.bytesToHexString(salt));
			
			prep.setString(4, email);
			userInserted = prep.executeUpdate();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 return userInserted;
	 }

	 public static boolean setSuperPassword(String username, String password, int flag){
		 
		 try{
			 int succeeded = 0;
			 Connection conn = DBManager.getConnection();
			 PreparedStatement prep = null;
			 prep = conn.prepareCall("select SALT from SUPERUSER where USERNAME=?");
			 prep.setString(1, username);
			 ResultSet	rs = prep.executeQuery();
			 byte[] salt=new byte[4];
			 if(rs.next()){
					String saltHextString = rs.getString("SALT");
					salt = MgrBytes.hexStringToBytes(saltHextString);
			}
			prep.close();
			prep=null;
			rs =null;
			prep = conn.prepareStatement("UPDATE SUPERUSER SET PASSWORD=?, IS_OTP = ? WHERE USERNAME=?");
			 
			String saltPasswd = MgrHash.generateStringdHash(
						MgrBytes.combineByteArray(salt,password.getBytes())); 
			prep.setString(1, saltPasswd);
			prep.setInt(2, flag);
			prep.setString(3, username);
			 succeeded = prep.executeUpdate();
			 if(succeeded==1)
			 {
				 return true;
			 }
				 //System.out.println("Succeeded!");
		 }
		 catch(Exception e){
			 e.printStackTrace();
		 }
		 return false;
	 }

	 public static String getSuperEmail(String username){
		 try{
			 Connection conn = DBManager.getConnection();
			 PreparedStatement prep = null;
			 ResultSet rs = null;
			 prep = conn.prepareStatement("SELECT EMAIL FROM SUPERUSER WHERE USERNAME=?");
			 prep.setString(1, username);
			 rs = prep.executeQuery();
			 if(rs.next()){
				 String email = rs.getString("EMAIL");
				 return email;
			 }
			 else{
				 return null;
			 }
		 }
		 catch(Exception e){
			 e.printStackTrace();
			 return null;
		 }
	 }
	 
	 public static int getSuperLoginAttempts(String username){
		 try{
			 Connection conn = DBManager.getConnection();
			 PreparedStatement prep = null;
			 ResultSet rs = null;
			 prep = conn.prepareStatement("SELECT LOGIN_ATTEMPTS FROM SUPERUSER WHERE USERNAME=?");
			 prep.setString(1, username);
			 rs = prep.executeQuery();
			 if(rs.next()){
				 int attempts = rs.getInt("LOGIN_ATTEMPTS");
				 return attempts;
			 }
			 else{
				 return 10;
			 }
		 }
		 catch(Exception e){
			 e.printStackTrace();
			 return 10;
		 }
	 }
	 
	 public static int getSuperIsOTP(String username){
		 try{
			 Connection conn = DBManager.getConnection();
			 PreparedStatement prep = null;
			 ResultSet rs = null;
			 prep = conn.prepareStatement("SELECT IS_OTP FROM SUPERUSER WHERE USERNAME=?");
			 prep.setString(1, username);
			 rs = prep.executeQuery();
			 if(rs.next()){
				 int isOtp = rs.getInt("IS_OTP");
				 return isOtp;
			 }
			 else{
				 return 10;
			 }
		 }
		 catch(Exception e){
			 e.printStackTrace();
			 return 10;
		 }
	 }
	 
	 
	 public static boolean setSuperLoginAttempts(String username, int attempts){
		 try{
			 Connection conn = DBManager.getConnection();
			 PreparedStatement prep = null;
			 prep = conn.prepareStatement("UPDATE SUPERUSER SET LOGIN_ATTEMPTS=? WHERE USERNAME=?");
			 prep.setInt(1, attempts);
			 prep.setString(2, username);
			 prep.executeUpdate();
			 return true;
			 //System.out.println("Succeeded!");
		 }
		 catch(Exception e){
			 e.printStackTrace();
		 }
		 return false;
	 }
	 
	 
	 public static User passwordAuthentcateSuper(String username, String password) {
		 User user = null;	
		 PreparedStatement prep = null;
			ResultSet rs = null;
			Connection conn = DBManager.getConnection();
			try {
				
				prep = conn.prepareCall("select SALT from SUPERUSER where USERNAME=?");
				prep.setString(1, username);
				rs = prep.executeQuery();
				byte[] salt=new byte[4];
				if(rs.next()){
					String saltHextString = rs.getString("SALT");
					salt = MgrBytes.hexStringToBytes(saltHextString);
				}
				prep.close();
				prep=null;
				rs =null;
				
				prep = conn.prepareCall("select * from SUPERUSER where USERNAME = ? and PASSWORD = ?");
				prep.setString(1, username);
				String saltPasswd = MgrHash.generateStringdHash(
						MgrBytes.combineByteArray(salt,password.getBytes()));
				//System.out.println(MgrBytes.bytesToHexString(salt));
				//System.out.println(password);
				//System.out.println(saltPasswd);
				prep.setString(2, saltPasswd);
				rs = prep.executeQuery();
				if(rs.next()){
					user = new Voter();
					user.setUserId(rs.getString("USERNAME"));
					user.setIsOtp(rs.getInt("IS_OTP"));
					user.setLoginAttempts(rs.getInt("LOGIN_ATTEMPTS"));
					user.setEmail(rs.getString("EMAIL"));
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				if(conn != null){
					try {
						conn.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				if(prep != null){
					try {
						prep.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			
			return user;
			
		}
	
}
