package com.csci6545.evoting.dl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.csci6545.evoting.be.BcBallotFactor;

import Decoder.BASE64Encoder;

public class MgrBlindBallot {
	public static boolean insertBlindBallot(byte [] blindBallot, String username, 
			long electionId, byte[] voterSignature){
		int ballotInserted = 0;
		Connection conn = DBManager.getConnection();
		PreparedStatement prep = null;
		BASE64Encoder encoder = new BASE64Encoder();
		String base64BlindBallot = encoder.encodeBuffer(blindBallot);
		String base64VoterSig = encoder.encodeBuffer(voterSignature);
		try {
			long uId = MgrUser.getIDOfUser(username);
			prep = conn.prepareStatement("insert into BLIND_BALLOT(BLIND_BALLOT, ELECTION_ID, USER_ID, VOTER_SIG) "
			 		+ "values(?, ?, ?, ?)");
			prep.setString(1, base64BlindBallot);
			prep.setLong(2, electionId);
			prep.setLong(3,uId);
			prep.setString(4, base64VoterSig);
			ballotInserted = prep.executeUpdate();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(ballotInserted == 1)
		{		
			return true;
		}
		else
		{
			return false;
		}
	 }
	public static boolean isVoterVoted(long electionId, String username) 
			{
		Connection conn = DBManager.getConnection();
		PreparedStatement prep = null;
		ResultSet rs = null;
		BASE64Encoder encoder = new BASE64Encoder();
		try {
			long uId = MgrUser.getIDOfUser(username);
			prep = conn.prepareStatement("SELECT * FROM BLIND_BALLOT "
						+ "WHERE ELECTION_ID=? and USER_ID = ?");
			prep.setLong(1, electionId);
			prep.setLong(2, uId);
			rs = prep.executeQuery();
			while(rs.next()){
				if(rs.getInt("VOTED") == 1)
					return true;
			}
			
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return false;

	}
	
	public static boolean markVoterasVoted(String username, byte [] signature){
		 try{	
			 Connection conn = DBManager.getConnection();
			 PreparedStatement prep = null;
			 BASE64Encoder encoder = new BASE64Encoder();
			 String base64VoterSig = encoder.encodeBuffer(signature);
			 long uId = MgrUser.getIDOfUser(username);
			 prep = conn.prepareStatement("UPDATE BLIND_BALLOT SET VOTED=1 WHERE USER_ID = ? and VOTER_SIG = ?");
			 prep.setLong(1, uId);
			 prep.setString(2, base64VoterSig);
			 prep.executeUpdate();
			 return true;
			 //System.out.println("Succeeded!");
		 }
		 catch(Exception e){
			 e.printStackTrace();
		 }
		 return false;
	 }
}
