package com.csci6545.evoting.dl;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;


public class LoadConfig {
	public static String SERVER_IP_ADDRESS;
	public static int SERVER_PORT;
	public static String PATH_TO_TRUSTSTORE;
	public static String PATH_TO_KEYSTORE;
	public static String PWD_OF_KEYSTORE;
	public static String PWD_OF_TRUSTSTORE;
	public static String MYSQL_USERNAME;
	public static String MYSQL_PASSWORD;
	public static String MYSQL_HOST;
	public static String COUNTER_IP_ADDRESS;
	public static int COUNTER_PORT;
	public static int WAITING_COUNTER_PORT;
	public static String SERVER_ALIAS;
	public static String COUNTER_ALIAS;
	public static String DB_NAME;
	public static void loadServerDBConfig()
	{
		try 
		{						 
			File xml = new File("./resources/config/server_db_config.xml");
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.parse(xml);

			NodeList nodeList = doc.getElementsByTagName("MYSQL");
			Node node = nodeList.item(0);				
			Element element = (Element) node;
			MYSQL_USERNAME = element.getElementsByTagName("username").item(0).getTextContent();
			MYSQL_PASSWORD = element.getElementsByTagName("password").item(0).getTextContent();
			MYSQL_HOST = element.getElementsByTagName("host").item(0).getTextContent();
			DB_NAME = element.getElementsByTagName("dbname").item(0).getTextContent();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}

	public static void loadCounterDBConfig()
	{
		try 
		{						 
			File xml = new File("./resources/config/counter_db_config.xml");
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.parse(xml);

			NodeList nodeList = doc.getElementsByTagName("MYSQL");
			Node node = nodeList.item(0);				
			Element element = (Element) node;
			MYSQL_USERNAME = element.getElementsByTagName("username").item(0).getTextContent();
			MYSQL_PASSWORD = element.getElementsByTagName("password").item(0).getTextContent();
			MYSQL_HOST = element.getElementsByTagName("host").item(0).getTextContent();
			DB_NAME = element.getElementsByTagName("dbname").item(0).getTextContent();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}

	public static void loadServerConfig()
	{
		try 
		{			 
			File xml = new File("./resources/config/server_config.xml");
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.parse(xml);
				 
			NodeList nodeList = doc.getElementsByTagName("SERVER");
			Node node = nodeList.item(0);				
			Element element = (Element) node;
			PATH_TO_KEYSTORE = element.getElementsByTagName("path_to_keystore").item(0).getTextContent();
			PWD_OF_KEYSTORE = element.getElementsByTagName("password_keystore").item(0).getTextContent();
			SERVER_PORT = Integer.parseInt(element.getElementsByTagName("listening_port").item(0).getTextContent());
			WAITING_COUNTER_PORT = Integer.parseInt(element.getElementsByTagName("listening_counter_port").item(0).getTextContent());
			PATH_TO_TRUSTSTORE = (element.getElementsByTagName("path_to_truststore").item(0).getTextContent());
			PWD_OF_TRUSTSTORE = element.getElementsByTagName("password_truststore").item(0).getTextContent();
			COUNTER_ALIAS = element.getElementsByTagName("counter_alias").item(0).getTextContent();
			SERVER_ALIAS = element.getElementsByTagName("server_alias").item(0).getTextContent();
			COUNTER_IP_ADDRESS = element.getElementsByTagName("counter_ip").item(0).getTextContent();
			COUNTER_PORT = Integer.parseInt(element.getElementsByTagName("counter_port").item(0).getTextContent());
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	public static void loadCounterConfig()
	{
		try 
		{			 
			File xml = new File("./resources/config/counter_config.xml");
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.parse(xml);
				 
			NodeList nodeList = doc.getElementsByTagName("COUNTER");
			Node node = nodeList.item(0);				
			Element element = (Element) node;
			PATH_TO_KEYSTORE = element.getElementsByTagName("path_to_keystore").item(0).getTextContent();
			PWD_OF_KEYSTORE = element.getElementsByTagName("password_keystore").item(0).getTextContent();
			COUNTER_PORT = Integer.parseInt(element.getElementsByTagName("counter_port").item(0).getTextContent());
			COUNTER_ALIAS = element.getElementsByTagName("counter_alias").item(0).getTextContent();
			PATH_TO_TRUSTSTORE = (element.getElementsByTagName("path_to_truststore").item(0).getTextContent());
			PWD_OF_TRUSTSTORE = element.getElementsByTagName("password_truststore").item(0).getTextContent();
			SERVER_ALIAS = element.getElementsByTagName("server_alias").item(0).getTextContent();
			SERVER_IP_ADDRESS = element.getElementsByTagName("server_ip").item(0).getTextContent();
			SERVER_PORT = Integer.parseInt(element.getElementsByTagName("server_port").item(0).getTextContent());
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	public static void loadClientConfig()
	{
		try 
		{			 
			File xml = new File("./resources/config/client_config.xml");
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.parse(xml);
				 
			NodeList nodeList = doc.getElementsByTagName("CLIENT");
	
			Node node = nodeList.item(0);				
			Element element = (Element) node;			
			PATH_TO_TRUSTSTORE = element.getElementsByTagName("path_to_truststore").item(0).getTextContent();
			PWD_OF_TRUSTSTORE = element.getElementsByTagName("password").item(0).getTextContent();
			SERVER_IP_ADDRESS = element.getElementsByTagName("server_ip").item(0).getTextContent();
			SERVER_PORT = Integer.parseInt(element.getElementsByTagName("server_port").item(0).getTextContent());
			SERVER_ALIAS = element.getElementsByTagName("server_alias").item(0).getTextContent();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
}
