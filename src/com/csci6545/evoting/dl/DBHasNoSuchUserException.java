package com.csci6545.evoting.dl;

import com.mysql.jdbc.exceptions.MySQLDataException;

public class DBHasNoSuchUserException extends MySQLDataException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DBHasNoSuchUserException(String reason){
		super(reason);
	}

}
