package com.csci6545.evoting.dl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBManager {
	
	private static final String URL = "jdbc:mysql://"+LoadConfig.MYSQL_HOST+":3306/EVOTING"; 
	//private static final String USERNAME = "evs";
	//private static final String PASSWORD = "gwucsci6545";
	//private static final String USERNAME = "root";
	//private static final String PASSWORD = "";
	//private static final String USERNAME = "root";
	//private static final String PASSWORD = "root";
	private static final String USERNAME = LoadConfig.MYSQL_USERNAME;
	private static final String PASSWORD = LoadConfig.MYSQL_PASSWORD;
	private static Connection conn = null;
	
	public static Connection getConnection()
	{
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(URL, USERNAME, PASSWORD);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return conn;
	}
	

	public void closeConnection()
	{
		if(conn != null)
		{
			try {
				conn.close();
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
		}
	}
}