/* Autheor: Alemberhan Getahun 
 * Date: 11/01/2013*/
package com.csci6545.evoting.dl;

import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.InputMismatchException;

import javax.swing.JList;

import com.csci6545.evoting.be.Counter;
import com.csci6545.evoting.be.Election;
import com.csci6545.evoting.be.Title;
import com.csci6545.evoting.be.User;
import com.csci6545.evoting.be.Voter;
import com.csci6545.evoting.bl.MgrHash;

public class MgrElection {
	private static final String DB_TABLE_ELECTION = "ELECTION";
	private static final String DB_TABLE_CANDIDATES_IN_ELECTIONS =
			"CANDIDATE_ELECTION";
	private static final String DB_TABLE_USERS = "USER";
	private static final String DB_COLUMN_USERNAME = 
			DB_TABLE_USERS+".USERNAME";

	/**
	 * @author Yunfan Zhang
	 * @param election an Election Object
	 * @return A Map contains the candidates of the input election
	 * @throws SQLException
	 */
	public static Map<String,String> getElectionCandidates(long electionID) 
			throws SQLException{
		Connection conn = DBManager.getConnection();
		PreparedStatement prep = null;
		ResultSet rs = null;
		prep = conn.prepareStatement("select U.ID, U.FIRST_NAME, U.LAST_NAME from USER as U "
				+ "inner join (ELECTION as E,CANDIDATE_ELECTION as CE) "
				+ "on (U.ID=CE.CANDIDATE_ID and E.ID=CE.ELECTION_ID) "
				+ "where E.ID=?");
		prep.setLong(1, electionID);
		rs = prep.executeQuery();
		List<String> candidatesList = new ArrayList<String> ();
		List<String> usernameList = new ArrayList<String>();
		while(rs.next()){
			usernameList.add(rs.getString("U.ID"));
			candidatesList.add(rs.getString("U.FIRST_NAME") +" "+ rs.getString("U.LAST_NAME"));
		}
		conn.close();
		//Integer sizeLength =  candidatesList.size();
		//Integer indexStringLength = sizeLength.toString().length();
		int index =0;
		Map<String,String> candidatesMap = new HashMap<String,String> ();
		for(String e:candidatesList){
			/*String key = String.format("%"+indexStringLength.toString()+"d", 
					index).replace(" ", "0");*/
			candidatesMap.put(usernameList.get(index),e);
			index++;
		}
		return candidatesMap;
	}
	/**
	 * @author Yunfan Zhang
	 * @param election an Election Object
	 * @return true if the CANDIDATE_ELECTION table of the database 
	 * @throws SQLException 
	 */
	private static boolean checkCandidateElection(Election election) 
			throws SQLException{
		if(election.getCandidate().size()<1){
			return true;
		}
		long eid = election.getId();
		Map<String,String> candidateMap = election.getCandidate();
		Connection conn = DBManager.getConnection();
		PreparedStatement prep = null;
		ResultSet rs = null;
		for(String key:candidateMap.keySet()){
			if(!MgrUser.existsUser(candidateMap.get(key))){
				System.err.println("The candidate does not "
						+ "exist in the USER TABLE");
				throw new DBHasNoSuchUserException("No such "
						+ "user:" +candidateMap.get(key)
						+" in the USER table!");
			}
			long currentCandidateID = 
					MgrUser.getIDOfUser(candidateMap.get(key));
			if(!MgrCandidate.existsCandidate(currentCandidateID)){
				System.err.println("The candidate does not "
						+ "exist in the CANDIDATE TABLE");
				throw new DBHasNoSuchCandidateException("No such candidate "
						+ "with the USER_ID:"+currentCandidateID
						+ " in the CANDIDATE table!");
			}
			prep = conn.prepareStatement("select * from CANDIDATE_ELECTION "
					+ "where CANDIDATE_ID=? and ELECTION_ID=?");
			prep.setLong(1, currentCandidateID);
			prep.setLong(2, election.getId());
			rs = prep.executeQuery();
			if(!rs.next()){
				conn.close();
				return false;
			}
			prep=null;
		}
		conn.close();
		return true;
	}
	/**
	 * 
	 * @param election
	 * @return
	 * @throws DBHasNoSuchCandidateException 
	 */
	private static boolean insertCandidateElection(Election election) 
			throws DBHasNoSuchCandidateException{
		if(election.getCandidate().size() <1){
			throw new InputMismatchException("No candidates "
					+ "in this election!");
		}
		Connection conn = DBManager.getConnection();
		PreparedStatement prep = null;
		ResultSet rs = null;
		//prep = conn.prepareStatement("insert into ");
		return false;
	}
	
	public static List<Election> getOngoingElections() {
		List<Election> electionList = new ArrayList<Election>();
		Connection conn = DBManager.getConnection();
		PreparedStatement prep = null;
		ResultSet rs = null;
		Map <String,String> unknownCandidate = new HashMap<String,String>();
		unknownCandidate.put("unknown", "unknown");
		Counter unknownCounter = new Counter();
		unknownCounter.setUserId("unknown");
		unknownCounter.setfName("unknown");
		unknownCounter.setlName("unknown");
		try {
			prep = conn.prepareStatement("select * from ELECTION WHERE END_DATE >= NOW();");
			rs = prep.executeQuery();
			while(rs.next()){
				Election election = new Election();
				election.setId(rs.getLong("ID"));
				election.setName(rs.getString("ELECTION_NAME"));
				election.setStart(rs.getTimestamp("START_DATE"));
				election.setEnd(rs.getTimestamp("END_DATE"));
				Map<String,String> candidates = getElectionCandidates(election.getId());
				if(candidates.isEmpty())
				{
					election.setCandidate(unknownCandidate);
				}
				else
				{
					election.setCandidate(candidates);
				}	
				election.setCounter(unknownCounter);
				electionList.add(election);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			if(conn != null){
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(prep != null){
				try {
					prep.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		return electionList;
	}

	public static List<Election> getAllElections() {
		List<Election> electionList = new ArrayList<Election>();
		Connection conn = DBManager.getConnection();
		PreparedStatement prep = null;
		ResultSet rs = null;
		Map <String,String> unknownCandidate = new HashMap<String,String>();
		unknownCandidate.put("unknown", "unknown");
		Counter unknownCounter = new Counter();
		unknownCounter.setUserId("unknown");
		unknownCounter.setfName("unknown");
		unknownCounter.setlName("unknown");
		try {
			prep = conn.prepareStatement("select * from ELECTION");
			rs = prep.executeQuery();
			while(rs.next()){
				Election election = new Election();
				election.setId(rs.getLong("ID"));
				election.setName(rs.getString("ELECTION_NAME"));
				election.setStart(rs.getTimestamp("START_DATE"));
				election.setEnd(rs.getTimestamp("END_DATE"));
				Map<String,String> candidates = getElectionCandidates(election.getId());
				if(candidates.isEmpty())
				{
					election.setCandidate(unknownCandidate);
				}
				else
				{
					election.setCandidate(candidates);
				}	
				election.setCounter(unknownCounter);
				electionList.add(election);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			if(conn != null){
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(prep != null){
				try {
					prep.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		return electionList;
	}
	
	public static List<Integer> getElectionIdsForVoter(Voter voter) {
		List<Integer> ids = new ArrayList<Integer>();
		PreparedStatement prep = null;
		ResultSet rs = null;
		Connection conn = DBManager.getConnection();
		
		try {
			prep = conn.prepareStatement("select id from Election");
			rs = prep.executeQuery();
			while(rs.next()){
				ids.add(rs.getInt(1));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			if(conn != null){
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(prep != null){
				try {
					prep.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return ids;
	}
	
	// Poom	
	
	public static List<Election> getRegisteredElections(String userName, String titleName) {
		List<Election> electionList = new ArrayList<Election>();
		Connection conn = DBManager.getConnection();
		PreparedStatement prep = null;
		ResultSet rs = null;
	
		Map <String,String> unknownCandidate = new HashMap<String,String>();
		unknownCandidate.put("unknown", "unknown");
		Counter unknownCounter = new Counter();
		unknownCounter.setUserId("unknown");
		unknownCounter.setfName("unknown");
		unknownCounter.setlName("unknown");
		try {
			long userId = MgrUser.getIDOfUser(userName);
			int titleId = MgrTitle.getIDByTitleName(titleName);
			prep = conn.prepareStatement("Select election.ID, ELECTION_NAME,START_DATE, END_DATE " +
					"from election, user_election " +
					"where election.ID = user_election.election_id and END_DATE >= NOW()" +
					"and user_election.user_id = ? and election.ID not in" +
					"(Select election_id from Blind_ballot where user_id = ? and voted = 1) ");
			prep.setLong(1, userId);
			prep.setLong(2, userId);
			rs = prep.executeQuery();
			while(rs.next()){
				Election election = new Election();
				election.setId(rs.getLong("ID"));
				election.setName(rs.getString("ELECTION_NAME"));
				election.setStart(rs.getTimestamp("START_DATE"));
				election.setEnd(rs.getTimestamp("END_DATE"));
				Map<String,String> candidates = getElectionCandidates(election.getId());
				if(candidates.isEmpty())
				{
					election.setCandidate(unknownCandidate);
				}
				else
				{
					election.setCandidate(candidates);
				}	
				election.setCounter(unknownCounter);
				electionList.add(election);
			}
			conn.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			if(conn != null){
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(prep != null){
				try {
					prep.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return electionList;
	}
	
	public static List<Election> getAllRegisteredElections(String userName, String titleName) {
		List<Election> electionList = new ArrayList<Election>();
		Connection conn = DBManager.getConnection();
		PreparedStatement prep = null;
		ResultSet rs = null;
	
		Map <String,String> unknownCandidate = new HashMap<String,String>();
		unknownCandidate.put("unknown", "unknown");
		Counter unknownCounter = new Counter();
		unknownCounter.setUserId("unknown");
		unknownCounter.setfName("unknown");
		unknownCounter.setlName("unknown");
		try {
			long userId = MgrUser.getIDOfUser(userName);
			int titleId = MgrTitle.getIDByTitleName(titleName);
			prep = conn.prepareStatement("Select election.ID, ELECTION_NAME,START_DATE, END_DATE " +
					"from election, user_election " +
					"where election.ID = user_election.election_id " +
					"and user_election.user_id = ? and election.ID in " +
					"(Select election_id from Blind_ballot where user_id = ? and voted = 1) ");
			prep.setLong(1, userId);
			prep.setLong(2, userId);
			rs = prep.executeQuery();
			while(rs.next()){
				Election election = new Election();
				election.setId(rs.getLong("ID"));
				election.setName(rs.getString("ELECTION_NAME"));
				election.setStart(rs.getTimestamp("START_DATE"));
				election.setEnd(rs.getTimestamp("END_DATE"));
				Map<String,String> candidates = getElectionCandidates(election.getId());
				if(candidates.isEmpty())
				{
					election.setCandidate(unknownCandidate);
				}
				else
				{
					election.setCandidate(candidates);
				}	
				election.setCounter(unknownCounter);
				electionList.add(election);
			}
			conn.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			if(conn != null){
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(prep != null){
				try {
					prep.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return electionList;
	}
	
	public static long getElectionId(String electionName) {
		long eId = 0;
		PreparedStatement prep = null;
		ResultSet rs = null;
		Connection conn = DBManager.getConnection();
		try {
			prep = conn.prepareStatement("select id from Election where ELECTION_NAME = ?");
			prep.setString(1, electionName);
			rs = prep.executeQuery();
			while(rs.next()){
				eId = rs.getInt(1);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return eId;
	}
	
	// Poom
	static boolean setCandidate(Map<String,String> candidate, String electionName){
		int electionInserted = 0;
		Connection conn = DBManager.getConnection();
		long candidateId= 0;
		PreparedStatement prep = null;
		long eId = getElectionId(electionName);
		try {
			Iterator it = candidate.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry pairs = (Map.Entry)it.next();
				candidateId = MgrUser.getIDOfUser((String) pairs.getValue());
				prep = conn.prepareStatement("insert into CANDIDATE_ELECTION(ELECTION_ID, CANDIDATE_ID) "
				 		+ "values(?, ?)");
				prep.setLong(1, eId);
				prep.setLong(2, candidateId);
				electionInserted = prep.executeUpdate();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(electionInserted == 1)
		{
			return true;
		}
		else
		{
			return false;
		}
	 }
	
	static boolean setTitleElection(String electionName, List<Title> titleName) throws SQLException{
		int electionInserted = 0;
		Connection conn = DBManager.getConnection();
		PreparedStatement prep = null;
		long eId = MgrElection.getElectionId(electionName);
		try {
			for(int i = 0;i<titleName.size();i++)
			{
				int titleId = MgrTitle.getIDByTitleName(titleName.get(i).getTitleName());
				prep = conn.prepareStatement("insert into ELECTION_TITLE(ELECTION_ID, TITLE_ID) "
				 		+ "values(?, ?)");
				prep.setLong(1, eId);
				prep.setLong(2, titleId);
				electionInserted += prep.executeUpdate();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		conn.close();
		if(electionInserted == titleName.size())
		{
			
			return true;
		}
		else
		{
			return false;
		}
	 }
	
	static boolean setExtendedPermissionAllow(String electionName, List<User> user) throws SQLException{
		int electionInserted = 0;
		Connection conn = DBManager.getConnection();
		PreparedStatement prep = null;
		long eId = MgrElection.getElectionId(electionName);
		try {
			for(int i = 0;i<user.size();i++)
			{
				if(user.get(i).getUserId().equals("unknown"))
				{
					electionInserted = user.size();
					break;
				}
				long userId = MgrUser.getIDOfUser(user.get(i).getUserId());
				prep = conn.prepareStatement("insert into ELECTION_EXTENDED_PERMISSION(ELECTION_ID, USER_ID, IS_ALLOWED) "
				 		+ "values(?, ?, 1)");
				prep.setLong(1, eId);
				prep.setLong(2, userId);
				electionInserted += prep.executeUpdate();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		conn.close();
		if(electionInserted == user.size())
		{
			
			return true;
		}
		else
		{
			return false;
		}
	 }
	
	static boolean setExtendedPermissionDeny(String electionName, List<User> user) throws SQLException{
		int electionInserted = 0;
		Connection conn = DBManager.getConnection();
		PreparedStatement prep = null;
		long eId = MgrElection.getElectionId(electionName);
		try {
			for(int i = 0;i<user.size();i++)
			{
				long userId = MgrUser.getIDOfUser(user.get(i).getUserId());
				prep = conn.prepareStatement("insert into ELECTION_EXTENDED_PERMISSION(ELECTION_ID, USER_ID, IS_ALLOWED) "
				 		+ "values(?, ?, 0)");
				prep.setLong(1, eId);
				prep.setLong(2, userId);
				electionInserted += prep.executeUpdate();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		conn.close();
		if(electionInserted == user.size())
		{
			
			return true;
		}
		else
		{
			return false;
		}
	 }
	
	// Poom
	static boolean existsElection(String electionName) 
			throws SQLException{
		Connection conn = DBManager.getConnection();
		PreparedStatement prep = null;
		ResultSet rs = null;
	    prep = conn.prepareStatement("SELECT * FROM ELECTION "
					+ "WHERE ELECTION_NAME=?");
		prep.setString(1, electionName);
		rs = prep.executeQuery();
		if(rs.next()){
			System.err.println("The election is already existed");
			return true;
		}
		return false;

	}
	
	
	
	// Poom
	public static boolean createElection(Election election, List<Title> title
			, List<User> allowedUser, List<User> diniedUser) throws SQLException{
		int electionInserted = 0;
		Connection conn = DBManager.getConnection();
		PreparedStatement prep = null;
		try {
			if(existsElection(election.getName())){
				return false;
			}
			prep = conn.prepareStatement("insert into ELECTION(ELECTION_NAME, START_DATE, END_DATE) "
			 		+ "values(?, ?, ?)");
			prep.setString(1, election.getName());
			prep.setDate(2, new java.sql.Date(election.getStart().getTime()));
			prep.setDate(3, new java.sql.Date(election.getEnd().getTime()));
			electionInserted = prep.executeUpdate();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(electionInserted == 1 && setCandidate(election.getCandidate(),election.getName()) 
				&& setTitleElection(election.getName(), title) 
				&& setExtendedPermissionAllow(election.getName(), allowedUser)
				&& setExtendedPermissionDeny(election.getName(), diniedUser))
		{		
			return true;
		}
		else
		{
			return false;
		}
	 }
	 public static Boolean checkUserEligibility(String username, long eletcionID, String titleName) {
		 User user = null;
			PreparedStatement prep = null;
			ResultSet rs = null;
			Connection conn = DBManager.getConnection();
			try {
				long userId = MgrUser.getIDOfUser(username);
				int titleId = MgrTitle.getIDByTitleName(titleName);
				prep = conn.prepareCall("Select * from " +
						"(Select ID, ELECTION_NAME,START_DATE, END_DATE " +
						"from election, election_title " +
						"where election.ID = election_title.election_id " +
						"and title_id = ? and ID = ? " +
						"union select elec.ID, ELECTION_NAME,START_DATE, END_DATE " +
						"from election elec, election_extended_permission elec_p " +
						"where elec.ID = elec_p.election_id " +
						"and is_allowed = 1 and elec_p.user_id = ? and election_id = ?) tt " +
						"where tt.ID not in " +
						"(select tmp2.election_id " +
						"from election tmp1, election_extended_permission tmp2 " +
						"where tmp1.ID = tmp2.election_id " +
						"and is_allowed = 0 and tmp2.user_id = ? and election_id = ?);");
				prep.setLong(1,titleId);
				prep.setLong(2,eletcionID);
				prep.setLong(3,userId);
				prep.setLong(4,eletcionID);
				prep.setLong(5,userId);
				prep.setLong(6,eletcionID);
				rs = prep.executeQuery();
				if(rs.next()){
					//conn.close();
					//prep.close();
					return true;				
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			finally {
				if(conn != null){
					try {
						conn.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				if(prep != null){
					try {
						prep.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			return false;
			
		}

}
