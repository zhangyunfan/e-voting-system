package com.csci6545.evoting.dl;

import java.security.PublicKey;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import Decoder.BASE64Decoder;

import com.csci6545.evoting.bl.MgrIntegrity;



public class MgrSupervisor {
	public static PublicKey queryVoterPublicKey(String username)
	{
		PublicKey publicKey = null;
		BASE64Decoder decoder = new BASE64Decoder();
		PreparedStatement prep = null;
		ResultSet rs = null;
		Connection conn = DBManager.getConnection();
		try {
			prep = conn.prepareCall("select * from USER where USERNAME = ?");
			prep.setString(1, username);
			rs = prep.executeQuery();
			if(rs.next()){
				publicKey = MgrIntegrity.byteToPublicKey(decoder.decodeBuffer(rs.getString("PUBLIC_KEY")));		 
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return publicKey;
	}
}
