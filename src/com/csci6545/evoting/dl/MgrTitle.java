package com.csci6545.evoting.dl;

import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.csci6545.evoting.be.Title;
import com.csci6545.evoting.be.User;
import com.csci6545.evoting.bl.MgrHash;

public class MgrTitle {
	private static final int TITLE_EXISTS = -1;

	public static boolean existsTitle(String titleName) {
		Connection conn = DBManager.getConnection();
		PreparedStatement prep = null;
		ResultSet rs = null;
		try {
			prep = conn.prepareStatement("SELECT * FROM TITLE "
					+ "WHERE TITLE_NAME=?");
			prep.setString(1, titleName);
			rs = prep.executeQuery();
			if (rs.next()) {
				System.err.println("The title already exists");
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			if (prep != null) {
				try {
					prep.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return false;

	}

	public static Boolean createTitle(String titleName, int permission) {
		int titleInserted = 0;
		Connection conn = DBManager.getConnection();
		PreparedStatement prep = null;
		byte[] salt;
		try {
			if (existsTitle(titleName)) {
				return false;
			}
			prep = conn
					.prepareStatement("insert into TITLE(TITLE_NAME, PERMISSION) "
							+ "values(?, ?)");
			prep.setString(1, titleName);
			prep.setInt(2, permission);
			titleInserted = prep.executeUpdate();
			if (titleInserted == 1) {
				return true;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			if (prep != null) {
				try {
					prep.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return false;
	}

	public static String getTitleByID(int titleID) {
		Connection conn = DBManager.getConnection();
		PreparedStatement prep = null;
		ResultSet rs = null;
		try {
			prep = conn.prepareStatement("SELECT * FROM TITLE " + "WHERE ID=?");
			prep.setInt(1, titleID);
			rs = prep.executeQuery();
			if (rs.next()) {
				return rs.getString("TITLE_NAME");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			if (prep != null) {
				try {
					prep.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return null;

	}

	public static int getIDByTitleName(String titleName) {
		Connection conn = DBManager.getConnection();
		PreparedStatement prep = null;
		ResultSet rs = null;
		try {
			prep = conn.prepareStatement("SELECT * FROM TITLE "
					+ "WHERE TITLE_NAME=?");
			prep.setString(1, titleName);
			rs = prep.executeQuery();
			if (rs.next()) {
				return rs.getInt("ID");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			if (prep != null) {
				try {
					prep.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return 0;

	}

	public static List<Title> getAllTitle() throws SQLException {
		List<Title> titleList = new ArrayList<Title>();
		Connection conn = DBManager.getConnection();
		PreparedStatement prep = null;
		ResultSet rs = null;
		prep = conn.prepareStatement("SELECT * FROM TITLE ");
		rs = prep.executeQuery();
		while (rs.next()) {
			Title t = new Title();
			t.setTitleId(rs.getInt("ID"));
			t.setTitleName(rs.getString("TITLE_NAME"));
			t.setPermission(rs.getInt("PERMISSION"));
			titleList.add(t);
		}
		return titleList;
	}

	public static int getPermissionbyId(int titleID) {
		Connection conn = DBManager.getConnection();
		PreparedStatement prep = null;
		ResultSet rs = null;
		try {
			prep = conn.prepareStatement("SELECT * FROM TITLE " + "WHERE ID=?");
			prep.setInt(1, titleID);
			rs = prep.executeQuery();
			if (rs.next()) {
				return rs.getInt("PERMISSION");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			if (prep != null) {
				try {
					prep.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return 0;

	}

	public static Boolean updateTitlePermission(int permission, int titleId) {

		Connection conn = DBManager.getConnection();
		PreparedStatement prep = null;
		try {

			prep = conn
					.prepareStatement("UPDATE TITLE SET PERMISSION=? WHERE ID=?");
			prep.setInt(1, permission);
			prep.setInt(2, titleId);
			if (prep.executeUpdate() == 1) {
				return true;
			}

			// System.out.println("Succeeded!");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			if (prep != null) {
				try {
					prep.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return false;
	}
}
