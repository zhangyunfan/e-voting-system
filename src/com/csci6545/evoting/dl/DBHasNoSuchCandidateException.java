package com.csci6545.evoting.dl;

import com.mysql.jdbc.exceptions.MySQLDataException;

public class DBHasNoSuchCandidateException extends MySQLDataException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DBHasNoSuchCandidateException(String reason){
		super(reason);
	}
}
