package com.csci6545.evoting.dl;

import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

import com.csci6545.evoting.be.Candidate;
import com.csci6545.evoting.be.User;
import com.csci6545.evoting.be.Voter;
import com.csci6545.evoting.bl.MgrConfidentiality;
import com.csci6545.evoting.bl.MgrHash;
/**
 * 
 * @author Yunfan Zhang
 *
 */
public class MgrCandidate{
	private static final int CANDIDATE_EXISTS =-2;
	/**
	 * @author Yunfan Zhang
	 * @param candidateID the ID of candidate which may be stored in CANDIDATE table of the DB
	 * @return true if the database has already have the same candidateID else false
	 * @throws SQLException
	 */
	public static boolean existsCandidate(long candidateID) 
			throws SQLException{
		Connection conn = DBManager.getConnection();
		PreparedStatement prep = null;
		ResultSet rs = null;
	    prep = conn.prepareStatement("SELECT * FROM CANDIDATE "
					+ "WHERE USER_ID=?");
		prep.setLong(1, candidateID);
		rs = prep.executeQuery();
		if(rs.next()){
			System.err.println("The candidate has already been existing");
			
			conn.close();
			return true;
		}
		conn.close();
		return false;

	}
	/**
	 * @author Yunfan Zhang
	 * @param user the User object
	 * @return -2 if candidate exists, 0 if creation failed, 1 if the creation success
	 * @throws SQLException 
	 * @throws DBHasNoSuchUserException if the candidate has no been inserted as a user
	 */
	 public static int createCandidate(Candidate candidate) 
			 throws DBHasNoSuchUserException, SQLException{
		 int candidateInserted = 0;
		 Connection conn = DBManager.getConnection();
		 PreparedStatement prep = null;
		 ResultSet rs = null;
		 if(!MgrUser.existsUser(candidate.getUserId())){
				 throw new DBHasNoSuchUserException("The candidate is not "
				 		+ "in the USER table");
		}
		 else if(existsCandidate(candidate.getId())){
			 return CANDIDATE_EXISTS;
		 }
			prep = conn.prepareStatement("insert into CANDIDATE "
					+ "(USER_ID) values(?)");
			prep.setLong(1, candidate.getId());
			candidateInserted = prep.executeUpdate();
			conn.close();
		 return candidateInserted;
	 }
	 
	 /*public static void main(String[] args) 
			 throws NoSuchAlgorithmException, SQLException, ParseException{
		 Candidate user = new Candidate();
		 String dob = "7/30/1863";
		 SimpleDateFormat format =  new SimpleDateFormat("MM/dd/yyyy", Locale.US);		 
		 user.setfName("Henry");
		 user.setlName("Henry");
		 user.setTitle("Candidate");
		 user.setPassword("12345678");
		 user.setUserId("ford");
	     user.setDob(format.parse(dob));

		 System.out.println(MgrUser.createUser(user));
		 long idOfCandidate = MgrUser.getIDOfUser(user.getUserId());
		 user.setId(idOfCandidate);
		 System.out.println(createCandidate(user));
	 }*/
}
