package com.csci6545.evoting.dl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import Decoder.BASE64Decoder;
import Decoder.BASE64Encoder;

import com.csci6545.evoting.be.BcBallotFactor;
import com.csci6545.evoting.be.Election;
import com.csci6545.evoting.bl.MgrBytes;
import com.csci6545.evoting.bl.MgrHash;

public class MgrResult {

	// Poom
	public static boolean insertBcBallot(byte[] bcBallot, long electionId,
			byte[] supSignature, int entry) {
		int ballotInserted = 0;
		Connection conn = DBManager.getConnection();
		PreparedStatement prep = null;
		BASE64Encoder encoder = new BASE64Encoder();
		String base64BcBallot = encoder.encodeBuffer(bcBallot);
		String base64SupSig = encoder.encodeBuffer(supSignature);
		try {
			prep = conn
					.prepareStatement("insert into EVOTING.BALLOT(BC_BALLOT, ELECTION_ID, COUNTER_ID, SUP_SIG, ENTRY) "
							+ "values(?, ?, ?, ?, ?)");
			prep.setString(1, base64BcBallot);
			prep.setLong(2, electionId);
			prep.setString(3, "Counter");
			prep.setString(4, base64SupSig);
			prep.setInt(5, entry);
			ballotInserted = prep.executeUpdate();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (ballotInserted == 1) {
			return true;
		} else {
			return false;
		}
	}

	public static BcBallotFactor getBcBallot(int entry) {
		Connection conn = DBManager.getConnection();
		PreparedStatement prep = null;
		ResultSet rs = null;
		BcBallotFactor bcBallot = new BcBallotFactor();
		BASE64Decoder decoder = new BASE64Decoder();
		try {
			prep = conn
					.prepareStatement("SELECT * FROM EVOTING.BALLOT WHERE ID=?");
			prep.setInt(1, entry);
			rs = prep.executeQuery();
			if (rs.next()) {
				BcBallotFactor bcFactor = new BcBallotFactor();
				String base64BcBallot = rs.getString("BC_BALLOT");
				String base64Ballot = rs.getString("BALLOT");
				String base64Key = rs.getString("BC_BALLOT_KEY");
				String base64Iv = rs.getString("BC_BALLOT_IV");
				bcFactor.setEntry(rs.getInt("ID"));
				bcFactor.setBallot("0,0".getBytes());
				bcFactor.setKey("unknown".getBytes());
				bcFactor.setIv("unknown".getBytes());
				if (base64Ballot != null) {
					bcFactor.setBallot(decoder.decodeBuffer(base64Ballot));
					bcFactor.setKey(decoder.decodeBuffer(base64Key));
					bcFactor.setIv(decoder.decodeBuffer(base64Iv));
				}
				bcFactor.setBcBallot(decoder.decodeBuffer(base64BcBallot));
				bcFactor.setElectionId(rs.getLong("ELECTION_ID"));
			}

			return bcBallot;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			if (prep != null) {
				try {
					prep.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return bcBallot;
	}

	public static List<BcBallotFactor> verifyBcBallot(Long electionId) {
		Connection conn = DBManager.getConnection();
		PreparedStatement prep = null;
		ResultSet rs = null;
		List<BcBallotFactor> bcBallot = new ArrayList<BcBallotFactor>();
		BASE64Decoder decoder = new BASE64Decoder();
		try {
			prep = conn
					.prepareStatement("SELECT * FROM EVOTING.BALLOT WHERE ELECTION_ID=?");
			prep.setLong(1, electionId);
			rs = prep.executeQuery();
			int i=0;
			while (rs.next()) {
				BcBallotFactor bcFactor = new BcBallotFactor();
				String base64BcBallot = rs.getString("BC_BALLOT");
				String base64Ballot = rs.getString("BALLOT");
				String base64Key = rs.getString("BC_BALLOT_KEY");
				String base64Iv = rs.getString("BC_BALLOT_IV");
				String base64SupSig = rs.getString("SUP_SIG");
				bcFactor.setEntry(rs.getInt("ENTRY"));
				bcFactor.setBallot("0,0".getBytes());
				bcFactor.setKey("unknown".getBytes());
				bcFactor.setIv("unknown".getBytes());
				if (base64Ballot != null) {
					bcFactor.setBallot(decoder.decodeBuffer(base64Ballot));
					bcFactor.setKey(decoder.decodeBuffer(base64Key));
					bcFactor.setIv(decoder.decodeBuffer(base64Iv));
				}
				bcFactor.setBcBallot(decoder.decodeBuffer(base64BcBallot));
				bcFactor.setElectionId(rs.getLong("ELECTION_ID"));
				bcFactor.setSupSignature(decoder.decodeBuffer(base64SupSig));
				bcBallot.add(bcFactor);
				System.out.println("i   "+i);
			}
			conn.close();
			return bcBallot;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			if (prep != null) {
				try {
					prep.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return bcBallot;
	}

	public static List<BcBallotFactor> checkResult(Long electionId) {
		Connection conn = DBManager.getConnection();
		PreparedStatement prep = null;
		ResultSet rs = null;
		List<BcBallotFactor> bcBallot = new ArrayList<BcBallotFactor>();
		BASE64Decoder decoder = new BASE64Decoder();
		try {
			prep = conn
					.prepareStatement("SELECT * FROM EVOTING.BALLOT WHERE ELECTION_ID=? AND BALLOT IS NOT NULL");
			prep.setLong(1, electionId);
			rs = prep.executeQuery();
			int i=0;
			while (rs.next()) {
				BcBallotFactor bcFactor = new BcBallotFactor();
				String base64BcBallot = rs.getString("BC_BALLOT");
				String base64Ballot = rs.getString("BALLOT");
				String base64Key = rs.getString("BC_BALLOT_KEY");
				String base64Iv = rs.getString("BC_BALLOT_IV");
				String base64SupSig = rs.getString("SUP_SIG");
				bcFactor.setEntry(rs.getInt("ENTRY"));
				bcFactor.setBallot("0,0".getBytes());
				bcFactor.setKey("unknown".getBytes());
				bcFactor.setIv("unknown".getBytes());
				if (base64Ballot != null) {
					bcFactor.setBallot(decoder.decodeBuffer(base64Ballot));
					bcFactor.setKey(decoder.decodeBuffer(base64Key));
					bcFactor.setIv(decoder.decodeBuffer(base64Iv));
				}
				bcFactor.setBcBallot(decoder.decodeBuffer(base64BcBallot));
				bcFactor.setElectionId(rs.getLong("ELECTION_ID"));
				bcFactor.setSupSignature(decoder.decodeBuffer(base64SupSig));
				bcBallot.add(bcFactor);
			}
			conn.close();
			return bcBallot;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			if (prep != null) {
				try {
					prep.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return bcBallot;
	}
	
	
	public static boolean setKeyForBcBallot(BcBallotFactor ballotFactor) {
		Connection conn = DBManager.getConnection();
		PreparedStatement prep = null;
		try {

			BASE64Encoder encoder = new BASE64Encoder();
			String base64Ballot = encoder
					.encodeBuffer(ballotFactor.getBallot());
			String base64Key = encoder.encodeBuffer(ballotFactor.getKey());
			String base64Iv = encoder.encodeBuffer(ballotFactor.getIv());
			String base64BcBallot = encoder.encodeBuffer(ballotFactor
					.getBcBallot());

			prep = conn
					.prepareStatement("UPDATE EVOTING.BALLOT SET BALLOT=?, BC_BALLOT_KEY = ?, "
							+ "BC_BALLOT_IV = ? WHERE ENTRY = ?");
			prep.setString(1, base64Ballot);
			prep.setString(2, base64Key);
			prep.setString(3, base64Iv);
			prep.setInt(4, ballotFactor.getEntry());
			prep.executeUpdate();
			return true;
			// System.out.println("Succeeded!");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			if (prep != null) {
				try {
					prep.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return false;
	}
	
	
	public static List<String> checkAuthorized(Long electionId) {
		Connection conn = DBManager.getConnection();
		PreparedStatement prep = null;
		ResultSet rs = null;
		List<String> all = new ArrayList<String>();
		try {
			prep = conn
					.prepareStatement("SELECT * FROM EVOTING.BLIND_BALLOT, EVOTING.USER " +
							"WHERE ELECTION_ID=? AND VOTED = 1 AND USER_ID = USER.ID");
			prep.setLong(1, electionId);
			rs = prep.executeQuery();
			int i=0;
			while (rs.next()) {
				String name = rs.getString("USERNAME");
				all.add(name);
			}
			return all;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			if (prep != null) {
				try {
					prep.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return all;
	}
}
