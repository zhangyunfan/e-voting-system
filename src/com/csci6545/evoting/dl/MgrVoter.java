package com.csci6545.evoting.dl;

import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;




import Decoder.BASE64Decoder;

import com.csci6545.evoting.be.Voter;
import com.csci6545.evoting.bl.MgrHash;
import com.csci6545.evoting.bl.MgrIntegrity;

public class MgrVoter {
	public static Voter passwordAuthentcateVoter(String username, String password) {
		Voter voter = null;
		PreparedStatement prep = null;
		ResultSet rs = null;
		Connection conn = DBManager.getConnection();
		try {
			prep = conn.prepareCall("select * from USER where USERNAME = ? and PASSWORD = ?");
			prep.setString(1, username);
			prep.setString(2, MgrHash.generateStringdHash(password.getBytes()));
			rs = prep.executeQuery();
			if(rs.next()){
				 voter = new Voter();
				 voter.setId(rs.getLong("ID"));
				 voter.setfName(rs.getString("FIRST_NAME"));
				 voter.setlName(rs.getString("LAST_NAME"));
				 voter.setTitle(rs.getString("TITLE"));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if(conn != null){
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(prep != null){
				try {
					prep.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		return voter;
		
	}

	public static boolean passwordAuthentcateVoter(String username, String password,
			String dob) {
		boolean isValid = false;
		PreparedStatement prep =null;
		ResultSet rs = null;
		SimpleDateFormat format =  new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.US);
		Date dateDob = null;
		try {
			dateDob = format.parse(dob);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		System.out.println(dob.toString());
		Connection conn = DBManager.getConnection();
		try {
			prep = conn.prepareCall("select * from USER where USERNAME = ? and PASSWORD = ? and DOB = ?");
			prep.setString(1, username);
			try {
				prep.setString(2, MgrHash.generateStringdHash(password.getBytes()));
			} catch (NoSuchAlgorithmException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				prep.setString(3, MgrHash.generateDobHash(dateDob));
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			rs = prep.executeQuery();
			if(rs.next()){
				isValid = true;
				System.out.println(isValid);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if(conn != null){
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(prep != null){
				try {
					prep.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return isValid;
	}
	
	public static PublicKey queryPublicKey(String username)
	{
		PublicKey publicKey = null;
		BASE64Decoder decoder = new BASE64Decoder();
		PreparedStatement prep = null;
		ResultSet rs = null;
		Connection conn = DBManager.getConnection();
		try {
			prep = conn.prepareCall("select * from USER where USERNAME = ?");
			prep.setString(1, username);
			rs = prep.executeQuery();
			if(rs.next()){
				publicKey = MgrIntegrity.byteToPublicKey(decoder.decodeBuffer(rs.getString("PUBLIC_KEY")));		 
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return publicKey;
	}


}
