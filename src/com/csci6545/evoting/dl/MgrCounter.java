package com.csci6545.evoting.dl;

import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import Decoder.BASE64Decoder;
import Decoder.BASE64Encoder;

import com.csci6545.evoting.be.BcBallotFactor;
import com.csci6545.evoting.be.Election;
import com.csci6545.evoting.be.User;
import com.csci6545.evoting.be.Voter;
import com.csci6545.evoting.bl.MgrBytes;
import com.csci6545.evoting.bl.MgrHash;

public class MgrCounter {

	//Poom
	public static boolean insertBcBallot(byte [] bcBallot, 
			long electionId, byte[] supSignature){
		int ballotInserted = 0;
		Connection conn = DBManager.getConnection();
		PreparedStatement prep = null;
		BASE64Encoder encoder = new BASE64Encoder();
		String base64BcBallot = encoder.encodeBuffer(bcBallot);
		String base64SupSig = encoder.encodeBuffer(supSignature);
		try {
			prep = conn.prepareStatement("insert into COUNTER.BALLOT(BC_BALLOT, ELECTION_ID, COUNTER_ID, SUP_SIG, IS_COUNTED, IS_PUBLISHED) "
			 		+ "values(?, ?, ?, ?, 0, 0)");
			prep.setString(1, base64BcBallot);
			prep.setLong(2, electionId);
			prep.setString(3,"Counter");
			prep.setString(4, base64SupSig);
			ballotInserted = prep.executeUpdate();
			conn.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(ballotInserted == 1)
		{		
			return true;
		}
		else
		{
			return false;
		}
	 }
	
	public static BcBallotFactor getBcBallot(int entry){
		Connection conn = DBManager.getConnection();
		PreparedStatement prep = null;
		ResultSet rs = null;
		BcBallotFactor bcFactor = new BcBallotFactor();
		BASE64Decoder decoder = new BASE64Decoder();
		try {
			prep = conn.prepareStatement("SELECT * FROM COUNTER.BALLOT WHERE ID=?");
			prep.setInt(1, entry);
			rs = prep.executeQuery();
			if(rs.next()){
				
				String base64BcBallot = rs.getString("BC_BALLOT");
				String base64Ballot = rs.getString("BALLOT");
				String base64Key = rs.getString("BC_BALLOT_KEY");
				String base64Iv = rs.getString("BC_BALLOT_IV");
				String base64Supsig = rs.getString("SUP_SIG");
				bcFactor.setEntry(rs.getInt("ID"));
				bcFactor.setBallot("0,0".getBytes());
				bcFactor.setKey("unknown".getBytes());
				bcFactor.setIv("unknown".getBytes());
				if(base64Ballot!=null)
				{
					bcFactor.setBallot(decoder.decodeBuffer(base64Ballot));
					bcFactor.setKey(decoder.decodeBuffer(base64Key));
					bcFactor.setIv(decoder.decodeBuffer(base64Iv));
				}
				bcFactor.setSupSignature(decoder.decodeBuffer(base64Supsig));
				bcFactor.setBcBallot(decoder.decodeBuffer(base64BcBallot));
				bcFactor.setElectionId(rs.getLong("ELECTION_ID"));
			 }
			return bcFactor;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if(conn != null){
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(prep != null){
				try {
					prep.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return bcFactor;
	 }
	
	
	public static List<BcBallotFactor> getAllBcBallot(Long electionId){
		Connection conn = DBManager.getConnection();
		PreparedStatement prep = null;
		ResultSet rs = null;
		List<BcBallotFactor> bcBallot = new ArrayList<BcBallotFactor>();
		BASE64Decoder decoder = new BASE64Decoder();
		 BASE64Encoder encoder = new BASE64Encoder();
		try {
			prep = conn.prepareStatement("SELECT * FROM COUNTER.BALLOT WHERE ELECTION_ID=? AND IS_PUBLISHED = 0");
			prep.setLong(1, electionId);
			rs = prep.executeQuery();
			while(rs.next()){
				BcBallotFactor bcFactor = new BcBallotFactor();
				String base64BcBallot = rs.getString("BC_BALLOT");
				String base64Supsig = rs.getString("SUP_SIG");
				bcFactor.setEntry(rs.getInt("ID"));
				bcFactor.setBcBallot(decoder.decodeBuffer(base64BcBallot));
				bcFactor.setElectionId(electionId);
				bcFactor.setSupSignature(decoder.decodeBuffer(base64Supsig));
				setPublishBallot(rs.getInt("ID"), conn);
				bcBallot.add(bcFactor);
			 }
			return bcBallot;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			if(conn != null){
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(prep != null){
				try {
					prep.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return bcBallot;
	 }
	
	public static List<BcBallotFactor> getAllBallot(Long electionId){
		Connection conn = DBManager.getConnection();
		PreparedStatement prep = null;
		ResultSet rs = null;
		List<BcBallotFactor> bcBallot = new ArrayList<BcBallotFactor>();
		BASE64Decoder decoder = new BASE64Decoder();
		 BASE64Encoder encoder = new BASE64Encoder();
		try {
			prep = conn.prepareStatement("SELECT * FROM COUNTER.BALLOT WHERE ELECTION_ID = ? AND BALLOT IS NOT NULL AND IS_COUNTED=0");
			prep.setLong(1, electionId);
			rs = prep.executeQuery();
			while(rs.next()){
				BcBallotFactor bcFactor = new BcBallotFactor();
				String base64BcBallot = rs.getString("BC_BALLOT");
				String base64Ballot = rs.getString("BALLOT");
				String base64Key = rs.getString("BC_BALLOT_KEY");
				String base64Iv = rs.getString("BC_BALLOT_IV");
				String base64Supsig = rs.getString("SUP_SIG");
				bcFactor.setEntry(rs.getInt("ID"));
				bcFactor.setBallot("0,0".getBytes());
				bcFactor.setKey("unknown".getBytes());
				bcFactor.setIv("unknown".getBytes());
				if(base64Ballot!=null)
				{
					bcFactor.setBallot(decoder.decodeBuffer(base64Ballot));
					bcFactor.setKey(decoder.decodeBuffer(base64Key));
					bcFactor.setIv(decoder.decodeBuffer(base64Iv));
				}
				bcFactor.setSupSignature(decoder.decodeBuffer(base64Supsig));
				bcFactor.setBcBallot(decoder.decodeBuffer(base64BcBallot));
				bcFactor.setElectionId(rs.getLong("ELECTION_ID"));
				bcFactor.setSupSignature(decoder.decodeBuffer(base64Supsig));
				System.out.println(encoder.encodeBuffer(bcFactor.getBcBallot()));
				setCountBallot(rs.getInt("ID"), conn);
				bcBallot.add(bcFactor);
			 }
			return bcBallot;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			if(conn != null){
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(prep != null){
				try {
					prep.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return bcBallot;
	 }
	
	static void setCountBallot(int entry, Connection conn){
		 try{
			 
			 PreparedStatement prep = null;
			 prep = conn.prepareStatement("UPDATE COUNTER.BALLOT SET IS_COUNTED = 1 " +
			 		"WHERE ID = ?");
			 prep.setInt(1, entry);
			 prep.executeUpdate();

			 //System.out.println("Succeeded!");
		 }
		 catch(Exception e){
			 e.printStackTrace();
		 }

	 }
	
	static void setPublishBallot(int entry, Connection conn){
		 try{
			 
			 PreparedStatement prep = null;
			 prep = conn.prepareStatement("UPDATE COUNTER.BALLOT SET IS_PUBLISHED = 1 " +
			 		"WHERE ID = ?");
			 prep.setInt(1, entry);
			 prep.executeUpdate();

			 //System.out.println("Succeeded!");
		 }
		 catch(Exception e){
			 e.printStackTrace();
		 }

	 }
	
	public static Boolean setKeyForBcBallot(BcBallotFactor ballotFactor){
		 Connection conn = DBManager.getConnection();
		 PreparedStatement prep = null;
		 try{
			 
			 BASE64Encoder encoder = new BASE64Encoder();
			 String base64Ballot = encoder.encodeBuffer(ballotFactor.getBallot());
			 String base64Key = encoder.encodeBuffer(ballotFactor.getKey());
			 String base64Iv = encoder.encodeBuffer(ballotFactor.getIv());
			
			 prep = conn.prepareStatement("UPDATE COUNTER.BALLOT SET BALLOT=?, BC_BALLOT_KEY = ?, " +
			 		"BC_BALLOT_IV = ? WHERE ID = ?");
			 prep.setString(1, base64Ballot);
			 prep.setString(2, base64Key);
			 prep.setString(3, base64Iv);
			 prep.setInt(4, ballotFactor.getEntry());
			 prep.executeUpdate();
			 return true;
			 //System.out.println("Succeeded!");
		 } 
		 catch(Exception e){
			 e.printStackTrace();
		 }
		 finally {
				if(conn != null){
					try {
						conn.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				if(prep != null){
					try {
						prep.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		 return false;
	 }
	
	public static List<BcBallotFactor> getBcBallotStatus(){
		Connection conn = DBManager.getConnection();
		PreparedStatement prep = null;
		ResultSet rs = null;
		List<BcBallotFactor> bcBallot = new ArrayList<BcBallotFactor>();
		BASE64Decoder decoder = new BASE64Decoder();
		try {
			prep = conn.prepareStatement("SELECT DISTINCT ELECTION_ID FROM COUNTER.BALLOT WHERE IS_PUBLISHED = 0");
			rs = prep.executeQuery();
			while(rs.next()){
				BcBallotFactor bcFactor = new BcBallotFactor();
				//String base64BcBallot = rs.getString("BC_BALLOT");
				//String base64Supsig = rs.getString("SUP_SIG");
				//bcFactor.setEntry(rs.getInt("ID"));
				//bcFactor.setBcBallot(decoder.decodeBuffer(base64BcBallot));
				bcFactor.setElectionId(rs.getInt("ELECTION_ID"));
				//bcFactor.setSupSignature(decoder.decodeBuffer(base64Supsig));
				bcBallot.add(bcFactor);
			 }
			return bcBallot;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			if(conn != null){
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(prep != null){
				try {
					prep.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return bcBallot;
	 }
	
	public static List<BcBallotFactor> getBallotStatus(){
		Connection conn = DBManager.getConnection();
		PreparedStatement prep = null;
		ResultSet rs = null;
		List<BcBallotFactor> bcBallot = new ArrayList<BcBallotFactor>();
		BASE64Decoder decoder = new BASE64Decoder();
		try {
			prep = conn.prepareStatement("SELECT DISTINCT ELECTION_ID FROM COUNTER.BALLOT WHERE IS_PUBLISHED = 1 and IS_COUNTED = 0");
			rs = prep.executeQuery();
			while(rs.next()){
				BcBallotFactor bcFactor = new BcBallotFactor();
				//String base64BcBallot = rs.getString("BC_BALLOT");
				//String base64Supsig = rs.getString("SUP_SIG");
				//bcFactor.setEntry(rs.getInt("ID"));
				//bcFactor.setBcBallot(decoder.decodeBuffer(base64BcBallot));
				bcFactor.setElectionId(rs.getInt("ELECTION_ID"));
				//bcFactor.setSupSignature(decoder.decodeBuffer(base64Supsig));
				bcBallot.add(bcFactor);
			 }
			return bcBallot;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			if(conn != null){
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(prep != null){
				try {
					prep.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return bcBallot;
	 }
	
	
	// Add new counter
	
	public static long getIDOfUser(String userName) 
			throws SQLException{
		Connection conn = DBManager.getConnection();
		PreparedStatement prep = null;
		ResultSet rs = null;
	    prep = conn.prepareStatement("SELECT ID,USERNAME FROM USER "
					+ "WHERE USERNAME=?");
		prep.setString(1, userName);
		rs = prep.executeQuery();
		if(!rs.next()){
			throw new DBHasNoSuchUserException("No such "
					+ "user in the USER table!");
		}
		return rs.getLong("ID");
		
	}
	/**
	 * 
	 * @param user the User object
	 * @return -1 if user exists, 0 if creation failed, 1 if the creation success
	 */
	 public static int createCounter(String username, String password, String email){
		 int userInserted = 0;
		 Connection conn = DBManager.getConnection();
		 PreparedStatement prep = null;
		 SecureRandom secureRandom = new SecureRandom();
		 byte[] salt;
		 try {
			 if(existsCounter(username)){
				 return 0;
			 }
			prep = conn.prepareStatement("insert into COUNTER(USERNAME, PASSWORD, SALT, EMAIL) "
			 		+ "values(?, ?, ?, ?)");
			 salt = new byte[4];
				secureRandom.nextBytes(salt);
			prep.setString(1, username);
			String saltPasswd = MgrHash.generateStringdHash(
					MgrBytes.combineByteArray(salt,password.getBytes()));
			prep.setString(2, saltPasswd);			
			prep.setString(3, MgrBytes.bytesToHexString(salt));
			
			prep.setString(4, email);
			userInserted = prep.executeUpdate();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 return userInserted;
	 }
	 /**
	  * @author Poom
	  * @param user the User Object
	  * @param String password
	  */
	 public static boolean setCounterpassword(String username, String password, int flag){
		 
		 try{
			 int succeeded = 0;
			 Connection conn = DBManager.getConnection();
			 PreparedStatement prep = null;
			 prep = conn.prepareCall("select SALT from COUNTER where USERNAME=?");
			 prep.setString(1, username);
			 ResultSet	rs = prep.executeQuery();
			 byte[] salt=new byte[4];
			 if(rs.next()){
					String saltHextString = rs.getString("SALT");
					salt = MgrBytes.hexStringToBytes(saltHextString);
			}
			prep.close();
			prep=null;
			rs =null;
			prep = conn.prepareStatement("UPDATE COUNTER SET PASSWORD=?, IS_OTP = ? WHERE USERNAME=?");
			 
			String saltPasswd = MgrHash.generateStringdHash(
						MgrBytes.combineByteArray(salt,password.getBytes())); 
			prep.setString(1, saltPasswd);
			prep.setInt(2, flag);
			prep.setString(3, username);
			 succeeded = prep.executeUpdate();
			 if(succeeded==1)
			 {
				 return true;
			 }
				 //System.out.println("Succeeded!");
		 }
		 catch(Exception e){
			 e.printStackTrace();
		 }
		 return false;
	 }
	 
	 /**
	  * @author Yunfan Zhang
	  * @param user the User Object
	  * @return userPublicKey the byte array of user's public key that is read from the database
	  */
	 public static String getCounterEmail(String username){
		 try{
			 Connection conn = DBManager.getConnection();
			 PreparedStatement prep = null;
			 ResultSet rs = null;
			 prep = conn.prepareStatement("SELECT EMAIL FROM COUNTER WHERE USERNAME=?");
			 prep.setString(1, username);
			 rs = prep.executeQuery();
			 if(rs.next()){
				 String email = rs.getString("EMAIL");
				 return email;
			 }
			 else{
				 return null;
			 }
		 }
		 catch(Exception e){
			 e.printStackTrace();
			 return null;
		 }
	 }
	 
	 public static int getCounterLoginAttempts(String username){
		 try{
			 Connection conn = DBManager.getConnection();
			 PreparedStatement prep = null;
			 ResultSet rs = null;
			 prep = conn.prepareStatement("SELECT LOGIN_ATTEMPTS FROM COUNTER WHERE USERNAME=?");
			 prep.setString(1, username);
			 rs = prep.executeQuery();
			 if(rs.next()){
				 int attempts = rs.getInt("LOGIN_ATTEMPTS");
				 return attempts;
			 }
			 else{
				 return 10;
			 }
		 }
		 catch(Exception e){
			 e.printStackTrace();
			 return 10;
		 }
	 }
	 
	 public static int getCounterIsOTP(String username){
		 try{
			 Connection conn = DBManager.getConnection();
			 PreparedStatement prep = null;
			 ResultSet rs = null;
			 prep = conn.prepareStatement("SELECT IS_OTP FROM COUNTER WHERE USERNAME=?");
			 prep.setString(1, username);
			 rs = prep.executeQuery();
			 if(rs.next()){
				 int isOtp = rs.getInt("IS_OTP");
				 return isOtp;
			 }
			 else{
				 return 10;
			 }
		 }
		 catch(Exception e){
			 e.printStackTrace();
			 return 10;
		 }
	 }
	 
	 
	 public static boolean setCounterLoginAttempts(String username, int attempts){
		 try{
			 Connection conn = DBManager.getConnection();
			 PreparedStatement prep = null;
			 prep = conn.prepareStatement("UPDATE COUNTER SET LOGIN_ATTEMPTS=? WHERE USERNAME=?");
			 prep.setInt(1, attempts);
			 prep.setString(2, username);
			 prep.executeUpdate();
			 return true;
			 //System.out.println("Succeeded!");
		 }
		 catch(Exception e){
			 e.printStackTrace();
		 }
		 return false;
	 }
	 
	 
	 public static User passwordAuthentcateCounter(String username, String password) {
		 User user = null;	
		 PreparedStatement prep = null;
			ResultSet rs = null;
			Connection conn = DBManager.getConnection();
			try {
				
				prep = conn.prepareCall("select SALT from COUNTER where USERNAME=?");
				prep.setString(1, username);
				rs = prep.executeQuery();
				byte[] salt=new byte[4];
				if(rs.next()){
					String saltHextString = rs.getString("SALT");
					salt = MgrBytes.hexStringToBytes(saltHextString);
				}
				prep.close();
				prep=null;
				rs =null;
				
				prep = conn.prepareCall("select * from COUNTER where USERNAME = ? and PASSWORD = ?");
				prep.setString(1, username);
				String saltPasswd = MgrHash.generateStringdHash(
						MgrBytes.combineByteArray(salt,password.getBytes()));
				System.out.println(MgrBytes.bytesToHexString(salt));
				System.out.println(password);
				System.out.println(saltPasswd);
				prep.setString(2, saltPasswd);
				rs = prep.executeQuery();
				if(rs.next()){
					user = new Voter();
					user.setUserId(rs.getString("USERNAME"));
					user.setIsOtp(rs.getInt("IS_OTP"));
					user.setLoginAttempts(rs.getInt("LOGIN_ATTEMPTS"));
					user.setEmail(rs.getString("EMAIL"));
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				if(conn != null){
					try {
						conn.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				if(prep != null){
					try {
						prep.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			
			return user;
			
		}
	 public static boolean existsCounter(String userName) 
				throws SQLException{
			Connection conn = DBManager.getConnection();
			PreparedStatement prep = null;
			ResultSet rs = null;
		    prep = conn.prepareStatement("SELECT * FROM COUNTER "
						+ "WHERE USERNAME=?");
			prep.setString(1, userName);
			rs = prep.executeQuery();
			if(rs.next()){
				System.err.println("The username already exists");
				return true;
			}
			return false;

		}
	
}
