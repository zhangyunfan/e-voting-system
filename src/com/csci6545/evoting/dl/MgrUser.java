/* Author: Alemberhan Getahun,Yunfan Zhang
 * Date: 11/02/2013*/
package com.csci6545.evoting.dl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import Decoder.BASE64Decoder;
import Decoder.BASE64Encoder;

import com.csci6545.evoting.be.Counter;
import com.csci6545.evoting.be.User;
import com.csci6545.evoting.be.Election;
import com.csci6545.evoting.be.Voter;
import com.csci6545.evoting.bl.MgrClientConnection;
import com.csci6545.evoting.bl.MgrConfidentiality;
import com.csci6545.evoting.bl.MgrHash;
import com.csci6545.evoting.bl.MgrBytes;

public class MgrUser {
	private static final int SALT_SIZE = 4;
	private static final int USER_EXISTS =-1;
	/**
	 * @author Yunfan Zhang
	 * @param userName the String of username which may be stored in USER table of the DB
	 * @return true if the database has already have the same username else false
	 * @throws SQLException
	 */
	public static boolean existsUser(String userName) 
			throws SQLException{
		Connection conn = DBManager.getConnection();
		PreparedStatement prep = null;
		ResultSet rs = null;
	    prep = conn.prepareStatement("SELECT * FROM USER "
					+ "WHERE USERNAME=?");
		prep.setString(1, userName);
		rs = prep.executeQuery();
		if(rs.next()){
			System.err.println("The username already exists");
			return true;
		}
		return false;

	}
	/**
	 * @author Yunfan Zhang
	 * @param userName userName the String of username which may be stored in USER table of the DB
	 * @return the ID field of the USER table whose USERNAME field equals the input userName
	 * @throws SQLException 
	 */
	public static long getIDOfUser(String userName) 
			throws SQLException{
		Connection conn = DBManager.getConnection();
		PreparedStatement prep = null;
		ResultSet rs = null;
	    prep = conn.prepareStatement("SELECT ID,USERNAME FROM USER "
					+ "WHERE USERNAME=?");
		prep.setString(1, userName);
		rs = prep.executeQuery();
		if(!rs.next()){
			throw new DBHasNoSuchUserException("No such "
					+ "user in the USER table!");
		}
		return rs.getLong("ID");
		
	}
	/**
	 * 
	 * @param user the User object
	 * @return -1 if user exists, 0 if creation failed, 1 if the creation success
	 */
	 public static int createUser(User user, int titleId){
		 int userInserted = 0;
		 Connection conn = DBManager.getConnection();
		 PreparedStatement prep = null;
		 SecureRandom secureRandom = new SecureRandom();
		 byte[] salt;
		 try {
			 if(existsUser(user.getUserId())){
				 return USER_EXISTS;
			 }
			prep = conn.prepareStatement("insert into USER(FIRST_NAME, LAST_NAME, TITLE_ID, DOB, PASSWORD, USERNAME, SALT, EMAIL) "
			 		+ "values(?, ?, ?, ?, ?, ?, ?, ?)");
			prep.setString(1, user.getfName());
			prep.setString(2, user.getlName());
			prep.setInt(3, titleId);
			prep.setString(4, MgrHash.generateDobHash(user.getDob()));
		    salt = new byte[SALT_SIZE];
			secureRandom.nextBytes(salt);
			String saltPasswd = MgrHash.generateStringdHash(
					MgrBytes.combineByteArray(salt,user.getPassword().getBytes()));
			prep.setString(5, saltPasswd);
			//prep.setString(5, MgrHash.generateStringdHash(user.getPassword().getBytes()));
			prep.setString(6, user.getUserId());
			prep.setString(7, MgrBytes.bytesToHexString(salt));
			//prep.setString(7, "1234");
			prep.setString(8, user.getEmail());
			
			userInserted = prep.executeUpdate();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 return userInserted;
	 }
	 /**
	  * @author Poom
	  * @param user the User Object
	  * @param String password
	  */
	 public static boolean setUserpassword(String username, String password, int flag){
		 
		 try{
			 int succeeded = 0;
			 Connection conn = DBManager.getConnection();
			 PreparedStatement prep = null;
			 prep = conn.prepareCall("select SALT from USER where USERNAME=?");
			 prep.setString(1, username);
			 ResultSet	rs = prep.executeQuery();
			 byte[] salt=new byte[SALT_SIZE];
			 if(rs.next()){
					String saltHextString = rs.getString("SALT");
					salt = MgrBytes.hexStringToBytes(saltHextString);
			}
			prep.close();
			prep=null;
			rs =null;
			prep = conn.prepareStatement("UPDATE USER SET PASSWORD=?, IS_OTP = ? WHERE USERNAME=?");
			 
			String saltPasswd = MgrHash.generateStringdHash(
						MgrBytes.combineByteArray(salt,password.getBytes())); 
			prep.setString(1, saltPasswd);
			prep.setInt(2, flag);
			prep.setString(3, username);
			 succeeded = prep.executeUpdate();
			 if(succeeded==1)
			 {
				 return true;
			 }
				 //System.out.println("Succeeded!");
		 }
		 catch(Exception e){
			 e.printStackTrace();
		 }
		 return false;
	 }
	 
	 public static byte[] getUserPublicKey(User user){
		 BASE64Decoder decoder = new BASE64Decoder();
		 try{
			 Connection conn = DBManager.getConnection();
			 PreparedStatement prep = null;
			 ResultSet rs = null;
			 prep = conn.prepareStatement("SELECT PUBLIC_KEY FROM USER WHERE USERNAME=?");
			 prep.setString(1, user.getUserId());
			 rs = prep.executeQuery();
			 if(rs.next()){
				 String base64PublicKey = rs.getString("PUBLIC_KEY");
				 return decoder.decodeBuffer(base64PublicKey);
			 }
			 else{
				 return null;
			 }
		 }
		 catch(Exception e){
			 e.printStackTrace();
			 return null;
		 }
	 }
	 
	 public static int getUserPermission(String username){
		 Connection conn = DBManager.getConnection();
		 int permisison = 0;
		 PreparedStatement prep = null;
		 try{
			
			 ResultSet rs = null;
			 prep = conn.prepareStatement("SELECT TITLE_ID FROM USER WHERE USERNAME=?");
			 prep.setString(1, username);
			 rs = prep.executeQuery();
			 if(rs.next()){
				 permisison = MgrTitle.getPermissionbyId(rs.getInt("TITLE_ID"));
				 return permisison;
			 }
			 else{
				 return 0;
			 }
		 }
		 catch(Exception e){
			 e.printStackTrace();
			 return 0;
		 }
		 finally {
				if(conn != null){
					try {
						conn.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				if(prep != null){
					try {
						prep.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
					
		}

	 
	 /**
	  * @author Yunfan Zhang
	  * @param user the User Object
	  * @param userPublicKey the byte array of user's public key that will be write into the database
	  */
	 public static void setUserPublicKey(User user,byte[] userPublicKey){
		 BASE64Encoder encoder = new BASE64Encoder();
		 String base64PublicKey = encoder.encodeBuffer(userPublicKey);
		 try{
			 Connection conn = DBManager.getConnection();
			 PreparedStatement prep = null;
			 prep = conn.prepareStatement("UPDATE USER SET PUBLIC_KEY=? WHERE USERNAME=?");
			 prep.setString(1, base64PublicKey);
			 prep.setString(2, user.getUserId());
			 prep.executeUpdate();
			 //System.out.println("Succeeded!");
		 }
		 catch(Exception e){
			 e.printStackTrace();
		 }
	 }
	 /**
	  * @author Yunfan Zhang
	  * @param user the User Object
	  * @return userPublicKey the byte array of user's public key that is read from the database
	  */
	 public static String getUserEmail(String username){
		 try{
			 Connection conn = DBManager.getConnection();
			 PreparedStatement prep = null;
			 ResultSet rs = null;
			 prep = conn.prepareStatement("SELECT EMAIL FROM USER WHERE USERNAME=?");
			 prep.setString(1, username);
			 rs = prep.executeQuery();
			 if(rs.next()){
				 String email = rs.getString("EMAIL");
				 return email;
			 }
			 else{
				 return null;
			 }
		 }
		 catch(Exception e){
			 e.printStackTrace();
			 return null;
		 }
	 }
	 
	 public static int getUserLoginAttempts(String username){
		 try{
			 Connection conn = DBManager.getConnection();
			 PreparedStatement prep = null;
			 ResultSet rs = null;
			 prep = conn.prepareStatement("SELECT LOGIN_ATTEMPTS FROM USER WHERE USERNAME=?");
			 prep.setString(1, username);
			 rs = prep.executeQuery();
			 if(rs.next()){
				 int attempts = rs.getInt("LOGIN_ATTEMPTS");
				 return attempts;
			 }
			 else{
				 return 10;
			 }
		 }
		 catch(Exception e){
			 e.printStackTrace();
			 return 10;
		 }
	 }
	 
	 public static int getUserIsOTP(String username){
		 try{
			 Connection conn = DBManager.getConnection();
			 PreparedStatement prep = null;
			 ResultSet rs = null;
			 prep = conn.prepareStatement("SELECT IS_OTP FROM USER WHERE USERNAME=?");
			 prep.setString(1, username);
			 rs = prep.executeQuery();
			 if(rs.next()){
				 int isOtp = rs.getInt("IS_OTP");
				 return isOtp;
			 }
			 else{
				 return 10;
			 }
		 }
		 catch(Exception e){
			 e.printStackTrace();
			 return 10;
		 }
	 }
	 
	 
	 public static boolean setUserLoginAttempts(String username, int attempts){
		 try{
			 Connection conn = DBManager.getConnection();
			 PreparedStatement prep = null;
			 prep = conn.prepareStatement("UPDATE USER SET LOGIN_ATTEMPTS=? WHERE USERNAME=?");
			 prep.setInt(1, attempts);
			 prep.setString(2, username);
			 prep.executeUpdate();
			 return true;
			 //System.out.println("Succeeded!");
		 }
		 catch(Exception e){
			 e.printStackTrace();
		 }
		 return false;
	 }
	 
	 
	 public static User passwordAuthentcateVoter(String username, String password) {
		 User user = null;
			PreparedStatement prep = null;
			ResultSet rs = null;
			Connection conn = DBManager.getConnection();
			try {
				prep = conn.prepareCall("select SALT from USER where USERNAME=?");
				prep.setString(1, username);
				rs = prep.executeQuery();
				byte[] salt=new byte[SALT_SIZE];
				if(rs.next()){
					String saltHextString = rs.getString("SALT");
					salt = MgrBytes.hexStringToBytes(saltHextString);
				}
				prep.close();
				prep=null;
				rs =null;
				
				prep = conn.prepareCall("select * from USER where USERNAME = ? and PASSWORD = ?");
				prep.setString(1, username);
				String saltPasswd = MgrHash.generateStringdHash(
						MgrBytes.combineByteArray(salt,password.getBytes()));
				//System.out.println(MgrBytes.bytesToHexString(salt));
				//System.out.println(password);
				//System.out.println(saltPasswd);
				prep.setString(2, saltPasswd);
				rs = prep.executeQuery();
				if(rs.next()){
					user = new Voter();
					user.setId(rs.getLong("ID"));
					user.setfName(rs.getString("FIRST_NAME"));
					user.setlName(rs.getString("LAST_NAME"));
					user.setTitle(MgrTitle.getTitleByID(rs.getInt("TITLE_ID")));
					user.setIsOtp(rs.getInt("IS_OTP"));
					user.setLoginAttempts(rs.getInt("LOGIN_ATTEMPTS"));
					user.setEmail(rs.getString("EMAIL"));
					user.setPermission(MgrTitle.getPermissionbyId(rs.getInt("TITLE_ID")));
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				if(conn != null){
					try {
						conn.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				if(prep != null){
					try {
						prep.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			
			return user;
			
		}
	 
	 
	 public static List<User> getUserInfo() {
			List<User> userList = new ArrayList<User>();
			Connection conn = DBManager.getConnection();
			PreparedStatement prep = null;
			ResultSet rs = null;
			try {
				prep = conn.prepareStatement("select * from USER,TITLE where TITLE.ID = USER.TITLE_ID");
				rs = prep.executeQuery();
				while(rs.next()){
					User u = new User();
					u.setUserId(rs.getString("USERNAME"));
					u.setfName(rs.getString("FIRST_NAME"));
					u.setlName(rs.getString("LAST_NAME"));
					u.setTitle(rs.getString("TITLE_NAME"));
					u.setPermission(rs.getInt("PERMISSION"));
					userList.add(u);
				}
				conn.close();
				prep.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return userList;
		}
	 
	 public static boolean passwordAuthentcateVoter(String username, String password,
				String dob) {
			boolean isValid = false;
			PreparedStatement prep =null;
			ResultSet rs = null;
			SimpleDateFormat format =  new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.US);
			Date dateDob = null;
			try {
				dateDob = format.parse(dob);
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			System.out.println(dob.toString());
			Connection conn = DBManager.getConnection();
			try {
				prep = conn.prepareCall("select * from USER where USERNAME = ? and PASSWORD = ? and DOB = ?");
				prep.setString(1, username);
				try {
					prep.setString(2, MgrHash.generateStringdHash(password.getBytes()));
				} catch (NoSuchAlgorithmException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				try {
					prep.setString(3, MgrHash.generateDobHash(dateDob));
				} catch (NoSuchAlgorithmException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				rs = prep.executeQuery();
				if(rs.next()){
					isValid = true;
					System.out.println(isValid);
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				if(conn != null){
					try {
						conn.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				if(prep != null){
					try {
						prep.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}

			return isValid;
		}
	 public static boolean alreadyRegistered(long uId, long electionId) 
				throws SQLException{
			Connection conn = DBManager.getConnection();
			PreparedStatement prep = null;
			ResultSet rs = null;
		    prep = conn.prepareStatement("SELECT * FROM USER_ELECTION "
						+ "WHERE USER_ID=? and ELECTION_ID = ?");
			prep.setLong(1, uId);
			prep.setLong(2, electionId);
			rs = prep.executeQuery();
			if(rs.next()){
				System.err.println("The user already registered");
				conn.close();
				prep.close();
				return true;
			}
			
			return false;

		}
	 public static Boolean registerForElection(String userName, long electionId){
			int registered = 0;
			Connection conn = DBManager.getConnection();
			PreparedStatement prep = null;
			 try {
				 long uId = getIDOfUser(userName);
				 if(alreadyRegistered(uId,electionId)){
					 return false;
				 }
				prep = conn.prepareStatement("insert into USER_ELECTION(USER_ID, ELECTION_ID) "
				 		+ "values(?, ?)");
				prep.setLong(1, uId);
				prep.setLong(2, electionId);
				
				if(prep.executeUpdate() == 1)
				{		
					return true;
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				if(conn != null){
					try {
						conn.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				if(prep != null){
					try {
						prep.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}

			 return false;
		 }
	 
	 public static boolean updateUserInfo(User user){
		 Connection conn = DBManager.getConnection();
		 PreparedStatement prep = null;
		 try{
			 int succeeded = 0;
			
			 prep = conn.prepareStatement("UPDATE USER SET FIRST_NAME=?, LAST_NAME = ? , EMAIL = ?" +
			 		"WHERE USERNAME=?");
			 prep.setString(1, user.getfName());
			 prep.setString(2, user.getlName());
			 prep.setString(3, user.getEmail());
			// prep.setString(4, MgrHash.generateDobHash(user.getDob()));
			 prep.setString(4, user.getUserId());
			 succeeded = prep.executeUpdate();
			 if(succeeded == 1)
			 {
				
				return true;
			 }
			 //System.out.println("Succeeded!");
		 }
		 catch(Exception e){
			 e.printStackTrace();
		 }finally {
				if(conn != null){
					try {
						conn.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				if(prep != null){
					try {
						prep.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		 
		 return false;
	 }
	 
	 
	 /*public static void main(String[] args) throws NoSuchAlgorithmException{
		 LoadConfig.loadServerDBConfig();
		 User user = new Voter();
		 String dob = "7/30/1863";
		 SimpleDateFormat format =  new SimpleDateFormat("MM/dd/yyyy", Locale.US);		 
		 user.setfName("Test");
		 user.setlName("Test");
		 try {
			user.setDob(format.parse(dob));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 user.setTitle("Candidate");
		 user.setPassword("12345678");
		 user.setUserId("Test");
		 user.setEmail("test@hotmail.com");
		 System.out.println(createUser(user,1));
		 KeyPair kp = MgrConfidentiality.generateRSAKeyPair();
		 PublicKey publicKey = kp.getPublic();
		 setUserPublicKey(user,publicKey.getEncoded());
		 byte[] returnByte = getUserPublicKey(user);
		 for(int i=0;i<returnByte.length;i++){
			 if(returnByte[i]!=publicKey.getEncoded()[i]){
				 System.out.println("Failed");
			 }
		 }
		 System.out.println("Succeeded");
	 }*/
	 
	 public static Boolean updateUserTitle(String username,int titleId){

		 Connection conn = DBManager.getConnection();
		 PreparedStatement prep = null;
		 try{
			
			 prep = conn.prepareStatement("UPDATE USER SET TITLE_ID=? WHERE USERNAME=?");
			 prep.setInt(1, titleId);
			 prep.setString(2, username);
			 if(prep.executeUpdate() == 1)
			 {
				 return true;
			 }
			 
			 //System.out.println("Succeeded!");
		 }
		 catch(Exception e){
			 e.printStackTrace();
		 }finally {
				if(conn != null){
					try {
						conn.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				if(prep != null){
					try {
						prep.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		 return false;
	 }
 
}
