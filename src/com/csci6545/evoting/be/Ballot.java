package com.csci6545.evoting.be;

import com.csci6545.evoting.bl.MgrClone;


public class Ballot {

	private long electionID;
	private byte [] bytes;
	private long candidateID;
	public long getcandidateID() {
		return candidateID;
	}

	public void setcandidateID(long candidateID) {
		this.candidateID = candidateID;
	}

	public void setElectionID(long electionID) {
		this.electionID = electionID;
	}
	public byte[] getBytes() {
		String ballot = electionID+","+candidateID;
		bytes = ballot.getBytes();
		return MgrClone.cloneByteArray(bytes);
		//return bytes;
	}

	public void setBytes(byte[] bytes) {
		this.bytes = MgrClone.cloneByteArray(bytes);
	}

	public long getElectionID() {
		return electionID;
	}
	public String getOriginalBallot() {
		String ballot = electionID+","+candidateID;
		return ballot;
	}
	public Ballot(long electionID, long candidateID) {
		this.electionID = electionID;
		this.candidateID = candidateID;
	}

}
