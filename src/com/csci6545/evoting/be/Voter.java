package com.csci6545.evoting.be;

import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.Cipher;

import org.bouncycastle.crypto.engines.RSABlindingEngine;
import org.bouncycastle.crypto.generators.RSABlindingFactorGenerator;
import org.bouncycastle.crypto.signers.PSSSigner;

import com.csci6545.evoting.bl.MgrConfidentiality;
import com.csci6545.evoting.bl.MgrIntegrity;

public class Voter extends User {
	private List<Election> elections;
	
	//add key to do bit commitment
	//add iv to do bit commitment
	//add supervisor's certificate
	//add counter's certificate
	//remove key to decrypt the private key
	//remove iv variable to decrypt the private key
	//remove passKey variable
	private byte [] keyForBc;
	private byte [] ivForBc;
	private byte [] blindingFactor;
	private byte [] blindBallot;
	private byte [] bcBallot;
	private byte [] supervisorSigOnBcBallot;
	private byte [] supervisorBlindSignature;
	private PublicKey supPublicKey;
	private String passkeyForBcBallot;
	private X509Certificate adminCert;
	private X509Certificate counterCert;
	private byte [] nuance;
	private byte [] sessionKey;
	private TicketElection ticketElction;
	private int entry;
	private Map<Long, byte []> keyMap = new HashMap<Long, byte []>();
	private Map<Long, byte []> ivMap = new HashMap<Long, byte []>();
	private Map<Long, byte []> BallotMap = new HashMap<Long, byte []>();
	public Map<Long, byte[]> getBallotMap() {
		return BallotMap;
	}

	public void setBallotMap(Map<Long, byte[]> ballotMap) {
		BallotMap = ballotMap;
	}

	private Map<Long, Integer> mapElectionEntry = new HashMap<Long, Integer>();
	private Map<Long, byte []> bcBallotMap = new HashMap<Long, byte []>();
	private List<Long> electionIdList = new ArrayList<Long>();
	

	public List<Long> getElectionIdList() {
		return electionIdList;
	}

	public void setElectionIdList(List<Long> electionIdList) {
		this.electionIdList = electionIdList;
	}

	public Map<Long, byte[]> getBcBallotMap() {
		return bcBallotMap;
	}

	public void setBcBallotMap(Map<Long, byte[]> bcBallotMap) {
		this.bcBallotMap = bcBallotMap;
	}

	public Map<Long, Integer> getMapElectionEntry() {
		return mapElectionEntry;
	}

	public void setMapElectionEntry(Map<Long, Integer> mapElectionEntry) {
		this.mapElectionEntry = mapElectionEntry;
	}

	private String counterIPAddress;
	private int counterPort;
	public String getCounterIPAddress() {
		return counterIPAddress;
	}

	public void setCounterIPAddress(String counterIPAddress) {
		this.counterIPAddress = counterIPAddress;
	}

	public int getCounterPort() {
		return counterPort;
	}

	public void setCounterPort(int counterPort) {
		this.counterPort = counterPort;
	}

	public Map<Long, byte[]> getKeyMap() {
		return keyMap;
	}

	public void setKeyMap(Map<Long, byte[]> keyMap) {
		this.keyMap = keyMap;
	}

	public Map<Long, byte[]> getIvMap() {
		return ivMap;
	}

	public void setIvMap(Map<Long, byte[]> ivMap) {
		this.ivMap = ivMap;
	}

	public int getEntry() {
		return entry;
	}

	public void setEntry(int entry) {
		this.entry = entry;
	}

	private PublicKey counterPublicKey;
	
	//add method doBitCommitment
	//add method signBallot
	//add method sendBallot(to counter)
	//remove method passkeyHahser
	
	
	
	public PublicKey getCounterPublicKey() {
		return counterPublicKey;
	}

	public void setCounterPublicKey(PublicKey counterPublicKey) {
		this.counterPublicKey = counterPublicKey;
	}

	public PublicKey getSupPublicKey() {
		return supPublicKey;
	}

	public TicketElection getTicketElction() {
		return ticketElction;
	}

	public void setTicketElction(TicketElection ticketElction) {
		this.ticketElction = ticketElction;
	}

	public byte[] getSessionKey() {
		return sessionKey;
	}

	public void setSessionKey(byte[] sessionKey) {
		this.sessionKey = sessionKey;
	}

	public byte[] getNuance() {
		return nuance;
	}

	public void setNuance(byte[] nuance) {
		this.nuance = nuance;
	}

	public void setSupPublicKey(PublicKey supPublicKey) {
		this.supPublicKey = supPublicKey;
	}
	
	public byte[] getSupervisorBlindSignature() {
		return supervisorBlindSignature;
	}

	public void setSupervisorBlindSignature(byte[] supervisorBlindSignature) {
		this.supervisorBlindSignature = supervisorBlindSignature;
	}
	
	public String getPasskeyForBcBallot() {
		return passkeyForBcBallot;
	}

	public void setPasskeyForBcBallot(String passkeyForBcBallot) {
		this.passkeyForBcBallot = passkeyForBcBallot;
	}

	public byte[] getBlindBallot() {
		return blindBallot;
	}

	public void setBlindBallot(byte[] blindBallot) {
		this.blindBallot = blindBallot;
	}
	
	public byte[] getBlindingFactor() {
		return blindingFactor;
	}

	public byte[] getBcBallot() {
		return bcBallot;
	}

	public void setBcBallot(byte[] bcBallot) {
		this.bcBallot = bcBallot;
	}

	public byte[] getSupervisorSigOnBcBallot() {
		return supervisorSigOnBcBallot;
	}

	public void setSupervisorSigOnBcBallot(byte[] supervisorSigOnBcBallot) {
		this.supervisorSigOnBcBallot = supervisorSigOnBcBallot;
	}

	public void setBlindingFactor(byte[] blindingFactor) {
		this.blindingFactor = blindingFactor;
	}

	
	public List<Election> getElections() {
		return elections;
	}


	public byte[] getKeyForBc() {
		return keyForBc;
	}

	public void setKeyForBc(byte[] keyForBc) {
		this.keyForBc = keyForBc;
	}
	
	public byte[] getIvForBc() {
		return ivForBc;
	}

	public void setIvForBc(byte[] ivForBc) {
		this.ivForBc = ivForBc;
	}
	
	public X509Certificate getAdminCert() {
		return adminCert;
	}

	public void setAdminCert(X509Certificate adminCert) {
		this.adminCert = adminCert;
	}

	public X509Certificate getCounterCert() {
		return counterCert;
	}

	public void setCounterCert(X509Certificate counterCert) {
		this.counterCert = counterCert;
	}
	
	public void setElections(List<Election> elections) {
		this.elections = elections;
	}

	public Ballot makeVote(Election election) {
		// True Make an election
		return null;
	}

	public byte [] getBlindBallot(byte [] bcBallot, PublicKey supervisorPubKey, byte [] blindingFactor)
	{
		// make blind using blind signature
		return MgrIntegrity.makeBlind(supervisorPubKey, new BigInteger(blindingFactor), bcBallot);
	}
		
	public void sendBallot(byte [] ballot, byte[] mySignature, String ipAddr) {
		// Send blind Ballot to either the supervisor
		// Send bc Ballot to the counter 
	}
	public byte [] doBitCommitment(Ballot ballot,byte [] key, byte [] iv) 
			throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
		// Do bit commitment(encryption) to ballot
		return MgrConfidentiality.encryptAESBallot(ballot.getBytes(), key, iv);
	}
	
	public byte [] getSingedBcBallot(byte[] supervisorBlindSign, PublicKey supervisorPubKey, 
			byte [] blindingFactor)
	{
		return MgrIntegrity.unBlind(supervisorPubKey, new BigInteger(blindingFactor), supervisorBlindSign);	
	}	
	
	public byte[] hasher(byte[] key, long entry){
	
		// hash the key and entry
		return null;
	}
	
	public byte[] hasher(Signature adminSign,byte[] bcBallot){
		
		// hash the BlindBallot and supervisor signature
		return null;
	}
	
	public byte[] encryptBallot(byte[] bcBallot,byte[] supervisorSign, 
			byte[] hash, PublicKey counterPubKey){
		//Concatenate BcBallot, supervisor 's signature and their hash
		//Encrypt them with counterPubKey

		return null;
	}
	
	public byte[] encryptBallot(byte[] key, long entry, 
			byte[] hash, PublicKey counterPubKey){
		//Concatenate key for BcBallot decryption, entry of the ballot, and their hash
		//Encrypt them with counterPubKey
		return null;
	}

	public boolean checkBallotEntry(byte bcBallot){
		// check the entry of the current voter's ballot
		return false;
	}
	
	public void sendKeyEntry(byte[] encryptedKey,
			String ipAddr){
		// send the encrypted key and entry to the counter

	}
	
	public byte[] generateBlindingFactor(PublicKey supervisorPubKey) {
		return MgrIntegrity.getBlindingFactor(supervisorPubKey).toByteArray();
	}
	
	public byte[] generateKeyForBc() 
			throws NoSuchAlgorithmException, NoSuchProviderException {
		return MgrConfidentiality.generateSecreteKey();
	}

	public byte[] generateIvForBc() {
		return MgrConfidentiality.getRandomIV();
	}
	
	public byte[] generateNuance()
	{
		// Generate random nuance
		return null;
	}
	
}

