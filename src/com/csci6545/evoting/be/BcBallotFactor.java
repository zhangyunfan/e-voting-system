package com.csci6545.evoting.be;

import java.math.BigInteger;
import com.csci6545.evoting.bl.MgrClone;

public class BcBallotFactor {
	// This class is for counter to gather bcBallot from voters
	private long electionId;
	private byte [] bcBallot;
	private int entry;
	private byte [] key = "unknown".getBytes();
	private byte [] iv = "unknown".getBytes();
	private byte [] supSignature;
	private byte [] ballot = "unknown".getBytes();
	
	public long getElectionId() {
		return electionId;
	}
	public void setElectionId(long electionId) {
		this.electionId = electionId;
	}
	public byte[] getBallot() {
		return MgrClone.cloneByteArray(ballot);
	}
	public void setBallot(byte[] ballot) {
		this.ballot = MgrClone.cloneByteArray(ballot);
	}
	public byte[] getBcBallot() {
		//return MgrClone.cloneByteArray(bcBallot);
		return bcBallot;
	}
	public void setBcBallot(byte[] bcBallot) {
		this.bcBallot = MgrClone.cloneByteArray(bcBallot);
	}
	public int getEntry() {
		return entry;
	}
	public void setEntry(int entry) {
		this.entry = entry;
	}
	public byte[] getKey() {
		return MgrClone.cloneByteArray(key);
	}
	public void setKey(byte[] key) {
		this.key = MgrClone.cloneByteArray(key);
	}
	public byte[] getIv() {
		return MgrClone.cloneByteArray(iv);
	}
	public void setIv(byte[] iv) {
		this.iv = MgrClone.cloneByteArray(iv);
	}
	public byte[] getSupSignature() {
		//return MgrClone.cloneByteArray(supSignature);
		return supSignature;
	}
	public void setSupSignature(byte[] supSignature) {
		this.supSignature = MgrClone.cloneByteArray(supSignature);
	}
	
	/*public byte[] getBytes() {
		
		String stringTicket = this.getEntry()+","+new BigInteger(this.getBcBallot())+","+
				new BigInteger(this.getSupSignature())+","+new BigInteger(this.getKey())+","+
				new BigInteger(this.getIv())+","+new BigInteger(this.ballot);
		return stringTicket.getBytes();	
	}*/	
	
	public byte[] getBytes() {
		
		String stringTicket = this.getEntry()+","+new BigInteger(this.getBcBallot())+","
		+new BigInteger(this.getKey())+","+ new BigInteger(this.getIv())+","+
				new BigInteger(this.ballot)+","+electionId;
		//return stringTicket.getBytes();
		return MgrClone.cloneByteArray(stringTicket.getBytes());
	}
}
