package com.csci6545.evoting.be;

public class TicketElection extends Ticket{
	private long electionID;

	public long getElectionID() {
		return electionID;
	}

	public void setElectionID(long electionID) {
		this.electionID = electionID;
	}
	
	// get unsigned Ticket
	public byte[] getSigningRequest() {
			
		String stringTicket = this.getUserId()+","+this.getUserIP()+","+electionID+","+this.getIssueDate().toString()+","+
		this.getExpirationDate().toString();
		return stringTicket.getBytes();	
	}
		
	// get signed Ticket
	public byte[] getBytes() {
			
		String stringTicket = this.getUserId()+","+this.getUserIP()+","+electionID+","+this.getIssueDate().toString()+","+
				this.getExpirationDate().toString()+","+this.getSignature();
		return stringTicket.getBytes();	
	}
}
