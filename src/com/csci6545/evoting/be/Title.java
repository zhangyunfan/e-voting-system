package com.csci6545.evoting.be;

public class Title {
	private String titleName;
	private int TitleId;
	private int permission;
	public int getPermission() {
		return permission;
	}
	public void setPermission(int permission) {
		this.permission = permission;
	}
	public String getTitleName() {
		return titleName;
	}
	public void setTitleName(String titleName) {
		this.titleName = titleName;
	}
	public int getTitleId() {
		return TitleId;
	}
	public void setTitleId(int titleId) {
		TitleId = titleId;
	}
}
