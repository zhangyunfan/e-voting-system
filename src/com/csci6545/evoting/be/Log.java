package com.csci6545.evoting.be;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class Log {
	private File file;
	
	public Log()
	{
		file = new File("./log/audit.log");
	}
	
	public File getFile() {
		return file;
	}

	// for encrypted log
	public void write (byte[] obj)
	{
		
		FileWriter fs = null;
		
		try {
			fs = new FileWriter(file, true);
			
			fs.write(new String(obj));
			fs.write(System.getProperty("line.separator"));
		} catch (Exception e) {
			e.printStackTrace();
		} finally
		{
			try {
				fs.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	//plaintext log
	public void write (String obj)
	{
		
		FileWriter fs = null;
		
		try {
			fs = new FileWriter(file, true);
			
			fs.write(obj);
			fs.write(System.getProperty("line.separator"));
		} catch (Exception e) {
			e.printStackTrace();
		} finally
		{
			try {
				fs.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}


	

}
