package com.csci6545.evoting.be;

import java.security.PublicKey;
import com.csci6545.evoting.bl.MgrClone;

public class Counter extends User{

	private byte [] keyForBc;
	private byte [] ivForBc;
	private byte [] supervisorSigOnBcBallot;
	private PublicKey supPublicKey;
	private byte [] sessionKey;
	
	public byte[] getKeyForBc() {
		return MgrClone.cloneByteArray(keyForBc);
	}

	public void setKeyForBc(byte[] keyForBc) {
		this.keyForBc =MgrClone.cloneByteArray(keyForBc);
	}

	public byte[] getIvForBc() {
		return MgrClone.cloneByteArray(ivForBc);
	}

	public void setIvForBc(byte[] ivForBc) {
		this.ivForBc = MgrClone.cloneByteArray(ivForBc);
	}

	public byte[] getSupervisorSigOnBcBallot() {
		return MgrClone.cloneByteArray(supervisorSigOnBcBallot);
	}

	public void setSupervisorSigOnBcBallot(byte[] supervisorSigOnBcBallot) {
		this.supervisorSigOnBcBallot = 
				MgrClone.cloneByteArray(supervisorSigOnBcBallot);
	}

	public PublicKey getSupPublicKey() {
		return supPublicKey;
	}

	public void setSupPublicKey(PublicKey supPublicKey) {
		this.supPublicKey = supPublicKey;
	}

	public byte[] getSessionKey() {
		return MgrClone.cloneByteArray(sessionKey);
	}

	public void setSessionKey(byte[] sessionKey) {
		this.sessionKey = MgrClone.cloneByteArray(sessionKey);
	}

	public void publish()
	{
		//publish bcBallot and Entry to Bulletin Board
		//publish Ballot, key, bcBallot, supervisor' signature to Bulletin Board
	}
	
	public byte[] decryptHybridMessage()
	{
		//decrypt HybridMessage sent from voters
		return null;
	}
	
	public byte[] extractSessionKey()
	{
		//Extract Session Key
		return null;
	}
}
