package com.csci6545.evoting.be;


public class TicketBB extends Ticket{

	private String role;
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	
	// get unsigned Ticket
	public byte[] getSigningRequest() {
		
		String stringTicket = this.getUserId()+","+this.getUserIP()+","+this.getIssueDate().toString()+","+
		this.getExpirationDate().toString()+","+role+","+this.getPermission();
		return stringTicket.getBytes();	
	}
	
	// get signed Ticket
	public byte[] getBytes() {
		
		String stringTicket = this.getUserId()+","+this.getUserIP()+","+this.getIssueDate().toString()+","+
				this.getExpirationDate().toString()+","+role+","+this.getPermission()+","+this.getSignature();
		return stringTicket.getBytes();	
	}
	
	// get hmac Ticket
	public byte[] getBytesWithHmac() {
		
		String stringTicket = this.getUserId()+","+this.getUserIP()+","+this.getIssueDate().toString()+","+
				this.getExpirationDate().toString()+","+role+","+this.getPermission()+","+this.getHmac();
		return stringTicket.getBytes();	
	}

	
		
}
