package com.csci6545.evoting.be;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.X509Certificate;
import java.util.Map;

import com.csci6545.evoting.bl.MgrIntegrity;

public class Supervisor extends Voter {
	
	private X509Certificate certificate;
	private PublicKey voterPublickey;
	private byte []  blindBallot;
	private byte []  voterSignature;
	private byte []  myBlindSignature;
	private byte [] nuance;
	private byte [] sessionKey;
	
	public byte[] getSessionKey() {
		return sessionKey;
	}

	public void setSessionKey(byte[] sessionKey) {
		this.sessionKey = sessionKey;
	}

	public byte[] getNuance() {
		return nuance;
	}

	public void setNuance(byte[] nuance) {
		this.nuance = nuance;
	}

	public byte[] getBlindBallot() {
		return blindBallot;
	}

	public void setBlindBallot(byte[] blindBallot) {
		this.blindBallot = blindBallot;
	}

	public PublicKey getVoterPublickey() {
		return voterPublickey;
	}

	public void setVoterPublickey(PublicKey voterPublickey) {
		this.voterPublickey = voterPublickey;
	}


	public byte[] getVoterSignature() {
		return voterSignature;
	}

	public void setVoterSignature(byte[] voterSignature) {
		this.voterSignature = voterSignature;
	}

	public byte[] getMyBlindSignature() {
		return myBlindSignature;
	}

	public void setMyBlindSignature(byte[] myBlindSignature) {
		this.myBlindSignature = myBlindSignature;
	}

	public X509Certificate getCertificate() {
		return certificate;
	}

	public void setCertificate(X509Certificate certificate) {
		this.certificate = certificate;
	}
	
	public static void createElection(Election election)
	{
		// create Election
	}
		
	public static void checkVoterEligibility(Voter voter)
	{
		//check user eligibility
	}
	
	public static void readAudit(File f)
	{
		// read Audit file
		try {
			BufferedReader br = new BufferedReader(new FileReader(f.getPath()));
			String line = "";
			try {
				while ((line = br.readLine()) != null)
				{
					System.out.println(line);
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public byte[] signBlindBallot(byte[] bb, PrivateKey privKey)
	{
		// sign the ballot
		return MgrIntegrity.signBlindedSignature(privKey, bb);
	}
	
	public static void sendSignedBallot(byte[] sign)
	{
		// send the signed ballot
	}
	
	public static void publishAllBlindBallot(Map<byte[],byte[]> bblist, String userid)
	{
		// publish all blind ballot
	}
	
	public PublicKey loadPublickey(String path) {
		return MgrIntegrity.loadpubkey(path);
	}
	
	public byte[] generateNuance()
	{
		// Generate random nuance
		return null;
	}
}
