package com.csci6545.evoting.be;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Date;
import java.util.List;

import com.csci6545.evoting.bl.MgrClone;
import com.csci6545.evoting.bl.MgrConfidentiality;
import com.csci6545.evoting.bl.MgrIntegrity;

public class User {
	// Change id to long in Mysql
	private long id;
	private String fName;
	private String lName;
	private String title;
	private Date dob;
	private String hashDob;
	private String hashPwd;
	private PublicKey myPublicKey;
	private PrivateKey myPrivateKey;
	private String userId; // Authentication
	private String password; // Authentication
	private TicketBB ticketBB;
	private String email;
	private int isOtp;
	private int loginAttempts;
	private int permission;

	public int getPermission() {
		return permission;
	}

	public void setPermission(int permission) {
		this.permission = permission;
	}

	public int getIsOtp() {
		return isOtp;
	}

	public void setIsOtp(int isOtp) {
		this.isOtp = isOtp;
	}

	public int getLoginAttempts() {
		return loginAttempts;
	}

	public void setLoginAttempts(int loginAttempts) {
		this.loginAttempts = loginAttempts;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	// add key to decrypt the private key
	// add iv variable to decrypt the private key
	// add passKey variable
	// add passKeyHasher method
	private byte[] keyForPrivKey;
	private byte[] ivForPrivKey;
	private String passKey;

	@Override
	public String toString() {
		return "User [id=" + id + ", fName=" + fName + ", lName=" + lName
				+ ", title=" + title + ", dob=" + dob + "]";
	}

	public String getHashDob() {
		return hashDob;
	}

	public void setHashDob(String hashDob) {
		this.hashDob = hashDob;
	}

	public String getHashPwd() {
		return hashPwd;
	}

	public void setHashPwd(String hashPwd) {
		this.hashPwd = hashPwd;
	}

	public TicketBB getTicketBB() {
		return ticketBB;
	}

	public void setTicketBB(TicketBB ticketBB) {
		this.ticketBB = ticketBB;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public PublicKey getMyPublicKey() {
		return myPublicKey;
	}

	public void setMyPublicKey(PublicKey myPublicKey) {
		this.myPublicKey = myPublicKey;
	}

	public PrivateKey getMyPrivateKey() {

		return myPrivateKey;
	}

	public void setMyPrivateKey(PrivateKey myPrivateKey) {
		this.myPrivateKey = myPrivateKey;
	}

	public Date getDob() {
		return (Date) dob.clone();
	}

	public void setDob(Date dob) {
		this.dob = (Date) dob.clone();
	}

	public byte[] getKeyForPrivKey() {
		return MgrClone.cloneByteArray(keyForPrivKey);
	}

	public void setKeyForPrivKey(byte[] keyForPrivKey) {
		this.keyForPrivKey = MgrClone.cloneByteArray(keyForPrivKey);
	}

	public byte[] getIvForPrivKey() {
		return MgrClone.cloneByteArray(ivForPrivKey);
	}

	public void setIvForPrivKey(byte[] ivForPrivKey) {
		this.ivForPrivKey = MgrClone.cloneByteArray(ivForPrivKey);
	}

	public String getPassKey() {
		return passKey;
	}

	public void setPassKey(String passKey) {
		this.passKey = passKey;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getfName() {
		return fName;
	}

	public void setfName(String fName) {
		this.fName = fName;
	}

	public String getlName() {
		return lName;
	}

	public void setlName(String lName) {
		this.lName = lName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int calculateAge(Date dob) {

		// Take the dob and calculate age
		return 0;
	}

	public byte[] passKeyHasher(String passKey) {

		// hash passkey and spit first 128 bits to be a key
		// and the following 128 bits to be an iv
		return null;
	}

	public byte[] loadMyPrivateKey(String filename, byte[] key, byte[] iv) {

		return MgrConfidentiality.getPrivateKey(filename, key, iv);
	}

	public byte[] sign(byte[] msg, PrivateKey counterPrivKey) {
		// return signature
		return MgrIntegrity.sign(counterPrivKey, msg);
	}

	public boolean verify(byte[] msg, byte[] signature, PublicKey PublicKey) {
		// verify signature
		return MgrIntegrity.verify(PublicKey, msg, signature);
	}

	public User clone() {
		User returnUser = new User();
		if (this.dob != null) {
			returnUser.setDob((Date) this.dob.clone());
		}
		if (this.email != null) {
			returnUser.setEmail(this.email);
		}
		if (this.fName != null) {
			returnUser.setfName(this.fName);
		}
		if (this.hashDob != null) {
			returnUser.setHashDob(this.hashDob);
		}
		if (this.hashPwd != null) {
			returnUser.setHashPwd(this.hashPwd);
		}

		returnUser.setId(this.id);

		returnUser.setIsOtp(this.isOtp);

		if (this.ivForPrivKey != null) {
			returnUser.setIvForPrivKey(MgrClone
					.cloneByteArray(this.ivForPrivKey));
		}
		if (this.keyForPrivKey != null) {
			returnUser.setKeyForPrivKey(MgrClone
					.cloneByteArray(this.keyForPrivKey));
		}
		if (this.lName != null) {
			returnUser.setlName(this.lName);
		}
		returnUser.setLoginAttempts(this.loginAttempts);
		if (this.myPrivateKey != null) {
			returnUser.setMyPrivateKey(this.myPrivateKey);
		}
		if (this.myPublicKey != null) {
			returnUser.setMyPublicKey(this.myPublicKey);
		}
		if (this.passKey != null) {
			returnUser.setPassKey(this.passKey);
		}
		if (this.password != null) {
			returnUser.setPassword(this.password);
		}
		returnUser.setPermission(this.permission);
		if (this.ticketBB != null) {
			returnUser.setTicketBB(this.ticketBB);
		}
		if (this.title != null) {
			returnUser.setTitle(this.title);
		}
		if (this.userId != null) {
			returnUser.setUserId(this.userId);
		}

		return returnUser;
	}

}