package com.csci6545.evoting.be;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Date;
import java.util.InputMismatchException;
import java.util.Map;
import java.util.HashMap;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import Decoder.BASE64Encoder;

import com.csci6545.evoting.bl.MgrBytes;
import com.csci6545.evoting.bl.MgrClone;
import com.csci6545.evoting.bl.MgrMessage;

public class Election {
	private String name;
	private long id;
	private Timestamp start;
	private Timestamp end;
	/**
	 * {@candidate} Map<candidate ID String, candidate's Name>
	 */
	private Map<String,String> candidate;
	/**
	 * {@winners} Map<winner ID String, winner's Name>
	 */
	private Map<String,String> winners;
	private Counter counter;
	


	public Counter getCounter() {
		return counter;
	}

	public void setCounter(Counter counter) {
		this.counter = counter;
	}

	private static boolean checkIllegalString(String s){
		Pattern illegalPattern = Pattern.compile("[^0-9A-Za-z ()]");
		Matcher illegalMatcher = illegalPattern.matcher(s);
		return illegalMatcher.find();
	}
	
	private static boolean checkIllegalMap(Map<String,String> map){
		java.util.Set<String> keys = map.keySet();
		java.util.Collection<String> values = map.values();
		for(String k:keys){
			if(checkIllegalString(k)){
				return true;
			}
		}
		for(String v:values){
			if(checkIllegalString(v)){
				return true;
			}
		}
		return false;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getId() {
		return id;
	}
	public void setId(long uid) {
		this.id = uid;
	}
	public Timestamp getStart() {
		return (Timestamp) start.clone();
	}
	public void setStart(Timestamp start) {
		this.start = (Timestamp) start.clone();
	}
	public Timestamp getEnd() {
		return (Timestamp) end.clone();
	}
	public void setEnd(Timestamp end) {
		this.end = (Timestamp) end.clone();
	}
	public Map<String, String> getCandidate() {
		//return MgrClone.cloneStringStringMap(candidate);
		return candidate;
	}
	public void setCandidate(Map<String, String> candidate) {
		if(checkIllegalMap(candidate)){
			throw new InputMismatchException("The candidate contains"+
					"illegal character!");
		}
		this.candidate = MgrClone.cloneStringStringMap(candidate);
	}
	public Map<String, String> getWinners() {
		return MgrClone.cloneStringStringMap(winners);
	}
	public void setWinners(Map<String, String> winners) {
			if(checkIllegalMap(winners)){
					throw new InputMismatchException("The winners contains"+
						"illegal character!");
			}
		this.winners = MgrClone.cloneStringStringMap(winners);
	}
	
	public byte[] getBytes()
	{
		BASE64Encoder encoder = new BASE64Encoder();
		String base64Candidate = encoder.encodeBuffer(MgrBytes.stringStringMapToBytes(this.getCandidate()));
		String electionStr = this.getId()+","+this.getName()+","+this.getStart().toString()
				+","+this.getEnd().toString()+","+counter.getUserId()+","+counter.getfName()
				+","+counter.getlName()+","+base64Candidate;

		//return electionStr.getBytes();
		return MgrClone.cloneByteArray(electionStr.getBytes());
	}
	
}
