package com.csci6545.evoting.be;

import java.net.InetAddress;
import java.util.Date;

// this class is created to handle each action during auditing
public class Event {

	public static final String ALERT = "ALERT";
	public static final String INFO = "INFO";
	public static final String WARN = "WARNING";
	public static final String ERROR = "ERROR";
	
	private String severity;
	private String objectName;
	private InetAddress location; // IP address of the machine
	private Date time;
	private String detail;

	public Event(){}
	public Event(String severity,
			String objectName, InetAddress inetAddress, Date time,
			String detail) {
		
		this.severity = severity;
		this.objectName = objectName;
		this.location = inetAddress;
		this.time = time;
		this.detail = detail;
	}
	
	

	public String getSeverity() {
		return severity;
	}
	public void setSeverity(String severity) {
		this.severity = severity;
	}
	public Date getTime() {
		return (Date) time.clone();
	}

	public void setTime(Date time) {
		this.time = (Date) time.clone();
	}


	

	public String getObjectName() {
		return objectName;
	}

	public void setObjectName(String objectName) {
		this.objectName = objectName;
	}

	public InetAddress getLocation() {
		return location;
	}

	public void setLocation(InetAddress location) {
		this.location = location;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	@Override
	public String toString() {
		return severity + " - " + time + " - "   + objectName + " - " + location + " - " + detail; 
	}

	

}
