package com.csci6545.evoting.be;

import java.math.BigInteger;
import java.security.Key;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import Decoder.BASE64Encoder;

import com.csci6545.evoting.bl.MgrBallot;
import com.csci6545.evoting.bl.MgrElection;
import com.csci6545.evoting.bl.MgrResult;
import com.csci6545.evoting.bl.MgrTitle;
import com.csci6545.evoting.bl.MgrUser;
import com.csci6545.evoting.bl.MgrClone;


public class Message {

	private String type;
	private PrivateKey privateKey;
	private String voterId;
	private String userTitle;
	private String userEmail;
	private String counterIPAddress;
	private int titleAmount;
	private BigInteger sha;
	private byte [] hybrid;
	private byte [] supSigOnBcBallot;
	private byte [] counterSignature;
	private int counterPort;
	public int getCounterPort() {
		return counterPort;
	}
	public void setCounterPort(int counterPort) {
		this.counterPort = counterPort;
	}
	public byte[] getCounterSignature() {
		return counterSignature;
	}
	public void setCounterSignature(byte[] counterSignature) {
		this.counterSignature = counterSignature;
	}
	public byte[] getSupSigOnBcBallot() {
		return MgrClone.cloneByteArray(supSigOnBcBallot);
	}
	public void setSupSigOnBcBallot(byte[] supSigOnBcBallot) {
		this.supSigOnBcBallot = MgrClone.cloneByteArray(supSigOnBcBallot);
	}
	public byte[] getHybrid() {
		return MgrClone.cloneByteArray(hybrid);
	}
	public void setHybrid(byte[] hybrid) {
		this.hybrid = MgrClone.cloneByteArray(hybrid);
	}
	public BigInteger getSha() {
		return sha;
	}
	public void setSha(BigInteger sha) {
		this.sha = sha;
	}
	public int getTitleAmount() {
		return titleAmount;
	}
	public void setTitleAmount(int titleAmount) {
		this.titleAmount = titleAmount;
	}
	public String getCounterIPAddress() {
		return counterIPAddress;
	}
	public void setCounterIPAddress(String counterIPAddress) {
		this.counterIPAddress = counterIPAddress;
	}

	private int isOtp;
	public int getIsOtp() {
		return isOtp;
	}
	public void setIsOtp(int isOtp) {
		this.isOtp = isOtp;
	}
	public String getUserEmail() {
		return userEmail;
	}
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	public String getUserTitle() {
		return userTitle;
	}
	public void setUserTitle(String userTitle) {
		this.userTitle = userTitle;
	}
	public String getVoterId() {
		return voterId;
	}
	public void setVoterId(String voterId) {
		this.voterId = voterId;
	}

	private PublicKey voterPublicKey;
	private PublicKey userPublicKey;
	private PublicKey supPublicKey;
	private PublicKey counterPublicKey;
	private String username;
	private String password;
	private String dob;
	private BigInteger supSignature;
	private BigInteger userSignature;
	private TicketBB ticketBB;
	private String supUsername;	
	private long electionId;
	private String electionName;
	private Date startDate;
	private Date endDate;
	private TicketElection ticketElection;
	private BigInteger blindBallot;
	private BigInteger blindSignature;
	private String c;
	private BigInteger bcBallot;
	private Ballot ballot;
	private int entry;
	private BigInteger keyBcBallot;
	private BigInteger ivBcBallot;
	private BcBallotFactor bcBallotFactor;
	private String detail;
	private int amountOfEntry;
	private BcBallotFactor[] bcArray;
	private String fname;
	private String lname;
	private Election election;
	private Election[] electionArray;
	private User[] userArray;
	private User[] allowUserArray;
	private User[] denyUserArray;
	private List<String> allAuthorizedUserBallot;
	public List<String> getAllAuthorizedUserBallot() {
		return allAuthorizedUserBallot;
	}
	public void setAllAuthorizedUserBallot(List<String> allAuthorizedUserBallot) {
		this.allAuthorizedUserBallot = allAuthorizedUserBallot;
	}
	public User[] getAllowUserArray() {
		return MgrClone.cloneUserArray(allowUserArray);
		//return allowUserArray;
	}
	public void setAllowUserArray(User[] allowUserArray) {
		this.allowUserArray =MgrClone.cloneUserArray(allowUserArray);
		//this.allowUserArray = allowUserArray;
	}
	public User[] getDenyUserArray() {
		return MgrClone.cloneUserArray(denyUserArray);
		//return denyUserArray;
	}
	public void setDenyUserArray(User[] denyUserArray) {
		this.denyUserArray = MgrClone.cloneUserArray(denyUserArray);
		//this.denyUserArray = denyUserArray;
	}

	private String sqlStatement;
	private User user;
	private int permission;
	private List<Title> title;

	public List<Title> getTitle() {
		return title;
	}
	public void setTitle(List<Title> title) {
		this.title = title;
	}
	public int getPermission() {
		return permission;
	}
	public void setPermission(int permission) {
		this.permission = permission;
	}
	public PublicKey getUserPublicKey() {
		return userPublicKey;
	}
	public void setUserPublicKey(PublicKey userPublicKey) {
		this.userPublicKey = userPublicKey;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public String getSqlStatement() {
		return sqlStatement;
	}
	public void setSqlStatement(String sqlStatement) {
		this.sqlStatement = sqlStatement;
	}
	public User[] getUserArray() {
		return userArray;
	}
	public void setUserArray(User[] userArray) {
		this.userArray = userArray;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public BcBallotFactor[] getBcArray() {
		return bcArray;
	}
	public void setBcArray(BcBallotFactor[] bcArray) {
		this.bcArray = bcArray;
	}
	public int getAmountOfEntry() {
		return amountOfEntry;
	}
	public void setAmountOfEntry(int amountOfEntry) {
		this.amountOfEntry = amountOfEntry;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public BcBallotFactor getBcBallotFactor() {
		return bcBallotFactor;
	}
	public void setBcBallotFactor(BcBallotFactor bcBallotFactor) {
		this.bcBallotFactor = bcBallotFactor;
	}
	public BigInteger getIvBcBallot() {
		return ivBcBallot;
	}
	public void setIvBcBallot(BigInteger ivBcBallot) {
		this.ivBcBallot = ivBcBallot;
	}
	public BigInteger getKeyBcBallot() {
		return keyBcBallot;
	}
	public void setKeyBcBallot(BigInteger keyBcBallot) {
		this.keyBcBallot = keyBcBallot;
	}
	public PrivateKey getPrivateKey() {
		return privateKey;
	}
	public void setPrivateKey(PrivateKey privateKey) {
		this.privateKey = privateKey;
	}
	public PublicKey getVoterPublicKey() {
		return voterPublicKey;
	}
	public void setVoterPublicKey(PublicKey voterPublicKey) {
		this.voterPublicKey = voterPublicKey;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public PublicKey getSupPublicKey() {
		return supPublicKey;
	}
	public void setSupPublicKey(PublicKey supPublicKey) {
		this.supPublicKey = supPublicKey;
	}
	public PublicKey getCounterPublicKey() {
		return counterPublicKey;
	}
	public void setCounterPublicKey(PublicKey counterPublicKey) {
		this.counterPublicKey = counterPublicKey;
	}
	public BigInteger getSupSignature() {
		return supSignature;
	}
	public void setSupSignature(BigInteger supSignature) {
		this.supSignature = supSignature;
	}
	public BigInteger getUserSignature() {
		return userSignature;
	}
	public void setUserSignature(BigInteger userSignature) {
		this.userSignature = userSignature;
	}
	public TicketBB getTicketBB() {
		return ticketBB;
	}
	public void setTicketBB(TicketBB ticketBB) {
		this.ticketBB = ticketBB;
	}
	public String getSupUsername() {
		return supUsername;
	}
	public void setSupUsername(String supUsername) {
		this.supUsername = supUsername;
	}
	public long getElectionId() {
		return electionId;
	}
	public void setElectionId(long electionId) {
		this.electionId = electionId;
	}
	public String getElectionName() {
		return electionName;
	}
	public void setElectionName(String electionName) {
		this.electionName = electionName;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public TicketElection getTicketElection() {
		return ticketElection;
	}
	public void setTicketElection(TicketElection ticketElection) {
		this.ticketElection = ticketElection;
	}
	public BigInteger getBlindBallot() {
		return blindBallot;
	}
	public void setBlindBallot(BigInteger blindBallot) {
		this.blindBallot = blindBallot;
	}
	public BigInteger getBlindSignature() {
		return blindSignature;
	}
	public void setBlindSignature(BigInteger blindSignature) {
		this.blindSignature = blindSignature;
	}
	public String getC() {
		return c;
	}
	public void setC(String c) {
		this.c = c;
	}
	public BigInteger getBcBallot() {
		return bcBallot;
	}
	public void setBcBallot(BigInteger bcBallot) {
		this.bcBallot = bcBallot;
	}
	public Ballot getBallot() {
		return ballot;
	}
	public void setBallot(Ballot ballot) {
		this.ballot = ballot;
	}
	public int getEntry() {
		return entry;
	}
	public void setEntry(int entry) {
		this.entry = entry;
	}

	public Election getElection() {
		return election;
	}
	public void setElection(Election election) {
		this.election = election;
	}
	public byte [] genKeyMsg(){
		//genKey message
		String msg = type+","+username+","+new BigInteger(password.getBytes())+","+dob+","+
		new BigInteger(userPublicKey.getEncoded());		
		return msg.getBytes();
	}
	
	public byte [] ackKeyGenMsg(){
		//give key from KDC
		String msg = type+","+username+","+detail;
		return msg.getBytes();
	}
	
	public byte [] loginMsg()
	{
		//login msg
		String msg = type+","+username+","+new BigInteger(password.getBytes())+","+ userSignature;
		return msg.getBytes();
	}
	
	public byte [] ackLoginMsg(){
		//give Ticket BB from AS
		String msg = type+","+detail+","+fname+","+lname+","+userTitle+","+isOtp+","+userEmail+
				","+permission+","+new String(ticketBB.getBytes());
		return msg.getBytes();
	}
	
	public byte [] counterLoginMsg()
	{
		//login msg
		String msg = type+","+username+","+new BigInteger(password.getBytes());
		return msg.getBytes();
	}
	
	public byte [] ackCounterLoginMsg(){
		//give Ticket BB from AS
		String msg = type+","+detail+","+new String(ticketBB.getBytes());
		return msg.getBytes();
	}
	// Supervisor Get UserInfo
	public byte [] getUserInfoMsg(){
		//create Election Message from supervisor
		String msg = type+","+new String(ticketBB.getBytes());
		return msg.getBytes();
	}
	
	public byte [] ackGetUserInfoMsg(){
		//create Election Message from supervisor
		BASE64Encoder encoder = new BASE64Encoder();
		String msg = type+","+detail+","+amountOfEntry+","+encoder.encodeBuffer(MgrUser.allUser(this.userArray))+","+new String(ticketBB.getBytes());
		return msg.getBytes();
	}
	
	public byte [] getTitleMsg(){
		//create Election Message from supervisor
		String msg = type+","+new String(ticketBB.getBytes());
		return msg.getBytes();
	}
	
	public byte [] ackGetTitleMsg(){
		//create Election Message from supervisor
		String msg="";
		try {
			msg = type+","+detail+","+com.csci6545.evoting.dl.MgrTitle.getAllTitle().size()+","+new BigInteger(MgrTitle.allTitle())+","+new String(ticketBB.getBytes());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return msg.getBytes();
	}
	
	public byte [] createElectionMsg(){
		//create Election Message from supervisor
		BASE64Encoder encoder = new BASE64Encoder();
		String msg = type+","+supUsername+","+new BigInteger(election.getBytes())+","+
				entry+","+encoder.encodeBuffer(MgrUser.overridedPermissionUser(Arrays.asList(this.denyUserArray)))+","+
				amountOfEntry+","+encoder.encodeBuffer(MgrUser.overridedPermissionUser(Arrays.asList(this.allowUserArray)))+","+
				titleAmount+","+new BigInteger(MgrTitle.eligibleTitle(title))+","+new String(ticketBB.getBytes());
		return msg.getBytes();
	}
	
	public byte [] ackCreateElectionMsg(){
		//create Election Message from supervisor
		String msg = type+","+detail+","+new String(ticketBB.getBytes());
		return msg.getBytes();
	}
	
	public byte [] errorMsg(){
		//error message from server
		return null;
	}
	
	//3.5 Ask For election Information 
	public byte[] electionInfoMsg(){
		String msg = type+","+username+","+new String(ticketBB.getBytes());
		return msg.getBytes();
	}
	
	public byte[] ackElectionInfoMsg(){
		BASE64Encoder encoder = new BASE64Encoder();
		String msg = type+","+detail+","+amountOfEntry+","+encoder.encodeBuffer(MgrElection.allElections(electionArray))+","+new String(ticketBB.getBytes());
		return msg.getBytes();
	}
	
	public Election[]  getElectionArray() {
		return electionArray;
	}
	public void setElectionArray(Election[]  electionArray) {
		this.electionArray = electionArray;
	}
	// 4. Register For an Election
	public byte [] registerForElectionMsg(){
		//registerForElectionMessage from voter
		String msg = type+","+electionId+","+new String(ticketBB.getBytes());
		return msg.getBytes();
	}	
	// System gives TicketElection to voter
	public byte [] ackRegisterForElectionMsg(){
		//giveTicketElectionMessage from AS
		String msg = type+","+detail+","+new String(ticketBB.getBytes());
		return msg.getBytes();
	}
	
	// 5. MakeVote
	// send BlindBallot,Sig, TicketElection to supervisor
	public byte [] blindBallotMsg(){
		//blindBallotMsg from a voter to supervisor
		String msg = type+","+blindBallot+","+userSignature+
				","+electionId+","+ new String(ticketBB.getBytes());
		return msg.getBytes();
	}	
	
	// Supervisor send blindSignature to a voter
	public byte [] blindSignatureMsg(){
		//blindSignatureMsg from a sup to voter
		String msg = type+","+detail+","+blindSignature+","+counterIPAddress+","+counterPort+","+
				new BigInteger(counterPublicKey.getEncoded())+","+new String(ticketBB.getBytes());
		return msg.getBytes();
	}
	
	// voter sends sup confirmation of signature verification
	public byte [] confirmVerifySignatureMsg(){
		//manage confirmVerifySignatureMsg from voter
		String msg = type+","+detail+","+userSignature+","+new String(ticketBB.getBytes());
				return msg.getBytes();
	}
	
	//6. Submit and verify
	// voter sends counter the bcBallot
	public byte [] submitBcBallotMsg(){
		//submitBcBallotMsg from voter
		BASE64Encoder encoder = new BASE64Encoder();
		String msg = type+","+bcBallot+","+encoder.encodeBuffer(supSigOnBcBallot)+","+electionId;
		return msg.getBytes();
	}
	
	public byte [] submitKeyMsg(){
		//submitBcBallotMsg from voter
		String msg = type+","+keyBcBallot+","+ivBcBallot+","+entry;
		return msg.getBytes();
	}
	
	public byte [] submitHybridMsg() {
		//submitBcBallotMsg from voter
		String msg = c+","+ivBcBallot+","+sha;
		return msg.getBytes();
	}
	
	public byte [] counterAckMsg(){
		//publishbcBallottMsg from Counter

		String msg = type+","+detail;
		return msg.getBytes();
	}
	
	//Counter publish bcBallot to Bulletin Board
	public byte [] publishBcBallotMsg(){
		//publishbcBallottMsg from Counter
		BASE64Encoder encoder = new BASE64Encoder();
		String msg = type+","+electionId+","+bcBallot+","+supSignature+","+encoder.encodeBuffer(counterSignature);
		return msg.getBytes();
	}
	
	public byte [] ackPublishBcBallotMsg(){
		//publishbcBallottMsg from Counter

		String msg = type+","+detail;
		return msg.getBytes();
	}
	
	public byte [] counterGetBcBallotMsg(){
		//publishbcBallottMsg from Counter
		BASE64Encoder encoder = new BASE64Encoder();
		String msg = type+","+entry+","+encoder.encodeBuffer(counterSignature);
		return msg.getBytes();
	}
	
	public byte [] ackCounterGetBcBallotMsg(){
		//publishbcBallottMsg from Counter

		String msg = type+","+detail+","+new BigInteger(bcBallotFactor.getBytes());
		return msg.getBytes();
	}
	
	// Counter publish Ballot to Bulletin Board
	public byte [] publishBcBallotFactorMsg(){
		//publishbcBallottMsg from Counter
		BASE64Encoder encoder = new BASE64Encoder();
		String msg = type+","+electionId+","+new BigInteger(bcBallotFactor.getBytes());
		return msg.getBytes();
	}
	
	//Server Listener acknowledgment
	public byte [] ackPublishBcBallotFactorMsg(){
		//publishbcBallottMsg from Counter

		String msg = type+","+detail;
		return msg.getBytes();
	}
	

	public byte [] checkBcBallotFactorMsg(){

		String msg = type+","+electionId+","+new BigInteger(ticketBB.getBytes());
		return msg.getBytes();

	}
	
	public byte [] checkAuthorizedBallotMsg(){

		String msg = type+","+electionId+","+new BigInteger(ticketBB.getBytes());
		return msg.getBytes();

	}
	public byte [] ackCheckAuthorizedBallotMsg(){
		BASE64Encoder encoder = new BASE64Encoder();
		String msg = type+","+detail+","+amountOfEntry+","+encoder.encodeBuffer(MgrResult.authorizedUserBallotToBsytes(allAuthorizedUserBallot))
				+","+new BigInteger(ticketBB.getBytes());
		return msg.getBytes();

	}
	
	public byte [] requestBcBallotFactorMsg(){

		String msg = type+","+electionId;
		return msg.getBytes();

	}
	
	public byte [] ackRequestBcBallotFactorMsg(){
		BASE64Encoder encoder = new BASE64Encoder();
		String msg = type+","+detail+","+amountOfEntry+","+electionId+","+encoder.encodeBuffer(MgrBallot.allBcBalot(bcArray))+","+new BigInteger(ticketBB.getBytes());
		return msg.getBytes();

	}
	
	public byte [] replyAckRequestBcBallotFactorMsg(){
		String msg = type+","+detail+","+new BigInteger(ticketBB.getBytes());
		return msg.getBytes();

	}
	
///////bcArray = all BcBalllotFactor concatenated
	public byte [] ackCheckBcBallotFactorMsg(){
		BASE64Encoder encoder = new BASE64Encoder();
		String msg = type+","+detail+","+amountOfEntry+","+new BigInteger(ticketBB.getBytes())+","+encoder.encodeBuffer(MgrBallot.allBcBalot(bcArray));
		//System.out.println(encoder.encodeBuffer(MgrBallot.allBcBalot(bcArray)));
		return msg.getBytes();

	}
	
	public byte [] checkResultMsg(){

		String msg = type+","+new BigInteger(ticketBB.getBytes());
		return msg.getBytes();

	}
	
	public byte [] resetPasswordMsg(){

		String msg = type+","+username;
		return msg.getBytes();

	}
	
	public byte [] ackResetPasswordMsg(){

		String msg = type+","+detail+","+userEmail;
		return msg.getBytes();

	}
	
	public byte [] updatePasswordMsg(){

		String msg = type+","+username+","+password+","+
				new BigInteger(userPublicKey.getEncoded());
		return msg.getBytes();

	}
	
	public byte [] ackUpdatePasswordMsg(){

		String msg = type+","+detail;
		return msg.getBytes();

	}
	public byte [] updatePersonalInfoMsg(){

		String msg = type+","+new BigInteger(MgrUser.updateInfoUserBytes(username, fname, lname, userEmail))+
				","+new BigInteger(ticketBB.getBytes());
		return msg.getBytes();

	}
	
	public byte [] ackUpdatePersonalInfoMsg(){

		String msg = type+","+detail+","+new BigInteger(ticketBB.getBytes());
		return msg.getBytes();

	}
	
	public byte [] logOutMsg()
	{
		String msg = type+","+new BigInteger(ticketBB.getBytes());
		return msg.getBytes();

	}
	
}
