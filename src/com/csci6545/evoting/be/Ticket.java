package com.csci6545.evoting.be;

import java.math.BigInteger;
import java.util.Date;


public class Ticket {

	private String userId;
	private Date issueDate;
	private Date expirationDate;
	private BigInteger signature;
	private BigInteger hmac;
	private int permission;
	
	public int getPermission() {
		return permission;
	}
	public void setPermission(int permission) {
		this.permission = permission;
	}
	public BigInteger getHmac() {
		return hmac;
	}
	public void setHmac(BigInteger hmac) {
		this.hmac = hmac;
	}
	private String userIP;
	
	public String getUserIP() {
		return userIP;
	}
	public void setUserIP(String userIP) {
		this.userIP = userIP;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Date getIssueDate() {
		return issueDate;
	}
	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}
	public Date getExpirationDate() {
		return expirationDate;
	}
	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}
	public BigInteger getSignature() {
		return signature;
	}
	public void setSignature(BigInteger signature) {
		this.signature = signature;
	}

		
}
