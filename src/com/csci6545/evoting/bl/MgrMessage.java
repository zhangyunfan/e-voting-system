package com.csci6545.evoting.bl;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.math.BigInteger;
import java.net.Socket;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.text.SimpleDateFormat;
import java.util.Locale;

import Decoder.BASE64Decoder;
import Decoder.BASE64Encoder;

import com.csci6545.evoting.be.BcBallotFactor;
import com.csci6545.evoting.be.Counter;
import com.csci6545.evoting.be.Election;
import com.csci6545.evoting.be.Message;
import com.csci6545.evoting.be.Supervisor;
import com.csci6545.evoting.be.TicketBB;
import com.csci6545.evoting.be.TicketElection;
import com.csci6545.evoting.be.Title;
import com.csci6545.evoting.be.User;
import com.csci6545.evoting.be.Voter;

public class MgrMessage {	
	
	public static String getMessgaeType(byte[] msgBytes)
	{
		try
		{
			String stringTicket = new String(msgBytes);
			String[] parts = new String [2];
			int i = 0;
			for (String retval: stringTicket.split(",",2)){
			    parts[i] = retval;
			    i++;
			 }
			return parts[0];
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return null;
	}
	
	public static byte[] genKeyMsg(String username, String password,
			Date dob, PublicKey publicKey) throws NoSuchAlgorithmException{
		//genKey message
		Message m = new Message();
		m.setUsername(username);
		m.setPassword(password);
		m.setUserPublicKey(publicKey);
		m.setType("GenKey");
		m.setDob(dob.toString());
		return m.genKeyMsg();
	}
	public static Message recoverGenKeyMsg(byte [] msgBytes){
		//genKey message
		try
		{
			Message msg = new Message();
			String stringTicket = new String(msgBytes);
			String[] parts = new String [5];
			int i = 0;
			for (String retval: stringTicket.split(",",5)){
			    parts[i] = retval;
			    i++;
			 }
			msg.setType(parts[0]);
			msg.setUsername(parts[1]);
			msg.setPassword(new String(new BigInteger(parts[2]).toByteArray()));
			msg.setDob(parts[3]);
			msg.setUserPublicKey(MgrIntegrity.byteToPublicKey(new BigInteger(parts[4]).toByteArray()));
			return msg;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return null;
	}
	
	public static byte[] ackKeyGenMsg(String detail, String username){
		//give key from KDC
		Message m = new Message();
		m.setType("AckKeyGen"); // GenKeySucceeded || GenKeyFailed
		m.setUsername(username);
		m.setDetail(detail);
		return m.ackKeyGenMsg();
	}
	
	public static Message recoverAckKeyGenMsg(byte [] msgBytes){
		//genKey message
		try
		{
			Message msg = new Message();
			String stringTicket = new String(msgBytes);
			String[] parts = new String [3];
			int i = 0;
			for (String retval: stringTicket.split(",",3)){
			    parts[i] = retval;
			    i++;
			 }
			msg.setType(parts[0]);
			msg.setUsername(parts[1]);
			msg.setDetail(parts[2]);
			return msg;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return null;
	}
	
	public static byte[] loginMsg(String username, String password, PrivateKey privateKey) throws NoSuchAlgorithmException{
		//login msg
		String msg = username+","+password;
		BigInteger signature = new BigInteger(MgrIntegrity.sign(privateKey,msg.getBytes()));
		Message m = new Message();
		m.setUsername(username);
		m.setPassword(password);
		m.setType("Login");
		m.setUserSignature(signature);
		return m.loginMsg();
	}
	
	public static Message recoverLoginMsg(byte [] msgBytes){
		//genKey message
		try
		{
			Message msg = new Message();
			String stringTicket = new String(msgBytes);
			String[] parts = new String [4];
			int i = 0;
			for (String retval: stringTicket.split(",",4)){
			    parts[i] = retval;
			    i++;
			 }
			msg.setType(parts[0]);
			msg.setUsername(parts[1]);
			msg.setPassword(new String(new BigInteger(parts[2]).toByteArray()));
			msg.setUserSignature(new BigInteger(parts[3]));
			return msg;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return null;
	}

	public static byte[] ackLoginMsg(TicketBB ticketBB, String detail, User user){
		//give Ticket BB from AS
		Message m = new Message();
		m.setType("AckLogin");
		m.setDetail(detail);
		m.setTicketBB(ticketBB);
		m.setIsOtp(user.getIsOtp());
		m.setFname(user.getfName());
		m.setLname(user.getlName());
		m.setUserEmail(user.getEmail());
		m.setUserTitle(user.getTitle());
		m.setPermission(user.getPermission());
		return m.ackLoginMsg();
	}
	
	public static Message recoverAckLoginMsg(byte [] msgBytes){
		//genKey message
				try
				{
					User u = null;
					Message msg = new Message();
					String stringTicket = new String(msgBytes);
					String[] parts = new String [9];
					int i = 0;
					for (String retval: stringTicket.split(",",9)){
					    parts[i] = retval;
					    i++;
					 }
					if(Integer.parseInt(parts[7]) > 3)
					{
						u = new Supervisor();
					}
					else
					{
						u = new Voter();
					}
					u.setfName(parts[2]);
					u.setlName(parts[3]);
					u.setTitle(parts[4]);
					u.setIsOtp(Integer.parseInt(parts[5]));
					u.setEmail(parts[6]);
					u.setPermission(Integer.parseInt(parts[7]));
					u.setTicketBB(MgrTicket.recoverTicketBB(parts[8].getBytes()));
					msg.setType(parts[0]);
					msg.setDetail(parts[1]);
					msg.setUser(u);
					msg.setFname(parts[2]);
					msg.setLname(parts[3]);
					msg.setUserTitle(parts[4]);
					msg.setIsOtp(Integer.parseInt(parts[5]));
					msg.setUserEmail((parts[6]));	
					msg.setTicketBB(MgrTicket.recoverTicketBB(parts[8].getBytes()));		
					
					return msg;
				}
				catch(Exception e)
				{
					System.out.println(e);
				}
				return null;
	}
	
	public static byte[] counterLoginMsg(String username, String password){
		//login msg
		Message m = new Message();
		m.setUsername(username);
		m.setPassword(password);
		m.setType("CounterLogin");
		return m.counterLoginMsg();
	}
	
	public static Message recoverCounterLoginMsg(byte [] msgBytes){
		//genKey message
		try
		{
			Message msg = new Message();
			String stringTicket = new String(msgBytes);
			String[] parts = new String [3];
			int i = 0;
			for (String retval: stringTicket.split(",",3)){
			    parts[i] = retval;
			    i++;
			 }
			msg.setType(parts[0]);
			msg.setUsername(parts[1]);
			msg.setPassword(new String(new BigInteger(parts[2]).toByteArray()));
			return msg;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return null;
	}
	
	public static byte[] ackCounterLoginMsg(TicketBB ticketBB, String detail){
		//give Ticket BB from AS
		Message m = new Message();
		m.setType("AckCounterLogin");
		m.setDetail(detail);
		m.setTicketBB(ticketBB);
		return m.ackCounterLoginMsg();
	}
	
	public static Message recoverAckCounterLoginMsg(byte [] msgBytes){
		//genKey message
				try
				{
					User u = null;
					Message msg = new Message();
					String stringTicket = new String(msgBytes);
					String[] parts = new String [3];
					int i = 0;
					for (String retval: stringTicket.split(",",3)){
					    parts[i] = retval;
					    i++;
					 }
					msg.setType(parts[0]);
					msg.setDetail(parts[1]);
					msg.setTicketBB(MgrTicket.recoverTicketBB(parts[2].getBytes()));		
					
					return msg;
				}
				catch(Exception e)
				{
					System.out.println(e);
				}
				return null;
	}
	
	// Supervisor Get UserInfo
	public static byte [] getUserInfoMsg(TicketBB ticketBB){
		//create Election Message from supervisor
		Message m = new Message();
		m.setType("getUserInfo");
		m.setTicketBB(ticketBB);
		return m.getUserInfoMsg();
	}
	public static Message recoverGetUserInfoMsg(byte [] msgBytes)
	{
		try
		{
			Message msg = new Message();
			String stringTicket = new String(msgBytes);
			String[] parts = new String [2];
			int i = 0;
			for (String retval: stringTicket.split(",",2)){
			    parts[i] = retval;
			    i++;
			 }
			msg.setType(parts[0]);
			msg.setTicketBB(MgrTicket.recoverTicketBB(parts[1].getBytes()));
			return msg;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return null;
	}
		
	public static byte[] ackGetUserInfoMsg(String detail, int numberOfEntries, User [] user, TicketBB ticketBB)
	{
		Message m = new Message();
		m.setType("AckUserInfo"); 
		m.setDetail(detail);
		m.setAmountOfEntry(numberOfEntries);
		m.setUserArray(user); //--> All User Array
		m.setTicketBB(ticketBB);
		return m.ackGetUserInfoMsg();

	}
	public static Message recoverAckGetUserInfoMsg(byte[] msgBytes)
	{
		try
		{
			BASE64Decoder decoder  = new BASE64Decoder();
			Message msg = new Message();
			String stringTicket = new String(msgBytes);
			String[] parts = new String [5];
			int i = 0;
			for (String retval: stringTicket.split(",",5)){
			    parts[i] = retval;
			    i++;
			 }
			msg.setType(parts[0]); 
			msg.setDetail(parts[1]);
			msg.setAmountOfEntry(Integer.parseInt(parts[2]));
			msg.setUserArray(MgrUser.recoverUserArray(msg.getAmountOfEntry(), 
					decoder.decodeBuffer(parts[3])));
			msg.setTicketBB(MgrTicket.recoverTicketBB(parts[4].getBytes()));
			return msg;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return null;
	}
	
	
	public static byte[] getTitleMsg(TicketBB ticketBB){
		//create Election Message from supervisor
		Message m = new Message();
		m.setType("GetTitle");
		m.setTicketBB(ticketBB);
		return m.getTitleMsg();
	}
	
	public static Message recoverGetTitleMsg(byte [] msgBytes){
		//genKey message
		try
		{
			Message msg = new Message();
			String stringTicket = new String(msgBytes);
			String[] parts = new String [2];
			int i = 0;
			for (String retval: stringTicket.split(",",2)){
			    parts[i] = retval;
			    i++;
			 }
			msg.setType(parts[0]);
			msg.setTicketBB(MgrTicket.recoverTicketBB(parts[1].getBytes()));
			return msg;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return null;
	}
	
	public static byte[] ackgGetTitleMsg(String detail,TicketBB ticketBB){
		//create Election Message from supervisor
		Message m = new Message();
		m.setType("AckGetTitle");
		m.setTicketBB(ticketBB);
		m.setDetail(detail);
		return m.ackGetTitleMsg();
	}
	
	public static Message recoverAckgGetTitleMsg(byte [] msgBytes){
		//genKey message
		try
		{
			Message msg = new Message();
			String stringTicket = new String(msgBytes);
			String[] parts = new String [5];
			int i = 0;
			for (String retval: stringTicket.split(",",5)){
			    parts[i] = retval;
			    i++;
			 }
			msg.setType(parts[0]);
			msg.setDetail(parts[1]);
			msg.setTitle(MgrTitle.recoverTitleList(Integer.parseInt(parts[2]), new BigInteger(parts[3]).toByteArray()));
			msg.setTicketBB(MgrTicket.recoverTicketBB(parts[4].getBytes()));
			return msg;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return null;
	}
	
	public static byte[] createElectionMsg(String supUsername, Election election, 
			TicketBB ticketBB, List<User> allowUsers, List<User> denyUsers, List<Title> listTitle){
		//create Election Message from supervisor
		Message m = new Message();
		m.setType("CreateElection");
		m.setSupUsername(supUsername);
		m.setElection(election);
		m.setTicketBB(ticketBB);
		m.setEntry(denyUsers.size());
		m.setDenyUserArray(denyUsers.toArray(new User[denyUsers.size()]));
		m.setAmountOfEntry(allowUsers.size());
		m.setAllowUserArray(allowUsers.toArray(new User[allowUsers.size()]));
		m.setTitleAmount(listTitle.size());
		m.setTitle(listTitle);
		return m.createElectionMsg();
	}
	
	public static Message recoverCreateElectionMsg(byte [] msgBytes){
		//genKey message
		try
		{
			BASE64Decoder decoder = new BASE64Decoder();
			Message msg = new Message();
			String stringTicket = new String(msgBytes);
			
			String[] parts = new String [10];
			int i = 0;
			for (String retval: stringTicket.split(",",10)){
			    parts[i] = retval;
			    i++;
			 }
			msg.setType(parts[0]);
			msg.setSupUsername(parts[1]);
			msg.setElection(MgrElection.recoverElection(new BigInteger(parts[2]).toByteArray()));
			msg.setDenyUserArray(MgrUser.recoverOverridedPermissionUser(Integer.parseInt(parts[3]), 
					decoder.decodeBuffer(parts[4])).toArray(new User[Integer.parseInt(parts[3])]));
			msg.setAllowUserArray(MgrUser.recoverOverridedPermissionUser(Integer.parseInt(parts[5]), 
					decoder.decodeBuffer(parts[6])).toArray(new User[Integer.parseInt(parts[5])]));
			msg.setTitle(MgrTitle.recoverTitleList(Integer.parseInt(parts[7]), new BigInteger(parts[8]).toByteArray()));
			msg.setTicketBB(MgrTicket.recoverTicketBB(parts[9].getBytes()));
			return msg;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return null;
	}
	
	public static byte [] ackCreateElectionMsg(String detail, String supUsername, long electionId, TicketBB ticketBB){
		//create Election Message from supervisor
		Message m = new Message();
		m.setType("AckCreateElection");// ElectionCreateSucceeded || ElectionCreatedFailed
		m.setDetail(detail);
		m.setTicketBB(ticketBB);
		return m.ackCreateElectionMsg();
	}
	
	public static Message recoverAckCreateElectionMsg(byte [] msgBytes){
		//genKey message
		try
		{
			Message msg = new Message();
			String stringTicket = new String(msgBytes);
			String[] parts = new String [3];
			int i = 0;
			for (String retval: stringTicket.split(",",3)){
			    parts[i] = retval;
			    i++;
			 }
			msg.setType(parts[0]);
			msg.setDetail(parts[1]);
			msg.setTicketBB(MgrTicket.recoverTicketBB(parts[2].getBytes()));
			return msg;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return null;
	}
	
	public static byte [] errorMsg(){
		//error message from server
		return null;
	}
	
	//3.5 
	public static byte[] getElectionInfoMsg(String type, TicketBB ticketBB, String username)
	{
		Message m = new Message();
		m.setType(type);//getElectionInfo || getRegisteredElections || GetAllElections
		m.setUsername(username);
		m.setTicketBB(ticketBB);	
		return m.electionInfoMsg();
	}
	public static Message recoverGetElectionInfoMsg(byte [] msgBytes)
	{
		try
		{
			Message msg = new Message();
			String stringTicket = new String(msgBytes);
			String[] parts = new String [3];
			int i = 0;
			for (String retval: stringTicket.split(",",3)){
			    parts[i] = retval;
			    i++;
			 }
			msg.setType(parts[0]);
			msg.setUsername(parts[1]);
			msg.setTicketBB(MgrTicket.recoverTicketBB(parts[2].getBytes()));
			return msg;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return null;
	}
	public static byte[] ackGetElectionInfoMsg(String detail, int numberOfEntries, Election [] election, TicketBB ticketBB)
	{
		Message m = new Message();
		m.setType("AckElectionInfo"); 
		m.setDetail(detail);
		m.setAmountOfEntry(numberOfEntries);
		m.setElectionArray(election); //--> All Elections concatenated
		m.setTicketBB(ticketBB);
		return m.ackElectionInfoMsg();

	}
	public static Message recoverAckGetElectionInfoMsg(byte[] msgBytes)
	{
		try
		{
			BASE64Decoder decoder = new BASE64Decoder();
			Message msg = new Message();
			String stringTicket = new String(msgBytes);
			String[] parts = new String [5];
			int i = 0;
			for (String retval: stringTicket.split(",",5)){
			    parts[i] = retval;
			    i++;
			 }
			msg.setType(parts[0]); 
			msg.setDetail(parts[1]);
			msg.setAmountOfEntry(Integer.parseInt(parts[2]));
			msg.setElectionArray(MgrElection.recoverElectionArray(msg.getAmountOfEntry(), 
					decoder.decodeBuffer(parts[3])));
			msg.setTicketBB(MgrTicket.recoverTicketBB(parts[4].getBytes()));
			return msg;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return null;
	}
	// 4. Register For an Election
	public static byte [] registerForElectionMsg(long electionId, TicketBB ticketBB){
		//registerForElectionMessage from voter
		Message m = new Message();
		m.setType("RegisterElection");
		m.setElectionId(electionId);
		m.setTicketBB(ticketBB);
		return m.registerForElectionMsg();
	}
	
	public static Message recoverRegisterForElectionMsg(byte [] msgBytes){
		//genKey message
		try
		{
			Message msg = new Message();
			String stringTicket = new String(msgBytes);
			String[] parts = new String [3];
			int i = 0;
			for (String retval: stringTicket.split(",",3)){
			    parts[i] = retval;
			    i++;
			 }
			msg.setType(parts[0]);
			msg.setElectionId(Long.parseLong(parts[1]));
			msg.setTicketBB(MgrTicket.recoverTicketBB(parts[2].getBytes()));
			return msg;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return null;
	}
	
	// System gives TicketElection to voter
	public static byte [] ackRegisterForElectionMsg(String detail, TicketBB ticketBB){
		//giveTicketElectionMessage from AS
		Message m = new Message();
		m.setType("AckRegister");
		m.setDetail(detail);
		m.setTicketBB(ticketBB);
		return m.ackRegisterForElectionMsg();
	}
	
	public static Message recoverAckRegisterForElectionMsg(byte [] msgBytes){
		//genKey message
		try
		{
			Message msg = new Message();
			String stringTicket = new String(msgBytes);
			String[] parts = new String [3];
			int i = 0;
			for (String retval: stringTicket.split(",",3)){
			    parts[i] = retval;
			    i++;
			 }
			msg.setType(parts[0]);
			msg.setDetail(parts[1]);
			msg.setTicketBB(MgrTicket.recoverTicketBB(parts[2].getBytes()));
			return msg;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return null;
	}
	
	// 5. MakeVote
	// send BlindBallot,Sig, TicketElection to supervisor
	public static byte [] blindBallotMsg(byte [] blindBallot, byte [] signature, 
			TicketBB ticketBB, long electionId){
		//blindBallotMsg from a voter to supervisor
		Message m = new Message();
		m.setType("BlindBallot");
		m.setBlindBallot(new BigInteger(blindBallot));
		m.setUserSignature(new BigInteger(signature));
		m.setElectionId(electionId);
		m.setTicketBB(ticketBB);
		return m.blindBallotMsg();
	}
	
	public static Message recoverBlindBallotMsg(byte [] msgBytes){
		//genKey message
		try
		{
			Message msg = new Message();
			String stringTicket = new String(msgBytes);
			String[] parts = new String [5];
			int i = 0;
			for (String retval: stringTicket.split(",",5)){
			    parts[i] = retval;
			    i++;
			 }
			msg.setType(parts[0]);
			msg.setBlindBallot(new BigInteger(parts[1]));
			msg.setUserSignature(new BigInteger(parts[2]));
			msg.setElectionId(Long.parseLong(parts[3]));
			msg.setTicketBB(MgrTicket.recoverTicketBB(parts[4].getBytes()));
			return msg;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return null;
	}
	
	// Supervisor send blindSignature to a voter
	public static byte [] blindSignatureMsg(String detail, byte [] blindSignature, 
			TicketBB ticketBB, String counterIPAddress, int counterPort, PublicKey counterPubKey){
		//blindSignatureMsg from a sup to voter
		Message m = new Message();
		m.setType("BlindSignature");
		m.setDetail(detail);
		m.setBlindSignature(new BigInteger(blindSignature));
		m.setTicketBB(ticketBB);
		m.setCounterIPAddress(counterIPAddress);
		m.setCounterPublicKey(counterPubKey);
		m.setCounterPort(counterPort);
		return m.blindSignatureMsg();
	}
	
	public static Message recoverBlindSignatureMsg(byte [] msgBytes){
		//genKey message
		try
		{
			Message msg = new Message();
			String stringTicket = new String(msgBytes);
			String[] parts = new String [7];
			int i = 0;
			for (String retval: stringTicket.split(",",7)){
			    parts[i] = retval;
			    i++;
			 }
			msg.setType(parts[0]);
			msg.setDetail(parts[1]);
			msg.setBlindSignature(new BigInteger(parts[2]));
			msg.setCounterIPAddress(parts[3]);
			msg.setCounterPort(Integer.parseInt(parts[4]));
			msg.setCounterPublicKey(MgrIntegrity.byteToPublicKey(new BigInteger(parts[5]).toByteArray()));
			msg.setTicketBB(MgrTicket.recoverTicketBB(parts[6].getBytes()));
			return msg;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return null;
	}
	
	// voter sends sup confirmation of signature verification
	public static byte [] confirmVerifySignatureMsg(String detail, TicketBB ticketBB, byte [] voterSignature){
		//manage confirmVerifySignatureMsg from voter
		Message m = new Message();
		m.setType("ConfirmBlindSignature");
		m.setDetail(detail);
		m.setTicketBB(ticketBB);
		m.setUserSignature(new BigInteger(voterSignature));
		return m.confirmVerifySignatureMsg();
	}
	
	public static Message recoverConfirmVerifySignatureMsg(byte [] msgBytes){
		//genKey message
		try
		{
			Message msg = new Message();
			String stringTicket = new String(msgBytes);
			String[] parts = new String [4];
			int i = 0;
			for (String retval: stringTicket.split(",",4)){
			    parts[i] = retval;
			    i++;
			 }
			msg.setType(parts[0]);
			msg.setDetail(parts[1]);
			msg.setUserSignature(new BigInteger(parts[2]));
			msg.setTicketBB(MgrTicket.recoverTicketBB(parts[3].getBytes()));
			return msg;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}
	
	//6. Submit and verify
	
	public static byte [] submitHybridMsg(byte [] hybrid, byte [] iv){
		//submitBcBallotMsg from voter
		 BASE64Encoder encoder = new BASE64Encoder();
		 String base64Hybrid = encoder.encodeBuffer(hybrid);

		Message m = new Message();
		m.setC(base64Hybrid);
		m.setIvBcBallot(new BigInteger(iv));
		try {
			m.setSha(new BigInteger(MgrConfidentiality.generatePasswordHash((m.getC()+","+m.getIvBcBallot()))));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return m.submitHybridMsg();
	}
	
	public static Message recoverSubmitHybridMsg(byte [] msgBytes){
		//genKey message
		 BASE64Decoder decoder = new BASE64Decoder();
		try
		{
			Message msg = new Message();
			String stringTicket = new String(msgBytes);
			String[] parts = new String [3];
			int i = 0;
			for (String retval: stringTicket.split(",",3)){
			    parts[i] = retval;
			    i++;
			 }
			msg.setHybrid(decoder.decodeBuffer(parts[0]));
			msg.setC(parts[0]);
			msg.setIvBcBallot(new BigInteger(parts[1]));
			msg.setSha(new BigInteger(parts[2]));
			return msg;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return null;
	}	
	
	// voter sends counter the bcBallot
	public static byte [] submitBcBallotMsg(byte [] bcBallot, byte [] supSig, long electionId){
		//submitBcBallotMsg from voter
		
		Message m = new Message();
		m.setType("bcBallot");
		m.setBcBallot(new BigInteger(bcBallot));
		m.setSupSigOnBcBallot(supSig);
		m.setElectionId(electionId);
		return m.submitBcBallotMsg();
	}
	
	public static Message recoverSubmitBcBallotMsg(byte [] msgBytes){
		//genKey message
		try
		{
			BASE64Decoder decoder = new BASE64Decoder();
			Message msg = new Message();
			String stringTicket = new String(msgBytes);
			String[] parts = new String [4];
			int i = 0;
			for (String retval: stringTicket.split(",",4)){
			    parts[i] = retval;
			    i++;
			 }
			msg.setType(parts[0]);
			msg.setBcBallot(new BigInteger(parts[1]));
			msg.setSupSigOnBcBallot(decoder.decodeBuffer(parts[2]));
			msg.setElectionId(Long.parseLong(parts[3]));
			return msg;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return null;
	}	
	
	public static byte [] submitKeyMsg(byte [] key, byte [] iv, int entry){
		//submitBcBallotMsg from voter
		Message m = new Message();
		m.setType("bcKey");
		m.setKeyBcBallot(new BigInteger(key));
		m.setIvBcBallot(new BigInteger(iv));
		m.setEntry(entry);
		return m.submitKeyMsg();
	}
	
	public static Message recoverSubmitKeyMsg(byte [] msgBytes){
		//genKey message
		try
		{
			Message msg = new Message();
			String stringTicket = new String(msgBytes);
			String[] parts = new String [4];
			int i = 0;
			for (String retval: stringTicket.split(",",4)){
			    parts[i] = retval;
			    i++;
			 }
			msg.setType(parts[0]);
			msg.setKeyBcBallot(new BigInteger(parts[1]));
			msg.setIvBcBallot(new BigInteger(parts[2]));
			msg.setEntry(Integer.parseInt(parts[3]));
			return msg;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return null;
	}
	// Counter publish bcBallot
	public static byte [] publishBcBallotMsg(byte[] bcBallot, long electionId, byte [] supSignature, byte [] mySignature){
		//publishbcBallottMsg from Counter
		Message m = new Message();
		m.setType("PublishBcBallot");
		m.setElectionId(electionId);
		m.setBcBallot(new BigInteger(bcBallot));
		m.setSupSignature(new BigInteger(supSignature));
		m.setCounterSignature(mySignature);
		return m.publishBcBallotMsg();
	}
	
	public static Message recoverPublishBcBallotMsg(byte [] msgBytes){
		//genKey message
		try
		{
			Message msg = new Message();
			String stringTicket = new String(msgBytes);
			String[] parts = new String [5];
			int i = 0;
			for (String retval: stringTicket.split(",",5)){
			    parts[i] = retval;
			    i++;
			}
			BASE64Decoder decoder = new BASE64Decoder();
			msg.setType(parts[0]);  //PublishBcBallot or PublishBallot
			msg.setElectionId(Long.parseLong(parts[1]));
			msg.setBcBallot(new BigInteger(parts[2]));
			msg.setSupSignature(new BigInteger(parts[3]));
			msg.setCounterSignature(decoder.decodeBuffer(parts[4]));
			return msg;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return null;
	}
	
	public static byte [] counterAckMsg(String type, String detail){
		//publishbcBallottMsg from Counter
		Message m = new Message();
		m.setType(type); //ackBcBallot || ackKey
		m.setDetail(detail);
		return m.counterAckMsg();
	}
	
	public static Message recoverCounterAckMsg(byte [] msgBytes){
		//genKey message
		try
		{
			Message msg = new Message();
			String stringTicket = new String(msgBytes);
			String[] parts = new String [2];
			int i = 0;
			for (String retval: stringTicket.split(",",2)){
			    parts[i] = retval;
			    i++;
			 }
			msg.setType(parts[0]); //ackBcBallot || ackKey
			msg.setDetail(parts[1]);
			return msg;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return null;
	}
	
	public static byte [] ackPublishBcBallotMsg(String type, String detail){
		//publishbcBallottMsg from Counter
		Message m = new Message();
		m.setType(type); //ackBcBallotMsg || ackBallotMsg
		m.setDetail(detail);
		return m.ackPublishBcBallotMsg();
	}
	
	public static Message recoverAckPublishBcBallotMsg(byte [] msgBytes){
		//genKey message
		try
		{
			Message msg = new Message();
			String stringTicket = new String(msgBytes);
			String[] parts = new String [2];
			int i = 0;
			for (String retval: stringTicket.split(",",2)){
			    parts[i] = retval;
			    i++;
			 }
			msg.setType(parts[0]); //ackBcBallotMsg || ackBallotMsg
			msg.setDetail(parts[1]);
			return msg;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return null;
	}
	
	//Counter Get Bc from entry to decrypt BcBallot
	public static byte [] counterGetBcBallotMsg(int entry, byte[] mySignature)
	{
		Message m = new Message();
		m.setType("CounterGetBcBallot"); 
		m.setEntry(entry);
		m.setCounterSignature(mySignature);
		return m.counterGetBcBallotMsg();
	}
	
	public static Message recoverCounterGetBcBallotMsg(byte [] msgBytes){
		//genKey message
		try
		{
			Message msg = new Message();
			String stringTicket = new String(msgBytes);
			String[] parts = new String [3];
			int i = 0;
			for (String retval: stringTicket.split(",",3)){
			    parts[i] = retval;
			    i++;
			 }
			BASE64Decoder decoder = new BASE64Decoder();
			msg.setType(parts[0]);  
			msg.setEntry(Integer.parseInt(parts[1]));
			msg.setCounterSignature(decoder.decodeBuffer(parts[2]));
			return msg;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return null;
	}
	
	public static byte [] ackCounterGetBcBallotMsg(BcBallotFactor bcFactor){
		Message m = new Message();
		m.setType("AckCounterGetBcBallot"); 
		m.setBcBallotFactor(bcFactor);
		return m.ackCounterGetBcBallotMsg();
	}
	
	public static Message recoverAckCounterGetBcBallotMsg(byte [] msgBytes){
		//genKey message
		try
		{
			Message msg = new Message();
			String stringTicket = new String(msgBytes);
			String[] parts = new String [2];
			int i = 0;
			for (String retval: stringTicket.split(",",2)){
			    parts[i] = retval;
			    i++;
			 }
			msg.setType(parts[0]);  
			msg.setBcBallotFactor(MgrBallot.recoverBcBallotFactor(new BigInteger(parts[1]).toByteArray()));
			return msg;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return null;
	}
	
	// Counter publish Ballot to Bulletin Board
	public static byte [] publishBcBallotFactorMsg(BcBallotFactor bcFactor){
		//publishbcBallottMsg from Counter
		Message m = new Message();
		m.setType("PublishBallot"); //PublishBcBallot or PublishBallot
		m.setBcBallotFactor(bcFactor);
		return m.publishBcBallotFactorMsg();
	}
	
	public static Message recoverPublishBcBallotFactorMsg(byte [] msgBytes){
		//genKey message
		try
		{
			Message msg = new Message();
			String stringTicket = new String(msgBytes);
			String[] parts = new String [2];
			int i = 0;
			for (String retval: stringTicket.split(",",2)){
			    parts[i] = retval;
			    i++;
			 }
			BASE64Decoder decoder = new BASE64Decoder();
			msg.setType(parts[0]);  //PublishBcBallot or PublishBallot
			msg.setBcBallotFactor(MgrBallot.recoverBcBallotFactor(new BigInteger(parts[1]).toByteArray()));
			return msg;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return null;
	}
	
	public static byte [] ackPublishBcBallotFactorMsg(String type, String detail, TicketBB ticketBB){
		//publishbcBallottMsg from Counter
		Message m = new Message();
		m.setType(type); //ackBcBallotMsg || ackBallotMsg
		m.setDetail(detail);
		m.setTicketBB(ticketBB);
		return m.ackPublishBcBallotFactorMsg();
	}
	
	public static Message recoverAckPublishBcBallotFactorMsg(byte [] msgBytes){
		//genKey message
		try
		{
			Message msg = new Message();
			String stringTicket = new String(msgBytes);
			String[] parts = new String [2];
			int i = 0;
			for (String retval: stringTicket.split(",",2)){
			    parts[i] = retval;
			    i++;
			 }
			msg.setType(parts[0]); //ackBcBallotMsg || ackBallotMsg
			msg.setDetail(parts[1]);
			return msg;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return null;
	}
	//server request bcFactor
	public static byte [] requestBcBallotFactorMsg(long electionId){
		//publishbcBallottMsg from Counter
		Message m = new Message();
		m.setType("requestBcFactor");
		m.setElectionId(electionId);
		return m.requestBcBallotFactorMsg();

	}
	
	public static Message recoverRequestBcBallotFactorMsg(byte [] msgBytes){
		//genKey message
		try
		{
			Message msg = new Message();
			String stringTicket = new String(msgBytes);
			String[] parts = new String [2];
			int i = 0;
			for (String retval: stringTicket.split(",",2)){
			    parts[i] = retval;
			    i++;
			 }
			msg.setType(parts[0]); 
			msg.setElectionId(Long.parseLong(parts[1]));
			return msg;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return null;
	}
	
	public static byte [] ackRequestBcBallotFactorMsg(String type ,long electionId,int numberOfEntries, BcBallotFactor[] allBcBallot, String detail, TicketBB ticketBB){
		Message m = new Message();
		m.setType(type); //AckRequestBcFactor || AckRequestResult
		m.setDetail(detail);
		m.setElectionId(electionId);
		m.setAmountOfEntry(numberOfEntries);
		m.setBcArray(allBcBallot); //--> All BcBalllotFactor concatenated
		m.setTicketBB(ticketBB);
		return m.ackRequestBcBallotFactorMsg();

	}	
	
	//Recover back AckCheckBcBallotFactorMsg
	public static Message recoverAckRequestBcBallotFactorMsg(byte [] msgBytes){
		//genKey message
		try
		{
			BASE64Decoder decoder = new BASE64Decoder();
			Message msg = new Message();
			String stringTicket = new String(msgBytes);
			String[] parts = new String [6];
			int i = 0;
			for (String retval: stringTicket.split(",",6)){
			    parts[i] = retval;
			    i++;
			 }
			msg.setType(parts[0]); 
			msg.setDetail(parts[1]);
			msg.setAmountOfEntry(Integer.parseInt(parts[2]));
			msg.setElectionId(Long.parseLong(parts[3]));
			msg.setBcArray(MgrBallot.recoverBcBallotFactorArray(msg.getAmountOfEntry(), 
					decoder.decodeBuffer(parts[4])));
			msg.setTicketBB(MgrTicket.recoverTicketBB(new BigInteger(parts[5]).toByteArray()));
			return msg;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return null;
	}
	
	public static byte [] replyAckRequestBcBallotFactorMsg(String type ,String detail, TicketBB ticketBB){
		Message m = new Message();
		m.setType(type); //AckRequestBcFactor || AckRequestResult
		m.setDetail(detail);
		m.setTicketBB(ticketBB);
		return m.replyAckRequestBcBallotFactorMsg();

	}	
	
	public static Message recoverReplyAckRequestBcBallotFactorMsg(byte [] msgBytes){
		//genKey message
		try
		{
			Message msg = new Message();
			String stringTicket = new String(msgBytes);
			String[] parts = new String [3];
			int i = 0;
			for (String retval: stringTicket.split(",",3)){
			    parts[i] = retval;
			    i++;
			 }
			msg.setType(parts[0]); 
			msg.setDetail(parts[1]);
			msg.setTicketBB(MgrTicket.recoverTicketBB(new BigInteger(parts[2]).toByteArray()));
			return msg;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return null;
	}
	// Voter checking entry or result
	public static byte [] checkBcBallotFactorMsg(String type,TicketBB ticketBB, long electionId){
		//publishbcBallottMsg from Counter
		Message m = new Message();
		m.setType(type); //CheckEntry || CheckResult
		m.setElectionId(electionId);
		m.setTicketBB(ticketBB);
		return m.checkBcBallotFactorMsg();

	}
	// recover back // Voter checking entry or result
	public static Message recoverCheckBcBallotFactorMsg(byte [] msgBytes){
		//genKey message
		try
		{
			Message msg = new Message();
			String stringTicket = new String(msgBytes);
			String[] parts = new String [3];
			int i = 0;
			for (String retval: stringTicket.split(",",3)){
			    parts[i] = retval;
			    i++;
			 }
			msg.setType(parts[0]); 
			msg.setElectionId(Long.parseLong(parts[1]));
			msg.setTicketBB(MgrTicket.recoverTicketBB(new BigInteger(parts[2]).toByteArray()));
			return msg;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return null;
	}
	
	
	
	
	// Acknowledgment of BcBallotFactor
	public static byte [] ackCheckBcBallotFactorMsg(String type, int numberOfEntries, BcBallotFactor[] allBcBallot, String detail, TicketBB ticketBB){
		Message m = new Message();
		m.setType(type); //AckCheckEntry || AckCheckResult
		m.setDetail(detail);
		m.setAmountOfEntry(numberOfEntries);
		m.setBcArray(allBcBallot); //--> All BcBalllotFactor concatenated
		m.setTicketBB(ticketBB);
		return m.ackCheckBcBallotFactorMsg();

	}	
	
	//Recover back AckCheckBcBallotFactorMsg
	public static Message recoverAckCheckBcBallotFactorMsg(byte [] msgBytes){
		//genKey message
		try
		{
			BASE64Decoder decoder = new BASE64Decoder();
			Message msg = new Message();
			String stringTicket = new String(msgBytes);
			String[] parts = new String [5];
			int i = 0;
			for (String retval: stringTicket.split(",",5)){
			    parts[i] = retval;
			    i++;
			 }
			msg.setType(parts[0]); 
			msg.setDetail(parts[1]);
			msg.setAmountOfEntry(Integer.parseInt(parts[2]));
			msg.setBcArray(MgrBallot.recoverBcBallotFactorArray(msg.getAmountOfEntry(), 
					decoder.decodeBuffer(parts[4])));
			msg.setTicketBB(MgrTicket.recoverTicketBB(new BigInteger(parts[3]).toByteArray()));
			return msg;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return null;
	}
	
	public static byte [] checkAuthorizedBallotMsg(TicketBB ticketBB, long electionId){
		//publishbcBallottMsg from Counter
		Message m = new Message();
		m.setType("checkAuthorizedBallot"); //CheckEntry || CheckResult
		m.setElectionId(electionId);
		m.setTicketBB(ticketBB);
		return m.checkAuthorizedBallotMsg();

	}
	public static Message recoverCheckAuthorizedBallotMsg(byte [] msgBytes){
		//genKey message
		try
		{
			Message msg = new Message();
			String stringTicket = new String(msgBytes);
			String[] parts = new String [3];
			int i = 0;
			for (String retval: stringTicket.split(",",3)){
			    parts[i] = retval;
			    i++;
			 }
			msg.setType(parts[0]); 
			msg.setElectionId(Long.parseLong(parts[1]));
			msg.setTicketBB(MgrTicket.recoverTicketBB(new BigInteger(parts[2]).toByteArray()));
			return msg;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return null;
	}
	
	public static byte [] ackCheckAuthorizedBallotMsg(int numberOfEntries, List<String> authorizedUserBallots, String detail, TicketBB ticketBB){
		Message m = new Message();
		m.setType("ackCheckAuthorizedBallot"); //AckCheckEntry || AckCheckResult
		m.setDetail(detail);
		m.setAmountOfEntry(numberOfEntries);
		m.setAllAuthorizedUserBallot(authorizedUserBallots); //--> All BcBalllotFactor concatenated
		m.setTicketBB(ticketBB);
		return m.ackCheckAuthorizedBallotMsg();

	}	
	
	public static Message recoverAckCheckAuthorizedBallotMsg(byte [] msgBytes){
		//genKey message
		try
		{
			BASE64Decoder decoder = new BASE64Decoder();
			Message msg = new Message();
			String stringTicket = new String(msgBytes);
			String[] parts = new String [5];
			int i = 0;
			for (String retval: stringTicket.split(",",5)){
			    parts[i] = retval;
			    i++;
			 }
			msg.setType(parts[0]); 
			msg.setDetail(parts[1]);
			msg.setAmountOfEntry(Integer.parseInt(parts[2]));
			msg.setAllAuthorizedUserBallot(MgrResult.recoverAuthorizedUserBallot(msg.getAmountOfEntry(), 
					decoder.decodeBuffer(parts[3])));
			msg.setTicketBB(MgrTicket.recoverTicketBB(new BigInteger(parts[4]).toByteArray()));
			return msg;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return null;
	}
	public static byte [] sqlOverSSL(String type,byte [] sqlStatement, TicketBB ticketBB){
		String m = type+","+new BigInteger(sqlStatement)+","+new BigInteger(ticketBB.getBytes());
		return m.getBytes();

	}	
	
	//Recover back AckCheckBcBallotFactorMsg
	public static Message recoverSqlOverSSL(byte [] msgBytes){
		//genKey message
		try
		{
			Message msg = new Message();
			String stringTicket = new String(msgBytes);
			String[] parts = new String [3];
			int i = 0;
			for (String retval: stringTicket.split(",",3)){
			    parts[i] = retval;
			    i++;
			}
			msg.setType(parts[0]); 
			msg.setSqlStatement(parts[1]); 
			msg.setTicketBB(MgrTicket.recoverTicketBB(new BigInteger(parts[2]).toByteArray()));
			return msg;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return null;
	}
	
	/*public static byte [] ackSqlOverSSL(String type, List linkedlist){
		String m = type+","+new BigInteger(linkedlist);
		return m.getBytes();

	}	
	
	//Recover back AckCheckBcBallotFactorMsg
	public static Message recoverSqlOverSSL(byte [] msgBytes){
		//genKey message
		try
		{
			Message msg = new Message();
			String stringTicket = new String(msgBytes);
			String[] parts = new String [2];
			int i = 0;
			for (String retval: stringTicket.split(",",2)){
			    parts[i] = retval;
			    i++;
			 }
			msg.setType(parts[0]); 
			msg.setSqlStatement(parts[1]); 
			return msg;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return null;
	}*/
	
	
	
	public static byte [] resetPasswordMsg(String username){
		//publishbcBallottMsg from Counter
		Message m = new Message();
		m.setType("ResetPassword"); 
		m.setUsername(username);
		return m.resetPasswordMsg();

	}
	// recover back // Voter checking entry or result
	public static Message recoverResetPasswordMsg(byte [] msgBytes){
		//genKey message
		try
		{
			Message msg = new Message();
			String stringTicket = new String(msgBytes);
			String[] parts = new String [2];
			int i = 0;
			for (String retval: stringTicket.split(",",2)){
			    parts[i] = retval;
			    i++;
			 }
			msg.setType(parts[0]); 
			msg.setUsername(parts[1]);
			return msg;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return null;
	}
	
	
	public static byte [] ackResetPasswordMsg(String detail, String userEmail){
		//publishbcBallottMsg from Counter
		Message m = new Message();
		m.setType("AckResetPassword"); 
		m.setDetail(detail);
		m.setUserEmail(userEmail);
		return m.ackResetPasswordMsg();

	}
	// recover back // Voter checking entry or result
	public static Message recoverAckResetPasswordMsg(byte [] msgBytes){
		//genKey message
		try
		{
			Message msg = new Message();
			String stringTicket = new String(msgBytes);
			String[] parts = new String [3];
			int i = 0;
			for (String retval: stringTicket.split(",",3)){
			    parts[i] = retval;
			    i++;
			 }
			msg.setType(parts[0]); 
			msg.setDetail(parts[1]);
			msg.setUserEmail(parts[2]);
			return msg;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return null;
	}
	
	
	
	
	public static byte [] updatePasswordMsg(String username, String password, PublicKey publicKey){
		//publishbcBallottMsg from Counter
		Message m = new Message();
		m.setType("UpdatePassword"); 
		m.setUsername(username);
		m.setPassword(password);
		m.setUserPublicKey(publicKey);
		return m.updatePasswordMsg();

	}
	// recover back // Voter checking entry or result
	public static Message recoverUpdatePasswordMsg(byte [] msgBytes){
		//genKey message
		try
		{
			Message msg = new Message();
			String stringTicket = new String(msgBytes);
			String[] parts = new String [4];
			int i = 0;
			for (String retval: stringTicket.split(",",4)){
			    parts[i] = retval;
			    i++;
			 }
			msg.setType(parts[0]); 
			msg.setUsername(parts[1]);
			msg.setPassword(parts[2]);
			msg.setUserPublicKey(MgrIntegrity.byteToPublicKey(new BigInteger(parts[3]).toByteArray()));		
			return msg;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return null;
	}
	
	
	public static byte [] ackUpdatePasswordMsg(String detail){
		//publishbcBallottMsg from Counter
		Message m = new Message();
		m.setType("AckUpdatePassword"); 
		m.setDetail(detail);
		return m.ackUpdatePasswordMsg();

	}
	// recover back // Voter checking entry or result
	public static Message recoverAckUpdatePasswordMsg(byte [] msgBytes){
		//genKey message
		try
		{
			Message msg = new Message();
			String stringTicket = new String(msgBytes);
			String[] parts = new String [2];
			int i = 0;
			for (String retval: stringTicket.split(",",2)){
			    parts[i] = retval;
			    i++;
			 }
			msg.setType(parts[0]); 
			msg.setDetail(parts[1]);
			return msg;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return null;
	}
	
	
	public static byte [] updatePersonalInfoMsg(String username, String fname, 
			String lname, String email, TicketBB ticketBB){
		//publishbcBallottMsg from Counter
		Message m = new Message();
		m.setType("UpdatePersonalInfo"); 
		m.setUsername(username);
		m.setFname(fname);
		m.setLname(lname);
		m.setUserEmail(email);
		//m.setDob(dob);
		m.setTicketBB(ticketBB);
		return m.updatePersonalInfoMsg();

	}
	// recover back // Voter checking entry or result
	public static Message recoverUpdatePersonalInfoMsg(byte [] msgBytes){
		//genKey message
		try
		{
			Message msg = new Message();
			String stringTicket = new String(msgBytes);
			String[] parts = new String [3];
			int i = 0;
			for (String retval: stringTicket.split(",",3)){
			    parts[i] = retval;
			    i++;
			 }
			msg.setType(parts[0]);
			msg.setUser(MgrUser.recoverUpdateInfoUser(new BigInteger(parts[1]).toByteArray()));
			msg.setTicketBB(MgrTicket.recoverTicketBB(new BigInteger(parts[2]).toByteArray()));
			return msg;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return null;
	}
	
	
	public static byte [] ackUpdatePersonalInfoMsg(String detail, TicketBB ticketBB){
		//publishbcBallottMsg from Counter
		Message m = new Message();
		m.setType("AckUpdatePersonalInfo"); 
		m.setDetail(detail);
		m.setTicketBB(ticketBB);
		return m.ackUpdatePersonalInfoMsg();

	}
	// recover back // Voter checking entry or result
	public static Message recoverAckUpdatePersonalInfoMsg(byte [] msgBytes){
		//genKey message
		try
		{
			Message msg = new Message();
			String stringTicket = new String(msgBytes);
			String[] parts = new String [3];
			int i = 0;
			for (String retval: stringTicket.split(",",3)){
			    parts[i] = retval;
			    i++;
			 }
			msg.setType(parts[0]); 
			msg.setDetail(parts[1]);
			msg.setTicketBB(MgrTicket.recoverTicketBB(new BigInteger(parts[2]).toByteArray()));
			return msg;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return null;
	}
	
	
	public static byte [] logOutMsg(TicketBB ticketBB){
		//publishbcBallottMsg from Counter
		Message m = new Message();
		m.setType("Logout"); 
		m.setTicketBB(ticketBB);
		return m.logOutMsg();

	}
	// recover back // Voter checking entry or result
	public static Message recoverLogOutMsg(byte [] msgBytes){
		//genKey message
		try
		{
			Message msg = new Message();
			String stringTicket = new String(msgBytes);
			String[] parts = new String [2];
			int i = 0;
			for (String retval: stringTicket.split(",",2)){
			    parts[i] = retval;
			    i++;
			 }
			msg.setType(parts[0]); 
			msg.setTicketBB(MgrTicket.recoverTicketBB(new BigInteger(parts[1]).toByteArray()));
			return msg;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return null;
	}
	
	
	//Send Msg
	public static void sendMsg(Socket ss, byte[] msg)
	{
		try {
			ObjectOutputStream outSocket = new ObjectOutputStream(ss.getOutputStream());
			outSocket.writeObject(msg);
			outSocket.flush();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	//Receive Msg
	public static byte[] recvMsg(Socket ss)
	{
		try {
			ObjectInputStream inSocket = new ObjectInputStream(ss.getInputStream());
			try {
				return (byte[]) inSocket.readObject();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	
	
}
