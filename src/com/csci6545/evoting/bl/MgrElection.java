package com.csci6545.evoting.bl;

import java.math.BigInteger;
import java.security.PrivateKey;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

import Decoder.BASE64Decoder;
import Decoder.BASE64Encoder;

import com.csci6545.evoting.be.BcBallotFactor;
import com.csci6545.evoting.be.Counter;
import com.csci6545.evoting.be.Election;
import com.csci6545.evoting.be.TicketElection;

public class MgrElection {
	public static Election createElection(long electionID,String electionName,
			Timestamp startDate, Timestamp endDate, Map<String,String> candidate, Counter counter) 
	{
		Election election = new Election();
		election.setId(electionID);
		election.setName(electionName);
		election.setStart(startDate);
		election.setEnd(endDate);
		election.setCandidate(candidate);
		election.setCounter(counter);
		return election;
	}
	public static Election recoverElection(byte [] byteElection) {
		try
		{
			BASE64Decoder decoder = new BASE64Decoder();
			Election election = new Election();
			Counter counter = new Counter();
			String stringElection = new String(byteElection);
			SimpleDateFormat format =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
			//SimpleDateFormat format =  new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.US);
			String[] parts = new String [8];
			int i = 0;
			for (String retval: stringElection.split(",",8)){
			   parts[i] = retval;
			   i++;
			}		
			election.setId(Long.parseLong(parts[0]));
			election.setName(parts[1]);
			election.setStart(new Timestamp(format.parse(parts[2]).getTime()));
			election.setEnd(new Timestamp(format.parse(parts[3]).getTime()));
			
			counter.setUserId(parts[4]);
			counter.setfName(parts[5]);
			counter.setlName(parts[6]);
			election.setCounter(counter);
			election.setCandidate(MgrBytes.bytesToStringStringMap(decoder.decodeBuffer(parts[7])));
			//System.out.println();
			return election;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return null;
	}
	
	public static byte[] allElections(Election[] election)
	{
		String electionMsg = "";
		BASE64Encoder encoder = new BASE64Encoder();
		for (int i = 0;i < election.length;i++)
		{
			
			
			String base64Candidate = encoder.encodeBuffer(MgrBytes.stringStringMapToBytes(election[i].getCandidate()));		
			if (i == election.length-1)
			{
				electionMsg = electionMsg+election[i].getId()+","+
						election[i].getName()+","+election[i].getStart().toString()+
						","+election[i].getEnd()+","+election[i].getCounter().getUserId()+
						","+election[i].getCounter().getfName()+","+election[i].getCounter().getlName()+
						","+base64Candidate;
			
			}
			else
			{
				electionMsg = electionMsg+election[i].getId()+","+
						election[i].getName()+","+election[i].getStart().toString()+
						","+election[i].getEnd()+","+election[i].getCounter().getUserId()+
						","+election[i].getCounter().getfName()+","+election[i].getCounter().getlName()+
						","+base64Candidate+"/";
			}
		}
		return electionMsg.getBytes();
	}
	
	public static Election[] recoverElectionArray(int entry, byte[] allElectionBytes)
	{
		Election[] elcetionArray = new Election[entry];
		String elcetionMsg = new String(allElectionBytes);
		System.out.println(elcetionMsg);
		int i = 0;
		for (String retval: elcetionMsg.split("/",entry)){
			elcetionArray[i] = MgrElection.recoverElection(retval.getBytes());
		    i++;
		}
		return elcetionArray;
	}
}
