package com.csci6545.evoting.bl;
import java.util.Properties;
import javax.mail.*;
import javax.mail.internet.*;

public class MgrEmail {
	final static String username = "secure.voting.system";
	final static String password = "evotingsystem";
	final static String host = "smtp.gmail.com";
	public static boolean sendEmail(String sendTo, String otp, String subject, String body){    
		 Properties properties = System.getProperties();
		 properties.put("mail.smtp.starttls.enable", "true");
		 properties.put("mail.smtp.host", host);
		 properties.put("mail.smtp.user", username);
		 properties.put("mail.smtp.password", password);
		 properties.put("mail.smtp.port", "587");
		 properties.put("mail.smtp.auth", "true");

	        Session session = Session.getDefaultInstance(properties);
	        MimeMessage message = new MimeMessage(session);
 
		try {
			 message.setSubject(subject);
	         message.setText(body+" "+otp);
	         message.addRecipient(Message.RecipientType.TO, new InternetAddress(sendTo));
	         Transport transport = session.getTransport("smtp");
	         transport.connect(host, username, password);
	         transport.sendMessage(message, message.getAllRecipients());
	         transport.close();
			return true;
		} catch (MessagingException e) {
			System.out.println(e);
			return false;
		}
	 }
}
