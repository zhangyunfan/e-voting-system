package com.csci6545.evoting.bl;

import java.io.FileInputStream;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.SecretKey;

import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.crypto.engines.RSABlindingEngine;
import org.bouncycastle.crypto.engines.RSAEngine;
import org.bouncycastle.crypto.generators.RSABlindingFactorGenerator;
import org.bouncycastle.crypto.params.RSABlindingParameters;
import org.bouncycastle.crypto.params.RSAKeyParameters;
import org.bouncycastle.crypto.signers.PSSSigner;
import org.bouncycastle.crypto.util.PrivateKeyFactory;
import org.bouncycastle.crypto.util.PublicKeyFactory;

import java.nio.file.Paths;
import java.nio.file.Files;


public class MgrIntegrity {
	
	// Load Public Key
	public static PublicKey loadpubkey(String pubkeyfile)
	{
		PublicKey pubKey = null;
		try
		{
			//Load public key
			byte[] EncodedPubKey =
					Files.readAllBytes(Paths.get(pubkeyfile));
			KeyFactory rSAKeyFactory1 = KeyFactory.getInstance("RSA");
			pubKey = rSAKeyFactory1.generatePublic(new X509EncodedKeySpec(EncodedPubKey));
			System.out.println("The public key was successfully loaded");
	
		}
		catch (Exception e)
		{
			System.out.print(e);
		}
		return pubKey;
	}
	
	//Load Private Key
	public static PrivateKey loadprivkey(String privkeyfile)
	{
		PrivateKey privKey = null;
		try
		{
			//Load private key
			FileInputStream fis = new FileInputStream(privkeyfile);
			byte[] EncodedPrivKey = new byte[fis.available()];
			fis.read(EncodedPrivKey);
			fis.close();
			KeyFactory rSAKeyFactory = KeyFactory.getInstance("RSA");
			privKey = rSAKeyFactory.generatePrivate(new PKCS8EncodedKeySpec(EncodedPrivKey));
			System.out.println("The private key was successfully  loaded");
		}
		catch (Exception e)
		{
			System.out.print(e);
		}
		return privKey;
	}
	
	//Convert from byte array to public key
	public static PublicKey byteToPublicKey(byte [] publicKey)
	{
		PublicKey pubKey = null;
		try
		{
			//Load public key
			KeyFactory rSAKeyFactory1 = KeyFactory.getInstance("RSA");
			pubKey = rSAKeyFactory1.generatePublic(new X509EncodedKeySpec(publicKey));
		}
		catch (Exception e)
		{
			System.out.print(e);
		}
		return pubKey;
	}
	
	//Convert from byte array to private key
	public static PrivateKey byteToPrivateKey(byte [] privateKey)
	{
		PrivateKey privKey = null;
		try
		{
			//Load private key
			KeyFactory rSAKeyFactory = KeyFactory.getInstance("RSA");
			privKey = rSAKeyFactory.generatePrivate(new PKCS8EncodedKeySpec(privateKey));
		}
		catch (Exception e)
		{
			System.out.print(e);
		}
		return privKey;
	}

	//_____________BoucyCastle Start_____________________//

	// Convert PrivateKey to CipherParameters
	static CipherParameters getPrivateKeyCipherParam(PrivateKey privateKey)
	{
		try {
			return (CipherParameters)PrivateKeyFactory.createKey(privateKey.getEncoded());
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}
	
	// Convert PublicKey to CipherParameters
	static CipherParameters getPublicKeyCipherParam(PublicKey publicKey)
	{
		try {
			return (CipherParameters)PublicKeyFactory.createKey(publicKey.getEncoded());
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}

	// To get Blinding Factor
    public static BigInteger getBlindingFactor(PublicKey supPublicKey) {
    	
    	CipherParameters supPublicKeyCipherParam = getPublicKeyCipherParam(supPublicKey);
    	RSABlindingFactorGenerator gen = new RSABlindingFactorGenerator();
        gen.init(supPublicKeyCipherParam);
        return gen.generateBlindingFactor();
    }
    
    // To make blinded ballot
    public static byte[] makeBlind(PublicKey supPublicKey, BigInteger blindingFactor, byte[] bcBallot) {
    	
    	CipherParameters supPublicKeyCipherParam = getPublicKeyCipherParam(supPublicKey);
    	RSABlindingParameters params = new RSABlindingParameters((RSAKeyParameters) supPublicKeyCipherParam, blindingFactor);
        PSSSigner blinder = new PSSSigner(new RSABlindingEngine(), new SHA256Digest(), 16); // sLen is salt len
        blinder.init(true, params);

        blinder.update(bcBallot, 0, bcBallot.length);

        byte[] blindedMsg = null;
        try {
        	blindedMsg = blinder.generateSignature();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return blindedMsg;
    }
   
    // Supervisor signed blinded signature
    public static byte[] signBlindedSignature(PrivateKey supPivateKey, byte[] blindedBallot) {
        
    	CipherParameters supPivateKeyCipherParam = getPrivateKeyCipherParam(supPivateKey);
    	RSAEngine signer = new RSAEngine();
        signer.init(true, supPivateKeyCipherParam);
        return signer.processBlock(blindedBallot, 0, blindedBallot.length);
    }
    
    // To unblind blinded signature
    public static byte[] unBlind(PublicKey supPublicKey, BigInteger blindingFactor, byte[] supBlindedSig) {
        
    	CipherParameters supPublicKeyCipherParam = getPublicKeyCipherParam(supPublicKey);
    	RSABlindingEngine eng = new RSABlindingEngine();
        RSABlindingParameters params = new RSABlindingParameters((RSAKeyParameters)supPublicKeyCipherParam, blindingFactor);
        eng.init(false, params); //false means unblind

        return eng.processBlock(supBlindedSig, 0, supBlindedSig.length);
    }

    // sign RSA Signature
    public static byte[] sign(PrivateKey pivateKey, byte[] msg) {
    	
    	CipherParameters pivateKeyCipherParam = getPrivateKeyCipherParam(pivateKey);
        PSSSigner signer = new PSSSigner(new RSAEngine(), new SHA256Digest(), 16); // sLen is salt len
        signer.init(true, pivateKeyCipherParam);
        signer.update(msg, 0, msg.length);
        
        byte[] signature = null;

        try {
        	signature = signer.generateSignature();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        
        return signature;
    }

    //Verify RSA Signature
    public static boolean verify(PublicKey publicKey, byte[] msg, byte[] signature) {
        
    	CipherParameters publicKeyCipherParam = getPublicKeyCipherParam(publicKey);
    	PSSSigner signer = new PSSSigner(new RSAEngine(), new SHA256Digest(), 16);
        signer.init(false, publicKeyCipherParam);

        signer.update(msg,0,msg.length);

        return signer.verifySignature(signature);
    
    }
    
    public static byte [] getHmac(SecretKey key, byte [] msg)
    {
    	try {
			Mac mac = Mac.getInstance("HmacSHA256");
			mac.init(key);
			return mac.doFinal(msg);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e);
		}
    	return null;
    }
    public static boolean verifyHmac(SecretKey key, byte [] hmac, byte [] msg)
    {
    	try {
			Mac mac = Mac.getInstance("HmacSHA256");
			mac.init(key);
			if(Arrays.equals(mac.doFinal(msg), hmac))
			{
				return true;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e);
		}
    	return false;
    }
    public static SecretKey getHmacKey()
    {
    	KeyGenerator gen = null;
		try {
			gen = KeyGenerator.getInstance("HmacSHA256");
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		SecretKey hmacKey = gen.generateKey();
    	return hmacKey;
    }
}
