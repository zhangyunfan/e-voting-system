package com.csci6545.evoting.bl;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
/**
 * 
 * @author Yunfan Zhang
 *
 */
public class MgrHash {
	private static final String HASH_MODE= "SHA-256";
	
	/**
	 * 
	 * @param bytes a byte array
	 * @return the hex format String of the hash value of the input bytes
	 * @throws NoSuchAlgorithmException
	 */
	public static String generateStringdHash(byte[] bytes)
			throws NoSuchAlgorithmException{
		MessageDigest sha256 = MessageDigest.getInstance(HASH_MODE);
		sha256.update(bytes);
		byte[] bytesHashed = sha256.digest();
		return MgrBytes.bytesToHexString(bytesHashed);
		}
	/**
	 * 
	 * @param dob Date of birth whose type is java.util.Date
	 * @return the hex format String of the hash value of the date of birth
	 * @throws NoSuchAlgorithmException
	 */
	public static String generateDobHash(Date dob) 
			throws NoSuchAlgorithmException {
		// TODO Auto-generated method stub
		String dateOfBirth = dob.toString();
		return generateStringdHash(dateOfBirth.getBytes());
	}
}
