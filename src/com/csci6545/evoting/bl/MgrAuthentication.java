/* Autheor: Alemberhan Getahun 
 * Date: 11/01/2013*/
package com.csci6545.evoting.bl;

import java.util.Date;

import com.csci6545.evoting.be.Voter;
import com.csci6545.evoting.dl.MgrVoter;

public class MgrAuthentication {

	public static boolean passwordAuthentcateVoter(String username,
			String password, String dob) {
		
		return MgrVoter.passwordAuthentcateVoter(username, password,dob);
	}

	public static Voter passwordAuthentcateVoter(String username,
			String password) {
		
		return MgrVoter.passwordAuthentcateVoter(username, password);
	}

	
}
