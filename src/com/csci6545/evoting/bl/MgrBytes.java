package com.csci6545.evoting.bl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.InputMismatchException;

/**
 * 
 * @author Yunfan Zhang
 *
 */
public class MgrBytes {
	/**
	 * 
	 * @param bytes a byte array
	 * @return a string (in which each two characters stand for the hex value of byte) representing the byte array
	 */
	public static String bytesToHexString(byte[] bytes){
		if (bytes.length<1){
			return "";
		}
		byte[] nextArray = new byte[bytes.length-1];
		for(int i=0;i<nextArray.length;i++){
			nextArray[i]=bytes[i+1];
		}
		String headString=String.format("%02x", bytes[0]);
		return headString+bytesToHexString(nextArray);
	}
	/**
	 * 
	 * @param hexString a String in hex format
	 * @return the byte array that the input String represents for
	 */
	public static byte[] hexStringToBytes(String hexString){
		if(hexString.length() % 2 !=0){
			throw new
			InputMismatchException("The string length must be even number!\n");
		}
		
		byte[] returnArray = new byte[hexString.length()/2];
		for(int i=0;i<returnArray.length;i++){
			String byteString = hexString.substring(i*2, i*2+2);
			returnArray[i] = (byte) Integer.parseInt(byteString, 16);
		}
		return returnArray;
	}
	
	/**
	 * 
	 * @param a byte array
	 * @param b byte array
	 * @return the result of a + b in which the elements in a locate before the elements in b
	 */
	public static byte[] combineByteArray(byte[] a,byte[] b){
		if(a.length<1){
			return b;
		}
		else if(b.length<1){
			return a;
		}
		byte[] returnArray =  new byte[a.length+b.length];
		for(int i=0;i<a.length;i++){
			returnArray[i] = a[i];
		}
		for(int i=0;i<b.length;i++){
			returnArray[a.length+i]=b[i];
		}
		return returnArray;
	}
	/**
	 * 
	 * @param bytes a byte array
	 * @return a String whose characters consist of the ascii code of the input byte array
	 */
	public static String bytesToString(byte[] bytes){
		if(bytes.length<1){
			return "";
		}
		byte[] nexts = new byte[bytes.length-1];
		for(int i=0;i<nexts.length;i++){
			nexts[i]=bytes[i+1];
		}
		String headString = String.format("%c", bytes[0]);
		return headString+bytesToString(nexts);
	}
	/**
	 * 
	 * @param map a HashMap whose keys and values are String
	 * @return a byte array representing the input Map
	 */
	public static byte[] stringStringMapToBytes(Map<String,String> map){
		Matcher remover = Pattern.compile("[{}]").matcher(map.toString());
		String returnString = remover.replaceAll("");
		return returnString.getBytes();
	}
	/**
	 * 
	 * @param bytes a byte array representing a HashMap whose keys and values are String
	 * @return the HashMap that the input byte array represents
	 */
	public static Map<String,String> bytesToStringStringMap(byte[] bytes){
		String mapString = bytesToString(bytes);
		Map<String, String> returnMap =  new HashMap<String,String> ();
		System.out.println(mapString);
		for(String record:mapString.split(",")){
			String[] keyAndValue = record.split("=");
			returnMap.put(keyAndValue[0].replace(" ", ""), keyAndValue[1]);
		}
		return returnMap;
	}
	/**
	 * 
	 * @param list an ArrayList whose elements are String
	 * @return a byte array representing the input List
	 */
	public static byte[] stringListToBytes(List<String> list){
		Matcher remover = Pattern.compile("[ \\[\\]]").matcher(list.toString());
		String returnString = remover.replaceAll("");
		return returnString.getBytes();
	}
	/**
	 * 
	 * @param bytes a byte array representing a ArrayList whose elements are String
	 * @return the ArrayList that the input byte array represents
	 */
	public static List<String> bytesToStringList(byte[] bytes){
		String listString = bytesToString(bytes);
		List<String> returnString =  new ArrayList<String> ();
		for(String e:listString.split(",")){
			returnString.add(e);
		}
		return returnString;
	}
}
