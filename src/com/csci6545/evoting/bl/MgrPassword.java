package com.csci6545.evoting.bl;

import java.io.BufferedReader;
import java.io.FileReader;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.SecureRandom;

import com.csci6545.evoting.dl.MgrCounter;
import com.csci6545.evoting.dl.MgrSuperuser;
import com.csci6545.evoting.dl.MgrUser;

public class MgrPassword {
	public static int [] md5 = new int [16777216];
	public static int [] sha512 = new int [16777216];
	public static Boolean [] entropy = new Boolean[4];
	public static int word_count = 0;
	public static double passwordEntropy = 0;
	public static int character = 0;
	public static int score = 0;
	public static double ic = 1;
	public static int smallCount = 0;
	public static int upperCount = 0;
	public static int individualWordLen = 0;
	public static boolean isStrong(String password)
	{
		loadPasswordDictionary();
		//String password = "";
		try
		{
			passwordEntropy = 0;
			//System.out.println("refinement --> "+replace_symbol(password));
			String password_dict = replaceSymbol(password).toLowerCase();
			checkConsecutiveEnglish(password);
			//C 0 refine password
			//C 1 test with dictionary
			//C 2 Test Length > 7 
			//C 3 Test Entropy > 45 score = 2*(h/45)
			//C 4 Test diversity score = 3*len, 2.5*len, len
			//C 5 Test english sentences deduct by wordlen*2.5
			//C 6 If contains letter only deduct by len/2 
			//C 7 If contains only either letter, number, or symbol deduct by len*(8/len), len*(4/len), len/2
			//C 8 If len > 16 score = 1.25*(len-16)
			//C 9 If len < 8 score = (8-len)*1.5;
			//C 10 If contains symbol score = 3 each
			//C 10 If contains number score = 1 each
			//C Check IC if > 0.05 score -= len*ic
			//C 10 score/len = level if > 2 --> strong
			score = 0;
			checkWord(password);
			checkWord(password_dict);
			sentenceCheck(password);
			if(!(isInDictionary(password) || isInDictionary(password_dict) || (password == "")))
			{
				//System.out.println("len: "+password.length());
				passwordEntropy += entropy(password);
				//System.out.println(passwordEntropy);
				checkDiversity(password);
				//If password contains only letter		
				if((entropy[0] || entropy[1]) && !(entropy[2] || entropy[3]))
				{
					correlation(password_dict);
				}
				score = calculateScore(password.length())/password.length();
				//System.out.println("Score = "+score);
				if(score >= 2)
				{
					//System.out.println(password+", strong");
					return true;
				}
				else
				{
					//System.out.println(password+", weak");
					return false;
				}
			}
			else
			{
				//System.out.println(password+", weak");
				return false;
			}
		}
		catch(Exception e)
		{
			return false;
		}
	}
	
	 
	public static Boolean generateRandomPassword(String username, String subject, String body)
	{
		 SecureRandom random = new SecureRandom();
		 String otp = new BigInteger(130, random).toString(32);
		 if(MgrUser.getUserEmail(username) != null && MgrEmail.sendEmail(MgrUser.getUserEmail(username), otp, subject , body))
		 {
			 MgrUser.setUserpassword(username, otp,1);
			 return true;
		 }
		 return false;	 
	}
	public static Boolean generateCounterRandomPassword(String username, String subject, String body)
	{
		 SecureRandom random = new SecureRandom();
		 String otp = new BigInteger(130, random).toString(32);
		 String email = MgrCounter.getCounterEmail(username);
		 if(email != null && MgrEmail.sendEmail(email, otp, subject , body))
		 {
			 MgrCounter.setCounterpassword(username, otp,1);
			 return true;
		 }
		 return false;	 
	}
	public static Boolean generateSuperRandomPassword(String username, String subject, String body)
	{
		 SecureRandom random = new SecureRandom();
		 String otp = new BigInteger(130, random).toString(32);
		 String email = MgrSuperuser.getSuperEmail(username);
		 if(email != null && MgrEmail.sendEmail(email, otp, subject , body))
		 {
			 MgrSuperuser.setSuperPassword(username, otp,1);
			 return true;
		 }
		 return false;	 
	}
	
	static void checkConsecutiveEnglish(String word)
	{
		upperCount = 0;
		smallCount = 0;
		for(int i = 0;i < word.length()-1;i++)
		{
			if(word.charAt(i) >= 'a' && word.charAt(i) <='z')
			{
				if(word.charAt(i+1) >= 'a' && word.charAt(i+1) <='z')
				{
					smallCount++;
				}
			}
			
			//if contains uppercase
			if(word.charAt(i) >= 'A' && word.charAt(i) <='Z')
			{
				if(word.charAt(i+1) >= 'A' && word.charAt(i+1) <='Z')
				{
					upperCount++;
				}
			}
		}
		//System.out.println(upperCount+smallCount);		
	
	}
	static double correlation(String word)
	{
		int [] charcount = new int [26];
		ic = 1;
		for(int i = 0;i < word.length();i++)
		{
			charcount[(word.charAt(i)-97)%26]++;
		}
		for(int i = 0;i < charcount.length;i++)
		{
			ic = ic + charcount[i]*(charcount[i]-1);
		}
		ic = ic/(word.length()*(word.length()-1));
		if(ic > 0.05)
		{
			score -= ic*(word.length());
		}
		return ic;
	}
	static void sentenceCheck(String word)
	{
		String password = "";
		//int count = 0;
		//int word_count=0;
		for(int i = 0;i<word.length();i++)
		{
			//Check word
			if(word.charAt(i) >= 'a' && word.charAt(i) <='z' || word.charAt(i) >= 'A' && word.charAt(i) <='Z')
			{
				//count++;
				password = password+word.charAt(i);
			}
		}
		password = password.toLowerCase();
		if(password.length()>0)
		{
			correlation(password);
		}
	}
	static void checkWord(String word)
	{
		String passTemp = "";
		//int count = 0;
		word_count=0;
		individualWordLen = 0;
		passwordEntropy = 0;
		for(int i = 0;i<word.length();i++)
		{
			//Check word
			if(((int)word.charAt(i) >= 32 && (int)word.charAt(i) <= 47) || (word.charAt(i) >= ':' && word.charAt(i) <= '@') || ((int)word.charAt(i) >= 91 && (int)word.charAt(i) <= 96) || ((int)word.charAt(i) >= 123) && ((int)word.charAt(i) <= 126) || (int)word.charAt(i) >= 48 && (int)word.charAt(i) <= 57)
			{
				//count++;
				if(word.charAt(i) == ' ')
				{
					score-=2;
				}
				passTemp = passTemp.toLowerCase();
				if(passTemp.length() > 1)
				{
					if(isInDictionary(passTemp))
					{
						word_count++;
						score -= passTemp.length()*0.5;
						individualWordLen += passTemp.length();
						passwordEntropy += Math.log(50000)/Math.log(2);
					}
				}
				passTemp = "";
			}
			else
			{
				passTemp = passTemp + word.charAt(i);
			}
			if(i == word.length()-1)
			{
				passTemp = passTemp.toLowerCase();
				if(passTemp.length() > 1)
				{
					if(isInDictionary(passTemp))
					{
						word_count++;
						score -= passTemp.length()*0.5;
						individualWordLen += passTemp.length();
						passwordEntropy += Math.log(50000)/Math.log(2);
					}
				}
			}
			
		}
		
	}
	static String replaceSymbol(String word)
	{
		// 1 ==> l
		// 3 ==> e
		// 4 ==> a, 
		// @ ==> a,
		// 7 ==> t
		// 8 ==> b
		// 0 ==> o
		// ! ==> i
		for (int i=0;i<word.length();i++)
		{
			if(word.charAt(i)=='1')
			{
				//IsInDictionary(String word)
				word =word .replace('1', 'l');
			}
			else if(word.charAt(i)=='3')
			{
				word = word.replace('3', 'e');
			}
			else if(word.charAt(i)=='!')
			{
				word = word.replace('!', 'i');
			}
			else if(word.charAt(i)=='4')
			{
				word = word.replace('4', 'a');
			}
			else if(word.charAt(i)=='6')
			{
				word = word.replace('6', 'b');
			}
			else if(word.charAt(i)=='0')
			{
				word = word.replace('0', 'o');
			}
			else if(word.charAt(i)=='@')
			{
				word = word.replace('@', 'a');
			}
		}
		return word;
	}
	static int calculateScore(int len)
	{
		score = (int) (score+2*(passwordEntropy/45));
		//Only letter
		if((entropy[0] || entropy[1]) && !(entropy[2] || entropy[3]))
		{
			score -= (8/len);
		}
		//Only number
		else if (!(entropy[0] || entropy[1] || entropy[2]) && (entropy[3]))
		{
			score -= (4/len);
		}
		//Only symbol
		else if (!(entropy[0] || entropy[1] || entropy[3]) && (entropy[2]))
		{
			score -= (4/len);
		}
		if(len<8)
		{
			score -= (8-len)*5.5;
		}
		else if(len>7 && len <16)
		{
			score += (1.25+(len-8)*0.03)*(len-8);
		}
		else
		{
			score += (1.25+(len-8)*0.08)*(len-8);
			//score += (1.25+(len-8)*0.03)*(len-8);
		}
		if(len<10)
		{
			score -= (upperCount+smallCount)*(10-len); 
		}
		else
		{
			score -= (upperCount+smallCount)*0.2;
		}
		//System.out.println("raw score "+score);
		return score;
	}
	static void checkDiversity (String word)
	{
		//System.out.println("set of character = "+character);
		if (character == 95)
		{
			score += 3*word.length();

		}
		else if (character >= 62)
		{
			score += 2.5*word.length();

		}
		else if (character >= 59)
		{
			score += word.length();

		}
		else if (character == 52)
		{
			score += word.length()/2;

		}

	}
	static double entropy(String word)
	{
		character = 0;
		for(int i=0;i<4;i++)
		{
			entropy[i] = false;
		}
		double h = 0;
		for(int i=0;i<word.length();i++)
		{
			//if contains lowercase
			if(word.charAt(i) >= 'a' && word.charAt(i) <='z')
			{
				entropy[0] = true;
			}
			//if contains uppercase
			if(word.charAt(i) >= 'A' && word.charAt(i) <='Z')
			{
				entropy[1] = true;
			}
			//If contains symbol
			if(((int)word.charAt(i) >= 32 && (int)word.charAt(i) <= 47) || (word.charAt(i) >= ':' && word.charAt(i) <= '@') || ((int)word.charAt(i) >= 91 && (int)word.charAt(i) <= 96) || ((int)word.charAt(i) >= 123) && ((int)word.charAt(i) <= 126))
			{
				entropy[2] = true;
				score += (word.length()/10);
			}
			// If contains numbers
			if ((int)word.charAt(i) >= 48 && (int)word.charAt(i) <= 57)
			{
				entropy[3] = true;
				score += word.length()*0.05;
			}			
		}
		if(entropy[0])
		{
			character+=26;
		}
		if(entropy[1])
		{
			character+=26;
		}
		if(entropy[2])
		{
			character+=33;
			
		}
		if(entropy[3])
		{
			character+=10;
		}
		h = (word.length()-individualWordLen)*Math.log10(character)/Math.log10(2);
		return h;
	}
	static Boolean isInDictionary(String word)
	{
		byte[] outputsha = new byte[1024];
		byte[] outputmd = new byte[1024];
		int check=0;
		Boolean match = false;
		String hex_temp= "";
		double index=0;
		//word = "mcS80392";
		try
		{
			// Check MD5
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(word.getBytes());
			outputmd = md.digest();
			//System.out.println(new BigInteger(1,output).toString(16));
			hex_temp = new BigInteger(1,outputmd).toString(16).substring(0, 6);
			//System.out.println(hex_temp);
			//hex_temp = "aa";
			for(int i=5;i>=0;i--)
			{
			
				if(hex_temp.charAt(i) == 'a')
				{
					index = index+Math.pow(16, 5-i) * 10;
				}
				else if(hex_temp.charAt(i) == 'b')
				{
					index = index+Math.pow(16, 5-i) * 11;
				}
				else if(hex_temp.charAt(i) == 'c')
				{
					index = index+Math.pow(16, 5-i) * 12;
				}
				else if(hex_temp.charAt(i) == 'd')
				{
					index = index+Math.pow(16, 5-i) * 13;
				}
				else if(hex_temp.charAt(i) == 'e')
				{
					index = index+Math.pow(16, 5-i) * 14;
				}
				else if(hex_temp.charAt(i) == 'f')
				{
					index = index+Math.pow(16, 5-i) * 15;
				}
				else
				{
					index = index+Math.pow(16, 5-i) * Integer.parseInt(hex_temp.charAt(i)+"");
				}
				
			}
			//System.out.println("MD"+index);
			if(md5[(int)index] == 1)
			{
				check++;
			}
			
			//Check sha512
			MessageDigest sha = MessageDigest.getInstance("SHA-512");
			sha.update(word.getBytes());
			outputsha = sha.digest();
			//System.out.println(new BigInteger(1,outputsha).toString(16));
			hex_temp = new BigInteger(1,outputsha).toString(16).substring(0, 6);
			//System.out.println(hex_temp);
			//hex_temp = "aa";
			index = 0;
			for(int i=5;i>=0;i--)
			{
			
				if(hex_temp.charAt(i) == 'a')
				{
					index = index+Math.pow(16, 5-i) * 10;
				}
				else if(hex_temp.charAt(i) == 'b')
				{
					index = index+Math.pow(16, 5-i) * 11;
				}
				else if(hex_temp.charAt(i) == 'c')
				{
					index = index+Math.pow(16, 5-i) * 12;
				}
				else if(hex_temp.charAt(i) == 'd')
				{
					index = index+Math.pow(16, 5-i) * 13;
				}
				else if(hex_temp.charAt(i) == 'e')
				{
					index = index+Math.pow(16, 5-i) * 14;
				}
				else if(hex_temp.charAt(i) == 'f')
				{
					index = index+Math.pow(16, 5-i) * 15;
				}
				else
				{
					index = index+Math.pow(16, 5-i) * Integer.parseInt(hex_temp.charAt(i)+"");
				}
				
			}
			//System.out.println("sha"+index);
			if(sha512[(int)index] == 1)
			{
				check++;
			}
			if(check==2)
			{
				match = true;
			}
			
		}
		catch (Exception e)
		{
			
		}
		return match;
	}
	
	static void loadPasswordDictionary()
	{
		int index=0;
		try {
			BufferedReader br = new BufferedReader(new FileReader("./resources/dict_md5.txt"));
			String word;
			while ((word = br.readLine()) != null)
			{
				index = Integer.parseInt(word);
				md5[index] = 1;

			}
			br.close();
				
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		index=0;
		try 
		{
			BufferedReader br = new BufferedReader(new FileReader("./resources/dict_sha512.txt"));
			String word;
			while ((word = br.readLine()) != null)
			{
				index = Integer.parseInt(word);
				sha512[index] = 1;
			}	
			br.close();
		} 
		catch (Exception e) 
		{
			
		}
		index=0;
		try 
		{
			BufferedReader br = new BufferedReader(new FileReader("common_sha512.txt"));
			String word;
			while ((word = br.readLine()) != null)
			{
				index = Integer.parseInt(word);
				sha512[index] = 1;
			}	
			br.close();
		} 
		catch (Exception e) 
		{
			
		}
		index=0;
		try 
		{
			BufferedReader br = new BufferedReader(new FileReader("common_md5.txt"));
			String word;
			while ((word = br.readLine()) != null)
			{
				index = Integer.parseInt(word);
				md5[index] = 1;
			}	
			br.close();
		} 
		catch (Exception e) 
		{
			
		}
	}

}
