package com.csci6545.evoting.bl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.csci6545.evoting.be.Ballot;
import com.csci6545.evoting.be.BcBallotFactor;
import com.csci6545.evoting.be.Election;
import com.csci6545.evoting.be.User;
import com.csci6545.evoting.be.Voter;
import com.csci6545.evoting.pl.MakeVoteGui;
import com.csci6545.evoting.pl.VoterGui;

public class MgrResult {
	public static int getEntry(BcBallotFactor [] bcFactor, byte [] bcBallot){
		
		for(int i = 0;i < bcFactor.length;i++)
		{
			if(Arrays.equals(bcFactor[i].getBcBallot(), bcBallot))
			{
				return bcFactor[i].getEntry();
			}
		}
		return 0;
	}
	
	public static Boolean verifyBallot(BcBallotFactor [] bcFactor, byte [] ballot){
		
		for(int i = 0;i < bcFactor.length;i++)
		{
			if(Arrays.equals(bcFactor[i].getBcBallot(), ballot))
			{
				return true;
			}
		}
		return false;
	}
	
	public static Boolean isVoted(Voter v,long electionId){
		
		if(v.getElectionIdList().isEmpty())
		{
			return false;
		}
		for(int i = 0;i < v.getElectionIdList().size();i++)
		{
			if(v.getElectionIdList().get(i) == electionId)
			{
				return true;
			}
		}
		return false;
	}
	
	public static Map<Long, Integer> storeBcEntry(int entry, long eletcionId, Voter v)
	{
		Map<Long, Integer> tempEntry = v.getMapElectionEntry();
		if(tempEntry == null)
		{
			tempEntry = new HashMap<Long,Integer>();
		}
		else
		{
			tempEntry.put(eletcionId, entry);
		}
		return tempEntry;				
	}
	
	public static int[] getResult(BcBallotFactor [] bcFactor, Election election)
	{
		List<Long> tally = new ArrayList<Long>();
		long maxId = 0;
		Set mapSet = (Set) election.getCandidate().entrySet();
		Iterator iterate = mapSet.iterator();		
		while(iterate.hasNext()){
			Map.Entry cadidate = (Map.Entry) iterate.next();
			long candidateId = Integer.parseInt(cadidate.getKey().toString().replace(" ", ""));		
			if(maxId < candidateId){
				maxId = candidateId;
			}
		}
		
		for( int i =0;i<bcFactor.length;i++)
		{
			Ballot eachBallot = MgrBallot.recoverBallot(bcFactor[i].getBallot());
			tally.add(eachBallot.getcandidateID());
			
		}
		int [] result = new int [(int) maxId+1];
		for(int i =0;i<tally.size();i++){
			result[tally.get(i).intValue()]++;
		}
		return result;
	}
	public static Boolean verifyMyBallot(BcBallotFactor [] bcFactor, int entry, byte [] myBallot)
	{
		byte [] ballot = null;
		for(int i = 0;i<bcFactor.length;i++){
			if(entry == bcFactor[i].getEntry())
			{
				ballot = bcFactor[i].getBallot();
				break;
			}
		}
		return Arrays.equals(myBallot, ballot);
	}
	public static byte [] authorizedUserBallotToBsytes(List<String> allAuthorized)
	{
		String allStr = "";
		for(int i=0;i<allAuthorized.size();i++)
		{
			if (i == allAuthorized.size()-1)
			{
				allStr = allStr+allAuthorized.get(i);
			
			}
			else
			{
				allStr = allStr+allAuthorized.get(i)+",";
						
			}
		}
		return allStr.getBytes();
	}
	
	public static List<String> recoverAuthorizedUserBallot(int entry, byte[] allAuthorized)
	{
		List<String> all = new ArrayList<String>();
		String titleMsg = new String(allAuthorized);
		for (String retval: titleMsg.split(",",entry)){
			all.add(retval);
		}
		return all;
	}
}
