package com.csci6545.evoting.bl;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.util.Properties;

import javax.net.ServerSocketFactory;
import javax.net.SocketFactory;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLServerSocket;

import com.csci6545.evoting.dl.LoadConfig;


public class MgrSSL {
	//public static final String SERVER_IP_ADDRESS = "161.253.119.41";
	
	public static final String SERVER_IP_ADDRESS = "127.0.0.1";
	private static final int SERVER_PORT = 12345;
	private static final int SERVER_LISTENING_PORT = 12345;
	private static final String PATH_TO_TRUSTSOTRE = "./resources/trustEvotingServer.jks";
	//public static final String ALIAS = "evoting.server";
	private static final String PATH_TO_KEYSOTRE = "./resources/evotingServer.jks";
	private static final String PWD_OF_TRUSTSOTRE = "123456";
	private static final String[] CIPHER_SUITES = 
		{"TLS_RSA_WITH_AES_128_CBC_SHA",
		"TLS_DHE_RSA_WITH_AES_128_CBC_SHA"};
	public static Socket initSSLClientToServerSocket()
	{
		MgrSystemProperties.initClientSystemProperty(LoadConfig.PATH_TO_TRUSTSTORE, LoadConfig.PWD_OF_TRUSTSTORE);
		SocketFactory f = SSLSocketFactory.getDefault();

        try {
        	SSLSocket s = 
        			(SSLSocket) f.createSocket(LoadConfig.SERVER_IP_ADDRESS, LoadConfig.SERVER_PORT);
        	s.setEnabledCipherSuites(CIPHER_SUITES);
			return s;
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public static ServerSocket initSSLServerCounterChannelSocket()
	{
		//LoadConfig.loadServerConfig();
		MgrSystemProperties.initClientSystemProperty(LoadConfig.PATH_TO_TRUSTSTORE, LoadConfig.PWD_OF_TRUSTSTORE);
		MgrSystemProperties.initServerSystemProperty(LoadConfig.PATH_TO_KEYSTORE, LoadConfig.PWD_OF_KEYSTORE);
		ServerSocketFactory f = SSLServerSocketFactory.getDefault();  
		SSLServerSocket ss = null;
		try {
			ss = (SSLServerSocket) f.createServerSocket(LoadConfig.WAITING_COUNTER_PORT);
			//ss.setEnabledCipherSuites(CIPHER_SUITES);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return ss;
	}
	
	public static Socket initSSLCounterToServerSocket()
	{
		
		MgrSystemProperties.initClientSystemProperty(LoadConfig.PATH_TO_TRUSTSTORE, LoadConfig.PWD_OF_TRUSTSTORE);
		MgrSystemProperties.initServerSystemProperty(LoadConfig.PATH_TO_KEYSTORE, LoadConfig.PWD_OF_KEYSTORE);
		Properties props = System.getProperties();
		SocketFactory f = SSLSocketFactory.getDefault();

        try {
        	SSLSocket s = 
        			(SSLSocket) f.createSocket(LoadConfig.SERVER_IP_ADDRESS, LoadConfig.SERVER_PORT);
        	s.setEnabledCipherSuites(CIPHER_SUITES);
			return s;
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public static Socket initSSLClientToCounterSocket(String counterIP, int counterPort)
	{
		MgrSystemProperties.initClientSystemProperty(LoadConfig.PATH_TO_TRUSTSTORE, LoadConfig.PWD_OF_TRUSTSTORE);
		SocketFactory f = SSLSocketFactory.getDefault();

        try {
        	SSLSocket s = 
        			(SSLSocket) f.createSocket(counterIP, counterPort);
        	//s.setEnabledCipherSuites(CIPHER_SUITES);
			return s;
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public static ServerSocket initSSLCounterSocket()
	{
		//LoadConfig.loadServerConfig();
		MgrSystemProperties.initServerSystemProperty(LoadConfig.PATH_TO_KEYSTORE, LoadConfig.PWD_OF_KEYSTORE);
		ServerSocketFactory f = SSLServerSocketFactory.getDefault();  
		SSLServerSocket ss = null;
		try {
			ss = (SSLServerSocket) f.createServerSocket(LoadConfig.COUNTER_PORT);
			//ss.setEnabledCipherSuites(CIPHER_SUITES);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return ss;
	}
	
	public static ServerSocket initSSLServerSocket()
	{
		//LoadConfig.loadServerConfig();
		MgrSystemProperties.initServerSystemProperty(LoadConfig.PATH_TO_KEYSTORE, LoadConfig.PWD_OF_KEYSTORE);
		ServerSocketFactory f = SSLServerSocketFactory.getDefault();  
		SSLServerSocket ss = null;
		try {
			ss = (SSLServerSocket) f.createServerSocket(LoadConfig.SERVER_PORT);
			//ss.setEnabledCipherSuites(CIPHER_SUITES);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return ss;
	}
	
	public static KeyPair getServerKeypairFromKeyStore() 
	{
		try
    	{
			//LoadConfig.loadServerConfig();			
			FileInputStream is = new FileInputStream(LoadConfig.PATH_TO_KEYSTORE);
	
	        KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
	        keystore.load(is, LoadConfig.PWD_OF_KEYSTORE.toCharArray());
	
	        Key key = keystore.getKey(LoadConfig.SERVER_ALIAS, LoadConfig.PWD_OF_KEYSTORE.toCharArray());
	        if (key instanceof PrivateKey) {
	          // Get certificate of public key
	        	Certificate cert = keystore.getCertificate(LoadConfig.SERVER_ALIAS);
	
	         // Get public key
	        	PublicKey publicKey = cert.getPublicKey();
	
	         // Return a key pair
	        	return  new KeyPair(publicKey, (PrivateKey) key);
	        }
	        else
	        {
	        	return null;
	        }
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;
	    
	}
	
	public static KeyPair getCounterKeypairFromKeyStore() 
	{
		try
    	{
			//LoadConfig.loadServerConfig();			
			FileInputStream is = new FileInputStream(LoadConfig.PATH_TO_KEYSTORE);
	
	        KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
	        keystore.load(is, LoadConfig.PWD_OF_KEYSTORE.toCharArray());
	
	        Key key = keystore.getKey(LoadConfig.COUNTER_ALIAS, LoadConfig.PWD_OF_KEYSTORE.toCharArray());
	        if (key instanceof PrivateKey) {
	          // Get certificate of public key
	        	Certificate cert = keystore.getCertificate(LoadConfig.COUNTER_ALIAS);
	
	         // Get public key
	        	PublicKey publicKey = cert.getPublicKey();
	
	         // Return a key pair
	        	return  new KeyPair(publicKey, (PrivateKey) key);
	        }
	        else
	        {
	        	return null;
	        }
		}
		catch (Exception e)
		{
			System.out.println(e);
		}
		return null;
	    
	}
	public static PublicKey getCounterPublicKeyFromTrustStore() {
		try
		{
			FileInputStream is = new FileInputStream(LoadConfig.PATH_TO_TRUSTSTORE);
		
		    KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
		    keystore.load(is, LoadConfig.PWD_OF_TRUSTSTORE.toCharArray());
		    Certificate cert = keystore.getCertificate(LoadConfig.COUNTER_ALIAS);
		    return  cert.getPublicKey();
		}
		catch (Exception e)
		{
			System.out.println(e);
		}
		return null;

	}
	
	public static PublicKey getServerPublicKeyFromTrustStore() {
		try
		{
			FileInputStream is = new FileInputStream(LoadConfig.PATH_TO_TRUSTSTORE);
		
		    KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
		    keystore.load(is, LoadConfig.PWD_OF_TRUSTSTORE.toCharArray());
		    Certificate cert = keystore.getCertificate(LoadConfig.SERVER_ALIAS);
		    return  cert.getPublicKey();
		}
		catch (Exception e)
		{
			System.out.println(e);
		}
		return null;

	}
}
