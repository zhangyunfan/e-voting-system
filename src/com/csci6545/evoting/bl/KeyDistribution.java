package com.csci6545.evoting.bl;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;

public class KeyDistribution {

	public static byte [] getUserPublicKey(String userId) 
	{
		//get user public key from database
		return null;
	}
		
	public static KeyPair GenerateKeyPairs() throws NoSuchAlgorithmException 
	{
		//Generate Key pair
		return MgrConfidentiality.generateRSAKeyPair();
	}
	
	public static PrivateKey GetPrivateKey(KeyPair keyPair) 
	{
		//Get User Private Key
		return keyPair.getPrivate();
	}
	
	public static PublicKey GetPublicKey(KeyPair keyPair) 
	{
		//Get User Public Key
		return keyPair.getPublic();
	}
	
	public static boolean savePublicKey(String userId, PublicKey publicKey)
	{
		//Save public key to data base
		return false;
	}
	
	    
}