package com.csci6545.evoting.bl;

import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.spec.InvalidKeySpecException;
import java.util.Date;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import java.nio.file.Paths;
import java.nio.file.Files;

import com.csci6545.evoting.bl.AsymCrypto;
import com.csci6545.evoting.bl.HybridCrypto;
import com.csci6545.evoting.bl.MgrHash;
/**
 * 
 * @author Yunfan Zhang
 *
 */
public class MgrConfidentiality {
	private static final int RSA_KEY_SIZE = 2048;
	private static final int SECRETE_KEY_SIZE = 128;
	private static final int SECRETE_KEY_BYTE_SIZE = SECRETE_KEY_SIZE/8;
	private static final int SECRETE_KEY_OFFSET=0;
	private static final int IV_SIZE = 128/8;
	private static final int IV_OFFSET=128/8;
	private static String HASH_MODE="SHA-256";
	
	/**
	 * @param 
	 * @return a random RSA key pair
	 * @throws NoSuchAlgorithmException 
	 * 
	 */
	public static KeyPair generateRSAKeyPair() 
			throws NoSuchAlgorithmException{
		KeyPairGenerator generator = 
				KeyPairGenerator.getInstance("RSA");
		generator.initialize(RSA_KEY_SIZE);
		return generator.generateKeyPair();
	}
	
	/**
	 * @param 
	 * @return a random secrete key of SECRETE_KEY_SIZE bits for encryption
	 * @throws NoSuchProviderException 
	 * @throws NoSuchAlgorithmException 
	 * 
	 */
	public static byte[] generateSecreteKey() 
			throws NoSuchAlgorithmException, NoSuchProviderException{
		KeyGenerator generator = KeyGenerator.getInstance("AES", "SunJCE");
		generator.init(SECRETE_KEY_SIZE);
		Key secreteKey = generator.generateKey();
		return secreteKey.getEncoded();
	}
	
	
	/**
	 * @param plainBallot a byte array of the plain text of a ballot
	 * @param publicKey a byte array of the raw binary public key
	 * @return a byte array of the raw cipher text of ballot (encrypted by RSA)
	 * @throws NoSuchPaddingException 
	 * @throws NoSuchAlgorithmException 
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 * @throws InvalidKeySpecException 
	 * @throws InvalidKeyException 
	 * 
	 */
	public static byte[] RSAEncryption(byte[] plainBallot,
			byte[] publicKey) throws NoSuchAlgorithmException,
			NoSuchPaddingException, InvalidKeyException, 
			InvalidKeySpecException, IllegalBlockSizeException, 
			BadPaddingException {
			AsymCrypto asymcryto = new AsymCrypto();
			return asymcryto.encrypt(plainBallot, publicKey);
	}
	/**
	 * @param cipherBytes a byte array of the RSA cipher text of a ballot
	 * @param privateKey a byte array of the raw binary private key
	 * @return a byte array of the raw plain text of ballot  
	 * @throws NoSuchPaddingException 
	 * @throws NoSuchAlgorithmException 
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 * @throws InvalidKeySpecException 
	 * @throws InvalidKeyException 
	 * 
	 */
	public static byte[] RSADecryption(byte[] cipherBytes,
			byte[] privateKey) throws NoSuchAlgorithmException, 
			NoSuchPaddingException, InvalidKeyException, 
			InvalidKeySpecException, IllegalBlockSizeException, 
			BadPaddingException{
		AsymCrypto asymcryto = new AsymCrypto();
		return asymcryto.decrypt(cipherBytes, privateKey);
	}
	
	/**
	 * @param plainBallotBytes a byte array of raw binary plain test of Ballot
	 * @param secretKey a byte array of raw binary secrete key
	 * @param ivBytes a byte array of raw binary initialization vector
	 * @return a byte array of raw cipher ballot
	 * @throws NoSuchPaddingException 
	 * @throws NoSuchAlgorithmException 
	 * @throws InvalidAlgorithmParameterException 
	 * @throws InvalidKeyException 
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 */
	public static byte[] encryptAESBallot(byte[] plainBallotBytes,
			byte[] secretKey,byte[] ivBytes) 
					throws NoSuchAlgorithmException, 
					NoSuchPaddingException, InvalidKeyException, 
					InvalidAlgorithmParameterException, 
					IllegalBlockSizeException, BadPaddingException{
		SecretKeySpec aesKeySpec = new SecretKeySpec(secretKey,"AES");
		IvParameterSpec ivSpec = new IvParameterSpec(ivBytes);
		Cipher aesCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		aesCipher.init(Cipher.ENCRYPT_MODE,aesKeySpec,ivSpec);
		return aesCipher.doFinal(plainBallotBytes);
	}
	
	/**
	 * @param cipherBallotBytes a byte array of raw binary cipher ballot
	 * @param secretKey a byte array of raw binary secret key
	 * @param ivBytes a byte array of raw binary initialization vector
	 * @return a byte array of raw plain ballot
	 * @throws NoSuchPaddingException 
	 * @throws NoSuchAlgorithmException 
	 * @throws InvalidAlgorithmParameterException 
	 * @throws InvalidKeyException 
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 */
	public static byte[] decryptAESBallot(byte[] cipherBallotBytes,
			byte[] secretKey, byte[] ivBytes) 
					throws NoSuchAlgorithmException, NoSuchPaddingException, 
					InvalidKeyException, InvalidAlgorithmParameterException, 
					IllegalBlockSizeException, BadPaddingException{
		SecretKeySpec aesKeySpec = new SecretKeySpec(secretKey,"AES");
		IvParameterSpec ivSpec = new IvParameterSpec(ivBytes);
		Cipher aesCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		aesCipher.init(Cipher.DECRYPT_MODE,aesKeySpec,ivSpec);
		return aesCipher.doFinal(cipherBallotBytes);
	}
	
	/**
	 * 
	 * @param publicKey a byte array of public key for encrypting the symmetric key
	 * @param plainText a byte array of the plain text
	 * @param ivBytes a byte array of raw binary initialization vector
	 * @return a byte array consisting the length of the encrypted symmetric key (the integer of the length is represented by the first two bytes), the bytes of encrypted symmetric key, and the bytes of the encrypted plain text
	 * @throws InvalidKeySpecException 
	 * @throws NoSuchProviderException 
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 * @throws InvalidAlgorithmParameterException 
	 * @throws NoSuchPaddingException 
	 * @throws NoSuchAlgorithmException 
	 * @throws InvalidKeyException 
	 * 
	 */
	public static byte[] hybridEncryption(byte[] plainText,
			byte[] publicKey, byte[] ivBytes) 
					throws InvalidKeyException, NoSuchAlgorithmException, 
					NoSuchPaddingException, InvalidAlgorithmParameterException, 
					IllegalBlockSizeException, BadPaddingException, 
					NoSuchProviderException, InvalidKeySpecException {
			return HybridCrypto.encryptAll(publicKey, plainText, ivBytes);
	}
	
	/**
	 * 
	 * @param cipherSymetricKeyAndText a byte array consisting the length of the encrypted symmetric key (the integer of the length is represented by the first two bytes), the bytes of encrypted symmetric key, and the bytes of the encrypted plain text
	 * @param privateKey a byte array of private key for decrypting the symmetric key
	 * @param ivBytes a byte array of raw binary initialization vector
	 * @return a byte array of the plain text
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 * @throws InvalidAlgorithmParameterException 
	 * @throws NoSuchPaddingException 
	 * @throws InvalidKeySpecException 
	 * @throws NoSuchProviderException 
	 * @throws NoSuchAlgorithmException 
	 * @throws InvalidKeyException 
	 *
	 */
	public static byte[] hybridDecryption(byte[] cipherSymetricKeyAndText,
			byte[] privateKey, byte[] ivBytes) 
					throws InvalidKeyException, NoSuchAlgorithmException, 
					NoSuchProviderException, InvalidKeySpecException, 
					NoSuchPaddingException, InvalidAlgorithmParameterException, 
					IllegalBlockSizeException, BadPaddingException {
		return HybridCrypto.decryptAll(cipherSymetricKeyAndText, 
				privateKey, ivBytes);

	}
	/**
	 * @param password a string of the password for encrypting the secrete key
	 * @return a byte array of raw binary hashed password
	 * @throws NoSuchAlgorithmException 
	 * @throws InvalidKeySpecException 
	 */
	public static byte[] generatePasswordHash(String password)
			throws NoSuchAlgorithmException, InvalidKeySpecException{
		PasswordToSecreteKey passwordToSecreteKey = new PasswordToSecreteKey(password);
		return passwordToSecreteKey.generateSecreteKey();
	}
	
	public static byte[] getSecretKey(byte[] hashedBytes){
		byte[] returnKey = new byte[SECRETE_KEY_BYTE_SIZE];
		for(int i = 0;i<SECRETE_KEY_BYTE_SIZE;i++)
		{
			returnKey[i]=hashedBytes[(SECRETE_KEY_BYTE_SIZE)+i];
		}
		return returnKey;
	}

	public static byte[] getIV(byte[] hashedBytes){
		byte[] returnIV = new byte[IV_SIZE];
		for(int i=0;i<IV_SIZE;i++){
			returnIV[i] = hashedBytes[IV_OFFSET+i];
		}
		return returnIV;
	}
	
	public static byte[] getPrivateKeyIV(byte[] hashedBytes){
		byte[] returnIV = new byte[IV_SIZE];
		for(int i=0;i<IV_SIZE;i++){
			returnIV[i] = hashedBytes[IV_OFFSET+i];
		}
		return returnIV;
	}
	
	public static byte[] getRandomIV(){
		byte[] returnIV = new byte[IV_SIZE];
		SecureRandom secureRandom = new SecureRandom();
		secureRandom.nextBytes(returnIV);
		return returnIV;
	}
	
	public static byte  []  getPrivateKey(String filename,
			 byte [] key, byte [] iv) /*throws IOException, 
			 NoSuchAlgorithmException, NoSuchPaddingException, 
			 InvalidKeyException, InvalidAlgorithmParameterException, 
			 IllegalBlockSizeException, BadPaddingException*/
	    {
	    try	
	    {
		 SecretKey k = new SecretKeySpec(key,"AES");
		    	byte[] encryptedPrivateBytes = 	
						Files.readAllBytes(Paths.get(filename));
				Cipher c = Cipher.getInstance("AES/CBC/PKCS5PADDING");
				
				c.init(Cipher.DECRYPT_MODE, k,new IvParameterSpec(iv));
				byte [] privateKeyBytes = c.doFinal(encryptedPrivateBytes); 
				return privateKeyBytes;
	    }
		catch	(Exception e)
		{
		
		}
		return null;
	    }
	 public static void byteToFile(byte[] bytes, String filename)
			 throws IOException{
		 FileOutputStream fos = new FileOutputStream(filename);
		 fos.write(bytes);
		 fos.close();
		 
	 }
	
}

