package com.csci6545.evoting.bl;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.Enumeration;
/**
 * 
 * @author Yunfan Zhang
 *
 */
public class MgrIP4Address {
	private static final String ETHER_NAME_PATTERN = "wlan0";
	private static final String IP4_ADDRESS_PATTERN = 
			"^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
					"([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
					"([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
					"([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";
	/**
	 * @author Yunfan Zhang
	 * @return a InetAddress interface of current ethernet interface
	 * @throws SocketException 
	 */
	private static InetAddress getCurrentIP4Interface() 
			throws SocketException{
		Enumeration<NetworkInterface> networkInterfaceSet = 
				NetworkInterface.getNetworkInterfaces();
		for (; networkInterfaceSet.hasMoreElements();){
			NetworkInterface netInterface = networkInterfaceSet.nextElement();
			if(netInterface.getName().contains(ETHER_NAME_PATTERN)){
				Enumeration<InetAddress> addressEnum = 
						netInterface.getInetAddresses();
				for(;addressEnum.hasMoreElements();){
					InetAddress inet4Address = addressEnum.nextElement();
					Pattern pattern = Pattern.compile(IP4_ADDRESS_PATTERN);
					Matcher matcher = 
							pattern.matcher(inet4Address.getHostAddress());
					if(matcher.find()){
						return inet4Address;
					}
				}
				throw new SocketException("No IP4 address has been "
						+ "found!");
			}
		}
		throw new SocketException("No "+ETHER_NAME_PATTERN+" "
				+"has been found!");
	}
	/**
	 * @author Yunfan Zhang
	 * @return a String of IPv4 address 
	 * @throws SocketException 
	 */
	public static String getCurrentIP4Address() 
			throws SocketException{
		InetAddress inet4Address = getCurrentIP4Interface();
		return inet4Address.getHostAddress();
	}
}
