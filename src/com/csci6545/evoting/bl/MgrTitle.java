package com.csci6545.evoting.bl;

import java.math.BigInteger;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.csci6545.evoting.be.Election;
import com.csci6545.evoting.be.Message;
import com.csci6545.evoting.be.Title;
import com.csci6545.evoting.be.User;

public class MgrTitle {
	
	public static byte [] allTitle() throws SQLException
	{
		List<Title> listTitle = com.csci6545.evoting.dl.MgrTitle.getAllTitle();
		String titleStr = "";
		for(int i=0;i<listTitle.size();i++)
		{
			if (i == listTitle.size()-1)
			{
				titleStr = titleStr+listTitle.get(i).getTitleName()+","+listTitle.get(i).getPermission();
			
			}
			else
			{
				titleStr = titleStr+listTitle.get(i).getTitleName()+","+listTitle.get(i).getPermission()+"/";
						
			}
		}
		return titleStr.getBytes();
	}
	
	public static byte [] eligibleTitle(List<Title> listTitle)
	{
		String titleStr = "";
		for(int i=0;i<listTitle.size();i++)
		{
			if (i == listTitle.size()-1)
			{
				titleStr = titleStr+listTitle.get(i).getTitleName()+","+listTitle.get(i).getPermission();
			
			}
			else
			{
				titleStr = titleStr+listTitle.get(i).getTitleName()+","+listTitle.get(i).getPermission()+"/";
						
			}
		}
		return titleStr.getBytes();
	}
	
	public static List<Title> recoverTitleList(int entry, byte[] allTitleBytes)
	{
		List<Title> titleList = new ArrayList<Title>();
		String titleMsg = new String(allTitleBytes);
		int i = 0;
		for (String retval: titleMsg.split("/",entry)){
			titleList.add(recoverTitle(retval.getBytes()));
		    i++;
		}
		return titleList;
	}
	public static Title recoverTitle(byte [] byteTitle) {
		try
		{
			Title title = new Title();
			String stringElection = new String(byteTitle);
			String[] parts = new String [2];
			int i = 0;
			for (String retval: stringElection.split(",",2)){
			   parts[i] = retval;
			   i++;
			}
			title.setTitleName(parts[0]);
			title.setPermission(Integer.parseInt(parts[1]));
			return title;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return null;
	}
}
