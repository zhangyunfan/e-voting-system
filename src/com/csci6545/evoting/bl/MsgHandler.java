package com.csci6545.evoting.bl;

import java.io.IOException;
import java.math.BigInteger;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import Decoder.BASE64Encoder;

import com.csci6545.evoting.be.Ballot;
import com.csci6545.evoting.be.BcBallotFactor;
import com.csci6545.evoting.be.Counter;
import com.csci6545.evoting.be.Election;
import com.csci6545.evoting.be.Event;
import com.csci6545.evoting.be.Log;
import com.csci6545.evoting.be.Message;
import com.csci6545.evoting.be.Supervisor;
import com.csci6545.evoting.be.TicketBB;
import com.csci6545.evoting.be.User;
import com.csci6545.evoting.be.Voter;
import com.csci6545.evoting.dl.MgrBlindBallot;
import com.csci6545.evoting.dl.MgrCounter;
import com.csci6545.evoting.dl.MgrElection;
import com.csci6545.evoting.dl.MgrResult;
import com.csci6545.evoting.dl.MgrUser;
import com.csci6545.evoting.dl.MgrVoter;
import com.csci6545.evoting.pl.CounterChannel;
import com.csci6545.evoting.pl.ServerGui;

public class MsgHandler {
	public static String username;

	public static Message userHandleMsg(String type, byte[] msgBytes,
			Socket sock) {
		Message m = null;

		if (type.equals("AckKeyGen")) {
			m = MgrMessage.recoverAckKeyGenMsg(msgBytes);
		} else if (type.equals("AckLogin")) {

			m = MgrMessage.recoverAckLoginMsg(msgBytes);
		} else if (type.equals("AckResetPassword")) {
			m = MgrMessage.recoverAckResetPasswordMsg(msgBytes);
		} else if (type.equals("AckUpdatePassword")) {
			m = MgrMessage.recoverAckUpdatePasswordMsg(msgBytes);
		}

		else if (type.equals("AckUpdatePersonalInfo")) {
			m = MgrMessage.recoverAckUpdatePersonalInfoMsg(msgBytes);
		}

		/* added for log for audit
		if (m != null) {
			Event e = new Event(Event.ALERT, m.getTicketBB().getUserId(),
					sock.getInetAddress(), new Date(), m.getDetail());
			MgrAudit.writeLog(e, new Log());
			System.out.println(e);
		}*/
		return m;
	}

	public static boolean serverHandleMsg(String type, byte[] msgBytes,
			Socket sock, PrivateKey myPrivateKey, PublicKey myPublicKey,
			SecretKey key, String counterIP, int counterPort,
			PublicKey counterPubKey) throws NoSuchAlgorithmException,
			ParseException {

		// added for log for log
		Event e = new Event();
		e.setLocation(sock.getInetAddress());
		e.setTime(new Date());
		if (type.equals("GenKey")) {
			// Store Public key to DB
			// Send Msg Back
			Message m = MgrMessage.recoverGenKeyMsg(msgBytes);
			ServerGui.appendText("Attempt to change keypair by "
					+ m.getUsername());
			// Verify username ,password, and dob
			SimpleDateFormat format = new SimpleDateFormat(
					"EEE MMM dd HH:mm:ss zzz yyyy", Locale.US);
			Date dob = format.parse(m.getDob());
			if (MgrUser.passwordAuthentcateVoter(m.getUsername(),
					m.getPassword(), m.getDob())) {

				// user must change key not using OTP
				System.out.println(MgrUser.getUserIsOTP(m.getUsername()));
				if (MgrUser.getUserIsOTP(m.getUsername()) == 0) {
					User u = new Voter();
					u.setUserId(m.getUsername());
					MgrUser.setUserPublicKey(u, m.getUserPublicKey()
							.getEncoded());

					MgrMessage.sendMsg(
							sock,
							MgrMessage.ackKeyGenMsg("succeeded",
									m.getUsername()));
					ServerGui.appendText("Keypair changed succeeded: "
							+ m.getUsername());
					try {
						sock.close();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					// added for log
					e.setObjectName(m.getUsername());
					e.setDetail(type + " - Keypair Changed succeeded");
					e.setSeverity(Event.INFO);
					MgrAudit.writeLog(e, new Log());
				}

				// failed
				else {
					MgrMessage.sendMsg(sock, MgrMessage.ackKeyGenMsg(
							"you cannot use OTP to encrypt your private key",
							m.getUsername()));
					ServerGui
							.appendText("Keypair changed failed (OTP is used): "
									+ m.getUsername());
					try {
						sock.close();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					// added for log
					e.setObjectName(m.getUsername());
					e.setDetail(type
							+ " - Keypair changed failed (OTP is used)");
					e.setSeverity(Event.INFO);

					MgrAudit.writeLog(e, new Log());
				}
				return true;

				// incorrect password, username, or dob
			} else {
				MgrMessage.sendMsg(sock, MgrMessage.ackKeyGenMsg(
						"username, password, or dob is incorrect",
						m.getUsername()));
				ServerGui.appendText("Keypair changed failed: "
						+ m.getUsername());
				try {
					sock.close();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				// added for log
				e.setObjectName(m.getUsername());
				e.setDetail(type + " - Keypair changed failed");
				e.setSeverity(Event.WARN);
				MgrAudit.writeLog(e, new Log());

				return false;
			}

		} else if (type.equals("Login")) {

			Message m = MgrMessage.recoverLoginMsg(msgBytes);
			ServerGui.appendText("Attempt to Login by " + m.getUsername());
			User user = MgrUser.passwordAuthentcateVoter(m.getUsername(),
					m.getPassword());
			// Check login attempts
			if ((user != null && user.getLoginAttempts() < 3) || user != null
					&& user.getIsOtp() == 1) {
				user.setUserId(m.getUsername());
				PublicKey userPublicKey = MgrIntegrity.byteToPublicKey(MgrUser
						.getUserPublicKey(user));
				String verifyMsg = m.getUsername() + "," + m.getPassword();
				// Check if signature is correct
				if (MgrIntegrity.verify(userPublicKey, verifyMsg.getBytes(), m
						.getUserSignature().toByteArray())
						&& user.getIsOtp() != 1) {
					MgrUser.setUserLoginAttempts(user.getUserId(), 0);
					username = m.getUsername();
					if (user.getTitle().equals("MANAGER")) {
						User sup = new Supervisor();
						// sup = user;
						sup.setfName(user.getfName());
						sup.setlName(user.getlName());
						sup.setUserId(user.getTitle());
						sup.setTitle(user.getTitle());
						sup.setIsOtp(user.getIsOtp());
						sup.setEmail(user.getEmail());
						sup.setPermission(user.getPermission());
						TicketBB tb = MgrTicket.createTicketBB(m.getUsername(),
								sock.getInetAddress().toString(),
								user.getTitle(), new Date(), myPrivateKey,
								user.getPermission());
						MgrMessage.sendMsg(sock,
								MgrMessage.ackLoginMsg(tb, "succeeded", sup));
						ServerGui.appendText("Login succeeded: "
								+ m.getUsername());
						System.out.println(new String(tb.getBytes()));
						// added for log
						e.setObjectName(m.getUsername());
						e.setDetail(user.getTitle() + " " + type
								+ " - Login succeeded");
						e.setSeverity(Event.INFO);
						MgrAudit.writeLog(e, new Log());

						return true;
					} else if (user.getTitle().equals("Counter")) {
						User counter = new Counter();
						counter = user;
						/*
						 * counter.setfName(user.getfName());
						 * counter.setlName(user.getlName());
						 * counter.setUserId(user.getTitle());
						 * counter.setTitle(user.getTitle());
						 * counter.setIsOtp(user.getIsOtp());
						 * counter.setEmail(user.getEmail());
						 */
						TicketBB tb = MgrTicket.createTicketBB(m.getUsername(),
								sock.getInetAddress().toString(),
								counter.getTitle(), new Date(), myPrivateKey,
								user.getPermission());
						MgrMessage.sendMsg(sock, MgrMessage.ackLoginMsg(tb,
								"succeeded", counter));
						ServerGui.appendText("Login succeeded: "
								+ m.getUsername());
						// added for log
						e.setObjectName(m.getUsername());
						e.setDetail(user.getTitle() + " " + type
								+ " - Login succeeded");
						e.setSeverity(Event.INFO);
						MgrAudit.writeLog(e, new Log());

						return true;
					} else {
						User voter = new Voter();
						voter = user;

						/*
						 * voter.setfName(user.getfName());
						 * voter.setlName(user.getlName());
						 * voter.setUserId(user.getTitle());
						 * voter.setTitle(user.getTitle());
						 * voter.setIsOtp(user.getIsOtp());
						 * voter.setEmail(user.getEmail());
						 */
						TicketBB tb = MgrTicket.createTicketBB(m.getUsername(),
								sock.getInetAddress().toString(),
								voter.getTitle(), new Date(), myPrivateKey,
								user.getPermission());
						MgrMessage.sendMsg(sock,
								MgrMessage.ackLoginMsg(tb, "succeeded", voter));
						ServerGui.appendText("Login succeeded: "
								+ m.getUsername());
						// added for log
						e.setObjectName(m.getUsername());
						e.setDetail(user.getTitle() + " " + type
								+ " - Login succeeded");
						e.setSeverity(Event.INFO);
						MgrAudit.writeLog(e, new Log());

						return true;
					}
				}
				// if signature is incorrect but OTP login
				else if (user != null && user.getIsOtp() == 1) {
					username = m.getUsername();
					TicketBB tb = MgrTicket.createTicketBB(m.getUsername(),
							sock.getInetAddress().toString(), "otp",
							new Date(), myPrivateKey, user.getPermission());
					ServerGui.appendText("Logged in by: " + m.getUsername()
							+ " using OTP");
					MgrUser.setUserLoginAttempts(user.getUserId(), 0);
					Voter unknowVoter = new Voter();
					unknowVoter.setfName("unknown");
					unknowVoter.setfName("unknown");

					MgrMessage.sendMsg(sock,
							MgrMessage.ackLoginMsg(tb, "otp", unknowVoter));
					// added for log
					e.setObjectName(m.getUsername());
					e.setDetail(user.getTitle() + " " + type
							+ " - Login using OTP");
					e.setSeverity(Event.INFO);
					MgrAudit.writeLog(e, new Log());
					return false;
				}
				// if signature is incorrect
				else {
					int attempts = user.getLoginAttempts();
					MgrUser.setUserLoginAttempts(m.getUsername(), ++attempts);
					TicketBB tb = MgrTicket.createTicketBB(m.getUsername(),
							sock.getInetAddress().toString(), "failed",
							new Date(), myPrivateKey, user.getPermission());
					ServerGui.appendText("Login signature incorrect: "
							+ m.getUsername());
					Voter unknowVoter = new Voter();
					unknowVoter.setfName("unknown");
					unknowVoter.setfName("unknown");

					MgrMessage.sendMsg(sock, MgrMessage.ackLoginMsg(tb,
							"Your Signature is incorrect", unknowVoter));
					try {
						sock.close();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

					// added for log
					e.setObjectName(m.getUsername());
					e.setDetail(user.getTitle() + " " + type
							+ " - Login signature incorrect:");
					e.setSeverity(Event.WARN);
					MgrAudit.writeLog(e, new Log());
					return false;
				}

			}
			// authentication failed
			else {
				TicketBB tb = MgrTicket.createTicketBB(m.getUsername(), sock
						.getInetAddress().toString(), "failed", new Date(),
						myPrivateKey, 0);
				int attempts = MgrUser.getUserLoginAttempts(m.getUsername());
				if (attempts < 3) {
					MgrUser.setUserLoginAttempts(m.getUsername(), ++attempts);
					ServerGui
							.appendText("Login failed username and/or password incorrect: "
									+ m.getUsername()
									+ " "
									+ MgrUser.getUserLoginAttempts(m
											.getUsername()) + "Attempt(s)");
					Voter unknowVoter = new Voter();
					unknowVoter.setfName("unknown");
					unknowVoter.setfName("unknown");

					MgrMessage.sendMsg(sock, MgrMessage.ackLoginMsg(tb,
							"Your username or password is incorrect",
							unknowVoter));
					try {
						sock.close();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

					// added for log
					e.setObjectName(m.getUsername());
					e.setDetail("unknown"
							+ " "
							+ type
							+ " - Login failed username and/or password incorrect."
							+ "("
							+ MgrUser.getUserLoginAttempts(m.getUsername())
							+ "attemps)");
					e.setSeverity(Event.WARN);
					MgrAudit.writeLog(e, new Log());
					return false;
				}
				// exceed maximum attempts --> lock account
				else {

					Voter unknowVoter = new Voter();
					unknowVoter.setfName("unknown");
					unknowVoter.setfName("unknown");

					ServerGui
							.appendText("Login failed username and/or password incorrect: "
									+ m.getUsername() + " has been locked");
					MgrMessage.sendMsg(sock, MgrMessage.ackLoginMsg(tb,
							"Your account has been locked. contact admin",
							unknowVoter));
					try {
						sock.close();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

					// added for log
					e.setObjectName(m.getUsername());
					e.setDetail("Unknown"
							+ " "
							+ type
							+ " - Login failed username and/or password incorrect."
							+ "(account has been locked)");
					e.setSeverity(Event.WARN);
					MgrAudit.writeLog(e, new Log());

					return false;
				}

			}

		} else if (type.equals("CreateElection")) {
			Message m = MgrMessage.recoverCreateElectionMsg(msgBytes);
			//System.out.println(m.getElection().getCandidate().get("0"));
			ServerGui.appendText("Attempt to create an election by "
					+ m.getSupUsername());
			if (MgrTicket.isTicketBBValid(m.getTicketBB(), myPublicKey,
					m.getUsername(), sock.getInetAddress().toString())) {
				// Renew Ticket
				TicketBB tb = MgrTicket.createTicketBB(m.getTicketBB()
						.getUserId(), sock.getInetAddress().toString(), m
						.getTicketBB().getRole(), new Date(), myPrivateKey, m
						.getTicketBB().getPermission());
				try {
					if(MgrUser.getUserPermission(m.getTicketBB().getUserId()) > 3)
					{
						if (MgrElection.createElection(m.getElection(),
								m.getTitle(), Arrays.asList(m.getAllowUserArray()),
								Arrays.asList(m.getDenyUserArray()))) {
							byte[] msg = MgrMessage.ackCreateElectionMsg(
									"succeeded", m.getTicketBB().getUserId(),
									m.getElectionId(), tb);
							MgrMessage.sendMsg(sock, msg);
							// added for log
							e.setObjectName(m.getTicketBB().getUserId());
							e.setDetail(type + " - Election Created.");
							e.setSeverity(Event.ALERT);
							MgrAudit.writeLog(e, new Log());
							return true;
						} else {
							byte[] msg = MgrMessage.ackCreateElectionMsg(
									"The election already existed", m.getTicketBB()
											.getUserId(), m.getElectionId(), tb);
							MgrMessage.sendMsg(sock, msg);
							// added for log
							e.setObjectName(m.getTicketBB().getUserId());
							e.setDetail(type + " - Election Creation Failed.");
							e.setSeverity(Event.ERROR);
							MgrAudit.writeLog(e, new Log());
							return true;
						}
					}
					else
					{
						byte[] msg = MgrMessage.ackCreateElectionMsg(
								"You are not allowed to create an election", m.getTicketBB()
										.getUserId(), m.getElectionId(), tb);
						MgrMessage.sendMsg(sock, msg);
						// added for log
						e.setObjectName(m.getTicketBB().getUserId());
						e.setDetail(type + " - Election Creation Failed.");
						e.setSeverity(Event.ERROR);
						MgrAudit.writeLog(e, new Log());
						return true;
					}
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			} else {
				TicketBB tb = MgrTicket.createTicketBB(m.getUsername(), sock
						.getInetAddress().toString(), "failed", new Date(),
						myPrivateKey, 0);
				byte[] msg = MgrMessage.ackCreateElectionMsg(
						"ticket expired!!", m.getTicketBB().getUserId(),
						m.getElectionId(), tb);
				MgrMessage.sendMsg(sock, msg);
				try {
					sock.close();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				// added for log
				e.setObjectName(m.getUsername());
				e.setDetail(type + " - user session timeout.");
				e.setSeverity(Event.INFO);
				MgrAudit.writeLog(e, new Log());
			}

			return false;
		} else if (type.equals("RegisterElection")) {
			Message m = MgrMessage.recoverRegisterForElectionMsg(msgBytes);
			System.out.println(new String(m.getTicketBB().getBytes()));
			if (MgrTicket.isTicketBBValid(m.getTicketBB(), myPublicKey,
					m.getUsername(), sock.getInetAddress().toString())) {
				// Renew Ticket
				// System.out.println("here");
				TicketBB tb = MgrTicket.createTicketBB(m.getTicketBB()
						.getUserId(), sock.getInetAddress().toString(), m
						.getTicketBB().getRole(), new Date(), myPrivateKey, m
						.getTicketBB().getPermission());

				ServerGui.appendText("Attempt to register an election by "
						+ m.getTicketBB().getUserId());
				byte[] msg = null;
				if (MgrElection.checkUserEligibility(m.getTicketBB()
						.getUserId(), m.getElectionId(), m.getTicketBB()
						.getRole())) {
					if (MgrUser.registerForElection(
							m.getTicketBB().getUserId(), m.getElectionId())) {
						msg = MgrMessage.ackRegisterForElectionMsg("succeeded",
								tb);
					} else {
						msg = MgrMessage.ackRegisterForElectionMsg(
								"You have already registered!!", tb);
					}

				} else {
					msg = MgrMessage.ackRegisterForElectionMsg(
							"You cannot register for this election", tb);
				}
				MgrMessage.sendMsg(sock, msg);
				// added for log
				e.setObjectName(m.getTicketBB().getUserId());
				e.setDetail(type + " - Attempt to register an election "
						+ m.getElectionId() );
				e.setSeverity(Event.INFO);
				MgrAudit.writeLog(e, new Log());
			} else {

				TicketBB tb = MgrTicket.createTicketBB(m.getUsername(), sock
						.getInetAddress().toString(), "failed", new Date(),
						myPrivateKey, 0);
				byte[] msg = MgrMessage.ackRegisterForElectionMsg(
						"ticket Expired!!", tb);
				MgrMessage.sendMsg(sock, msg);
				// added for log
				e.setObjectName(m.getTicketBB().getUserId());
				e.setDetail(type + " - Attempt to register an election "
						+ m.getElectionId() + " but ticket Expired");
				e.setSeverity(Event.INFO);
				MgrAudit.writeLog(e, new Log());
				try {
					sock.close();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			return true;
		} /*
		 * else if (type.equals("PublishBcBallot")) { Message m =
		 * MgrMessage.recoverPublishBcBallotMsg(msgBytes); // Store All Bc
		 * Ballot to DB
		 * CounterChannel.appendText("Counter Publish BcBallot: Eletcion ID "
		 * +m.getElectionId()); System.out.println(new String(msgBytes));
		 * if(MgrResult.insertBcBallot(m.getBcBallot().toByteArray(),
		 * m.getElectionId(),m.getSupSignature().toByteArray()), ) { // Send
		 * AckPublishBcBallot byte [] msg =
		 * MgrMessage.ackPublishBcBallotMsg("AckBcBallotMsg", "succeeded");
		 * System.out.println(new String(msg)); MgrMessage.sendMsg(sock, msg); }
		 * else { byte [] msg =
		 * MgrMessage.ackPublishBcBallotMsg("AckBcBallotMsg", "failed");
		 * MgrMessage.sendMsg(sock, msg); } return true;
		 * 
		 * }
		 */
		else if (type.equals("CheckEntry")) {
			Message m = MgrMessage.recoverCheckBcBallotFactorMsg(msgBytes);
			if (MgrTicket.isTicketBBValid(m.getTicketBB(), myPublicKey,
					m.getUsername(), sock.getInetAddress().toString())) {

				// Renew Ticket
				ServerGui.appendText("Check Ballot Entries by "
						+ m.getTicketBB().getUserId());
				TicketBB tb = MgrTicket.createTicketBB(m.getTicketBB()
						.getUserId(), sock.getInetAddress().toString(), m
						.getTicketBB().getRole(), new Date(), myPrivateKey, m
						.getTicketBB().getPermission());

				List<BcBallotFactor> bcf = MgrResult.verifyBcBallot(m
						.getElectionId());
				byte[] msg = null;
				if (!bcf.isEmpty()) {
					msg = MgrMessage.ackCheckBcBallotFactorMsg("AckCheckEntry",
							bcf.size(),
							bcf.toArray(new BcBallotFactor[bcf.size()]),
							"succeeded", tb);
					// added for log
					e.setObjectName(m.getTicketBB().getUserId());
					e.setDetail(type + " - Check Ballot Entries Succeeded");
					e.setSeverity(Event.INFO);
					MgrAudit.writeLog(e, new Log());
				} else {
					BcBallotFactor dummy = new BcBallotFactor();
					dummy = MgrBallot.createBcBalloFactor(0,
							"unknown".getBytes(), "unknown".getBytes(),
							"unknown".getBytes(), "unknown".getBytes(), 0, "unknown".getBytes());
					bcf.add(dummy);
					msg = MgrMessage.ackCheckBcBallotFactorMsg("AckCheckEntry",
							bcf.size(),
							bcf.toArray(new BcBallotFactor[bcf.size()]),
							"The election is not over", tb);
				}
				System.out.println(bcf.get(0).getEntry());
				System.out.println(new String(msg));
				MgrMessage.sendMsg(sock, msg);
			} else {
				// Failed Ticket
				TicketBB tb = MgrTicket.createTicketBB(m.getTicketBB()
						.getUserId(), sock.getInetAddress().toString(),
						"failed", new Date(), myPrivateKey, 0);
				BcBallotFactor[] bcf = new BcBallotFactor[1];
				bcf[0] = MgrBallot.createBcBalloFactor(0, "unknown".getBytes(), 
						"unknown".getBytes(), "unknown".getBytes(), "unknown".getBytes(), m
						.getElectionId(), "unknown".getBytes());
				byte[] msg = MgrMessage.ackCheckBcBallotFactorMsg(
						"AckCheckEntry", 1, bcf, "ticket expired!!", tb);
				// added for log
				e.setObjectName(m.getTicketBB().getUserId());
				e.setDetail(type + " - Check Ballot Entries but ticket expired!!");
				e.setSeverity(Event.INFO);
				MgrMessage.sendMsg(sock, msg);
				try {
					sock.close();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}

			return true;
		} else if (type.equals("PublishBallot")) {
			Message m = MgrMessage.recoverPublishBcBallotFactorMsg(msgBytes);
			if (MgrTicket.isTicketBBValid(m.getTicketBB(), myPublicKey,
					m.getUsername(), sock.getInetAddress().toString())) {

				// Renew Ticket
				TicketBB tb = MgrTicket.createTicketBB(m.getTicketBB()
						.getUserId(), sock.getInetAddress().toString(), m
						.getTicketBB().getRole(), new Date(), myPrivateKey, m
						.getTicketBB().getPermission());

				if (MgrResult.setKeyForBcBallot(m.getBcBallotFactor())) {
					// Send AckPublishBcBallot
					byte[] msg = MgrMessage.ackPublishBcBallotFactorMsg(
							"AckBcBallotMsg", "succeeded", tb);
					MgrMessage.sendMsg(sock, msg);
					// added for log
					e.setObjectName(m.getTicketBB().getUserId());
					e.setDetail(type + " - Publish Ballout Succeeded!!");
					e.setSeverity(Event.INFO);
					return true;
				} else {
					byte[] msg = MgrMessage.ackPublishBcBallotFactorMsg(
							"AckBcBallotMsg", "failed", tb);
					MgrMessage.sendMsg(sock, msg);
					// added for log
					e.setObjectName(m.getTicketBB().getUserId());
					e.setDetail(type + " - Publish Ballout Failed!!");
					e.setSeverity(Event.INFO);
					return false;
				}

			} else {
				// Failed Ticket
				TicketBB tb = MgrTicket.createTicketBB(m.getTicketBB()
						.getUserId(), sock.getInetAddress().toString(),
						"failed", new Date(), myPrivateKey, 0);
				byte[] msg = MgrMessage.ackPublishBcBallotFactorMsg(
						"AckBcBallotMsg", "ticket expired!!", tb);
				MgrMessage.sendMsg(sock, msg);
				// added for log
				e.setObjectName(m.getTicketBB().getUserId());
				e.setDetail(type + " - Publish Ballout but Ticket Expired!!");
				e.setSeverity(Event.INFO);
				try {
					sock.close();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				return false;
			}

		} else if (type.equals("CheckResult")) {
			Message m = MgrMessage.recoverCheckBcBallotFactorMsg(msgBytes);
			if (MgrTicket.isTicketBBValid(m.getTicketBB(), myPublicKey,
					m.getUsername(), sock.getInetAddress().toString())) {

				// Renew Ticket
				TicketBB tb = MgrTicket.createTicketBB(m.getTicketBB()
						.getUserId(), sock.getInetAddress().toString(), m
						.getTicketBB().getRole(), new Date(), myPrivateKey, m
						.getTicketBB().getPermission());
				
				List<BcBallotFactor> bcf = MgrResult.checkResult(m
						.getElectionId());
				//System.out.println("electionid "+bcf.isEmpty());
				if(bcf.isEmpty())
				{
					BcBallotFactor dummyBC = MgrBallot.createBcBalloFactor(0, "unknown".getBytes(), 
							"unknown".getBytes(), "unknown".getBytes(), "unknown".getBytes(), m
							.getElectionId(), "unknown".getBytes());
					bcf.add(dummyBC);
					byte[] msg = MgrMessage.ackCheckBcBallotFactorMsg(
							"AckCheckResult", bcf.size(),
							bcf.toArray(new BcBallotFactor[bcf.size()]),
							"The result is not ready for this election", tb);
					//System.out.println("here");
					MgrMessage.sendMsg(sock, msg);
					// added for log
					e.setObjectName(m.getTicketBB().getUserId());
					e.setDetail(type + " - check result for "+m.getElectionId() +" but the result is not ready!!");
					e.setSeverity(Event.INFO);
				}
				else
				{
					byte[] msg = MgrMessage.ackCheckBcBallotFactorMsg(
							"AckCheckResult", bcf.size(),
							bcf.toArray(new BcBallotFactor[bcf.size()]),
							"succeeded", tb);
					MgrMessage.sendMsg(sock, msg);
					// added for log
					e.setObjectName(m.getTicketBB().getUserId());
					e.setDetail(type + " - check result for "+m.getElectionId() +" Succeeded!!");
					e.setSeverity(Event.INFO);
				}
			} else {
				// Failed Ticket
				TicketBB tb = MgrTicket.createTicketBB(m.getTicketBB()
						.getUserId(), sock.getInetAddress().toString(),
						"failed", new Date(), myPrivateKey, 0);
				BcBallotFactor[] bcf = new BcBallotFactor[1];
				bcf[0] = MgrBallot.createBcBalloFactor(0, "unknown".getBytes(), 
						"unknown".getBytes(), "unknown".getBytes(), "unknown".getBytes(), m
						.getElectionId(), "unknown".getBytes());
				byte[] msg = MgrMessage.ackCheckBcBallotFactorMsg(
						"AckCheckResult", 1, bcf, "ticket expired!!", tb);
				MgrMessage.sendMsg(sock, msg);
				// added for log
				e.setObjectName(m.getTicketBB().getUserId());
				e.setDetail(type + " - check result for "+m.getElectionId() +" but Ticket Expired!!");
				e.setSeverity(Event.INFO);
				
			}

			return true;
		} else if (type.equals("getElectionInfo")) {
			// Verify TicketBB
			// Reply with All Election Info
			Message m = MgrMessage.recoverGetElectionInfoMsg(msgBytes);
			if (MgrTicket.isTicketBBValid(m.getTicketBB(), myPublicKey,
					m.getUsername(), sock.getInetAddress().toString())) {
				// Renew Ticket
				TicketBB tb = MgrTicket.createTicketBB(m.getTicketBB()
						.getUserId(), sock.getInetAddress().toString(), m
						.getTicketBB().getRole(), new Date(), myPrivateKey, m
						.getTicketBB().getPermission());

				ServerGui.appendText("Attempt to get ongoing elections by "
						+ m.getUsername());
				List<Election> electionList = MgrElection.getOngoingElections();
				Election[] election = electionList
						.toArray(new Election[electionList.size()]);
				byte[] msg = MgrMessage.ackGetElectionInfoMsg("succeeded",
						election.length, election, tb);
				System.out.println(new String(msg));
				MgrMessage.sendMsg(sock, msg);
				System.out.println("sent");
				return true;
			} else {
				// Failed Ticket
				TicketBB tb = MgrTicket.createTicketBB(m.getTicketBB()
						.getUserId(), sock.getInetAddress().toString(),
						"failed", new Date(), myPrivateKey, 0);
				Map<String, String> unknownCandidate = new HashMap<String, String>();
				unknownCandidate.put("unknown", "unknown");
				Counter unknownCounter = new Counter();
				unknownCounter.setUserId("unknown");
				Election[] dummy = new Election[1];
				dummy[0] = com.csci6545.evoting.bl.MgrElection.createElection(
						0, "unknown",
						new Timestamp(System.currentTimeMillis()),
						new Timestamp(System.currentTimeMillis()),
						unknownCandidate, unknownCounter);
				byte[] msg = MgrMessage.ackGetElectionInfoMsg("failed", 1,
						dummy, tb);
				MgrMessage.sendMsg(sock, msg);
				try {
					sock.close();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				return false;
			}

		} else if (type.equals("getAllElections")) {
			// Verify TicketBB
			// Reply with All Election Info
			Message m = MgrMessage.recoverGetElectionInfoMsg(msgBytes);
			if (MgrTicket.isTicketBBValid(m.getTicketBB(), myPublicKey,
					m.getUsername(), sock.getInetAddress().toString())) {
				// Renew Ticket
				TicketBB tb = MgrTicket.createTicketBB(m.getTicketBB()
						.getUserId(), sock.getInetAddress().toString(), m
						.getTicketBB().getRole(), new Date(), myPrivateKey, m
						.getTicketBB().getPermission());

				ServerGui.appendText("Attempt to get all elections by "
						+ m.getUsername());
				List<Election> electionList = MgrElection.getAllElections();
				Election[] election = electionList
						.toArray(new Election[electionList.size()]);
				byte[] msg = MgrMessage.ackGetElectionInfoMsg("succeeded",
						election.length, election, tb);
				System.out.println(new String(msg));
				MgrMessage.sendMsg(sock, msg);
				System.out.println("sent");
				return true;
			} else {
				// Failed Ticket
				TicketBB tb = MgrTicket.createTicketBB(m.getTicketBB()
						.getUserId(), sock.getInetAddress().toString(),
						"failed", new Date(), myPrivateKey, 0);
				Map<String, String> unknownCandidate = new HashMap<String, String>();
				unknownCandidate.put("unknown", "unknown");
				Counter unknownCounter = new Counter();
				unknownCounter.setUserId("unknown");
				Election[] dummy = new Election[1];
				dummy[0] = com.csci6545.evoting.bl.MgrElection.createElection(
						0, "unknown",
						new Timestamp(System.currentTimeMillis()),
						new Timestamp(System.currentTimeMillis()),
						unknownCandidate, unknownCounter);
				byte[] msg = MgrMessage.ackGetElectionInfoMsg("failed", 1,
						dummy, tb);
				MgrMessage.sendMsg(sock, msg);
				try {
					sock.close();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				return false;
			}

		} else if (type.equals("getAllRegisteredElections")) {
			// Verify TicketBB
			// Reply with All Election Info
			Message m = MgrMessage.recoverGetElectionInfoMsg(msgBytes);
			if (MgrTicket.isTicketBBValid(m.getTicketBB(), myPublicKey,
					m.getUsername(), sock.getInetAddress().toString())) {
				// Renew Ticket
				TicketBB tb = MgrTicket.createTicketBB(m.getTicketBB()
						.getUserId(), sock.getInetAddress().toString(), m
						.getTicketBB().getRole(), new Date(), myPrivateKey, m
						.getTicketBB().getPermission());

				ServerGui.appendText("Attempt to get all elections by "
						+ m.getUsername());
				List<Election> electionList = MgrElection
						.getAllRegisteredElections(m.getTicketBB().getUserId(), m
								.getTicketBB().getRole());
				Election[] election = electionList
						.toArray(new Election[electionList.size()]);
				byte[] msg = MgrMessage.ackGetElectionInfoMsg("succeeded",
						election.length, election, tb);
				System.out.println(new String(msg));
				MgrMessage.sendMsg(sock, msg);
				System.out.println("sent");
				return true;
			} else {
				// Failed Ticket
				TicketBB tb = MgrTicket.createTicketBB(m.getTicketBB()
						.getUserId(), sock.getInetAddress().toString(),
						"failed", new Date(), myPrivateKey, 0);
				Map<String, String> unknownCandidate = new HashMap<String, String>();
				unknownCandidate.put("unknown", "unknown");
				Counter unknownCounter = new Counter();
				unknownCounter.setUserId("unknown");
				Election[] dummy = new Election[1];
				dummy[0] = com.csci6545.evoting.bl.MgrElection.createElection(
						0, "unknown",
						new Timestamp(System.currentTimeMillis()),
						new Timestamp(System.currentTimeMillis()),
						unknownCandidate, unknownCounter);
				byte[] msg = MgrMessage.ackGetElectionInfoMsg("failed", 1,
						dummy, tb);
				MgrMessage.sendMsg(sock, msg);
				try {
					sock.close();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				return false;
			}

		} else if (type.equals("getRegisteredElections")) {
			// Verify TicketBB
			// Reply with All Election Info
			Message m = MgrMessage.recoverGetElectionInfoMsg(msgBytes);
			if (MgrTicket.isTicketBBValid(m.getTicketBB(), myPublicKey,
					m.getUsername(), sock.getInetAddress().toString())) {
				// Renew Ticket
				TicketBB tb = MgrTicket.createTicketBB(m.getTicketBB()
						.getUserId(), sock.getInetAddress().toString(), m
						.getTicketBB().getRole(), new Date(), myPrivateKey, m
						.getTicketBB().getPermission());

				ServerGui.appendText("Attempt to get registered elections by "
						+ m.getUsername());
				List<Election> electionList = MgrElection
						.getRegisteredElections(m.getTicketBB().getUserId(), m
								.getTicketBB().getRole());
				if (electionList.isEmpty()) {
					Map<String, String> unknownCandidate = new HashMap<String, String>();
					unknownCandidate.put("unknown", "unknown");
					Counter unknownCounter = new Counter();
					unknownCounter.setUserId("unknown");
					electionList.add(com.csci6545.evoting.bl.MgrElection
							.createElection(0, "empty",
									new Timestamp(System.currentTimeMillis()),
									new Timestamp(System.currentTimeMillis()),
									unknownCandidate, unknownCounter));
					Election[] election = electionList
							.toArray(new Election[electionList.size()]);
					byte[] msg = MgrMessage.ackGetElectionInfoMsg(
							"You do not have new elections", election.length,
							election, tb);
					MgrMessage.sendMsg(sock, msg);
				} else {
					Election[] election = electionList
							.toArray(new Election[electionList.size()]);
					byte[] msg = MgrMessage.ackGetElectionInfoMsg("succeeded",
							election.length, election, tb);
					MgrMessage.sendMsg(sock, msg);
					System.out.println("sent");
				}
				return true;
			} else {
				// Failed Ticket
				TicketBB tb = MgrTicket.createTicketBB(m.getTicketBB()
						.getUserId(), sock.getInetAddress().toString(),
						"failed", new Date(), myPrivateKey, 0);
				Map<String, String> unknownCandidate = new HashMap<String, String>();
				unknownCandidate.put("unknown", "unknown");
				Counter unknownCounter = new Counter();
				unknownCounter.setUserId("unknown");
				Election[] dummy = new Election[1];
				dummy[0] = com.csci6545.evoting.bl.MgrElection.createElection(
						0, "unknown",
						new Timestamp(System.currentTimeMillis()),
						new Timestamp(System.currentTimeMillis()),
						unknownCandidate, unknownCounter);
				byte[] msg = MgrMessage.ackGetElectionInfoMsg("failed", 1,
						dummy, tb);
				MgrMessage.sendMsg(sock, msg);
				try {
					sock.close();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				return false;
			}
		} else if (type.equals("getUserInfo")) {

			Message m = MgrMessage.recoverGetUserInfoMsg(msgBytes);
			if (MgrTicket.isTicketBBValid(m.getTicketBB(), myPublicKey,
					m.getUsername(), sock.getInetAddress().toString())) {

				// Renew Ticket
				TicketBB tb = MgrTicket.createTicketBB(m.getTicketBB()
						.getUserId(), sock.getInetAddress().toString(), m
						.getTicketBB().getRole(), new Date(), myPrivateKey, m
						.getTicketBB().getPermission());

				// Reply with User Infos
				ServerGui.appendText("Attempt to get candidate list by "
						+ m.getTicketBB().getUserId());
				List<User> userList = MgrUser.getUserInfo();
				User[] users = userList.toArray(new User[userList.size()]);
				byte[] msg = MgrMessage.ackGetUserInfoMsg("succeeded",
						users.length, users, tb);
				MgrMessage.sendMsg(sock, msg);
				return true;
			} else {
				// Failed Ticket
				TicketBB tb = MgrTicket.createTicketBB(m.getTicketBB()
						.getUserId(), sock.getInetAddress().toString(),
						"failed", new Date(), myPrivateKey, 0);
				User[] users = new User[1];
				users[0] = new User();
				users[0].setUserId("unknown");
				users[0].setfName("unknown");
				users[0].setlName("unknown");
				users[0].setTitle("unknown");
				byte[] msg = MgrMessage.ackGetUserInfoMsg("failed", 1, users,
						tb);
				MgrMessage.sendMsg(sock, msg);
				try {
					sock.close();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				return false;
			}
		}

		else if (type.equals("GetTitle")) {

			Message m = MgrMessage.recoverGetTitleMsg(msgBytes);
			if (MgrTicket.isTicketBBValid(m.getTicketBB(), myPublicKey,
					m.getUsername(), sock.getInetAddress().toString())) {

				// Renew Ticket
				TicketBB tb = MgrTicket.createTicketBB(m.getTicketBB()
						.getUserId(), sock.getInetAddress().toString(), m
						.getTicketBB().getRole(), new Date(), myPrivateKey, m
						.getTicketBB().getPermission());

				// Reply with User Infos
				ServerGui.appendText("Get title list by "
						+ m.getTicketBB().getUserId());
				byte[] msg = MgrMessage.ackgGetTitleMsg("succeeded", tb);
				MgrMessage.sendMsg(sock, msg);
				return true;
			} else {
				// Failed Ticket
				TicketBB tb = MgrTicket.createTicketBB(m.getTicketBB()
						.getUserId(), sock.getInetAddress().toString(),
						"failed", new Date(), myPrivateKey, 0);
				byte[] msg = MgrMessage.ackgGetTitleMsg("failed", tb);
				MgrMessage.sendMsg(sock, msg);
				try {
					sock.close();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				return false;
			}
		}

		else if (type.equals("BlindBallot")) {
			Message m = MgrMessage.recoverBlindBallotMsg(msgBytes);
			if (MgrTicket.isTicketBBValid(m.getTicketBB(), myPublicKey,
					m.getUsername(), sock.getInetAddress().toString())) {
				// Renew Ticket
				TicketBB tb = MgrTicket.createTicketBB(m.getTicketBB()
						.getUserId(), sock.getInetAddress().toString(), m
						.getTicketBB().getRole(), new Date(), myPrivateKey, m
						.getTicketBB().getPermission());

				User voter = new User();
				voter.setUserId(m.getTicketBB().getUserId());
				byte[] voterPubKey = MgrUser.getUserPublicKey(voter);
				// Verify Voter Signature succeeded
				int permission = MgrUser.getUserPermission(m.getTicketBB().getUserId());
				if(permission == 1 || permission == 3 || permission == 5 || permission == 7)
				{
					if (MgrIntegrity.verify(MgrIntegrity
							.byteToPublicKey(voterPubKey), m.getBlindBallot()
							.toByteArray(), m.getUserSignature().toByteArray())) {
						// Check if voter has already voted or not
						// Dont forget to change counter publicKey
						if (!MgrBlindBallot.isVoterVoted(m.getElectionId(), m
								.getTicketBB().getUserId())) {
							MgrBlindBallot.insertBlindBallot(m.getBlindBallot()
									.toByteArray(), m.getTicketBB().getUserId(), m
									.getElectionId(), m.getUserSignature()
									.toByteArray());
							byte[] msg = MgrMessage.blindSignatureMsg("succeeded",
									MgrIntegrity.signBlindedSignature(myPrivateKey,
											m.getBlindBallot().toByteArray()), tb,
									counterIP, counterPort, counterPubKey);
							MgrMessage.sendMsg(sock, msg);
							String notiEmail = MgrUser.getUserEmail(m.getTicketBB()
									.getUserId());
							MgrEmail.sendEmail(notiEmail, "", "Thank for voting", "We have received your vote for election "+m.getElectionId());
							
						}
						// already voted
						else {
							byte[] msg = MgrMessage.blindSignatureMsg(
									"You already submitted ballot",
									"unknown".getBytes(), tb, counterIP,
									counterPort, counterPubKey);
							MgrMessage.sendMsg(sock, msg);
						}					
					}
					// Verify Voter Signature failed
					else {
						byte[] msg = MgrMessage.blindSignatureMsg(
								"Your Signature is incorrect",
								"unknown".getBytes(), tb, counterIP, counterPort,
								counterPubKey);
						MgrMessage.sendMsg(sock, msg);
					}
				}
				else
				{
					byte[] msg = MgrMessage.blindSignatureMsg(
							"you are not allowed to voted",
							"unknown".getBytes(), tb, counterIP, counterPort,
							counterPubKey);
					MgrMessage.sendMsg(sock, msg);
				}

			} else {
				// Failed Ticket
				TicketBB tb = MgrTicket.createTicketBB(m.getTicketBB()
						.getUserId(), sock.getInetAddress().toString(),
						"failed", new Date(), myPrivateKey, 0);
				byte[] msg = MgrMessage.blindSignatureMsg("ticket expired!!",
						"unknown".getBytes(), tb, counterIP, counterPort,
						counterPubKey);
				MgrMessage.sendMsg(sock, msg);
				try {
					sock.close();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			return true;

		} else if (type.equals("ConfirmBlindSignature")) {
			Message m = MgrMessage.recoverConfirmVerifySignatureMsg(msgBytes);
			if (MgrTicket.isTicketBBValid(m.getTicketBB(), myPublicKey,
					m.getUsername(), sock.getInetAddress().toString())) {
				// mark voter as voted
				MgrBlindBallot.markVoterasVoted(m.getTicketBB().getUserId(), m
						.getUserSignature().toByteArray());

			} else {
				// Failed Ticket
				TicketBB tb = MgrTicket.createTicketBB(m.getTicketBB()
						.getUserId(), sock.getInetAddress().toString(),
						"failed", new Date(), myPrivateKey, 0);
			}
			return true;

		}

		else if (type.equals("ResetPassword")) {
			Message m = MgrMessage.recoverResetPasswordMsg(msgBytes);

			if (MgrPassword.generateRandomPassword(m.getUsername(),
					"Reset Password", "Your one time password is")) {
				ServerGui.appendText("Reset password succeeded: "
						+ m.getUsername());
				byte[] msg = MgrMessage.ackResetPasswordMsg("succeeded",
						MgrUser.getUserEmail(m.getUsername()));
				MgrMessage.sendMsg(sock, msg);
				// added for log
				e.setObjectName(m.getUsername());
				e.setDetail(type + " - Reset Password Succeeded.");
				e.setSeverity(Event.INFO);
			} else {
				ServerGui.appendText("Reset password failed: "
						+ m.getUsername());
				byte[] msg = MgrMessage.ackResetPasswordMsg(
						"reset password failed", "");
				MgrMessage.sendMsg(sock, msg);
				// added for log
				e.setObjectName(m.getUsername());
				e.setDetail(type + " - Reset Password Failed.");
				e.setSeverity(Event.INFO);
			}
			try {
				sock.close();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		} else if (type.equals("UpdatePassword")) {
			Message m = MgrMessage.recoverUpdatePasswordMsg(msgBytes);
			if (MgrUser.setUserpassword(m.getUsername(), m.getPassword(), 0)) {
				ServerGui.appendText("Update password succeeded: "
						+ m.getUsername());
				User u = new User();
				u.setUserId(m.getUsername());
				MgrUser.setUserPublicKey(u, m.getUserPublicKey().getEncoded());
				byte[] msg = MgrMessage.ackUpdatePasswordMsg("succeeded");
				MgrMessage.sendMsg(sock, msg);
				// added for log
				e.setObjectName(m.getUsername());
				e.setDetail(type + " -Update Password Succeeded.");
				e.setSeverity(Event.INFO);
			} else {
				ServerGui.appendText("Update password failed: "
						+ m.getUsername());
				byte[] msg = MgrMessage
						.ackUpdatePasswordMsg("update password failed");
				MgrMessage.sendMsg(sock, msg);
				// added for log
				e.setObjectName(m.getUsername());
				e.setDetail(type + " -Update Password Failed.");
				e.setSeverity(Event.INFO);
				// you cannot update
			}
			try {
				sock.close();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

		else if (type.equals("CounterGetBcBallot")) {
			Message m = MgrMessage.recoverCounterGetBcBallotMsg(msgBytes);
			if (MgrTicket.isTicketBBValid(m.getTicketBB(), myPublicKey,
					m.getUsername(), sock.getInetAddress().toString())) {
				// Renew Ticket
				TicketBB tb = MgrTicket.createTicketBB(m.getTicketBB()
						.getUserId(), sock.getInetAddress().toString(), m
						.getTicketBB().getRole(), new Date(), myPrivateKey, m
						.getTicketBB().getPermission());
				BcBallotFactor bcFactor = MgrResult.getBcBallot(m.getEntry());
				byte[] msg = MgrMessage.ackCounterGetBcBallotMsg(bcFactor);
				MgrMessage.sendMsg(sock, msg);
			} else {
				// Failed Ticket
				TicketBB tb = MgrTicket.createTicketBB(m.getTicketBB()
						.getUserId(), sock.getInetAddress().toString(),
						"failed", new Date(), myPrivateKey, 0);
				BcBallotFactor bcFactor = MgrBallot.createBcBalloFactor(0,
						"unknown".getBytes(), "unknown".getBytes(),
						"unknown".getBytes(), "unknown".getBytes(), 0, "unknown".getBytes());
				byte[] msg = MgrMessage.ackCounterGetBcBallotMsg(bcFactor);
				MgrMessage.sendMsg(sock, msg);
				try {
					sock.close();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			return true;
		}

		else if (type.equals("UpdatePersonalInfo")) {
			Message m = MgrMessage.recoverUpdatePersonalInfoMsg(msgBytes);
			if (MgrTicket.isTicketBBValid(m.getTicketBB(), myPublicKey,
					m.getUsername(), sock.getInetAddress().toString())) {
				// Renew Ticket
				TicketBB tb = MgrTicket.createTicketBB(m.getTicketBB()
						.getUserId(), sock.getInetAddress().toString(), m
						.getTicketBB().getRole(), new Date(), myPrivateKey, m
						.getTicketBB().getPermission());
				// If update succeeded
				if (MgrUser.updateUserInfo(m.getUser())) {
					ServerGui.appendText("Update user info succeeded: "
							+ m.getUsername());
					byte[] msg = MgrMessage.ackUpdatePersonalInfoMsg(
							"succeeded", tb);
					MgrMessage.sendMsg(sock, msg);
					// added for log
					e.setObjectName(m.getUsername());
					e.setDetail(type + " - Update user info succeeded.");
					e.setSeverity(Event.INFO);
				} else {
					ServerGui.appendText("Update user info failed: "
							+ m.getUsername());
					byte[] msg = MgrMessage.ackUpdatePersonalInfoMsg(
							"Update failed", tb);
					MgrMessage.sendMsg(sock, msg);
					// added for log
					e.setObjectName(m.getUsername());
					e.setDetail(type + " - Update user info Failed.");
					e.setSeverity(Event.INFO);
				}

			} else {
				// Failed Ticket
				TicketBB tb = MgrTicket.createTicketBB(m.getTicketBB()
						.getUserId(), sock.getInetAddress().toString(),
						"failed", new Date(), myPrivateKey, 0);
				byte[] msg = MgrMessage.ackUpdatePersonalInfoMsg(
						"ticket expired!!", tb);
				MgrMessage.sendMsg(sock, msg);
				try {
					sock.close();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			return true;
		}
		else if (type.equals("checkAuthorizedBallot")){
			Message m = MgrMessage.recoverCheckAuthorizedBallotMsg(msgBytes);
			if (MgrTicket.isTicketBBValid(m.getTicketBB(), myPublicKey,
					m.getUsername(), sock.getInetAddress().toString())) {
				// Renew Ticket
				TicketBB tb = MgrTicket.createTicketBB(m.getTicketBB()
						.getUserId(), sock.getInetAddress().toString(), m
						.getTicketBB().getRole(), new Date(), myPrivateKey, m
						.getTicketBB().getPermission());
				
				List<String> allAuthorized = MgrResult.checkAuthorized(m.getElectionId());
				if(allAuthorized.isEmpty())
				{
					allAuthorized.add("unknown");
					byte[] msg = MgrMessage.ackCheckAuthorizedBallotMsg(allAuthorized.size(), allAuthorized, "No authorized users published", tb);
					MgrMessage.sendMsg(sock, msg);
				}
				else
				{
					byte[] msg = MgrMessage.ackCheckAuthorizedBallotMsg(allAuthorized.size(), allAuthorized, "succeeded", tb);
					MgrMessage.sendMsg(sock, msg);
				}
			}
			else {
				// Failed Ticket
				TicketBB tb = MgrTicket.createTicketBB(m.getTicketBB()
						.getUserId(), sock.getInetAddress().toString(),
						"failed", new Date(), myPrivateKey, 0);
				List<String> allAuthorized = new ArrayList<String>();
				allAuthorized.add("unknown");
				byte[] msg = MgrMessage.ackCheckAuthorizedBallotMsg(allAuthorized.size(), allAuthorized, "ticket expired!!", tb);
				MgrMessage.sendMsg(sock, msg);
			}
		}
		else if (type.equals("Logout")) {
			Message m = MgrMessage.recoverLogOutMsg(msgBytes);
			try {
				sock.close();
				ServerGui
						.appendText("Logout by " + m.getTicketBB().getUserId());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			return true;
		} else if (type.equals("AckRequestBcFactor")) {
			Message m = MgrMessage.recoverAckRequestBcBallotFactorMsg(msgBytes);
			CounterChannel.appendText("Counter is publishing "
					+ m.getAmountOfEntry() + " BcBallot(s) for election: "
					+ m.getElectionId());
			for (int i = 0; i < m.getAmountOfEntry(); i++) {
				MgrResult.insertBcBallot(m.getBcArray()[i].getBcBallot(),
						m.getElectionId(), m.getBcArray()[i].getSupSignature(),
						m.getBcArray()[i].getEntry());
			}
		} else if (type.equals("AckRequestResult")) {
			Message m = MgrMessage.recoverAckRequestBcBallotFactorMsg(msgBytes);
			CounterChannel.appendText("Counter is publishing "
					+ m.getAmountOfEntry() + " Ballot(s) for election: "
					+ m.getElectionId());
			for (int i = 0; i < m.getAmountOfEntry(); i++) {
				MgrResult.setKeyForBcBallot(m.getBcArray()[i]);
			}
		} else if (type.equals("sql")) {
			String detail = MgrMessage.recoverSqlOverSSL(msgBytes).getDetail();
			if (detail.equals("queryUser")) {
				// Query User to show when they want to update personal info
			} else if (detail.equals("queryElection")) {
				// Query Election to show when supervisor wants to update
				// election info
			} else if (detail.equals("updateUser")) {
				// Update user personal info
			} else if (detail.equals("updateElection")) {
				// Update election info
			} else if (detail.equals("historyElection")) {
				// See history
			}
		}

		return false;
	}

	public static Message voterHandleMessage(String type, byte[] msgBytes,
			Socket sock) {

		if (type.equals("AckRegister")) {
			return MgrMessage.recoverAckRegisterForElectionMsg(msgBytes);
		} else if (type.equals("BlindSignature")) {
			// Send ConfirmBlindSigtature if verified
			return MgrMessage.recoverBlindSignatureMsg(msgBytes);
		} else if (type.equals("AckCheckEntry")) {
			return MgrMessage.recoverAckCheckBcBallotFactorMsg(msgBytes);
		} else if (type.equals("AckCheckResult")) {
			return MgrMessage.recoverAckCheckBcBallotFactorMsg(msgBytes);
		} else if (type.equals("AckElectionInfo")) {
			return MgrMessage.recoverAckGetElectionInfoMsg(msgBytes);
		} else if (type.equals("ackBcBallot")) {
			return MgrMessage.recoverCounterAckMsg(msgBytes);
		} else if (type.equals("ackKey")) {
			return MgrMessage.recoverCounterAckMsg(msgBytes);
		} else if (type.equals("ackCheckAuthorizedBallot")){
			return MgrMessage.recoverAckCheckAuthorizedBallotMsg(msgBytes);
		}
		else if (type.equals("Acksql")) {

		}
		return null;
	}

	public static Message supHandleMsg(String type, byte[] msgBytes, Socket sock) {
		if (type.equals("AckCreateElection")) {
			return MgrMessage.recoverAckCreateElectionMsg(msgBytes);
		} else if (type.equals("AckUserInfo")) {
			return MgrMessage.recoverAckGetUserInfoMsg(msgBytes);
		} else if (type.equals("AckGetTitle")) {
			return MgrMessage.recoverAckgGetTitleMsg(msgBytes);
		} else if (type.equals("Acksql")) {

		}
		return null;
	}

	public static void counterHandleMsg(String type, byte[] msgBytes,
			Socket clientSock, PrivateKey privateKey) {
		
		if (type.equals("bcBallot")) {
			Message m = MgrMessage.recoverSubmitBcBallotMsg(msgBytes);
			// Verify Sup Signature
			System.out.println("Received bcBallot");
			if (MgrIntegrity.verify(MgrSSL.getServerPublicKeyFromTrustStore(),
					m.getBcBallot().toByteArray(), m.getSupSigOnBcBallot())) {
				if (MgrCounter.insertBcBallot(m.getBcBallot().toByteArray(),
						m.getElectionId(), m.getSupSigOnBcBallot())) {
					byte[] msg = MgrMessage.counterAckMsg("ackBcBallot",
							"succeeded");
					MgrMessage.sendMsg(clientSock, msg);
					
				} else {
					byte[] msg = MgrMessage.counterAckMsg("ackBcBallot",
							"failed");
					MgrMessage.sendMsg(clientSock, msg);
				}

			} else {
				// send client ACK failed
				System.out.println("Supervisor Signature Failed");
				byte[] msg = MgrMessage.counterAckMsg("ackBcBallot",
						"Supervisor Signature Failed");
				MgrMessage.sendMsg(clientSock, msg);
			}

		} else if (type.equals("bcKey")) {
			System.out.println("Received Key");
			Message m = MgrMessage.recoverSubmitKeyMsg(msgBytes);
			BcBallotFactor ballot = MgrCounter.getBcBallot(m.getEntry());
			ballot.setKey(m.getKeyBcBallot().toByteArray());
			ballot.setIv(m.getIvBcBallot().toByteArray());
			BASE64Encoder encoder = new BASE64Encoder();
			System.out.println(m.getEntry());
			// System.out.println(encoder.encodeBuffer(ballot.getBcBallot()));
			try {
				ballot.setBallot(MgrConfidentiality.decryptAESBallot(
						ballot.getBcBallot(), ballot.getKey(), ballot.getIv()));
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if (ballot != null) {
				if (MgrCounter.setKeyForBcBallot(ballot)) {
					System.out.println("Ballot (" + m.getEntry()
							+ ") is ready to be tallied");
				}
			}
			// retrieve bcBallot, decrypt it and send it server
		} else if (type.equals("AckBallotMsg")) {
			Message msg = MgrMessage
					.recoverAckPublishBcBallotFactorMsg(msgBytes);
			// Send Ack back to voter
		} else if (type.equals("AckCounterGetBcBallot")) {
			Message msg = MgrMessage.recoverAckCounterGetBcBallotMsg(msgBytes);
		}
	}
	
	public static Boolean counterChannelHandleMsg(String type, byte[] msgBytes,
			Socket sock, PrivateKey myPrivateKey, PublicKey myPublicKey,
			SecretKey key, String counterIP, int counterPort,
			PublicKey counterPubKey) {
		if (type.equals("CounterLogin")) {
			Message m = MgrMessage.recoverCounterLoginMsg(msgBytes);
			CounterChannel.appendText("Attempt to Login by " + m.getUsername());
			User counter = MgrCounter.passwordAuthentcateCounter(m.getUsername(), m.getPassword());
			if ((counter != null && counter.getLoginAttempts() < 3) && counter.getIsOtp() == 0)
			{
				MgrCounter.setCounterLoginAttempts(counter.getUserId(), 0);
				TicketBB tb = MgrTicket.createCounterTicketBB(m.getUsername(),
						sock.getInetAddress().toString(),
						"Counter", new Date(), myPrivateKey,
						0);
				MgrMessage.sendMsg(sock,
						MgrMessage.ackCounterLoginMsg(tb, "succeeded"));
				CounterChannel.appendText("Login succeeded: "
						+ m.getUsername());
			}
			else if (counter != null && counter.getIsOtp() == 1) {
				username = m.getUsername();
				MgrCounter.setCounterLoginAttempts(counter.getUserId(), 0);
				TicketBB tb = MgrTicket.createCounterTicketBB(m.getUsername(),
						sock.getInetAddress().toString(), "otp",
						new Date(), myPrivateKey, 0);
				CounterChannel.appendText("Logged in by: " + m.getUsername()
						+ " using OTP");
				MgrMessage.sendMsg(sock,
						MgrMessage.ackCounterLoginMsg(tb, "otp"));
				return false;
			}
			// Authentication failed 
			else {
				TicketBB tb = MgrTicket.createCounterTicketBB(m.getUsername(), sock
						.getInetAddress().toString(), "failed", new Date(),
						myPrivateKey, 0);
				int attempts = MgrCounter.getCounterLoginAttempts(m.getUsername());
				if (attempts < 3) {
					MgrCounter.setCounterLoginAttempts(m.getUsername(), ++attempts);
					CounterChannel
							.appendText("Login failed username and/or password incorrect: "
									+ m.getUsername()
									+ " "
									+ MgrCounter.getCounterLoginAttempts(m
											.getUsername()) + "Attempt(s)");

					MgrMessage.sendMsg(sock, MgrMessage.ackCounterLoginMsg(tb,
							"Your username or password is incorrect"));
					// added for log
					return false;
				}
				// exceed maximum attempts --> lock account
				else {

					CounterChannel
							.appendText("Login failed username and/or password incorrect: "
									+ m.getUsername() + " has been locked");
					MgrMessage.sendMsg(sock, MgrMessage.ackCounterLoginMsg(tb,
							"Your account has been locked. Please contact admin"));
					//MgrPassword.generateCounterRandomPassword(m.getUsername(), "Reset password", "Your one time password is");
					// added for log
					return true;
				}

			}
		}
		 else if (type.equals("AckRequestBcFactor")) {
				Message m = MgrMessage.recoverAckRequestBcBallotFactorMsg(msgBytes);
				if(MgrTicket.isTicketBBValid(m.getTicketBB(), myPublicKey, "counter", sock.getInetAddress().toString()))
				{
					TicketBB tb = MgrTicket.createCounterTicketBB(m.getTicketBB().getUserId(),
							sock.getInetAddress().toString(),
							"counter", new Date(), myPrivateKey,
							0);
					
					CounterChannel.appendText("Counter is publishing "
							+ m.getAmountOfEntry() + " BcBallot(s) for election: "
							+ m.getElectionId());
					for (int i = 0; i < m.getAmountOfEntry(); i++) {
						MgrResult.insertBcBallot(m.getBcArray()[i].getBcBallot(),
								m.getElectionId(), m.getBcArray()[i].getSupSignature(),
								m.getBcArray()[i].getEntry());
					}
					byte [] msg = MgrMessage.replyAckRequestBcBallotFactorMsg("AckRequestBcFactor", "succeeded", tb);
					//System.out.println("werw"+new String(msg));
					MgrMessage.sendMsg(sock, msg);
				
				}
				else
				{
					TicketBB tb = MgrTicket.createCounterTicketBB(m.getTicketBB().getUserId(),
							sock.getInetAddress().toString(),
							"failed", new Date(), myPrivateKey,
							0);
					byte [] msg = MgrMessage.replyAckRequestBcBallotFactorMsg("AckRequestBcFactor", "ticket expired!!", tb);
					//System.out.println("werw"+new String(msg));
					MgrMessage.sendMsg(sock, msg);
				}
			} else if (type.equals("AckRequestResult")) {
				Message m = MgrMessage.recoverAckRequestBcBallotFactorMsg(msgBytes);
				if(MgrTicket.isTicketBBValid(m.getTicketBB(), myPublicKey, "counter", sock.getInetAddress().toString()))
					{
					TicketBB tb = MgrTicket.createCounterTicketBB(m.getTicketBB().getUserId(),
							sock.getInetAddress().toString(),
							"Counter", new Date(), myPrivateKey,
							0);
					CounterChannel.appendText("Counter is publishing "
							+ m.getAmountOfEntry() + " Ballot(s) for election: "
							+ m.getElectionId());
					for (int i = 0; i < m.getAmountOfEntry(); i++) {
						MgrResult.setKeyForBcBallot(m.getBcArray()[i]);
					}
					byte [] msg = MgrMessage.replyAckRequestBcBallotFactorMsg("AckRequestResult", "succeeded", tb);
					//System.out.println(new String(msg));
					MgrMessage.sendMsg(sock, msg);
				}
				else
				{
					TicketBB tb = MgrTicket.createCounterTicketBB(m.getTicketBB().getUserId(),
							sock.getInetAddress().toString(),
							"failed", new Date(), myPrivateKey,
							0);
					byte [] msg = MgrMessage.replyAckRequestBcBallotFactorMsg("AckRequestBcFactor", "ticket expired!!", tb);
					
					MgrMessage.sendMsg(sock, msg);
				}
			} else if (type.equals("UpdatePassword")) {
				Message m = MgrMessage.recoverUpdatePasswordMsg(msgBytes);
				if (MgrCounter.setCounterpassword(m.getUsername(), m.getPassword(), 0)) {
					CounterChannel.appendText("Update password succeeded: "
							+ m.getUsername());
					byte[] msg = MgrMessage.ackUpdatePasswordMsg("succeeded");
					MgrMessage.sendMsg(sock, msg);
				} else {
					CounterChannel.appendText("Update password failed: "
							+ m.getUsername());
					byte[] msg = MgrMessage
							.ackUpdatePasswordMsg("update password failed");
					MgrMessage.sendMsg(sock, msg);
					// you cannot update
				}
			}
		return false;

	}
}
