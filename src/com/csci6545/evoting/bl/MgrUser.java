package com.csci6545.evoting.bl;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.csci6545.evoting.be.Counter;
import com.csci6545.evoting.be.Election;
import com.csci6545.evoting.be.Title;
import com.csci6545.evoting.be.User;
import com.csci6545.evoting.be.Voter;
import com.csci6545.evoting.pl.MakeVoteGui;
import com.csci6545.evoting.pl.VoterGui;

public class MgrUser {
	public static byte [] overridedPermissionUser(List<User> user)
	{
		String userStr = "";
		for(int i=0;i<user.size();i++)
		{
			if (i == user.size()-1)
			{
				userStr = userStr+user.get(i).getUserId();
			
			}
			else
			{
				userStr = userStr+user.get(i).getUserId()+",";
						
			}
		}
		return userStr.getBytes();
	}
	
	public static List<User> recoverOverridedPermissionUser(int entry, byte[] allUserBytes)
	{
		List<User> userList = new ArrayList<User>();
		String titleMsg = new String(allUserBytes);
		int i = 0;
		for (String retval: titleMsg.split(",",entry)){
			User temp = new User();
			temp.setUserId(retval);
			userList.add(temp);
		    i++;
		}
		return userList;
	}
	
	public static User recoverUser(byte [] byteUser) {
		try
		{
			User user = new User();
			String stringElection = new String(byteUser);
			String[] parts = new String [5];
			int i = 0;
			for (String retval: stringElection.split(",",5)){
			   parts[i] = retval;
			   i++;
			}
			user.setUserId(parts[0]);
			user.setfName(parts[1]);
			user.setlName(parts[2]);
			user.setTitle(parts[3]);
			user.setPermission(Integer.parseInt(parts[4]));
			return user;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return null;
	}
	
	public static byte[] allUser(User[] user)
	{
		String userMsg = "";
		for (int i = 0;i < user.length;i++)
		{
			
			if (i == user.length-1)
			{
				userMsg = userMsg+user[i].getUserId()+","+
						user[i].getfName()+","+user[i].getlName()+","+user[i].getTitle()+","+user[i].getPermission();		
			
			}
			else
			{
				userMsg = userMsg+user[i].getUserId()+","+
						user[i].getfName()+","+user[i].getlName()+","+user[i].getTitle()+","+user[i].getPermission()+"-";
			}
		}
		return userMsg.getBytes();
	}
	
	public static User[] recoverUserArray(int entry, byte[] allUserBytes)
	{
		User[] userArray = new User[entry];
		String elcetionMsg = new String(allUserBytes);
		int i = 0;
		for (String retval: elcetionMsg.split("-",entry)){
			userArray[i] = MgrUser.recoverUser(retval.getBytes());
		    i++;
		}
		return userArray;
	}
	
	public static byte[] updateInfoUserBytes(String username,String fName, 
			String lName, String email) {
		String userStr = username+","+fName+","+lName+","+email;
		return userStr.getBytes();
	}
	public static User recoverUpdateInfoUser(byte [] userBytes) {
		try
		{
			User user = new User();
			String stringElection = new String(userBytes);
			//SimpleDateFormat format =  new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.US);
			//SimpleDateFormat format =  new SimpleDateFormat("MM/dd/yyyy", Locale.US);	
			String[] parts = new String [4];
			int i = 0;
			for (String retval: stringElection.split(",",4)){
			   parts[i] = retval;
			   i++;
			}
			user.setUserId(parts[0]);
			user.setfName(parts[1]);
			user.setlName(parts[2]);
			user.setEmail(parts[3]);
			//user.setDob(format.parse(parts[4]));
			return user;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return null;
	}
	
	public static Map<Long,byte[]> storeVoterKey(long electionId, byte [] key, Voter v)
	{
		Map<Long,byte[]> tempKey = v.getKeyMap();
		tempKey.put(electionId, key);
		return tempKey;				
	}
	
	public static Map<Long,byte[]> storeVoterIv(long electionId, byte [] iv, Voter v)
	{
		Map<Long,byte[]> tempIv = v.getIvMap();
		tempIv.put(electionId, iv);
		return tempIv;				
	}
	
	public static Map<Long,byte[]> storeVoterBallot(long electionId, byte [] ballot, Voter v)
	{
		Map<Long,byte[]> tempBallot = v.getBallotMap();
		tempBallot.put(electionId, ballot);
		return tempBallot;				
	}
	
	public static Map<Long,byte[]> storeVoterBcBallot(long electionId, byte [] bcBallot, Voter v)
	{
		Map<Long,byte[]> tempBcMap = v.getBcBallotMap();
		tempBcMap.put(electionId, bcBallot);
		return tempBcMap;				
	}
	
	public static List<Long> storeElectionId(long electionId, Voter v)
	{
		List<Long> tempElectionList = v.getElectionIdList();
		tempElectionList.add(electionId);
		return tempElectionList;				
	}
}
