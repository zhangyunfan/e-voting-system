package com.csci6545.evoting.bl;

import java.io.BufferedReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Scanner;
import java.util.logging.Logger;

import javax.crypto.SecretKey;

import com.csci6545.evoting.be.Event;
import com.csci6545.evoting.be.Log;
import com.csci6545.evoting.be.Message;
import com.csci6545.evoting.be.Supervisor;
import com.csci6545.evoting.pl.ServerGui;

public class MgrConnectionCounter extends Thread{
	public static Socket clientSock;
	//public static Socket serverSock;
	private PrivateKey privKey;
	public MgrConnectionCounter(Socket clientSocket, PrivateKey privKey)
	{
		clientSock = clientSocket;
		//serverSock = serverSocket;
		this.privKey = privKey;
	}
	public void run()
	{
		try
		{
			byte [] msgBytes = MgrMessage.recvMsg(clientSock);
				
			//Message hybridCipher = MgrMessage.recoverSubmitHybridMsg(msgBytes); 
				
			//byte [] msg =  MgrConfidentiality.hybridDecryption(hybridCipher.getHybrid(),
			//		privKey.getEncoded(), hybridCipher.getIvBcBallot().toByteArray());
				
			MsgHandler.counterHandleMsg(MgrMessage.getMessgaeType(msgBytes),
					msgBytes, clientSock, privKey); 
			clientSock.close();
		}
		catch(Exception e)
		{
			//System.out.println(e);
		}
	}
}
