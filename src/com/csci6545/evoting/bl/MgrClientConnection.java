package com.csci6545.evoting.bl;

import java.io.BufferedReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Scanner;
import java.util.logging.Logger;

import javax.crypto.SecretKey;

import com.csci6545.evoting.be.Event;
import com.csci6545.evoting.be.Log;
import com.csci6545.evoting.be.Supervisor;
import com.csci6545.evoting.pl.ServerGui;

public class MgrClientConnection extends Thread{
	public static Socket sock;
	private PrivateKey privKey;
	private PublicKey pubKey;
	private SecretKey secretKey;
	private PublicKey counterPubKey;
	private int counterPort;
	private String counterIP;
	public MgrClientConnection(Socket socket, PrivateKey privKey, PublicKey pubKey, 
			SecretKey key, PublicKey counterPubKey, String counterIP, int counterPort)
	{
		sock = socket;
		this.counterPort = counterPort;
		this.privKey = privKey;
		this.pubKey = pubKey;
		this.secretKey = key;
		this.counterPubKey = counterPubKey;
		this.counterIP = counterIP;
		
	}
	public void run()
	{
		try
		{

			byte [] msgBytes = MgrMessage.recvMsg(sock);
			boolean cm = MsgHandler.serverHandleMsg(MgrMessage.getMessgaeType(msgBytes), 
					msgBytes,sock,privKey, pubKey, secretKey, counterIP,counterPort,counterPubKey); 
			if(cm)
			{
					System.out.println("succeeded");
			}
			sock.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}
