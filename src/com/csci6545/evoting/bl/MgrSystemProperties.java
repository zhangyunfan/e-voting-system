package com.csci6545.evoting.bl;

public class MgrSystemProperties {
	public static void initClientSystemProperty(String pathtoTrustStore, String password)
	{
		System.setProperty("javax.net.ssl.trustStore", pathtoTrustStore);
    	System.setProperty("javax.net.ssl.trustStorePassword", password);
    	
    	//System.setProperty("javax.net.ssl.trustStore", "ts6564.jks");
    	//System.setProperty("javax.net.ssl.trustStorePassword", "cs6545");
	}
	
	public static void initServerSystemProperty(String pathToKeyStore, String password)
	{
		System.setProperty("javax.net.ssl.keyStore", pathToKeyStore);
    	System.setProperty("javax.net.ssl.keyStorePassword", password);
	}
}
