package com.csci6545.evoting.bl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.Inet4Address;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import com.csci6545.evoting.be.Event;
import com.csci6545.evoting.be.Log;


public class MgrAudit {
	
	
	// for encrypted log
	public static void writeLog(Event e, Log g, String passKey)
			throws InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException
	{
		
		
		byte [] event = MgrConfidentiality.RSAEncryption(e.toString().getBytes(), passKey.getBytes()) ;
    	g.write(event);
		
	}
	
	// for plain text log
	public static void writeLog(Event e, Log g)
	{
		g.write(e.toString().getBytes());

		
	}
	
	
	
	public static void readLog(Log g, String passKey) throws IOException, InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException
	{
		
		File in = g.getFile();
		File out = new File("log\\audit_dec.log");
		BufferedReader reader = new BufferedReader(new FileReader(in));
		FileWriter writer = null;
		byte[] b = null;
		String line = "";
		while((line = reader.readLine()) != null)
		{
			b = MgrConfidentiality.RSADecryption(line.getBytes(), passKey.getBytes());
			String message = new String(b);
			 writer = new FileWriter("log\\audit_Dec.log");
			writer.write(message);
		}
		
		
	}
	
/*	public static byte[] getData(File file) throws IOException
	{
		RandomAccessFile f = new RandomAccessFile(file, "r");
		int length = (int) f.length();
		byte[] data = new byte[length];
		f.readFully(data);
		return data;
	}*/
}
	
	
	
