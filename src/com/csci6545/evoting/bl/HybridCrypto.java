package com.csci6545.evoting.bl;

import java.nio.ByteBuffer;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Key;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.MGF1ParameterSpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.OAEPParameterSpec;
import javax.crypto.spec.PSource;
import javax.crypto.spec.SecretKeySpec;

/**
 * 
 * @author Yunfan Zhang
 *
 */
class HybridCrypto{
	private static final int HYBRID_KEY_LENGTH_OFFSET=0;
	private static final int HYBRID_KEY_LENGTH_SIZE = 8;
	private static final int HYVRID_SYMMETRIC_KEY_SIZE = 128;
	
	private static byte[] encryptSession(byte[] rsa_pubBytes,
			byte[] plainSessKey) 
					throws InvalidKeySpecException, 
					NoSuchAlgorithmException, 
					NoSuchPaddingException, InvalidKeyException, 
					IllegalBlockSizeException, BadPaddingException, NoSuchProviderException{
		//
		// Generate public key
		//
		KeyFactory rsaKeyFactory = KeyFactory.getInstance("RSA");
		X509EncodedKeySpec rsa_pubX509 = 
				new X509EncodedKeySpec(rsa_pubBytes);
		PublicKey rsa_pubKey = 
				rsaKeyFactory.generatePublic(rsa_pubX509);
		Cipher rsaCipher = 
				Cipher.getInstance("RSA/ECB/OAEPWithSHA-256AndMGF1Padding","SunJCE");
		rsaCipher.init(Cipher.ENCRYPT_MODE,rsa_pubKey);
		return rsaCipher.doFinal(plainSessKey);
	}
	
	private static byte[] symmetricEncrypt(byte[] plainSessKey,
			byte[] plainText,byte[] ivBytes) 
					throws NoSuchAlgorithmException, NoSuchPaddingException, 
					InvalidKeyException, InvalidAlgorithmParameterException, 
					IllegalBlockSizeException, BadPaddingException, NoSuchProviderException{
		//
		// Get IV parameter
		//
		IvParameterSpec ivSpec = new IvParameterSpec(ivBytes);
		//
		SecretKeySpec aesKeySpec = new SecretKeySpec(plainSessKey,"AES");
		//
		// Symmetric Decryption 
		//
		Cipher aesCipher = Cipher.getInstance("AES/CBC/PKCS5Padding","SunJCE");
		aesCipher.init(Cipher.ENCRYPT_MODE,aesKeySpec,ivSpec);
		return aesCipher.doFinal(plainText);
	}
	
	public static byte[] encryptAll(byte[] rsa_pubBytes,
			byte[] plainText, byte[] ivBytes) 
					throws InvalidKeyException, NoSuchAlgorithmException, 
					NoSuchPaddingException, InvalidAlgorithmParameterException, 
					IllegalBlockSizeException, BadPaddingException, 
					NoSuchProviderException, InvalidKeySpecException{
		KeyGenerator generator = KeyGenerator.getInstance("AES", "SunJCE");
		generator.init(HYVRID_SYMMETRIC_KEY_SIZE);
		Key symmetricKey = generator.generateKey();
		byte[] symmetricKeyBytes = symmetricKey.getEncoded(); 
		
		byte[] cipherText = 
				symmetricEncrypt(symmetricKeyBytes,plainText,ivBytes);
		byte[] cipherSymmetricKey = 
				encryptSession(rsa_pubBytes,symmetricKeyBytes);
		long cipherKeyLength = (int) cipherSymmetricKey.length;
	
		
		ByteBuffer buffer=
				ByteBuffer.allocate(HYBRID_KEY_LENGTH_SIZE);
		buffer.putLong(HYBRID_KEY_LENGTH_OFFSET, cipherKeyLength);
		byte[] encryptedKeyLengthBytes = buffer.array();
		
		byte[] returnArray = new byte[encryptedKeyLengthBytes.length+
		                              (int) cipherKeyLength+
		                              cipherText.length];
		
		for(int i=0;i<encryptedKeyLengthBytes.length;i++){
			returnArray[i] = encryptedKeyLengthBytes[i];
		}
		
		for(int i=0;i<(int) cipherKeyLength;i++){
			returnArray[encryptedKeyLengthBytes.length+i]=
					cipherSymmetricKey[i];
		}
		
		for(int i=0;i<cipherText.length;i++){
			returnArray[encryptedKeyLengthBytes.length+
			            cipherSymmetricKey.length+i]=cipherText[i];
		}
		
		return returnArray;
	}
	
	private static byte[] decrypt(byte[] rsa_privBytes,
			byte[] sesskeyBytes,
			byte[] ivBytes,
			byte[] cipherBytes) throws
			NoSuchAlgorithmException, 
			NoSuchProviderException, 
			InvalidKeySpecException,
			NoSuchPaddingException, 
			InvalidKeyException, 
			InvalidAlgorithmParameterException, 
			IllegalBlockSizeException, 
			BadPaddingException{

		KeyFactory rsaKeyFactory = KeyFactory.getInstance("RSA");
 
		//
		// Generate private key
		//
		PKCS8EncodedKeySpec rsa_privPKCS8E =
				new PKCS8EncodedKeySpec(rsa_privBytes);
		PrivateKey rsa_privKey =
				rsaKeyFactory.generatePrivate(rsa_privPKCS8E);
		//
		// Get IV parameter
		//
		IvParameterSpec ivSpec = new IvParameterSpec(ivBytes);
		//
		// Decryption of session key
		//
		Cipher rsaCipher = Cipher.getInstance("RSA/ECB/OAEPWithSHA-256AndMGF1Padding","SunJCE");
		OAEPParameterSpec oaepPara = new OAEPParameterSpec("SHA-256", "MGF1", 
				MGF1ParameterSpec.SHA1,
				PSource.PSpecified.DEFAULT);
		rsaCipher.init(Cipher.DECRYPT_MODE,rsa_privKey,oaepPara);
		byte[] k_s = rsaCipher.doFinal(sesskeyBytes);
		SecretKeySpec aesKeySpec = new SecretKeySpec(k_s,"AES");
		//
		// Symmetric Decryption 
		//
		Cipher aesCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		aesCipher.init(Cipher.DECRYPT_MODE,aesKeySpec,ivSpec);
		return aesCipher.doFinal(cipherBytes);
	}
	
	public static byte[] decryptAll(byte[] cipherSymetricKeyAndText,
			byte[] privateKey, byte[] ivBytes) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException, InvalidKeySpecException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException{
		ByteBuffer symmetricKeyLengthBuffer=
				ByteBuffer.allocate(HYBRID_KEY_LENGTH_SIZE);
		symmetricKeyLengthBuffer.put(cipherSymetricKeyAndText,
				HYBRID_KEY_LENGTH_OFFSET,HYBRID_KEY_LENGTH_SIZE);
		symmetricKeyLengthBuffer.flip();
		long symmetricKeyLength = symmetricKeyLengthBuffer.getLong();
		byte[] encryptedSymmetricKey = new byte[(int) symmetricKeyLength];
		byte[] cipherText = 
				new byte[cipherSymetricKeyAndText.length
				         -HYBRID_KEY_LENGTH_SIZE
				         -(int) symmetricKeyLength];
		
		for(int i=0;i<symmetricKeyLength;i++){
			encryptedSymmetricKey[i]=
					cipherSymetricKeyAndText[HYBRID_KEY_LENGTH_SIZE+i];
		}
		
		for(int i=0;i<cipherText.length;i++){
			cipherText[i]=
					cipherSymetricKeyAndText[HYBRID_KEY_LENGTH_SIZE
					                         +(int) symmetricKeyLength
					                         +i];
		}
		
		return decrypt(privateKey,encryptedSymmetricKey,ivBytes,
				cipherText);
	}
}