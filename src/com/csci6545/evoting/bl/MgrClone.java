package com.csci6545.evoting.bl;

import java.util.Map;
import java.util.HashMap;

import com.csci6545.evoting.be.User;

/**
 * 
 * @author Yunfan Zhang
 * @category Immutablizer
 *
 */
public class MgrClone {
	/**
	 * @author Yunfan Zhang
	 * @param bytes a byte array
	 * @return the copy of the input byte array
	 */
	public static byte[] cloneByteArray(byte[] bytes) {
		if (bytes.length < 1) {
			return new byte[0];
		} 
		else {
			byte[] returnBytes = new byte[bytes.length];
			System.arraycopy(bytes, 0, returnBytes, 0, bytes.length);
			return returnBytes;
		}
	}
	
	/**
	 * 
	 * @param map a map whose keys and values are all String
	 * @return the copy of the input map
	 */
	public static Map<String,String> cloneStringStringMap(Map<String,String> map){
		Map<String,String> returnMap = new HashMap<String,String>();
		for(String k:map.keySet()){
			returnMap.put(k, (String) map.get(k));
		}
		return returnMap;
	}
	/**
	 * 
	 * @param users an array whose type is com.csci6545.evoting.be.User
	 * @return the copy of the input User array
	 */
	public static User[] cloneUserArray(User[] users){
		User[] returnUsers = new User[users.length];
		for(int i=0;i<users.length;i++){
			returnUsers[i] = users[i].clone();
		}
		return returnUsers;
	}
}
