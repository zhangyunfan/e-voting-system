package com.csci6545.evoting.bl;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
/**
 * 
 * @author Yunfan Zhang
 *
 */
class AsymCrypto{
	private static final String cyptoAlgorithm = "RSA";
	private static final String blockCipherMode = "ECB";
	private static final String paddingMod =
			"OAEPWithSHA-256AndMGF1Padding";
	private static final String instanceArg =
			cyptoAlgorithm+'/'+blockCipherMode+'/'+paddingMod;
	
	private KeyFactory asymKeyFactory;
	private Cipher asymCipher;
	/**
	 * @param 
	 * @return an AsymCrypto Object for asymmetric encryption and decryption
	 * @throws NoSuchAlgorithmException 
	 * @throws NoSuchPaddingException 
	 * @throws IOException 
	 */
	public AsymCrypto() throws NoSuchAlgorithmException, 
	NoSuchPaddingException {
		asymKeyFactory = KeyFactory.getInstance("RSA");
		asymCipher = Cipher.getInstance(instanceArg);
	}
	
	/**
	 * 
	 * @param plainText a byte array that needs to be encrypted
	 * @param pubKeyByte a byte array used to be the public key for encryption
	 * @return a byte array of the encrypted plainText
	 * @throws InvalidKeySpecException 
	 * @throws InvalidKeyException 
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 */
	
	public byte[] encrypt(byte[] plainText, byte[] pubKeyByte) 
			throws InvalidKeySpecException, InvalidKeyException, 
			IllegalBlockSizeException, BadPaddingException{
		X509EncodedKeySpec keyPubX509 = new X509EncodedKeySpec(pubKeyByte);
		PublicKey publicKey = asymKeyFactory.generatePublic(keyPubX509);
		asymCipher.init(Cipher.ENCRYPT_MODE, publicKey);
		return asymCipher.doFinal(plainText);
	}
	/**
	 * 
	 * @param cipherText a byte array that needs to be decrypted
	 * @param privKeyByte a byte array used to be the private key for decryption
	 * @return a byte array of the decrypted text
	 * @throws InvalidKeySpecException 
	 * @throws InvalidKeyException 
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 */
	public byte[] decrypt(byte[] cipherText, byte[] privKeyByte)
			throws InvalidKeySpecException, 
			InvalidKeyException, IllegalBlockSizeException, 
			BadPaddingException{
		PKCS8EncodedKeySpec keyPrivPKCS8E =
                new PKCS8EncodedKeySpec(privKeyByte);
		PrivateKey privateKey = asymKeyFactory.generatePrivate(keyPrivPKCS8E);
		asymCipher.init(Cipher.DECRYPT_MODE, privateKey);
		return asymCipher.doFinal(cipherText);
	}
}