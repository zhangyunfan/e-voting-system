package com.csci6545.evoting.bl;

import java.math.BigInteger;
import java.net.Socket;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.crypto.SecretKey;

import com.csci6545.evoting.be.Ticket;
import com.csci6545.evoting.be.TicketBB;
import com.csci6545.evoting.be.TicketElection;

public class MgrTicket {
	public static final long tenSec = 10*1000;
	public static final long fiveMin = 60*5*1000;
	public static final long oneHour = 60*60*1000;
	public static TicketElection createTicketElection(String voterId, String voterIP,
			long electionId, Date startTime, Date expireTime, PrivateKey myPrivateKey) 
	{
		//Issue TicketSup
		TicketElection ticket = new TicketElection();
		ticket.setUserId(voterId);
		ticket.setUserIP(voterIP);
		ticket.setElectionID(electionId);
		ticket.setIssueDate(startTime);
		ticket.setExpirationDate(expireTime);
		byte []signature = MgrIntegrity.sign(myPrivateKey, ticket.getSigningRequest());
		ticket.setSignature(new BigInteger(signature));
		return ticket;
	}
	
	
	public static TicketBB createTicketBB(String userId, String userIP, String userRole,
			Date startTime, PrivateKey myPrivateKey, int permission) 
	{
		//Issue TicketSup
		Date expireTime = new Date(startTime.getTime()+fiveMin);
		TicketBB ticket = new TicketBB();
		ticket.setUserId(userId);
		ticket.setUserIP(userIP);
		ticket.setRole(userRole);
		ticket.setIssueDate(startTime);
		ticket.setExpirationDate(expireTime);
		ticket.setPermission(permission);
		byte []signature = MgrIntegrity.sign(myPrivateKey, ticket.getSigningRequest());
		ticket.setSignature(new BigInteger(signature));
		return ticket;
	}
	
	public static TicketBB createCounterTicketBB(String userId, String userIP, String userRole,
			Date startTime, PrivateKey myPrivateKey, int permission) 
	{
		//Issue TicketSup
		Date expireTime = new Date(startTime.getTime()+oneHour);
		TicketBB ticket = new TicketBB();
		ticket.setUserId(userId);
		ticket.setUserIP(userIP);
		ticket.setRole(userRole);
		ticket.setIssueDate(startTime);
		ticket.setExpirationDate(expireTime);
		ticket.setPermission(permission);
		byte []signature = MgrIntegrity.sign(myPrivateKey, ticket.getSigningRequest());
		ticket.setSignature(new BigInteger(signature));
		return ticket;
	}
	
	public static TicketBB createTicketBBWithHmac(String userId, String userIP, String userRole,
			Date startTime, SecretKey key) 
	{
		//Issue TicketSup
		Date expireTime = new Date(startTime.getTime()+fiveMin);
		TicketBB ticket = new TicketBB();
		ticket.setUserId(userId);
		ticket.setUserIP(userIP);
		ticket.setRole(userRole);
		ticket.setIssueDate(startTime);
		ticket.setExpirationDate(expireTime);
		byte []hmac = MgrIntegrity.getHmac(key, ticket.getSigningRequest());
		ticket.setHmac(new BigInteger(hmac));
		return ticket;
	}
	
	public static TicketBB createUnsignedTicketBB(String userId, String userIP, String userRole,
			Date startTime) 
	{
		//Issue TicketSup
		Date expireTime = new Date(startTime.getTime()+fiveMin);
		TicketBB ticket = new TicketBB();
		ticket.setUserId(userId);
		ticket.setUserIP(userIP);
		ticket.setRole(userRole);
		ticket.setIssueDate(startTime);
		ticket.setExpirationDate(expireTime);
		return ticket;
	}
	
	// recover TicketSup Information
	public static TicketElection recoverTicketElection(byte [] byteTicket) {
		try
		{
			TicketElection ticket = new TicketElection();
			String stringTicket = new String(byteTicket);
			SimpleDateFormat format =  new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.US);
			String[] parts = new String [6];
			int i = 0;
			for (String retval: stringTicket.split(",",6)){
			   parts[i] = retval;
			   i++;
			}
			ticket.setUserId(parts[0]);
			ticket.setUserIP(parts[1]);
			ticket.setElectionID(Long.parseLong(parts[2]));
			ticket.setIssueDate(format.parse(parts[3]));
			ticket.setExpirationDate(format.parse(parts[4]));
			ticket.setSignature(new BigInteger(parts[5]));
			return ticket;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return null;
	}
	
	// recover Ticket Information
	public static TicketBB recoverTicketBB(byte [] byteTicket) {
		try
		{
			TicketBB ticket = new TicketBB();
			String stringTicket = new String(byteTicket);
			SimpleDateFormat format =  new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.US);
			String[] parts = new String [7];
			int i = 0;
			for (String retval: stringTicket.split(",",7)){
			    parts[i] = retval;
			    i++;
			 }
			ticket.setUserId(parts[0]);
			ticket.setUserIP(parts[1]);
			ticket.setIssueDate(format.parse(parts[2]));
			ticket.setExpirationDate(format.parse(parts[3]));
			ticket.setRole(parts[4]);
			ticket.setPermission(Integer.parseInt(parts[5]));
			ticket.setSignature(new BigInteger(parts[6]));
			return ticket;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return null;
	}
		
	public static TicketBB recoverTicketBBWithHmac(byte [] byteTicket) {
		try
		{
			TicketBB ticket = new TicketBB();
			String stringTicket = new String(byteTicket);
			SimpleDateFormat format =  new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.US);
			String[] parts = new String [6];
			int i = 0;
			for (String retval: stringTicket.split(",",6)){
			    parts[i] = retval;
			    i++;
			 }
			ticket.setUserId(parts[0]);
			ticket.setUserIP(parts[1]);
			ticket.setIssueDate(format.parse(parts[2]));
			ticket.setExpirationDate(format.parse(parts[3]));
			ticket.setRole(parts[4]);
			ticket.setHmac(new BigInteger(parts[5]));
			return ticket;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return null;
	}
	
	public static TicketBB recoverUnsignedTicketBB(byte [] byteTicket) {
		try
		{
			TicketBB ticket = new TicketBB();
			String stringTicket = new String(byteTicket);
			SimpleDateFormat format =  new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.US);
			String[] parts = new String [5];
			int i = 0;
			for (String retval: stringTicket.split(",",5)){
			    parts[i] = retval;
			    i++;
			 }
			ticket.setUserId(parts[0]);
			ticket.setUserIP(parts[1]);
			ticket.setIssueDate(format.parse(parts[2]));
			ticket.setExpirationDate(format.parse(parts[3]));
			ticket.setRole(parts[4]);
			return ticket;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return null;
	}
	
	
	//Verify TicketBB
	public static boolean isTicketBBValid(TicketBB ticketBB, PublicKey asPublicKey, 
			String UserID, String ipAddress) 
	{
		// verify expiration
		if(ticketBB.getRole().equals("failed"))
		{
			return false;
		}
		if(ticketBB.getExpirationDate().before(new Date()))
		{
			return false;
		}
		// verify if user IPaddress matches the user IPaddress in the ticket
		if(!ipAddress.equals(ticketBB.getUserIP()))
		{
			return false;
		}
		// Verify Signature on the ticket
		//System.out.println(new String(ticketBB.getSigningRequest()));
		byte[] signingRequestTicket = ticketBB.getSigningRequest();
		byte[] signature = ticketBB.getSignature().toByteArray();
		return MgrIntegrity.verify(asPublicKey, signingRequestTicket, signature);
	}
	
	
	//Verify TicketBBWithHmac
		public static boolean isTicketBBWithHmacValid(TicketBB ticketBB, SecretKey key, 
				String UserID, String ipAddress) 
		{
			// verify expiration
			if(ticketBB.getRole().equals("failed"))
			{
				return false;
			}
			if(ticketBB.getExpirationDate().before(new Date()))
			{
				return false;
			}
			// verify if user IPaddress matches the user IPaddress in the ticket
			if(!ipAddress.equals(ticketBB.getUserIP()))
			{
				return false;
			}
			// Verify HMAC on the ticket
			byte[] signingRequestTicket = ticketBB.getSigningRequest();
			byte[] hmac = ticketBB.getHmac().toByteArray();
			return MgrIntegrity.verifyHmac(key, hmac, signingRequestTicket);
		}
	
		public static boolean isUnsignedTicketBBValid(TicketBB ticketBB, String ipAddress) 
		{
			// verify expiration
			if(ticketBB.getRole().equals("failed"))
			{
				return false;
			}
			if(ticketBB.getExpirationDate().before(new Date()))
			{
				return false;
			}
			// verify if user IPaddress matches the user IPaddress in the ticket
			if(!ipAddress.equals(ticketBB.getUserIP()))
			{
				return false;
			}
			// Verify Signature on the ticket
			return true;
		}
	
		
	//Verify Ticket
	public static boolean isTicketElectionValid(TicketElection ticketElection, 
			PublicKey asPublicKey, String UserID) 
	{
		// verify expiration
		if(ticketElection.getExpirationDate().before(new Date()))
		{
			return false;
		}
		// verify if userId matches the userID in the ticket
		if(!UserID.equals(ticketElection.getUserId()))
		{
			return false;
		}
		// Verify Signature on the ticket
		byte[] signingRequestTicket = ticketElection.getSigningRequest();
		byte[] signature = ticketElection.getSignature().toByteArray();
		return MgrIntegrity.verify(asPublicKey, signingRequestTicket, signature);
	}

}
