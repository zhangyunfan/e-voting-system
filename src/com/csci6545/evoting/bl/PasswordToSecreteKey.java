package com.csci6545.evoting.bl;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
/**
 * 
 * @author Yunfan Zhang
 *
 */
class PasswordToSecreteKey {
	private static final int SALT_LENGTH =16;
	private static final int ITERATION_COUNTS = 1000;
	private static final int KEY_BITS_LEGNTH = 256;
	private String password;
	private byte[] salt;
	
	private byte[] generateDefaultFixSalt(){
		byte[] salts = new byte[SALT_LENGTH];
		for(int i=0;i< salts.length;i++){
			salts[i] = (byte) 0;
		}
		return salts;
	}

	public PasswordToSecreteKey(String passwd){
		this.password = passwd;
		this.salt = generateDefaultFixSalt();
	}
	/**
	 * 
	 * @return a byte array of secrete key based on the password String and 16 0 bytes salt
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeySpecException
	 */
	public byte[] generateSecreteKey() 
			throws NoSuchAlgorithmException, InvalidKeySpecException{
		char[] passwordChars = password.toCharArray();
		PBEKeySpec spec = 
				new PBEKeySpec(passwordChars,this.salt,ITERATION_COUNTS,KEY_BITS_LEGNTH);
		SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
		byte[] secreteKeyBytes = skf.generateSecret(spec).getEncoded();
		return secreteKeyBytes;
	}
}
