package com.csci6545.evoting.bl;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.ByteBuffer;

import com.csci6545.evoting.be.Ballot;
import com.csci6545.evoting.be.BcBallotFactor;

public class MgrBallot {
	// This class is for managing Ballot and BCBallot
	// as well as converting Ballot object to byte array
	// and vice versa.
	private static int BALLOT_TO_BYTES_BUFFER_SIZE=16;
	private static int ELECTION_ID_OFFSET = 0;
	private static int ELECTION_ID_SIZE = 8;
	private static int CANDIDATE_ID_OFFSET = 8;
	private static int CANDIDATE_ID_SIZE = 8;
	
	public static Ballot recoverBallot(byte [] ballotBytes)
	{
		try
		{
			String stringTicket = new String(ballotBytes);
			String[] parts = new String [2];
			int i = 0;
			for (String retval: stringTicket.split(",",2)){
			    parts[i] = retval;
			    i++;
			 }
			return new Ballot(Long.parseLong(parts[0]),Long.parseLong(parts[1]));
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return null;
	}
	/**
	 * @param ballot a ballot object
	 * @return a binary array of raw binary ballot
	 * @throws IOException 
	 */
	private static byte[] ballotToBytes(Ballot ballot)
			throws IOException{
		ByteBuffer buffer=
				ByteBuffer.allocate(BALLOT_TO_BYTES_BUFFER_SIZE);
		buffer.putLong(ELECTION_ID_OFFSET, ballot.getElectionID());
		buffer.putLong(CANDIDATE_ID_OFFSET,ballot.getcandidateID());
		byte[] returnBytes = buffer.array();
		return returnBytes;
	}
	
	/**
	 * @param b a byte array that represents a certain ballot
	 * @return the ballot which the byte array represents
	 * 
	 */
	private static Ballot bytesToBallot(byte[] b){
		ByteBuffer electionIDBuffer=
				ByteBuffer.allocate(BALLOT_TO_BYTES_BUFFER_SIZE);
		ByteBuffer candidateIDBuffer=
				ByteBuffer.allocate(BALLOT_TO_BYTES_BUFFER_SIZE);
		electionIDBuffer.put(b,ELECTION_ID_OFFSET,ELECTION_ID_SIZE);
		electionIDBuffer.flip();
		long electionID = electionIDBuffer.getLong();
		
		candidateIDBuffer.put(b,CANDIDATE_ID_OFFSET,CANDIDATE_ID_SIZE);
		candidateIDBuffer.flip();
		long candidateID = candidateIDBuffer.getLong();
		
		return new Ballot(electionID,candidateID);
	}
	
	public static BcBallotFactor recoverBcBallotFactor(byte [] bcFactorBytes)
	{
		try
		{
			BcBallotFactor bcFactor = new BcBallotFactor();
			String bc = new String(bcFactorBytes);
			String[] parts = new String [7];
			int i = 0;
			for (String retval: bc.split(",",7)){
			    parts[i] = retval;
			    i++;
			 }
			bcFactor.setEntry(Integer.parseInt(parts[0]));
			bcFactor.setBcBallot(new BigInteger(parts[1]).toByteArray());
			bcFactor.setKey(new BigInteger(parts[2]).toByteArray());
			bcFactor.setIv(new BigInteger(parts[3]).toByteArray());
			bcFactor.setBallot(new BigInteger(parts[4]).toByteArray());
			bcFactor.setElectionId(Long.parseLong(parts[5]));
			bcFactor.setSupSignature(new BigInteger(parts[6]).toByteArray());
			return bcFactor;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}
	
	// For system sending all BcBallot entry back to voters 
	public static byte[] allBcBalot(BcBallotFactor[] bcFactor)
	{
		String bcFactorMsg = "";
		
		for (int i = 0;i < bcFactor.length;i++)
		{
			
			if (i == bcFactor.length-1)
			{
				bcFactorMsg = bcFactorMsg+bcFactor[i].getEntry()+","+
						new BigInteger(bcFactor[i].getBcBallot())+","+new BigInteger(bcFactor[i].getKey())+
						","+new BigInteger(bcFactor[i].getIv())+","+new BigInteger(bcFactor[i].getBallot())+","+
						bcFactor[i].getElectionId()+","+new BigInteger(bcFactor[i].getSupSignature());
			}
			else
			{
				bcFactorMsg = bcFactorMsg+bcFactor[i].getEntry()+","+
						new BigInteger(bcFactor[i].getBcBallot())+","+new BigInteger(bcFactor[i].getKey())+
						","+new BigInteger(bcFactor[i].getIv())+","+new BigInteger(bcFactor[i].getBallot())+","+
						bcFactor[i].getElectionId()+","+new BigInteger(bcFactor[i].getSupSignature())+"/";
			}			
		}
		//System.out.println(bcFactorMsg);
		return bcFactorMsg.getBytes();
	}
	
	// For voter getting back array of BcBallotFactor
	public static BcBallotFactor[] recoverBcBallotFactorArray(int entry, byte[] allBcFactorBytes)
	{
		BcBallotFactor[] bcArray = new BcBallotFactor[entry];
		String bcFactorMsg = new String(allBcFactorBytes);
		System.out.println(bcFactorMsg);
		int i = 0;
		for (String retval: bcFactorMsg.split("/",entry)){
		    bcArray[i] = MgrBallot.recoverBcBallotFactor(retval.getBytes());
		    i++;
		}
		return bcArray;
	}
	
	// Create BcBalloFactor
	public static BcBallotFactor createBcBalloFactor(int entry, byte[] bcBallot, byte[] key, 
			byte [] iv, byte[] ballot, long electionId, byte [] supSignature)
	{
		BcBallotFactor bcFactor = new BcBallotFactor();
		bcFactor.setEntry(entry);
		bcFactor.setBcBallot(bcBallot);
		bcFactor.setKey(key);
		bcFactor.setIv(iv);
		bcFactor.setBallot(ballot);
		bcFactor.setElectionId(electionId);
		bcFactor.setSupSignature(supSignature);
		return bcFactor;
	}
}
