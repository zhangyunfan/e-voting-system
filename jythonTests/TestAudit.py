#!/usr/bin/env jython
import sys
sys.path.append("../bin/")
sys.path.append("./bin/")
sys.path.append("./lib/mysql-connector-java-5.1.26-bin.jar")
sys.path.append("../lib/mysql-connector-java-5.1.26-bin.jar")
from com.csci6545.evoting import be,bl,pl,dl
from java.util import Date
from java.net import NetworkInterface
import re


def getWifiInterface():
    interfaceHandle = NetworkInterface.getNetworkInterfaces()
    netInterface = list()
    try:
        while True:
            netInterface.append(interfaceHandle.nextElement())
    except:
        pass
    finally:
        for i in netInterface:
            if "wlan" in str(i.getName()).lower():
                return i

def getIPv4Address(interface):
    inetAddr = interface.getInetAddresses()
    addrList = list()
    try:
        while True:
            addrList.append(inetAddr.nextElement())
    except:
        pass
    finally:
	    ip4pt=re.compile(".*[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}.*")
	    for i in addrList:
	        if ip4pt.match(str(i.toString())):
	            return i

def getEvent():
    event = be.Event()
    wifi = getWifiInterface()
    ip4Inet = getIPv4Address(wifi)
    event.setLocation(ip4Inet)
    return event

if __name__ == "__main__":
    wifi = getWifiInterface()
    event =getEvent()
