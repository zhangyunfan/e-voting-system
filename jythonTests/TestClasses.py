#!/usr/bin/env jython
import sys
sys.path.append("../bin/")
sys.path.append("./bin/")
sys.path.append("./lib/mysql-connector-java-5.1.26-bin.jar")
sys.path.append("../lib/mysql-connector-java-5.1.26-bin.jar")
from com.csci6545.evoting import be,bl,pl,dl
from java.util import HashMap,Map,ArrayList
from java.security import SecureRandom,Key,KeyPair,KeyFactory
from array import array
from javax.crypto import Cipher#,KeyGenerator
from javax.crypto.spec import OAEPParameterSpec,PSource
from java.security.spec import MGF1ParameterSpec,PKCS8EncodedKeySpec
from java.security.spec import X509EncodedKeySpec
from java.nio.file import Files,Paths
from java.lang import String
from hashlib import sha256
from sun.misc import BASE64Decoder, BASE64Encoder

def testRSA(length=8):
    plainText = array('b',[0]*length)
    SecureRandom().nextBytes(plainText)
    keyPair = bl.MgrConfidentiality.generateRSAKeyPair()
    cipherText = \
            bl.MgrConfidentiality.RSAEncryption(plainText, \
                    keyPair.getPublic().encode())
    decipherText = \
            bl.MgrConfidentiality.RSADecryption(cipherText, \
                    keyPair.getPrivate().encode())
    return decipherText == plainText

def testRSAPrinted(filename):
    #plainText = array('b',[0]*length)
    #SecureRandom().nextBytes(plainText)
    plainText = Files.readAllBytes(Paths.get(filename))
    keyPair = bl.MgrConfidentiality.generateRSAKeyPair()
    cipherText = \
            bl.MgrConfidentiality.RSAEncryption(plainText, \
                    keyPair.getPublic().encode())
    decipherText = \
            bl.MgrConfidentiality.RSADecryption(cipherText, \
                    keyPair.getPrivate().encode())
    print "The plain text is:",plainText
    print "Plaintext's length is",len(plainText)
    #print "The plain text is:",cipherText
    print "Ciphertext's length is",len(cipherText)
    print "The decipher text is:",decipherText
    print "The plain text is same as decipherText ",decipherText==plainText

def testAES(length=8):
    plainText = array('b',[0]*length)
    SecureRandom().nextBytes(plainText)
    sKey = array('b',[0]*16)
    SecureRandom().nextBytes(sKey)
    #iv = array('b',[0]*16)
    #SecureRandom().nextBytes(iv)
    iv=bl.MgrConfidentiality.getRandomIV()
    cipherText = \
            bl.MgrConfidentiality.encryptAESBallot(plainText,sKey,iv)
    decipherText =\
            bl.MgrConfidentiality.decryptAESBallot(cipherText,sKey,iv)
    return decipherText == plainText

def testAESPrinted(filename):
    #plainText = array('b',[0]*length)
    #SecureRandom().nextBytes(plainText)
    plainText = Files.readAllBytes(Paths.get(filename))
    sKey = array('b',[0]*16)
    SecureRandom().nextBytes(sKey)
    #iv = array('b',[0]*16)
    #SecureRandom().nextBytes(iv)
    iv=bl.MgrConfidentiality.getRandomIV()
    cipherText = \
            bl.MgrConfidentiality.encryptAESBallot(plainText,sKey,iv)
    decipherText =\
            bl.MgrConfidentiality.decryptAESBallot(cipherText,sKey,iv)
    print "The plain text is:",plainText
    print "Plaintext's length is",len(plainText)
    #print "The plain text is:",cipherText
    print "Ciphertext's length is",len(cipherText)
    print "The decipher text is:",decipherText
    print "The plain text is same as decipherText ",decipherText==plainText


class HybridCrypto:
    def __init__(self):
        #self.plainText = plainText
        self.keyPair = bl.MgrConfidentiality.generateRSAKeyPair()
        self.privKey = self.keyPair.getPrivate()
        self.pubKey = self.keyPair.getPublic()
        self.iv = array('b',[0]*16)
        SecureRandom().nextBytes(self.iv)
    def encrypt(self,plainText):
        return bl.MgrConfidentiality.hybridEncryption(plainText,self.pubKey.getEncoded(),self.iv)
    def decrypt(self,cipherKeyText):
        return bl.MgrConfidentiality.hybridDecryption(cipherKeyText,self.privKey.getEncoded(),self.iv)
        
def testHybrid(length=16):
    plainText = array('b',[0]*length)
    SecureRandom().nextBytes(plainText)
    hdl = HybridCrypto()
    cipher = hdl.encrypt(plainText)
    decipher = hdl.decrypt(cipher)
    return decipher == plainText


class RSACrypto:
    def __init__(self,plainText):
        self.plainText = plainText
        self.keyPair = bl.MgrConfidentiality.generateRSAKeyPair()
        self.privKey = self.keyPair.getPrivate()
        self.pubKey = self.keyPair.getPublic()
        self.rsaKF = KeyFactory.getInstance("RSA")
        self.oaePara = OAEPParameterSpec("SHA-256", \
                "MGF1", MGF1ParameterSpec.SHA1,\
                PSource.PSpecified.DEFAULT)
        self.cipherText = self.encrypt(self.plainText,self.pubKey)
        self.decipherText = self.decrypt(self.cipherText,self.privKey)

    def encrypt(self,pt,pub_key):
        cipher=Cipher.getInstance("RSA/None/OAEPWithSHA-256AndMGF1Padding")
        cipher.init(Cipher.ENCRYPT_MODE,pub_key)
        return cipher.doFinal(pt)

    def decrypt(self,cf,priv_key):
        cipher=Cipher.getInstance("RSA/None/OAEPWithSHA-256AndMGF1Padding")
        cipher.init(Cipher.DECRYPT_MODE,priv_key)
        return cipher.doFinal(cf)
    
    def encryptByteKey(self,pt,pubkeyByte):
        cipher=Cipher.getInstance("RSA/None/OAEPWithSHA-256AndMGF1Padding")
        rsaPubX509 = X509EncodedKeySpec(pubkeyByte)
        pub_key = self.rsaKF.generatePublic(rsaPubX509)
        cipher.init(Cipher.ENCRYPT_MODE,pub_key)
        return cipher.doFinal(pt)

    def decryptByteKey(self,cf,privkeyByte):
        cipher=Cipher.getInstance("RSA/None/OAEPWithSHA-256AndMGF1Padding")
        rsaPrivPKCS8E = PKCS8EncodedKeySpec(privkeyByte)
        priv_key = self.rsaKF.generatePrivate(rsaPrivPKCS8E)
        cipher.init(Cipher.DECRYPT_MODE,priv_key)
        return cipher.doFinal(cf)

def testHexHash(runs=25,length=50):
    for i in xrange(runs):
        randArray = array('b',[0]*length)
        SecureRandom().nextBytes(randArray)
        hasher=sha256()
        hasher.update(randArray)
        stdHash = String(hasher.hexdigest())
        ownHash = String(bl.MgrHash.generateStringdHash(randArray))
        if stdHash != ownHash:
            print "No Match!"
            print randArray
            print "Standard:",stdHash
            print "Mine:",ownHash
            break

def testByte2String(length=8):
    byteArray = array('b',[0]*length)
    SecureRandom().nextBytes(byteArray)
    hexString=bl.MgrBytes.bytesToHexString(byteArray)
    return byteArray == bl.MgrBytes.hexStringToBytes(hexString)

def testMap2Byte(hmp):
    hmpBytes = bl.MgrBytes.stringStringMapToBytes(hmp)
    deBytes = bl.MgrBytes.bytesToStringStringMap(hmpBytes)
    return deBytes

def testList2Byte(lst):
    lstBytes = bl.MgrBytes.stringListToBytes(lst)
    deBytes = bl.MgrBytes.bytesToStringList(lstBytes)
    return deBytes

def testIllegalElectionMap(hmp):
    tmpElection = be.Election()
    #tmpElection.setCandidate(hmp)
    tmpElection.setWinners(hmp)

if __name__ == "__main__":
#    testHexHash(runs=200,length=500)
#    print "Test Hybrid Encryption and Decryption"
#    print testHybrid(length=32)
#    for i in xrange(16,513):
#        if not testHybrid(length=i):
#            print i
#    print "Finished Hybrid"
#    print "Test RSA Encryption and Decryption"
#    for i in xrange(8,190):
#        if not testRSA(length=i):
#            print "RSA crypto failes when plaintext length is",i
#    print "Finished RSA"
#
#    print "Test AES Encryption and Decryption"
#    for i in xrange(8,190):
#        if not testAES(length=i):
#            print "AES crypto failes when plaintext length is",i
#    print "Finished AES"
    tu= be.User()
    tu.setUserId("lee")
    tu.setfName("Charlee")
    tu.setlName("Lee")

    ff=tu.clone()
    tu.setPassword("12345")
