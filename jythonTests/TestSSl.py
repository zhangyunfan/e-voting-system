#!/usr/bin/env jython
import sys
sys.path.append("../bin/")
sys.path.append("./bin/")
sys.path.append("./lib/mysql-connector-java-5.1.26-bin.jar")
sys.path.append("../lib/mysql-connector-java-5.1.26-bin.jar")
from com.csci6545.evoting import be,bl,pl,dl
from javax.net.ssl import SSLSocketFactory,SSLContext,KeyManagerFactory
from java.security import KeyStore
from java.lang import String
from java.io import FileInputStream,OutputStreamWriter
from array import array
import sys,time

CIPHER_SUITES = array(String,["TLS_RSA_WITH_AES_256_CBC_SHA256",\
        #"TLS_KRB5_WITH_3DES_EDE_CBC_SHA",\
        "TLS_DH_anon_WITH_AES_256_CBC_SHA256",\
        "TLS_DHE_RSA_WITH_AES_256_CBC_SHA256"])

def getSSLServerSocket():
    context = SSLContext.getInstance("SSL")
    kmf = KeyManagerFactory.getInstance("SunX509")
    ks = KeyStore.getInstance("JKS")
    password = String("123456").toCharArray()
    ks.load(FileInputStream("./resources/evotingServer.jks"),password)
    kmf.init(ks,password)
    context.init(kmf.getKeyManagers(),None,None)
    factory = context.getServerSocketFactory()
    server = factory.createServerSocket(12345)
    server.setEnabledCipherSuites(CIPHER_SUITES)
    return server

def getSSLClientSocket():
    curIP = str(bl.MgrIP4Address.getCurrentIP4Address())
    factory = SSLSocketFactory.getDefault()
    socket = factory.createSocket(curIP,12345)
    socket.setEnabledCipherSuites(CIPHER_SUITES)
    return socket

if __name__ == "__main__":
    if len(sys.argv)<2 or sys.argv[-1]=="-c":
        sslSocket =getSSLClientSocket()
        #out = OutputStreamWriter(sslSocket.getOutputStream())
        #while True:
            #out.write("Hello!")
            #time.sleep(10)
    else:
        sslServer = getSSLServerSocket()
        while True:
            theConnection = sslServer.accept()
            inS = theConnection.getInputStream()
            c = inS.read()
            while c != -1:
                print c
                c = inS.read()
            theConnection.close()
