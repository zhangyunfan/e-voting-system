		E-VOTING SYSTEM
Table of Content
i.    Configurations before Compile
ii.   Compile Instructions
iii.  Database Configurations
iv.   Connection Configurations
v.    Running Coding Instructions

	   i. Configurations Before Comile

1. Install Ant (http://ant.apache.org/)
2. Create a global environment variable in the system whose name is
  "JRE_HOME" and whose value is path of Java Runtime Environment.
3. JRE_HOME is not JAVA_HOME but the "jre" directory under JAVA_HOME.
 By default, in Linux system the path of JRE_HOME would be "/usr/java/jre/",
 in Windows it would be "C:\Program Files(x86)\jre7\", and in Mac OS
 it would be "/Library/Java/Home/jre/".
4. Defining the global environment variable:
    For Linux, add JRE_HOME="/usr/java/jre/"(Depending on the actual path)
        to "/etc/profile", or use the command:
		export JRE_HOME="/usr/java/jre/"
    For Windows, Computer->Properties -> 
        Advanced System Settings ->
        Environment variables-> Add JRE_HOME to the System Varaibles.
    For Mac, use the command:
		export JRE_HOME="/Library/Java/Home/jre/"
5. Ant cannot find the JRE libraries if the above settings have not done
   and it won't compile.
6. Copy ./findbugs/lib/findbugs-ant.jar to $ANT_HOME/lib/

       	  	ii.   Compile Instructions
1.Compile:
	a.First enter the e-voting-system directory
	b.Type in this command:
	       ant -f antEclipse.xml build
          (if it fails use: ant -f build.xml build)

2.Automatically generate Findbugs file(fbscan.xml):
	a.First enter the e-voting-system directory
	b.Type in this command:
	       ant -f antEclipse.xml findbugs

	       iii.  Database Configurations
1. Make sure MySQL server and client has been installed in the system
2. Make sure an empty database named EVOTING has been created in MySQL
3. Load the EVOTING database:
    mysql -u username -p"password" EVOTING < ./sql/EVOTING_DB_dump.sql
4. Load the COUNTER database:
   mysql -u username -p"password" EVOTING < ./sql/COUNTER_DB_dump.sql

    	 iv.   Connection Configurations
0.Every xml file mentioned in this chapter locates in ./resources/config/
1.For voters: Change the server_ip to the Server's IP in client_config.xml
2.For Server:
      a. Change the counter_ip to the Counter's IP in server_config.xml
      b. Enter the username & password of the local DB in server_db_config.xml
3.For Counter:
      a. Change the server_ip to the Server's IP in counter_config.xml
      b. Enter the username & password of the local DB in counter_db.config.xml

      	       	   v.    Running Coding Instructions
0. First, create superuser account
   a. java -cp ".:./bin/:./lib/mysql-connector-java-5.1.26-bin.jar:./lib/sun.misc.BASE64Decoder.jar:./lib/javax.mail.jar" com.csci6545.evoting.pl.CreateSuperuser
   b. follow the instructions
1. Before voting,the user accounts and a counter account must be created on the Server side:
   a. java -cp ".:./bin/:./lib/mysql-connector-java-5.1.26-bin.jar:./lib/sun.misc.BASE64Decoder.jar:./lib/javax.mail.jar" com.csci6545.evoting.pl.AdminPanel
   b. Login using superuser account 
   c. Select add new user.
   d. And then follow the instructions
   e. select add new counter.
   f. And then follow the instructions
2.For voting:
     a. Start the voting Server:
      	 java -cp ".:./bin/:./lib/mysql-connector-java-5.1.26-bin.jar:./lib/swingx-0.9.5-2.jar:./lib/sun.misc.BASE64Decoder.jar:./lib/javax.mail.jar:./lib/bcprov-ext-jdk15on-149.jar" com.csci6545.evoting.pl.ServerGui 
     b. Start the Counter:
     	java -cp ".:./bin/:./lib/mysql-connector-java-5.1.26-bin.jar:./lib/sun.misc.BASE64Decoder.jar:./lib/javax.mail.jar" com.csci6545.evoting.pl.CounterListener
     c. Begin voting:
     	java -cp ".:./bin/:./lib/mysql-connector-java-5.1.26-bin.jar:./lib/swingx-0.9.5-2.jar:./lib/sun.misc.BASE64Decoder.jar:./lib/javax.mail.jar:./lib/bcprov-ext-jdk15on-149.jar" com.csci6545.evoting.pl.ClientGui

3. For Publishing encrytped ballots:
   a. At the Server Side:
      java -cp ".:./bin/:./lib/mysql-connector-java-5.1.26-bin.jar:./lib/sun.misc.BASE64Decoder.jar:./lib/javax.mail.jar" com.csci6545.evoting.pl.CounterChannel
   b. At the Counter Side:
      java -cp ".:./bin/:./lib/mysql-connector-java-5.1.26-bin.jar:./lib/sun.misc.BASE64Decoder.jar:./lib/javax.mail.jar" com.csci6545.evoting.pl.CounterPublishChannel
      - login using the counter account having just created in step 1.
   c. At the Voter Side: Still using ClientGui then Select "Verify my ballots and send keys"

4. For Publishing result:
   a. At the Counter Side:
      - select publish result and select election ID
   b. At the Voter Side:
      - Now voter can select "Result"

For ballot verification, you have to use CounterPublishChannel to publish BcBallot first.
For viewing result, you have to use CounterPublishChannel to publish the result first.                   
