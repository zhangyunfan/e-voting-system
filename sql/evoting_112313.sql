-- MySQL dump 10.13  Distrib 5.6.10, for Win64 (x86_64)
--
-- Host: localhost    Database: evoting
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `evoting`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `evoting` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `evoting`;

--
-- Table structure for table `ballot`
--

DROP TABLE IF EXISTS `ballot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ballot` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `BALLOT` varchar(512) DEFAULT NULL,
  `BC_BALLOT` varchar(512) NOT NULL,
  `BC_BALLOT_KEY` varchar(512) DEFAULT NULL,
  `BC_BALLOT_IV` varchar(512) DEFAULT NULL,
  `ELECTION_ID` varchar(20) DEFAULT NULL,
  `COUNTER_ID` varchar(20) NOT NULL,
  `SUP_SIG` varchar(512) NOT NULL,
  `ENTRY` int(11) DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=130 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ballot`
--

LOCK TABLES `ballot` WRITE;
/*!40000 ALTER TABLE `ballot` DISABLE KEYS */;
INSERT INTO `ballot` VALUES (106,'MSwxMjM0\r\n','Ig+4n2AQ6B/fDK5dcNYY4w==\r\n','1ck4a4Ht+g7IYCWvXJhQzQ==\r\n','1ck4a4Ht+g7IYCWvXJhQzQ==\r\n','1','Counter','RqtodlekcnTuayKTX+kgGXQzN3mq35vMxIp5buTbsZ/SK2dG7k/bNgGC5nZpYylOTTseIGdqtpGF\r\nrHBYzrUWYFny6X/a+dp6jn4begNJX2ll/jhD7JOxy7+IoUPhG/VJT1G+HXkKYfbh742CALFPABZ/\r\n34Bj1pA6cbT1HHYcoqJ3A9OLevOl9uuu5TP1R53M+rrbMMGdckhLJP4cP1AePIjkmqsZk0uzWgq5\r\nmKc3q/ibs3aIdF2iNcMg5jjzI/CDhdzeA7RF4TEDpgGsVH7TDJJw4jhywvNe7plOdmR1SuQ9D3iY\r\nJY9iel7J1drUE0xloN+pWSgmp2QVmdAb7+TXng==\r\n',4),(107,NULL,'YwVCrsHgOZ7xx9J8Xj54gA==\r\n',NULL,NULL,'1','Counter','fDwK/NSE+3emaHUTrDAqNe0l+FWj+RVmYS5PD824GhStYuBIwW4WZxZzFO5n/bfjjj9FvP0tzchx\r\npX5I1bRZWO0Ddz6TnNlV2IG0GRB9RIJikChXPnp0W5FywynbRvzFoyWHjpcXKQ7UnM1xGCRjus95\r\nVotHuUaetjnXIjKEof00u8L5CzrK5oEGneEBMoL1Q9y2BBeFwA7iumNPziG/d3ya1mcrf0lkZNUb\r\nBOXKegtDi/a04rArk8gFSMPZTUxOzEpyyLOhH+iPpFpr18JM1msR8VYrfyNZ/zkX56BFQvQsdQ8J\r\nMV8vxeqIVu0Wje9mtuSGIB3EdPqdtOB9Vg6jJA==\r\n',5),(108,NULL,'RLSVH6DB0fz9itxCIwl90w==\r\n',NULL,NULL,'1','Counter','DAH7X0qS5kkH/qQPAjAdGqt9yd2TBZhA2/FVfeOiIYjT4Ajmw529qu8ZqWNdpfzubzibxGgKFgyH\r\nHv6/PNvVNX2xQAVUV1L8uGonjZABiRaskO4bwqgpnxLIcxqivbeCAokk+0PFI8d/4IOcf8zUUdOs\r\nwQnDQnS6C8PAOCaHR/0tVRofojZDcttgR2/AfT2ar/oKKPXkACyMKjHUldwJNsUZBKPLIcbVHBQn\r\nRNFUezUsOkhOmZxd/gQ06Nz1F7TFk/Zs7i2b7+nTTdh1DW5HT11B7BwshI1J9NqNXs9X+py+ZApV\r\n9KyMoj4PI7Olm99g/o2QNaYT0pachs3y7gKPWg==\r\n',6),(109,NULL,'eiDjxeHF/Ye6rDwGmZI7Mw==\r\n',NULL,NULL,'1','Counter','Yk//Hq4C5dxisFiF96o/QjDgKEMEIBWBDQBX5hzc/Lq/cvdlCwiut7h8P6thiXdHDAThKPWBVuFO\r\n+Y4dM8TCChahPcCXDTNW0/GsGcHYjxtgF42QGpnjZTF/yR4ZZ3/E+4H1IWbOARNGVlYaxVjmvepP\r\ne0+WpIZTTHwFglMaPsxWtix9+oZ7+IbPLKL2qtBULn8AxGoAm67tkrtYLkJLZYUK9uN5ORm4OFXF\r\nICFV1FbeEP+pWsmEMZ6jLNMYDE9Cc63nf2l9Bcn5/FSND2KzgvhzNmVE28kGg427/1pk9i0+IAc4\r\nMMZFkfWrNpo+z+OnvnvlPqrVMMeUyNjafczu1w==\r\n',7),(110,NULL,'f/ozbFj9rDBdzVrTy3dE6w==\r\n',NULL,NULL,'1','Counter','QgRbYs20MK6C1gJZ0BIen/VQllCQ6qXXrgkQBaw9YUpUHyrm8cWYUXeOqUohuwRlVUq0vJG6xq49\r\nxD8Pe26gIRdTCMgU8NtUso/URgjlUFM6E4sw7HG3aaUdnk33/VEVd80YtO3qK3j0UxkImpbuLyuV\r\nqca1hv2khGRUkXMsSTjbXmmW1DfiXVgWnEUx3ONLGo1gJjvgqy5EkNky+Og1nkW7P4sRPps55Ws2\r\nwQZ5d/0YvrRrSwLiRL6gB2lgCjoBi06iZFYxDkBzOy4fepttncWTC5A8kKoDwO4bJeD65FpfAcEB\r\nABG2KmctKlgiz1ctRK5MuHKfOlIqTxZqjidmcQ==\r\n',8),(111,NULL,'aytlTyzkAgdqQuUv13TbPw==\r\n',NULL,NULL,'1','Counter','SbPpJJXzk/9YCojvxvvGfR+tcd5hFvZlZGVJhvZzdgfkcJvrgVAAWPe+4o3lmkE1CBKU+Ax42J4A\r\nGBnMVGOpeZ8Ss+WVytcuqj62KmtlJ99WF2ibZSNrLi9C3Vsb6kAEkOrDNuSnYJatBAXUgf/IOa4w\r\no9CQdFfY2yEFbzc2bnNy008wBLpvuU0FXY21hAovREPHGL0eFmmkIIYHkQPlwCrSk5pocWGdjk1M\r\n5jVNG8H14JH/Wt00B1eIiJe5xWSBPM/FoPzaLU9X/pg/MvKnfwA0ZEHZnLtZcSFRa8dAvwzFCNGQ\r\nwU8ihYQAyI8Ujm/te9znOOekIypgnR0V3GjJCA==\r\n',9),(112,NULL,'ckSTLO8jvSgDEWML+4v6vg==\r\n',NULL,NULL,'1','Counter','PyGfeMwGi4HIx2N2Y5+uZj+frnpj5SCB14cQmg/lPHhqDqRw+u5k8UD8mTTmhI3l4yxTc4+yXz2h\r\ngY+AzrhMbD/3251EZ7lzohQhyAZ8zoicARFXuANJ6Kdoy/Oj1huZYjx2RkbsB93XYxfk/tC5644h\r\nvpYqpJro3VoYKSfA2daaG45G9J5glmkNvZ3xbvDr0Isgq+rypqMpmJhFJesjb7U/sP+RLLuJ/60L\r\nLW1GPSIY2PVmR4vu7N1TgxRlztgOH98+lBoI6CWEs/uiUqYo3S2rjFE8NsjVT8Ylt90mzpeZJAhT\r\ntmSEB6vwgpfnHBO/BV1dX5cY7Vprp4jV5rFycw==\r\n',10),(113,NULL,'g/bq6FIKvZOA6RjzGCmUqg==\r\n',NULL,NULL,'1','Counter','E8kvvzLKWJsLCRCfoshXdSgcMSP4JqVFASB9FHNCXc0hBuYGD/CTUkVJrcLccl9aPuCcV4etZChK\r\nInzwmF8WAq8S7un+BeiVGG8m+TD8gHEniLcYl+XBeDuYAG+z1ovbPXNQsCzdpoPYhGBIUBdXtCrW\r\n4rXtywIUWvxLzu2ofbnoMnPp7w+FtJ/duGnCnUGAj49hGcHDQXo+rOOT0fXuaTIWUoYynKd52I40\r\ng7Mduol59tXDhOSkd4jAOxx+yP9rShg8ac1nGYR080NekZ3nsnyK1WbjWSb614tFRjUGgARYpBNW\r\n0X3/vIYdYwqSwOjKjVZUQJmGi1zFBKiORZ0rrg==\r\n',11),(114,NULL,'YFWtQrUwa8AS2iuFHR/kCw==\r\n',NULL,NULL,'1','Counter','Sq1kWsRRlySEwMsr8PW8Xt1dIJn1aHCzAFlGfdsmc3HCuPFI63FCSCM6Z393T+kpNgG5hTieI/et\r\nNKRt/RbKa9TIYXism3iP84bRg8a5H1N8mCqRDFBTZgJBID9XUorgUz6/MhgEU8q49ReiV3sIaxvp\r\nwZnTRuiWagTr/eW87VTxq4b5iqPsjurX3H3U3fRYgJZ9tZrl5fYbqTq2KfAfg45ngCavAS56zu+/\r\nWhTlOEj6fn4/JuxQCgfUaWqa+vYEQDpHw0/4lT6+PLRrIPr7FuAKcdnmUo6nq0L9Ty3gq1oYcHub\r\nVISmdfCAQ9X4b90jnuwctVbDiOfTZ0cn1dL7Tw==\r\n',12),(115,NULL,'V3KBWGKVaFGnqGg0ggejuA==\r\n',NULL,NULL,'1','Counter','DZUCm4CbyDJ5PlgnHRE/0xJChaEIFmazp9fTDyd3CAmt0/TcBRrXDJX4gkTmo7R488Mf3tvRFvmJ\r\nQYc5tBg0Im4Cfdwx81/D1eLayL9k2u0UW3blrGJX+eZTn7y7B+zH/xUfJqlU714X3GfzfzlOSe+e\r\n3Cnrj0UGxpE4C1/kdCUjq3hmytx4iiaflsuNlh1etBGcTYPSQXq05ZEWpLst+bSBaVZSobI39hsR\r\n3fGbyYA8RgPfnxF10RfHN7R1oyF46deilNPKC4LVpTFuZ0sLG1KSEQ5cSRH5yRsRSgNMFKyyXrXO\r\nP+r16hvNW1aoV9gHe3bLWBtqNew7MWT5EILYIw==\r\n',13),(116,NULL,'MfGQ/KW9PzUMJFbcpbIuaQ==\r\n',NULL,NULL,'1','Counter','ffYvpa3Qxksa+bnZLnGEHrOpTY/4XyDUmZzbnX09Ur4vaMae9ydTRQ94OXCkEQrtjXJ3P9gcPVi2\r\nhNktli+zR9yy8ixz8Rs+Q+HC1saAe3I5nUDWyiIpRocqMkk+hcDfkeSD4/uI3Z+91adRJfwHdEQv\r\n2rg8wGZGcU15rObLtBc66vEWxc/6ZbvKURvn9v6cSIWKjAJhqwv3PITh1AyZdhA/OV30jxeBOQsn\r\nAGJ/vPqEcz3LSMMYEi3MCeleiuI31QakqoEHpwhH7GUAhbz31XQPEzAMFuuip9Q7UWJ+Dus0IYGD\r\n5lDq2rv/LgSYZFNYKGDoONnW1Pekao2daTNhcQ==\r\n',14),(117,NULL,'A9oBsOkGcGlocNn4wZKg5g==\r\n',NULL,NULL,'1','Counter','Jk0Nd3XTosHjGelj4vCygU8Vq08w8ZuMOCeaPzBqKEdzf7PdhUuTDXWQWQhY3anquFshj2MAGBiK\r\n8sbjcbaEPVcZuv0LgtcGymAgKKZ0OzDvFGErtEepby5abD2/jwgkuwlGYJGaG/179daaB3SJhr+4\r\nxNU9BJNbE8rOVrRIOKq1grONnnEZ3Q9u1Srjx5jRw0UNqgIyZynCWJg054bTizCDdt7GLn9AAA+K\r\nObHDaI9iGK9lc4kNaEMfCDgYwzcdJZMCdmyWMGOcycjnpQKwSqWUSNhX1TniM/nH0B0tddvwkFOD\r\n6JJB2RFgLsge9w7ZVj7Ah5ENwpAvLBLDOH6xdQ==\r\n',15),(118,NULL,'qGqqHY3VS/QzUVkWkrgblw==\r\n',NULL,NULL,'1','Counter','c0tQWjZnOWnPs/XFLmnDruknrrhVNjALrilVB/QoBotg6R9FYOImadmLYSLhjEg5egQxwif+SwIS\r\n3a27l5sjJ4oEDSGHPu2gLlqDEOlY7MNj2ix2kL9LKos6I8pihbzcB63kMiuvB+RplI3ag6JJ9gFg\r\nWR1xizyuheJxx6OnuaGvhqpfZOwhydkdrHHl6gRWEgl/93vlBrdLXc0vsMecG2/pgq5yQVPMtCRK\r\noqFtQtCT7i7WqYo6AYD1XLYWxcCmWAnHNcuT/RLsK+vWJCLAyNsqaumldPtZz9SD3LbuV6LYCHbk\r\n47FH2mXYIcEDDAKR2zoWuzdbgN4mG9QsMTIoxA==\r\n',16),(119,NULL,'Et/v+lDlabo76QALvCVMYQ==\r\n',NULL,NULL,'1','Counter','X4iDpPqxHUhsqzXYKdQAvX/VcJMGnsgpAqQTs5YVHCkxporUqqDyQuMl8nz2BEI8lf1Fu0Mx9Nwz\r\nutE1m6jT3xZSINBeb/W6P/PCOA2G/qcgsD3ATc4BvvVTcWTsUkw2V6f2cOFZCF1QQG1jBem1uH8B\r\nzf1RqDL+ByRF2b0fvD5+2EHvwFeZBh9qnYEHaYdRP9yHChgDk8LEvMBGdsBiNmwOlqm3ckPgLF3s\r\nA/9ke/dR3u58DEdwqpNvHS9LFPNWKQaSdx0d+AsoDtiIRgt8ht8Gsdo8CcuNnZSiuYaN1RgHtrFP\r\n/Qu8EMbbAzvs/hs3M9AOZgVuG68B6B2HZcYCqg==\r\n',17),(120,NULL,'lbTz5IvUNCfBDgWbfIdwtg==\r\n',NULL,NULL,'1','Counter','ZhbETkRUorjssubqQM+hDgJumCizr8gW0QgegbuPAldbuhavKupc781DF5ahDvzSPwcC6syt3iDd\r\nEErb7SkvTtfzGRigoVsTNN+hycYGADR5Ozs8E/nkDkbbpWHtbTwJJrOt12xXtaWXsYI7OdsoK+lx\r\narbWax8BJIox+Loy2f2nWqFQScHE9cQxDGqxjuFxLgBBxXz5ImPPflJ686299dMxMz7BIfe9+4fq\r\n7X5OkqiBcJ28yyFqRUcLenH5hHW5cKPwskxS5k/LzBQfxaz5RIG/r94/86FECeMh7pjSfJleiLyT\r\nz7UZ57qQXIW96qwLr60OrmYuRwBnPGoC9FiOTw==\r\n',18),(121,NULL,'HDmvbxX9Bao9H8IX9YxVng==\r\n',NULL,NULL,'1','Counter','WpAshWn/iKr/KW/t7nJ9bfvRgZGThUARBIDnwdEE5+YDHkosgJLYYM+Q6AsbNkb8akQ8SDpFgIOz\r\neSRpACleA065/wTjkZefIjYR92TUMkooFcu1UzhhYv9ngqYXEJ+lLT/iNtaEG6LWFc/5D2w3eONY\r\nh+mvU5M05CObreyAt/jxDfrFFRfvdNNEhEXjPjDCVrTIa/DiCRXQ3Xi2tOwMHWH6s8Kh00U6nJqG\r\nTJlc/9duF70FBWVF6N7a7U9Oevoczz4h7ZraxwyFXIUYw5iUBWGVrdJN5uuXKT+zxqQ1XrYut9iK\r\nN5rpHPJzeR/6jac7ECxpvepzr9F+wSZ2Dfi1qg==\r\n',20),(122,NULL,'v1Hvbb5G4wDsaW3iz7fs4g==\r\n',NULL,NULL,'1','Counter','K47GlQ64VK2yU69ZAGsjQXO0OeYj96IDrnaJUv1Xi8kyA/7vJXvR3cJDVWaNEIyjRXuamTs/3DPW\r\nmu+YO2RDaTDfj5oRRLitWTnhML+EQ3KHfcL30R5UWU/0mAGyPLzIwQ1IOLzwOC6qCfB18Ly6o+a8\r\nJhEoAo/ARtjCsJZnSpyPvnIaWsurzYWrKSM8lo9aFxwXwTMmAfqPOIa3Q7dlUWHlIGOzUoZpnQjT\r\ny6EACXMfooaMuBBKNWjCAqqPocpYMNXX5pBcwAkh7E8qX4U1XmXv4/zC+T686C5mouFi8EH1ieqk\r\ngYV4mb9Ts4hY4RrfMi3cSI/2ZGuPjSIn1hXEGw==\r\n',23),(123,NULL,'cOm6WMezOksvj8RHwTlviw==\r\n',NULL,NULL,'1','Counter','Gr5iTkuzcLa+EzA/078uZUWy2rti9yEO24gzmi0vjqdy493vDFcxfgWrN1A7ydTnP7lkTwdg5zQ+\r\njWBz0nk51/jAWFtv5MaHyCP4v1TLvR1vlSncTO5HM4qqkzekj5bgq9eW8I/RG1THynkHf97ikaod\r\nCmx3Q5YH4im1M9B0FW1azyJEYe7wspezJKvLfBS0p7QA0UyFo1yNEDi/J0t2OljmHgZd1XZswM4B\r\ndXlrwePZdK7UYptegxhucIepb5BEo04XSt6YOqMnNTsyL0Qfke1Jbv+u35USHbttqrkXY5pCyIl3\r\nlybHp2Z0xoDqPU6E4DCIsNhLqO0IapsvCMacpQ==\r\n',36),(124,NULL,'0B3lQmgBWXSYpL/+F2v7yA==\r\n',NULL,NULL,'1','Counter','PJxEb7jkV8cEqOV7oE9vgXMvIrDhWx8d9YQuo/JrvrVI9KjJrOIagXhfIk+fcl4/DNl6bgaBETc8\r\nDC1YBQbdnCPnHmRnOceHGnwF2kKifxfcSvcbRHDfeUnuTU1TsvV29v9PKcjTGoAV/Eljw+edKtQR\r\n4aQgm0cDgCOY8k5wf1Srks0OsAEIamwDkVeOf/1KFosqjeyH/WYuGVxH7ci59vHI/WfUhIIL38A2\r\na75pX8ttWsPLx11mZUe6eCkp5491tN+6c39/6yMEyE1LK52/bYcVqBefIh7RCzUFKMPazjfgNUja\r\nDHkgOhd+hiUJsfeJXArbEfIGScmIurdAxkFhrg==\r\n',39),(125,NULL,'cMsQwAFecVFuuBGdlGRr6g==\r\n',NULL,NULL,'1','Counter','J+SFUz13Fpoi2iFy1eIVRyfSsZBvMZ/GFYnhh1ILbStXu6J61CvOv3EdEhZHzhgiHVozTLJ/552u\r\nTA4l3MbN4nmR9/FdzUgKSfmZaPylOV05U9alpSe0F5kRiYDE3Nm4t70kWVkq0hXpn1Inb3LRBZx8\r\nL4mNUPxE4EkR9txQQTRWhqv7Goj/CnwJCJPTOAAx479gOJIxhhWv+0ee8CnpMeTNp+iRJO9euEXx\r\nWwPR6eJOAGKQPwP43Ilp7GvE8ck2+47p4kQgd8tELM8dWBXYtufxStBa6OJfW6SR62vVeACKaz0K\r\nIXlkkM5JbMa3prRQgJSfTuGOgy99dxUaaW3f0g==\r\n',41),(126,NULL,'sjUVqT4+nZG5qVKq8IQKHg==\r\n',NULL,NULL,'2','Counter','NK5T+m5PazqM6Q/sxWtRp71akYe5EiEtQ9Qgb1Ho6/emeMXjFtwsHTwMYfSgJGL3+L6OcfgDYRqd\r\na2P43hNAbXpbHqOTp+0ISXbEpv5qWJdodOTBHHreHO1G7q1KXIgS7yiOWa35lBwEbX/a9C6Tkw2T\r\n1r6lRwZT5hQ9Y3F1nNwfxiuku3PGjC9g0bpdTA7iVb15n432eNc20TfUNEUC/YSsLUX3EosSjQWa\r\ngRnWRWbC+lpraBOesEPXmtSlqG1lsMRi5JfYIPBuf9WP/JHp346wxsig7YmRdFjXyeNeAPMgTfap\r\n+PgpnP3O2nZSK/zM9u2MLjlZzZce2Kd2CCN+6g==\r\n',21),(127,NULL,'VcTUjSpOjsZN0iKMmEUQ8g==\r\n',NULL,NULL,'2','Counter','g1+f5G2svN4XiKG8PAull1ZIOxd9n+lY85SlCy7hiUn+7kyCgcXixrf2o4FZt3nAaX2qdeBvsLWG\r\nU6hgEQuxM/tbGJvbpQ1idevFhbFCFKUGY4uY/J2Sd7NdbdmcKw2+6fnOu8bINSJt9vTfyl7Ukwun\r\ncNx0PNiFGLuvg2kiy0iWNUziHYZwuxh1CBwo8CbFqUfHsO5G9+H7Jr5ErnK3xHP1r68o1uCrbTUG\r\nw4QWWHwO5yn7pLOeHG/hxCZUK9GVMsfwAfN4OqXn7OjTXvTrNOIvvjr+WvPPwXTDo8ijSJKv2fk/\r\nrAObB7mTmBdxItbHXKStoLo3uLGtegbGgXDkhA==\r\n',38),(128,NULL,'TC+lUWakllqA85HwUaaRxA==\r\n',NULL,NULL,'2','Counter','GJzl0RMzJprgoU2SyBoN2GPIRVlrxaBlOwrN/ReBApNPphCuV2MLP2QnnVUVvTWRL1uoZ1w6wWtU\r\n1RiOaieL6N0tbRwQuGnag2pjTRlg531Vy3qXZV/iH1hPUyWkXQn0qg93Hv4woYq7NAtB4yNvOwYD\r\nTJCYHobwHCWeEcdyjaWq9g13pMu0a541cP6XVSPf9d9trIG9MRF5HxfRSaVDRKRNauEs4FCqqGZD\r\nNiswJPVx9QDEZ2bsaysFJARPS7xE5svALgPPR+VzvOwcTmqBVGx3Aggzog5xDeunHSa3OfT1KOFN\r\nVhWeac0Pyrx5l4Vkc4ORhIRLmmFiXgqqhXBpdQ==\r\n',40),(129,NULL,'Uy8+x5M35GxwchHsZk8YKg==\r\n',NULL,NULL,'2','Counter','OAXfK35y7U2w9GMq532HVDCEoS3eJAxk3lB9M0O5URdcaSPgYGumaIk5NOj5nEZ1zJnOe63Pn+hs\r\n6EvIh7VnoCJODxEm72ytSsUTb+apkPZuMO3qIF/wu+9f8QqewiDuQIqLVOyccXXfufSWj8PG905j\r\ndEnbw3OJBYNasrSccY0xRD2JKpR9TrBAnbgByRbs1bMB3cDFUXA9kCJy979/c1SPqZ/1ubb+q9KM\r\nbDZ+GjJZ0zrN7u5oDhxHanGyofJ1yuk6duCYq8KkQ7smtPOzr/FaU1Y9stgha/f5uBAj6lOcgWx0\r\nfGcSaD1PY6GcaNqX4H11mJlNr9U+xNtSx9bsBw==\r\n',44);
/*!40000 ALTER TABLE `ballot` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blind_ballot`
--

DROP TABLE IF EXISTS `blind_ballot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blind_ballot` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BLIND_BALLOT` varchar(512) NOT NULL,
  `VOTER_SIG` varchar(512) NOT NULL,
  `USER_ID` int(11) NOT NULL,
  `ELECTION_ID` int(11) NOT NULL,
  `VOTED` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=175 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blind_ballot`
--

LOCK TABLES `blind_ballot` WRITE;
/*!40000 ALTER TABLE `blind_ballot` DISABLE KEYS */;
INSERT INTO `blind_ballot` VALUES (172,'VSPtX4xIM+77h5aNJxIdptx6vVyC4QKtRDJMy1QVxFRE78pmFsAGPovlY7sut+7pUUNy6AUYkbSb\r\n/7FC+Mgqnv0NVnGDcPJ4i5lNvWlJnOEO09s08zHhqnPuYxNCt4Nw9rOJubKY0BclBtqbZaOMBc5j\r\njUVEcpo1ScMV9HVYPtf45WMviwmBmz1Gb2raG9GDG1RbDQarJXWfhfhYX2br6VPNrVNt8VuU0dy8\r\nXqQi/OF4+WrXbINlPZ0gRXd8V94rrainhBmt8yz0tXJCUCd811hDQfwBiXI7H26Bvo2b+Lj2nZJi\r\nw1MlAO5AGyF02F6zhgwxU5wAdn82JjjpP99RLw==\r\n','NG/ZUaffJVqWk1cXE+Qit7xuDLj1YUFCQZivstf+aei7+dPBi24o/xABL9oA1M84bLOWazDURoIF\r\naW04JRWSv41hbzPsUia8g46oIm1mmMcEAJGeb/3hJTU2MeEJKMF38lBwT80XdICGlZNUUAc5e8mQ\r\nA4h81fIUyvNvcfe07gt5/4tHsh0gnRL4M0/JuvybORwt9JViq8Pksntm8pltvrMoMWewyMawiF/S\r\nBqyhjvzapd3lHCG4COJPbaWJlAdktcy3N2WnK+SU1HcRk9dwFxmnfTUHNGNAj3Au8GrjQMdFIpeH\r\nC3751zWVtDbQxWrhRILk3BhL0cSkZdTq8Hwimw==\r\n',2,1,1),(173,'U5QBpZlZkSK+GkuXDBMZYHtqCpJRZCT4+7LPrzUqwT2dr7N9PyUvhjbQNXlogXSQq32FnptNpu/t\r\nf9cM3iimjLL3tzmDLPqQu2F+ydFAnlMeenKdcwZYXpj+47nGTsJhIy7yHfZB+WG4khRwP/7bDlxR\r\nHmY6EPxmEuFobtlRvjR4xqUvW+MfjDsV8ZJMyBfp5li0BGN+xpHkRyu1vFawb9fZWX9IhlmJmRoY\r\ns0TMScFBv3qKJkRtWoRGkY8icHM9aaocJii3efxiL+PBem/xI7JWYNTweiQsftD5D6YIM19A63C2\r\n8ZwNtMdWzdDS7fDnoFNfMM9q89FouuQgMFkfeA==\r\n','C82aRcdWQr1WyNkC0+gEbUzbSZSar7x34V5WVL4CLtXaaNQKbgLrp05TUEJYzgKmuEcfMV3Gpa14\r\narWqi23Kz9TwdY+MmVWglmTeC/ghgWipthcgWcytVPTIwea3GQMOv96MEQrgHJ9ZLispqIiLN3El\r\nrLmprMhJqVh1lGNOPpfaR/fYhxbcisv4lgz7U1ygoMP2/4OP9whFIjKjwmT1YyvVTj20/BXdO+Yd\r\nqPduTjEDaUMpfjPLdRxpDXarcfP3AVRPXGKKZ9sDejipzVPQAuPq6qtwjkV91MEJbLCTtjUqejEQ\r\nzn6kUXeYqCCx7VQ9OvktqVdSvprAWtnnjdwMqQ==\r\n',2,2,1),(174,'AkzQ6TqKnyRZiCCq3oz7kWQog2vE/xaggOOjGuxYjSx6kk3LUNbRAaJa7wyfrVh7BXhhHYdYsuFy\r\ni2mODMmz9XF3iFXvziAxdwswws5XRwen4uSyOwKXbT7HxyysHB8ZbfbmtN+I/RfB7XFvob2MoT/o\r\nCqVPN9E5sBKIe+UeqQWHegPlxe5CAXcL3cGyDve1cZzDVPfMLkliGuM/6jab71cyFsC/8o7PQljW\r\nQlUmbD57ankRpabMtc6B9bst7yR4JpsaZz23BzUyYpVVJUeAXnvYhyhC6Dt93hZFyczNtUk2zAgt\r\nB62cDyQlnZbseXVAvM2R2l4U/htJqPUzZlR5xw==\r\n','XJDvk3bTl6tUryNXO+3vXoA0PMomMuaO5rmjd1c5qUGbL4AAP2dlt04+rALOA5TE89B+3yhfr8pO\r\nup0Zwka2+IL0DKFTgQWb2Ygi5ozi3ctHclkd68L4Kt01TJbJ4z7t8FL6c3Q8jJt6D8OciquIAJm+\r\nqtRDJOTgW1HXwTEkq1q57uIANgOHcU5EBJGxoi3zydVHjpSksjSbDvc21QUPMkYv9Z8BalVpUJ+i\r\nDLdp68dVag/RmimJra3mZpkbeqS3wJ+3o4lriZS2XnpjHNSf9FOtGaCnKVlIpdOnywvnth/C+eHQ\r\nIbhaOoWKTtvsvgOSp5mK/dNxQVd/RYz9q4vd\r\n',2,3,1);
/*!40000 ALTER TABLE `blind_ballot` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `candidate`
--

DROP TABLE IF EXISTS `candidate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `candidate` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `USER_ID` (`USER_ID`),
  CONSTRAINT `CANDIDATE_ibfk_1` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `candidate`
--

LOCK TABLES `candidate` WRITE;
/*!40000 ALTER TABLE `candidate` DISABLE KEYS */;
INSERT INTO `candidate` VALUES (1,5),(2,6),(3,7);
/*!40000 ALTER TABLE `candidate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `candidate_election`
--

DROP TABLE IF EXISTS `candidate_election`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `candidate_election` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CANDIDATE_ID` int(11) NOT NULL,
  `ELECTION_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `CANDIDATE_ID` (`CANDIDATE_ID`),
  KEY `ELECTION_ID` (`ELECTION_ID`),
  CONSTRAINT `CANDIDATE_ELECTION_ibfk_1` FOREIGN KEY (`CANDIDATE_ID`) REFERENCES `user` (`ID`),
  CONSTRAINT `CANDIDATE_ELECTION_ibfk_2` FOREIGN KEY (`ELECTION_ID`) REFERENCES `election` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `candidate_election`
--

LOCK TABLES `candidate_election` WRITE;
/*!40000 ALTER TABLE `candidate_election` DISABLE KEYS */;
INSERT INTO `candidate_election` VALUES (1,5,1),(2,6,1),(3,5,2),(4,7,2),(5,6,3),(6,7,3),(7,2,4),(8,2,5),(9,2,6),(10,2,7),(11,2,8),(12,2,9),(13,2,10),(14,2,11);
/*!40000 ALTER TABLE `candidate_election` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `counter`
--

DROP TABLE IF EXISTS `counter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `counter` (
  `ID` bigint(20) NOT NULL,
  `USER_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `USER_ID` (`USER_ID`),
  CONSTRAINT `COUNTER_ibfk_1` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `counter`
--

LOCK TABLES `counter` WRITE;
/*!40000 ALTER TABLE `counter` DISABLE KEYS */;
/*!40000 ALTER TABLE `counter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `election`
--

DROP TABLE IF EXISTS `election`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `election` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ELECTION_NAME` varchar(50) NOT NULL,
  `START_DATE` datetime DEFAULT NULL,
  `END_DATE` datetime DEFAULT NULL,
  `WINNER` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `election`
--

LOCK TABLES `election` WRITE;
/*!40000 ALTER TABLE `election` DISABLE KEYS */;
INSERT INTO `election` VALUES (1,'CEO ELECTION','2013-11-01 00:00:00','2013-11-13 00:00:00',NULL),(2,'CTO ELECTION','2013-11-01 00:00:00','2013-11-13 00:00:00',NULL),(3,'CHIEF DIRECTOR ELECTION','2013-11-01 00:00:00','2013-11-13 00:00:00',NULL),(4,'TestCreateDB','2013-11-14 00:00:00','2013-11-14 00:00:00',NULL),(5,'','2013-11-23 00:00:00','2013-11-24 00:00:00',NULL),(6,'123','2013-11-23 00:00:00','2013-11-24 00:00:00',NULL),(7,'214','2013-11-23 00:00:00','2013-11-24 00:00:00',NULL),(8,'12','2013-11-23 00:00:00','2013-11-24 00:00:00',NULL),(9,'12','2013-11-23 00:00:00','2013-11-24 00:00:00',NULL),(10,'wq','2013-11-23 00:00:00','2013-11-24 00:00:00',NULL),(11,'123123213','2013-11-23 00:00:00','2013-11-23 00:00:00',NULL);
/*!40000 ALTER TABLE `election` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `election_extended_permission`
--

DROP TABLE IF EXISTS `election_extended_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `election_extended_permission` (
  `ELECTION_ID` int(11) NOT NULL,
  `USER_ID` int(11) NOT NULL,
  `IS_ALLOWED` int(11) NOT NULL,
  PRIMARY KEY (`ELECTION_ID`,`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `election_extended_permission`
--

LOCK TABLES `election_extended_permission` WRITE;
/*!40000 ALTER TABLE `election_extended_permission` DISABLE KEYS */;
INSERT INTO `election_extended_permission` VALUES (2,2,1),(3,2,1),(9,1,1),(10,1,1),(10,2,0),(11,2,0);
/*!40000 ALTER TABLE `election_extended_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `election_title`
--

DROP TABLE IF EXISTS `election_title`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `election_title` (
  `ELECTION_ID` int(11) NOT NULL,
  `TITLE_ID` int(11) NOT NULL,
  PRIMARY KEY (`ELECTION_ID`,`TITLE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `election_title`
--

LOCK TABLES `election_title` WRITE;
/*!40000 ALTER TABLE `election_title` DISABLE KEYS */;
INSERT INTO `election_title` VALUES (1,1),(6,1),(6,4),(6,5),(7,1),(7,3),(7,5),(8,1),(8,2),(8,3),(9,1),(9,4),(10,1),(10,3),(10,5),(11,1),(11,2);
/*!40000 ALTER TABLE `election_title` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supervisor`
--

DROP TABLE IF EXISTS `supervisor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supervisor` (
  `ID` int(11) NOT NULL,
  `USER_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `USER_ID` (`USER_ID`),
  CONSTRAINT `SUPERVISOR_ibfk_1` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supervisor`
--

LOCK TABLES `supervisor` WRITE;
/*!40000 ALTER TABLE `supervisor` DISABLE KEYS */;
/*!40000 ALTER TABLE `supervisor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `title`
--

DROP TABLE IF EXISTS `title`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `title` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TITLE_NAME` varchar(45) NOT NULL,
  `PERMISSION` int(2) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `title`
--

LOCK TABLES `title` WRITE;
/*!40000 ALTER TABLE `title` DISABLE KEYS */;
INSERT INTO `title` VALUES (1,'MANAGER',7),(2,'DIRECTOR',7),(3,'ENGINEER',3),(4,'SENIOR ENGINEER',3),(5,'INTERN',1),(6,'COUNTER',0);
/*!40000 ALTER TABLE `title` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `FIRST_NAME` varchar(50) NOT NULL,
  `LAST_NAME` varchar(50) NOT NULL,
  `TITLE_ID` int(11) NOT NULL,
  `DOB` varchar(64) NOT NULL,
  `PASSWORD` varchar(64) NOT NULL,
  `USERNAME` varchar(50) NOT NULL,
  `PUBLIC_KEY` varchar(512) DEFAULT NULL,
  `SALT` varchar(8) NOT NULL,
  `IS_OTP` int(2) DEFAULT '0',
  `LOGIN_ATTEMPTS` int(8) DEFAULT '0',
  `EMAIL` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Yunfan','Zhang',1,'8bdd64eba00d4e0f8e07ddb1e6a8f16908a254b8c56eac7c450be384cc8b587c','9c60ed79f8f7f126f2ec73de556264c91ddf45fb7b687ecf7a7ac27a56d9d38b','zhang','MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA3nA05mwCDlF6RhSHiD7Z6XeKoxzWEP9A\nEKE4pnS/tzvi1+nGJt1Lh+ZjFgasj/woscFFvAII2is0EnMGDxgnyakHIM53pGZsBFUsaNCJjXja\nxThNFFZy0PlIomCGI7c2tsJeGAW7Hy0mgpWRoj0kd+GG6WKu53uD4X77XK60UhQ8TzquREBvxkHe\nR0EKUFY/0bn93t9mGZT6vEZS047CbOa4jmJLnnfWNTCm1yAwWL7iG9x3RM1hccqlO61LBqyMeDvM\nXI7lZjbk7KTTRjoySi5bwJVEZ7cNnpD++qa4gXkwM1vGrCRpLBj0kF4dtl7qvpyIDNzJRhdvmIAO\n86D9BQIDAQAB\n','1234',0,0,'test@email.com'),(2,'Jirapat','Temvuttirojn',1,'3c45977094377a684bbcba3794cba6ba048b97f281da54aec27cca6863b50f96','52302fd4e83f8700c7c171a86fab7771a94608ea39d0717f41ea0209e4b6e926','poom','MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAheqjxcgAmpSKmM5OaKG6alx0b83lTZ5U\r\nNwekAGoS6pk+Mlg5XKmYjL2eFuYNBXHELzqi+wYszEmR0rZiXso921U6IoUNV+jQPQnPlRDYLqjw\r\ngsP+0+H5sZ6EbLO9iF9t7KS90I+z6QTiSnmISShk50s9ueJBO3mPHHgvFon9wcLtT94pHtwKDK+1\r\ngHnk5E77LjmTep6eGcRFM5iKqX9v/jErcR53WJkcuWGKgIidAXZJrzW9paI6XbScQp8Cs7r3SKc7\r\njTThojFwmQmHVRw3HQu3NpQpNbObhlBM14OCkHNot65I9ojpMKl4Mwhg3e1c6qi3ZSgRclgWnetC\r\ndR4w7wIDAQAB\r\n','1234',0,0,'jipoom@gmail.com'),(3,'Alemberhan','Getahun',1,'c1c2d2ac12d24ea46eedfafea0eb10b6012a2e56f0f2b06d121a46e92760c28a','ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f','alem','MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAqLIP2xwEQ6xqDvjvVz/AEf8srmRZi4yS\nRzIXmpWcvQLWrkKROo0vPVzOg8C3VHhuvGE0XbwSHHj5p00GnCbKJAkYXkQGeXWFQeKuoY3MGh1m\nh1Y3ftXQXhmz2ei6qXbbYzk+lsH0coMGMSJ5Xs+GZkg+MZgZsrcyVqcJH0turSemG1CqdOSdHPmi\n/WK2vfabWR2gC+PkTVdN9wiesDTddI/Dcsh+fdFrHeTkCU+rfbLnVzTdhMaWWV+MjakOqTG3+9X8\n8Qgpq4qe9TL6pkwyypgeqdDlSKIOOMwRiO45pqK0ShyIQ61B7cuEoycT8WhBIf+LjZ0f1+W7r5aT\nLUYJJQIDAQAB\n','1234',0,0,'test@email.com'),(4,'John','Ordway',3,'70187d8ab21c68db97962f728b750c0a5264bf36ef607a29a0cd0d0f672c8800','ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f','ordway','MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlSZlrXecizcH63WS5bFNmB7a0m7Q9pC3\nJukMiNmQZJnca7JdNWdLI75k60PzCCfsRFpS5lkqNz3igItEZyN7yJsG9cJJu46ZPc9nzM4ireJv\n/AZruxGiuZBRrEJlCdl90oETmbVMWl9sIS5z/mNiSgVv9a94Yp7WbCKaQfvMqXRXaCVzJxVgXbuS\nxfGeqoew/BaGijuSXr8N2TdB1ICPsqMBWjHNFg/1TGggFOEqZQ8H4zvnZpLcU/tIUJk7OhWITQdJ\nU23NyZxu1Plx1yiQEr4JSYsy/e+A42oil7NymBmTnDpyGE1THx99hG+2BO5+tNLrd1rDI7XFIvkL\nWPL5vQIDAQAB\n','1234',0,0,'test@email.com'),(5,'Tom','Leykis',4,'0ea9ac2b8855b5b4babc0ebfa91e2c3f84450e5ad8c0e2a97e184608561ef876','ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f','tom','MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlADbXFCclOb0HdpFDK8aSnTKSL00ZN99\r\ndxrfXxqzu7KvG9KCDn1RWgIt1UEePLQJ7aamFsgfpcnNGCkqDaRQe03G8YpQY3irJNXQLTn1ro5x\r\n7ONI/0S+UzMtUAcdya9q1tpDIZ7/K06cg6zP/ZeXAo72GFL4XwQHPvCoXXWYdKtGcR0swDJZFOWK\r\nsH047vpUOe6yxi54eDpXtK9ViugBQPSQeAVXhenGNZS4BJdKNkuMUbtudxyin/vK53RGh+o7EdeE\r\nEG8bqW0UW/JcUIC04ecW5Rm9sc1p38RkdnGr6eo92LhdvjhmTLB84ZvMI/vIzK8Uj8aSbjnT6q7T\r\nVLGBAQIDAQAB\r\n','1234',0,0,'test@email.com'),(6,'Linus','Pauling',5,'9a0867d8fcfaf0be8bfc1e2247cef4baaaa0722a7cc24d2ae7d400b72131b260','ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f','pauling','MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAjcY8KWYkFjQJV4BBggvhzTMOwJmsQTXJ\nThpWWXqciPmdIy96UOG3WohK5j0uXVYgnAGzl8Lc6Kgmz4cPxQlveTYz4QB+7ozD9L3QcH3v62mS\nkP2K+oKk5Mxb4P6ECst0zKopjVHz/ku2I0C2JS9lPC4VYQJnR5nMWs2KBvnpxJCGK9+N4Opbp1nG\njAZcQSyfQeS7Tlr+OLAdc70YP9TWGQfObOKmY8k1lRWIEJjrb26fabJqdf5uMufDoP9qRnSdWrCg\nDyFd7uvzrUa+/g2dcrICVvRi9VKhqtcmp1FZl0skZJCfYB3INEAU9Ui6N0gm79Ajj5+zGM54os6I\nvbWYOwIDAQAB\n','1234',0,0,'test@email.com'),(7,'Henry','Ford',4,'9e0b38b2d6b1067084868fac3f1749c18735ce918a277cc4a907d9347ebd9c6f','ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f','ford','MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtIBEd9pEDv18SGLWQOWl3/nblHa/AC6a\nkmHsxpa4tNoi74SChFWqlzQaWdRbV06ex6ZxWCbn4PpWHtw2oYwDamurHd8L4h0eMQa9a3RXwG1z\ncPmoGCgJiqTyLfOhmnVn/t21eqmSHHLFX5Fl+mFsd2EEZlxINzQVIxRfRIZfgsbjKy1BUfy7wNnc\n2A9coCRoPnnvC0266btyHOCdG6KRBGmGXbZOrz6Aehgy9mqrFayePQAwbRvE5LWkj7b9C7CjXpjD\nN8yQXSk+xl9ZayUDmqd7BSwCnZhVnAdqjKK26Sx99IGnQiQPyqigbtvarqdBYw27BAO1SMbiW9An\n71M/mQIDAQAB\n','1234',0,0,'test@email.com'),(8,'Test','Test',6,'9e0b38b2d6b1067084868fac3f1749c18735ce918a277cc4a907d9347ebd9c6f','ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f','Test','MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAjokvzFLr34HWiekTwwKyaKhbrTQ5Ikt2\r\nNyZqLLFJwa4sEVYmfYlHAuqNDpPXvLh/RetWOwCX85lrCUPz7wdPNdTaBdt2yDNWevawoF3eZzbz\r\n4akZTvzQiR+3yWFZwGUxEYjgyCLPvKSCQAax3SbkyeG/kg4cZnNhwnwzasckMT59Fnk5yNgZUgdi\r\n7uK9CGUXIJ+CBGSGT0r/X2TAVOKu4mEiE3dhL7lGyMEunnAuWA/FZLctJmkIUG/xGDoAWFeAaB7i\r\nYLct+DfG1XUK9YZl2PjYaMddvR6cT34HKkkIEnH9I5QeJofwPRIDKVRhc3RfEv+o97xcFRR3P6xJ\r\nL6yCBwIDAQAB\r\n','1234',0,0,'test@hotmail.com'),(9,'pom','pom',4,'88f725d0b6822d7896ec5a9fd41bda5edadee21df27be4a8e6b692f07c8445ac','03b683271e0cece5d86329cdf41955d979d2dd1fecbebcad8ba394bcb835c6c1','pom','MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnnLxX9EpX6LIARBsEuyrNrEo78gwWrfM\r\nFJT3eoiS+tbz4Je+Y7fBbwbSCNBGu+xGbASApKimMlYaf8DvB83j3PklhwG/tkeyoedZ+v6Xuku2\r\nKUSSuoGI5j6CjH4CMPXDLKf70ndWEotou6pEuV79j4Oltsp3jGeOb63j1ZLJfn2lYVs2wfzLxiz9\r\nqVBEcO3WnFkomw4YLQyBcNHpH/29hrxW+skomhXjOzZDQR37xlc/EPRiEjjBIqiJeY8TqCfgPPkK\r\njUj0YUk43TAT6eHRFnm70Ihle4AM53nYFMukznogy2bwMqTPw7imcgruhfiPTiCJzYzsCcbZPmdM\r\nvPytVQIDAQAB\r\n','1234',1,0,'jipoom@gmail.com'),(10,'poomtest','poomtest',2,'3c45977094377a684bbcba3794cba6ba048b97f281da54aec27cca6863b50f96','f9ffbbddeb6000b63ba259e21ccfb10d6da914167672c44690cdc1a58461baf2','poomtest','MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAzfyDMNa+UhXOLOoDGqBAGGbDq9Id7P9X\r\nWJNFIEn3ZgJmAJs95vj7VAu/J1wyu1UYcYPaHpdDPY1ZFlM8I0DPp7ofoF11/Ye78HvZXOKQ+++p\r\nEF1MtZ5Je6ixFZpYdk63ejor9RWtwq4n+lWom94NOOhAkVRJ7DpP0dvcUKQ6GaaN04rmT+1osR5P\r\nS2ikRUAboVHYIyWM89VGV8r2/0Se6J90064wYVfT88Akd/LuPD0Oh7Rnsyl3867mz6k/Ubm/9PhR\r\nOR79eQiOeJy5xXcRvMg/3V2W8JmDLtPs9q6r15RaZYhCl+V8KHKUO9WQtFYQErz5as2ygRe7n9yn\r\nmEWQAwIDAQAB\r\n','1234',1,0,'jipoom@gmail.com'),(11,'Charle','Lee',3,'bd8b8a2e40976ec987e6eeb2a835f7137606b8809ed577d9e184e296a98c1c30','418b4c25f4f55506bddef4a065527051655dbf5bb9e1ea029c9bf80c8dd251fc','lee','MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAknrCXGA5uaOyPcE02pTT4lKDIGSux8JK\r\nLYWAx5k+emKltLzN+r2Q2yG60el1xPx4ww4rmn4MmR5bmrtROIM1ELQO+MIv3TDSbbB21Bx1suDw\r\nTWwcI59ZAyqNyLhTn6LqpAW80jvmdn/Zo1F2O4SE0x9OPVv99IXBAz6r5pQSEtguC9G3WetBen+6\r\n4wWAcLnuiwYyIvXN6m/azeKC08AZZcmmo2M5LGeELME0E1EaajjDqNb05yLWiyDhFsY21gaIQZEU\r\nnS8DvX0hXmPNa1XwAwC2qYlsYHnN/lQxORCj/q9PBjR8CI5Set/7ba0ohbaujklWS+2Z/8n0VFlD\r\nImREEQIDAQAB\r\n','c715b547',1,0,'zhangyunfan@gwmail.gwu.edu');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_election`
--

DROP TABLE IF EXISTS `user_election`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_election` (
  `USER_ID` int(11) NOT NULL,
  `ELECTION_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`USER_ID`,`ELECTION_ID`),
  KEY `ELECTION_ID` (`ELECTION_ID`),
  CONSTRAINT `USER_ELECTION_ibfk_1` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`ID`),
  CONSTRAINT `USER_ELECTION_ibfk_2` FOREIGN KEY (`ELECTION_ID`) REFERENCES `election` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_election`
--

LOCK TABLES `user_election` WRITE;
/*!40000 ALTER TABLE `user_election` DISABLE KEYS */;
INSERT INTO `user_election` VALUES (2,1),(2,2),(2,3),(2,6);
/*!40000 ALTER TABLE `user_election` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `voter`
--

DROP TABLE IF EXISTS `voter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `voter` (
  `ID` int(11) NOT NULL,
  `USER_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `USER_ID` (`USER_ID`),
  CONSTRAINT `VOTER_ibfk_1` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `voter`
--

LOCK TABLES `voter` WRITE;
/*!40000 ALTER TABLE `voter` DISABLE KEYS */;
/*!40000 ALTER TABLE `voter` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-11-23 23:52:17
