-- MySQL dump 10.13  Distrib 5.6.10, for Win64 (x86_64)
--
-- Host: localhost    Database: evoting
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `evoting`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `evoting` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `evoting`;

--
-- Table structure for table `ballot`
--

DROP TABLE IF EXISTS `ballot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ballot` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `BALLOT` varchar(512) DEFAULT NULL,
  `BC_BALLOT` varchar(512) NOT NULL,
  `BC_BALLOT_KEY` varchar(512) DEFAULT NULL,
  `BC_BALLOT_IV` varchar(512) DEFAULT NULL,
  `ELECTION_ID` varchar(20) DEFAULT NULL,
  `COUNTER_ID` varchar(20) NOT NULL,
  `SUP_SIG` varchar(512) NOT NULL,
  `ENTRY` int(11) DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=228 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ballot`
--

LOCK TABLES `ballot` WRITE;
/*!40000 ALTER TABLE `ballot` DISABLE KEYS */;
INSERT INTO `ballot` VALUES (153,NULL,'z59wUgDxfdq5v/sGOTjrkA==\r\n',NULL,NULL,'1','Counter','IkahFOvCL3UTQ9zHDFFYRLbEzcJLr0nBgHrccFRP70yeIpg3TBa693qxl2uLi9emxD2L6Rq8eGep\r\nVNRvB5RmLN33JYaQp6zQI6iLyqkl40htCWUFfpd9YisaXU2EULjMkGYkcCrB8aYYaI51IF1xUwG/\r\nTfclKoiEvINCKZLHT6noJh84oUK5OgJhywxvTn+kMhif9cwPmOv6AIblcc5nsEU7dU/llsWYphXF\r\nG4rPK5xLuIgtl732BTSZWIdkveSGXk6KZXw6JR105V1jDMI4Kd3pivJeeXI4oBNAsJNDQas0F4+Z\r\nd4+SalYH0zXiYNSKKzrCchxv63Vayay9+2BVjg==\r\n',50),(154,NULL,'D2oP4Bo6kmuftq+TWHbDQQ==\r\n',NULL,NULL,'1','Counter','WxVBv2h3piFgHQnKgMCszTKznh+Ldwr4EN3GEiQtK20/n1DqYR8sVfmmMUbQqmZ27Al7eE6YMTY/\r\n7KOUPLNs+XdDoU16lYY3AAW/R61pSFhDwsqV8jm4zmiuXuc3LxxnZor9k08PdxmwQO+Fo5jcV6ZC\r\ntyV0mMy/yLuh++Z0E4fcNRT/SJKYp9UV/sxTqZUGhK3RKqdDtGZdaiLsfG8xBfXhtNh7w4N36NnR\r\nDu3ViyP98wg2LeOXc46G4lFLA2zjNJ0e+VnOZPnw8DxWfn0AqLxWs19M7NZhkhB717GnfeISs3mB\r\nRU6esd21L2FKSJmDh7tXYjnAoQiYDd3/4MpPtw==\r\n',51),(155,NULL,'X6KiHevL5XKvhbT4of3N1Q==\r\n',NULL,NULL,'2','Counter','AdhPqgRE62ory3oBqglvmpTwQINgUB/mFfiM8icYJq0NKtc8iR2k2F622eDwOpsubZxVixz35Dum\r\nLiAQkZCQFu42jWKqjgSruSM9S4/OyLD0OeWIH8wwhYeIu8Xcod+eBHX2Ubs+CBoR9tLW6FVcLiKh\r\nYrZry2WHCi8qg4LGFcm0DcI/Udv3L/2Ebp6pf6CUsXA9dv3jYtD1lYV0jzmmo1Du7WvGCCcC+VN+\r\nEglrieNCcY/fWode3IxWo/gC67breR+RN203XJg3U1eVNQxp7EzvaGCnaZih4/2TjtG7KYfMKjEj\r\nzk7Zdb7AdpD6MasTcYelah4wUl6x5j4VjnERbA==\r\n',52),(156,NULL,'pQpXhSCjLApWmAhnYN43Cw==\r\n',NULL,NULL,'3','Counter','QtlM7i5NwIGXlLoYaU76k5W88DpT1kP0nt8xzfYvymausCL7npvWOFaiKta15cXonyo5hpuA+1pL\r\n77sb6X+e9T7IpD53ns6bSR4smZ9EmiVPxoVwMeXOxcm3cZJ5pLsorHzLr1KOAplbaVXgekUu2U0T\r\nZMgjJdaQizY1jsRrH/93wEHcwrjGY6fmv/cptPHzolg5dFcu9PTuQ2K/HWXN8BIEupavIiKiINzn\r\naBl8A95X9k3jKvrIGlz149i5VK5qFKDrSyAmJWry7QX1r0rUqSe8igHGVGdQ5XZLCkw4RkxxpeoF\r\ng2hEufuJsbz0tsf0j9PhW/ivxQ+Gv6B5zZ/ryw==\r\n',53),(157,NULL,'X6KiHevL5XKvhbT4of3N1Q==\r\n',NULL,NULL,'2','Counter','AdhPqgRE62ory3oBqglvmpTwQINgUB/mFfiM8icYJq0NKtc8iR2k2F622eDwOpsubZxVixz35Dum\r\nLiAQkZCQFu42jWKqjgSruSM9S4/OyLD0OeWIH8wwhYeIu8Xcod+eBHX2Ubs+CBoR9tLW6FVcLiKh\r\nYrZry2WHCi8qg4LGFcm0DcI/Udv3L/2Ebp6pf6CUsXA9dv3jYtD1lYV0jzmmo1Du7WvGCCcC+VN+\r\nEglrieNCcY/fWode3IxWo/gC67breR+RN203XJg3U1eVNQxp7EzvaGCnaZih4/2TjtG7KYfMKjEj\r\nzk7Zdb7AdpD6MasTcYelah4wUl6x5j4VjnERbA==\r\n',52),(158,NULL,'X6KiHevL5XKvhbT4of3N1Q==\r\n',NULL,NULL,'2','Counter','AdhPqgRE62ory3oBqglvmpTwQINgUB/mFfiM8icYJq0NKtc8iR2k2F622eDwOpsubZxVixz35Dum\r\nLiAQkZCQFu42jWKqjgSruSM9S4/OyLD0OeWIH8wwhYeIu8Xcod+eBHX2Ubs+CBoR9tLW6FVcLiKh\r\nYrZry2WHCi8qg4LGFcm0DcI/Udv3L/2Ebp6pf6CUsXA9dv3jYtD1lYV0jzmmo1Du7WvGCCcC+VN+\r\nEglrieNCcY/fWode3IxWo/gC67breR+RN203XJg3U1eVNQxp7EzvaGCnaZih4/2TjtG7KYfMKjEj\r\nzk7Zdb7AdpD6MasTcYelah4wUl6x5j4VjnERbA==\r\n',52),(159,NULL,'qWGwTQVplaywWklyUkiy+w==\r\n',NULL,NULL,'6','Counter','deJb5fO1iL2ynjBPRWEbiDqHcJ27lNP0N6idFuynrqXrOkHDLrAzg8bydnmyyYjorW43DgJzrWFE\r\nF2+w+BbpZPpKo/xfmtDR+yhTei8ylcHURE+5vfor3c36GjeHU2SJ2S/YI+NfmZanAga3xqA3XcDi\r\n1xsIOQxTRfXDY/m8ez0hQdXGnkWUZFxvlYKPt/hY5XC86SqzEXx0W2/BsLAsCUE9yHqMsmfTrCql\r\n+oZEyP1rRmbNH3FXYfi5rFt6wDHYWUir5+OZTP8SrH70NM+B3a3OYKjajDMs15iGJRilUfdr9POE\r\nFW8/LGLMkSUCiuAXEvO7QwGPIWv3Si9myYzdkw==\r\n',54),(160,NULL,'1z7apKycnTJcEv7nY9jTXA==\r\n',NULL,NULL,'1','Counter','X2sRTx884dBsLye3MH/Eixrc+j+CCoVEIVFDOgRHi33QIpMQtTuiTwGMkmqHjcvB7COTLFEEV8+b\r\npJUvTO9nMJ/H4rLFFKsf+A8PU4N17blVodpW4cPBIcyq3WBGfVrNazFU3ij9pKG8MhJwDo6mxvQ6\r\nQ8gQ7GDnQSN6bHYtGkFZvTra+/u6t7/ghU404UpY19WF+i4N7N9heAF78y5aXxm5b+QWduqgLWVb\r\nLLNnPBDdU0BhozD6JkoynQpUt6CJK8mqJ8umnBPU5QxlWwSHHrOsYL0ydH+X+kzis1kO3PaMLIdh\r\nrJwrVBSU74uAK2p8VrfE6PeMieFHWdu0nGPKXw==\r\n',55),(161,NULL,'IsrLPY8cKRY0hvDkhScZ0Q==\r\n',NULL,NULL,'1','Counter','YjTClEnbNYnz4Uud/23KPx7fB8yQfRTlp/rt18ekt1q4GD5IAcpQAazwO2X0ZZsxIdwq+PJWyZfe\r\nsTcN8f2mOIT8DGfXAPFD7+Qe/7oISaKBlfSLTYBm8vOmPUWlQcFOm3C0QSZBeAW7twjFDJjP6AZV\r\nsUCaapNpY7Y3+b3nGtx8l2ATMyS9wtZgvYelYOkh87yMZo76lCT1iLqcMWTyrzb2PjUEwlbwJwYh\r\nqQ30PzCkB4+/InBju2GPiPEavnPuyRg0xJZ+XdhsfHO5/J2VJdflNDZfDA8zgSPjrFuH4BY2fREA\r\nBn/3av52SsVp93VZwfB3S4xiP7jZjnkEeR94AA==\r\n',58),(162,NULL,'+HXO8M+KRoNmKyQy937SRA==\r\n',NULL,NULL,'1','Counter','TE3nw32Io+k7uLo7helN8jRr6e/yd7DfJ+ixIBiATUbe1Y9/NstyUAFNDcteRGqnjSUQBkGhy2L/\r\n26EtW2BVzyweP1gZosHFg0yK6nMLvIujTzv/ypo4W63NpX5OAMVkPbmcRACi0emngyWYwBQ6r2Vv\r\n9EMdb8JoE4xisnvQNcvrfd9grV/hkQ/e4Ie7ca5d+E/WeeAlOXM59mu0dmAD7xzKGT1Mihr0QqR5\r\nfQFEbESugUPk/rlt8enmIVqPrPVpH+3fd2KBiL3R6m7W7jGb1puaaHfAzuQ6VvzGBR5kleDQxOCZ\r\nzSIe+/L0h/FQTMV/UyoLBXOwJ15zZedj+dudIQ==\r\n',60),(163,NULL,'/SbYSYXWcOh0CDC+BL5Efg==\r\n',NULL,NULL,'6','Counter','T7s0SxEajTNCpSMNluDM0S07Od35DUY5b8E/06DOkzZudWohxmtQ3U2GwPCH1VSiJ50DVdXOgija\r\nyrirjLWXzLkANirapcqAJQwFQR2QgqAjuCg8AzS5RNOXAivsnAnxmyV31APCTWuXn2ixI8x6VUZN\r\nxiE6M9T4i7uO9y62S/xLcDgz1QQ4QVNuuYMFQqCRpnivfR5+IbeBqt9zjNrV//vT9Zaa+PtIPnKh\r\noVNF9LOGIVlimgyWsDamtYrDWSamdE1dev8FKshtL9JX56zKLIr45tz8vWze39BHB32l1RcyXhha\r\njzFnbdEjts3l/UsHDdJYruh69/0Kei3SO3xpfA==\r\n',61),(164,NULL,'lJvLo2PvpvPx8ZulOvCIGg==\r\n',NULL,NULL,'7','Counter','ITsdWWPdFkG2+yfIJs635mLWB40oEQzl0plgJCOwnOR0SF2vlCS7xHqvQxipoHHmXaTK4R32iNBE\r\nJ4sijCB2VfNEoQTCXDkcirM+wSVVFANIp6ULVUqBpw/5bpwit3I6OHzo9H9sK1svnVLrCf0zEKfY\r\nUF4hGCIazAYIe9BU5HMYmi8t7ioZN8yOR5SdfgIX+kmEE6G7tEiKirYkT83d7K1+Srud66RCWsgN\r\nC265kIG6asVJ6jenyz3PtqR/oBlHpVzrm7tLNucjcmRkBPs42Lfn0n5q8X2zwvd8YAEsWmC9PpJm\r\n40KHTWCc7Z2/b9x/7p8UCDiXWTs9cn88Dpz0mQ==\r\n',65),(165,NULL,'lF5/XwND/xOB5vIy19oPKw==\r\n',NULL,NULL,'1','Counter','Jph1ewxJPAN4suVVoK9irKODAYGS9figl22bOBKTcKnYFiYWHT9gGgipz/1hWYVkLQSOzSakPHjr\r\nHodj3H5Ro/LrdQn5pdbkAAU1xgpQcvRAXclibcen47lQpfvIK6hFBnDEAOzaKxOMeKdDcbYz7crV\r\nz21OKpy4fuQVCEMQmRanMwFYepiibRlW1IIv4oujJPgkVAu+sbn9pQrl1UmtPZJoUAbFat7Cc8T0\r\nZfUQhyCp2ylmEC6CzokS6pvEUDqL//E30rf8q9i1FkZSTGp9xc3P7q65JoJ+fHRXM+oFgpQOu7Tw\r\nCBK8iBtKzIcQyNGYvEur7CUQkg4wII6iKtkXOg==\r\n',64),(166,NULL,'2ZrjJpT/LwTwuSvn/tWzfA==\r\n',NULL,NULL,'1','Counter','GP24MFdixI6n+pLtvJobRs2utxbiX/fJ0/UiG4Iei9aKNuQgMQjFIBoCkepDRz6USb0tApYIBaDV\r\nGCgWNRsSYceyZTGoJfi8wYvobvvRSawNvPQxKC5kSWsOFtXfaEJpI4/KFYdj8vkdSsqZZP4ccTBJ\r\n+3R8AZcYhpfF4jLF2AtbMLsWKkN1bKDvoF9Jj3tRAR/iuRSeEB7nDinzEDDvXuqX5shFKnN0PZyc\r\n64ziIrS8icAnE8jtr50Pmdhbb5nZoC5u+aTQsMsfrIVAEAAJ4YHkgUhkF4rKALO1gW9PqulDTXtK\r\np2gKCi3H6ZKz617kFvcTY3T8/BDTxdgNADJ0Gw==\r\n',66),(167,NULL,'sk2I0ff5NyMOepyqQFxS2w==\r\n',NULL,NULL,'2','Counter','OykKgs/vQne00TVwd0eFTX7RtK9Re4a+Ce8OU4rD76WVWvlFhF0Uo/lwOKSCK0FN2Xcqhg9pXkWO\r\n7So9Vj/GFhscAAAMO53wHBrIVmDjuJwE+/t87X4VAFYwW4Xbr6aEpXngA+nTyt6hQD4OBeiktfgz\r\nyDYO7RWQpY9JTTjWzUmHTbKcg7H2ipui/FZ4R2dKQovd8ZJVZb0G+e3KPr2dBur76jqTZsDVksr7\r\nGWakei/lKwvnnISnsF2CFQ9rrEVBYLdv8q4ioNAUgZZA+LgRXZpwt/5I1YzsS+3I8fwzowy0Zin8\r\nf8LK0x1Db6HfdgqMIsQBi1LcS0aNdbCd4S77CQ==\r\n',56),(168,NULL,'H5+DvyFDERzjf6DCu+liGw==\r\n',NULL,NULL,'2','Counter','BQeuHw6e9s6F0DkaKf4IZf6uxnesMwlaEJzbqMDqKr0HUjpGPURqqEWadR2BiyFH8Bz/nojDfvm7\r\n0fWBnOAtfLJotzVUrzoo8U1pGFemXR3WBSFZ/Grov2+e4daZeF3/KjbHkZidLJnZLJRgBeYzCkkP\r\n7xpc+ofY5m43b/MHcbfatYScKtzULb9XuHalAaT5iw/DV/WBgN7RzUURuTUS2w5+yEwntodAl0dl\r\ng0A/3cZZZk5uXn+Xp1rdYyfQw/buyw3Ngu1fjYiuNtqAPEvVUBbUha4Uw1h3lgS38zGuwebyROAd\r\n6vQaRY46Hq9OpvFJHKOKxMAPtWYbbIFudi4SmA==\r\n',59),(169,NULL,'OD7jQEMhG4JR1/aVVsLnqQ==\r\n',NULL,NULL,'2','Counter','DJNavDcY0I584wkvBgoSO7jcKpGiiuwZyH69LedTPyXWYJeE39suVqnvY3FnQAjEEsA7S0VUprcT\r\nL+Xmc8iJVuy7RJBXWmZsCNcO5lpxCuyvSLymO1DCCyIopvd2W/Fk/ewl9OeyPDVq8w0DFBhxMJcx\r\nFrjiKBMu4q4Lq7S5F532lSLvNDPDM1Pqohy0JQLGxoCgwjz5ZkqNY0t8rW/c2uuzfbT/PqcYxfY4\r\na5yqPRxbi9wLydStWLcDPRCvAxMBpVOk+KPp2RJf7sdmKwjK2kHj7ZrK1ImUfGA0jzM1nVM3S9Dg\r\nbjwaP7hLh18MtzaXEnQ9vdKfYVs6Qn9a/drDhg==\r\n',62),(170,NULL,'Wp5S45cQhdgv0O4GiayCxw==\r\n',NULL,NULL,'2','Counter','NvXygYEXYyY6gLZzEgVcldIBiyKbfD7V74+0VhkIqJQh/FkqSv6wpSW6xdL9BIOeJaKNSCMpCj9E\r\nhM5Nb9fCLu7mqGg9oydC3OfLGkswRqdi/NjYhI08UzjYDg958T5gdionPMSnhZE5kUjrWd+Tpg2o\r\nheXUx0bde/rEkq3Gm7CCYHnxL0k5G54EqbgKlEJJL9ekzGrEm1fQRnmu9Ux1md/TYqq70HT1i6Mh\r\nYUT8hLd14kiIgixoSIBH2GbNzVXND7AuBR19EpFJtKwxGMi4daSEAdxjcyPvefRtBbGFj/sFy9z1\r\nevp2XNn2ZTmS9nnR1DfMdpn66U+y7BG9J1CXIA==\r\n',63),(171,'Miw1\r\n','m93lkY9RfZV5cb5J5yYQtg==\r\n','lVdO+UXb9EVKoBYAnuLlIA==\r\n','4Vnov2s7sHD+kGSvokeudg==\r\n','2','Counter','b1/jSa5VSJxDQYwW1N6dnzoSF9qLFT/yZJkObSv69mp9Cxr1h7xxpzQF3VbaqIHxrzbwKcVtYZJX\r\nVr25PoFpWQD/bmwonJD//QSKYNrgTvUGIKM4Udf+ZrJgkJH/SDd0AWfpckI/4h5uB9VcoHpDabQI\r\nxL+N6qKdTfuFgDTpSNUbf7bTWycE4lULFXWiSDYSbvXbPmr+jH+UPeA++tgpUt6wOALW2N+MSUyV\r\nEKlmmkUXWqShrgJ9RF2P+v8akwHxIE6sph/OfbvRqkAbsBW5TH2rukyyrNTCaythrQ9tUxvAw07w\r\nqNYbxuq7H9hIeghKA+32Qpfey9p+6rjg7rKEcw==\r\n',67),(172,NULL,'Ig+4n2AQ6B/fDK5dcNYY4w==\r\n',NULL,NULL,'1','Counter','RqtodlekcnTuayKTX+kgGXQzN3mq35vMxIp5buTbsZ/SK2dG7k/bNgGC5nZpYylOTTseIGdqtpGF\r\nrHBYzrUWYFny6X/a+dp6jn4begNJX2ll/jhD7JOxy7+IoUPhG/VJT1G+HXkKYfbh742CALFPABZ/\r\n34Bj1pA6cbT1HHYcoqJ3A9OLevOl9uuu5TP1R53M+rrbMMGdckhLJP4cP1AePIjkmqsZk0uzWgq5\r\nmKc3q/ibs3aIdF2iNcMg5jjzI/CDhdzeA7RF4TEDpgGsVH7TDJJw4jhywvNe7plOdmR1SuQ9D3iY\r\nJY9iel7J1drUE0xloN+pWSgmp2QVmdAb7+TXng==\r\n',4),(173,NULL,'YwVCrsHgOZ7xx9J8Xj54gA==\r\n',NULL,NULL,'1','Counter','fDwK/NSE+3emaHUTrDAqNe0l+FWj+RVmYS5PD824GhStYuBIwW4WZxZzFO5n/bfjjj9FvP0tzchx\r\npX5I1bRZWO0Ddz6TnNlV2IG0GRB9RIJikChXPnp0W5FywynbRvzFoyWHjpcXKQ7UnM1xGCRjus95\r\nVotHuUaetjnXIjKEof00u8L5CzrK5oEGneEBMoL1Q9y2BBeFwA7iumNPziG/d3ya1mcrf0lkZNUb\r\nBOXKegtDi/a04rArk8gFSMPZTUxOzEpyyLOhH+iPpFpr18JM1msR8VYrfyNZ/zkX56BFQvQsdQ8J\r\nMV8vxeqIVu0Wje9mtuSGIB3EdPqdtOB9Vg6jJA==\r\n',5),(174,NULL,'RLSVH6DB0fz9itxCIwl90w==\r\n',NULL,NULL,'1','Counter','DAH7X0qS5kkH/qQPAjAdGqt9yd2TBZhA2/FVfeOiIYjT4Ajmw529qu8ZqWNdpfzubzibxGgKFgyH\r\nHv6/PNvVNX2xQAVUV1L8uGonjZABiRaskO4bwqgpnxLIcxqivbeCAokk+0PFI8d/4IOcf8zUUdOs\r\nwQnDQnS6C8PAOCaHR/0tVRofojZDcttgR2/AfT2ar/oKKPXkACyMKjHUldwJNsUZBKPLIcbVHBQn\r\nRNFUezUsOkhOmZxd/gQ06Nz1F7TFk/Zs7i2b7+nTTdh1DW5HT11B7BwshI1J9NqNXs9X+py+ZApV\r\n9KyMoj4PI7Olm99g/o2QNaYT0pachs3y7gKPWg==\r\n',6),(175,NULL,'eiDjxeHF/Ye6rDwGmZI7Mw==\r\n',NULL,NULL,'1','Counter','Yk//Hq4C5dxisFiF96o/QjDgKEMEIBWBDQBX5hzc/Lq/cvdlCwiut7h8P6thiXdHDAThKPWBVuFO\r\n+Y4dM8TCChahPcCXDTNW0/GsGcHYjxtgF42QGpnjZTF/yR4ZZ3/E+4H1IWbOARNGVlYaxVjmvepP\r\ne0+WpIZTTHwFglMaPsxWtix9+oZ7+IbPLKL2qtBULn8AxGoAm67tkrtYLkJLZYUK9uN5ORm4OFXF\r\nICFV1FbeEP+pWsmEMZ6jLNMYDE9Cc63nf2l9Bcn5/FSND2KzgvhzNmVE28kGg427/1pk9i0+IAc4\r\nMMZFkfWrNpo+z+OnvnvlPqrVMMeUyNjafczu1w==\r\n',7),(176,NULL,'f/ozbFj9rDBdzVrTy3dE6w==\r\n',NULL,NULL,'1','Counter','QgRbYs20MK6C1gJZ0BIen/VQllCQ6qXXrgkQBaw9YUpUHyrm8cWYUXeOqUohuwRlVUq0vJG6xq49\r\nxD8Pe26gIRdTCMgU8NtUso/URgjlUFM6E4sw7HG3aaUdnk33/VEVd80YtO3qK3j0UxkImpbuLyuV\r\nqca1hv2khGRUkXMsSTjbXmmW1DfiXVgWnEUx3ONLGo1gJjvgqy5EkNky+Og1nkW7P4sRPps55Ws2\r\nwQZ5d/0YvrRrSwLiRL6gB2lgCjoBi06iZFYxDkBzOy4fepttncWTC5A8kKoDwO4bJeD65FpfAcEB\r\nABG2KmctKlgiz1ctRK5MuHKfOlIqTxZqjidmcQ==\r\n',8),(177,NULL,'aytlTyzkAgdqQuUv13TbPw==\r\n',NULL,NULL,'1','Counter','SbPpJJXzk/9YCojvxvvGfR+tcd5hFvZlZGVJhvZzdgfkcJvrgVAAWPe+4o3lmkE1CBKU+Ax42J4A\r\nGBnMVGOpeZ8Ss+WVytcuqj62KmtlJ99WF2ibZSNrLi9C3Vsb6kAEkOrDNuSnYJatBAXUgf/IOa4w\r\no9CQdFfY2yEFbzc2bnNy008wBLpvuU0FXY21hAovREPHGL0eFmmkIIYHkQPlwCrSk5pocWGdjk1M\r\n5jVNG8H14JH/Wt00B1eIiJe5xWSBPM/FoPzaLU9X/pg/MvKnfwA0ZEHZnLtZcSFRa8dAvwzFCNGQ\r\nwU8ihYQAyI8Ujm/te9znOOekIypgnR0V3GjJCA==\r\n',9),(178,NULL,'ckSTLO8jvSgDEWML+4v6vg==\r\n',NULL,NULL,'1','Counter','PyGfeMwGi4HIx2N2Y5+uZj+frnpj5SCB14cQmg/lPHhqDqRw+u5k8UD8mTTmhI3l4yxTc4+yXz2h\r\ngY+AzrhMbD/3251EZ7lzohQhyAZ8zoicARFXuANJ6Kdoy/Oj1huZYjx2RkbsB93XYxfk/tC5644h\r\nvpYqpJro3VoYKSfA2daaG45G9J5glmkNvZ3xbvDr0Isgq+rypqMpmJhFJesjb7U/sP+RLLuJ/60L\r\nLW1GPSIY2PVmR4vu7N1TgxRlztgOH98+lBoI6CWEs/uiUqYo3S2rjFE8NsjVT8Ylt90mzpeZJAhT\r\ntmSEB6vwgpfnHBO/BV1dX5cY7Vprp4jV5rFycw==\r\n',10),(179,NULL,'g/bq6FIKvZOA6RjzGCmUqg==\r\n',NULL,NULL,'1','Counter','E8kvvzLKWJsLCRCfoshXdSgcMSP4JqVFASB9FHNCXc0hBuYGD/CTUkVJrcLccl9aPuCcV4etZChK\r\nInzwmF8WAq8S7un+BeiVGG8m+TD8gHEniLcYl+XBeDuYAG+z1ovbPXNQsCzdpoPYhGBIUBdXtCrW\r\n4rXtywIUWvxLzu2ofbnoMnPp7w+FtJ/duGnCnUGAj49hGcHDQXo+rOOT0fXuaTIWUoYynKd52I40\r\ng7Mduol59tXDhOSkd4jAOxx+yP9rShg8ac1nGYR080NekZ3nsnyK1WbjWSb614tFRjUGgARYpBNW\r\n0X3/vIYdYwqSwOjKjVZUQJmGi1zFBKiORZ0rrg==\r\n',11),(180,NULL,'YFWtQrUwa8AS2iuFHR/kCw==\r\n',NULL,NULL,'1','Counter','Sq1kWsRRlySEwMsr8PW8Xt1dIJn1aHCzAFlGfdsmc3HCuPFI63FCSCM6Z393T+kpNgG5hTieI/et\r\nNKRt/RbKa9TIYXism3iP84bRg8a5H1N8mCqRDFBTZgJBID9XUorgUz6/MhgEU8q49ReiV3sIaxvp\r\nwZnTRuiWagTr/eW87VTxq4b5iqPsjurX3H3U3fRYgJZ9tZrl5fYbqTq2KfAfg45ngCavAS56zu+/\r\nWhTlOEj6fn4/JuxQCgfUaWqa+vYEQDpHw0/4lT6+PLRrIPr7FuAKcdnmUo6nq0L9Ty3gq1oYcHub\r\nVISmdfCAQ9X4b90jnuwctVbDiOfTZ0cn1dL7Tw==\r\n',12),(181,NULL,'V3KBWGKVaFGnqGg0ggejuA==\r\n',NULL,NULL,'1','Counter','DZUCm4CbyDJ5PlgnHRE/0xJChaEIFmazp9fTDyd3CAmt0/TcBRrXDJX4gkTmo7R488Mf3tvRFvmJ\r\nQYc5tBg0Im4Cfdwx81/D1eLayL9k2u0UW3blrGJX+eZTn7y7B+zH/xUfJqlU714X3GfzfzlOSe+e\r\n3Cnrj0UGxpE4C1/kdCUjq3hmytx4iiaflsuNlh1etBGcTYPSQXq05ZEWpLst+bSBaVZSobI39hsR\r\n3fGbyYA8RgPfnxF10RfHN7R1oyF46deilNPKC4LVpTFuZ0sLG1KSEQ5cSRH5yRsRSgNMFKyyXrXO\r\nP+r16hvNW1aoV9gHe3bLWBtqNew7MWT5EILYIw==\r\n',13),(182,NULL,'MfGQ/KW9PzUMJFbcpbIuaQ==\r\n',NULL,NULL,'1','Counter','ffYvpa3Qxksa+bnZLnGEHrOpTY/4XyDUmZzbnX09Ur4vaMae9ydTRQ94OXCkEQrtjXJ3P9gcPVi2\r\nhNktli+zR9yy8ixz8Rs+Q+HC1saAe3I5nUDWyiIpRocqMkk+hcDfkeSD4/uI3Z+91adRJfwHdEQv\r\n2rg8wGZGcU15rObLtBc66vEWxc/6ZbvKURvn9v6cSIWKjAJhqwv3PITh1AyZdhA/OV30jxeBOQsn\r\nAGJ/vPqEcz3LSMMYEi3MCeleiuI31QakqoEHpwhH7GUAhbz31XQPEzAMFuuip9Q7UWJ+Dus0IYGD\r\n5lDq2rv/LgSYZFNYKGDoONnW1Pekao2daTNhcQ==\r\n',14),(183,NULL,'A9oBsOkGcGlocNn4wZKg5g==\r\n',NULL,NULL,'1','Counter','Jk0Nd3XTosHjGelj4vCygU8Vq08w8ZuMOCeaPzBqKEdzf7PdhUuTDXWQWQhY3anquFshj2MAGBiK\r\n8sbjcbaEPVcZuv0LgtcGymAgKKZ0OzDvFGErtEepby5abD2/jwgkuwlGYJGaG/179daaB3SJhr+4\r\nxNU9BJNbE8rOVrRIOKq1grONnnEZ3Q9u1Srjx5jRw0UNqgIyZynCWJg054bTizCDdt7GLn9AAA+K\r\nObHDaI9iGK9lc4kNaEMfCDgYwzcdJZMCdmyWMGOcycjnpQKwSqWUSNhX1TniM/nH0B0tddvwkFOD\r\n6JJB2RFgLsge9w7ZVj7Ah5ENwpAvLBLDOH6xdQ==\r\n',15),(184,NULL,'qGqqHY3VS/QzUVkWkrgblw==\r\n',NULL,NULL,'1','Counter','c0tQWjZnOWnPs/XFLmnDruknrrhVNjALrilVB/QoBotg6R9FYOImadmLYSLhjEg5egQxwif+SwIS\r\n3a27l5sjJ4oEDSGHPu2gLlqDEOlY7MNj2ix2kL9LKos6I8pihbzcB63kMiuvB+RplI3ag6JJ9gFg\r\nWR1xizyuheJxx6OnuaGvhqpfZOwhydkdrHHl6gRWEgl/93vlBrdLXc0vsMecG2/pgq5yQVPMtCRK\r\noqFtQtCT7i7WqYo6AYD1XLYWxcCmWAnHNcuT/RLsK+vWJCLAyNsqaumldPtZz9SD3LbuV6LYCHbk\r\n47FH2mXYIcEDDAKR2zoWuzdbgN4mG9QsMTIoxA==\r\n',16),(185,NULL,'Et/v+lDlabo76QALvCVMYQ==\r\n',NULL,NULL,'1','Counter','X4iDpPqxHUhsqzXYKdQAvX/VcJMGnsgpAqQTs5YVHCkxporUqqDyQuMl8nz2BEI8lf1Fu0Mx9Nwz\r\nutE1m6jT3xZSINBeb/W6P/PCOA2G/qcgsD3ATc4BvvVTcWTsUkw2V6f2cOFZCF1QQG1jBem1uH8B\r\nzf1RqDL+ByRF2b0fvD5+2EHvwFeZBh9qnYEHaYdRP9yHChgDk8LEvMBGdsBiNmwOlqm3ckPgLF3s\r\nA/9ke/dR3u58DEdwqpNvHS9LFPNWKQaSdx0d+AsoDtiIRgt8ht8Gsdo8CcuNnZSiuYaN1RgHtrFP\r\n/Qu8EMbbAzvs/hs3M9AOZgVuG68B6B2HZcYCqg==\r\n',17),(186,NULL,'lbTz5IvUNCfBDgWbfIdwtg==\r\n',NULL,NULL,'1','Counter','ZhbETkRUorjssubqQM+hDgJumCizr8gW0QgegbuPAldbuhavKupc781DF5ahDvzSPwcC6syt3iDd\r\nEErb7SkvTtfzGRigoVsTNN+hycYGADR5Ozs8E/nkDkbbpWHtbTwJJrOt12xXtaWXsYI7OdsoK+lx\r\narbWax8BJIox+Loy2f2nWqFQScHE9cQxDGqxjuFxLgBBxXz5ImPPflJ686299dMxMz7BIfe9+4fq\r\n7X5OkqiBcJ28yyFqRUcLenH5hHW5cKPwskxS5k/LzBQfxaz5RIG/r94/86FECeMh7pjSfJleiLyT\r\nz7UZ57qQXIW96qwLr60OrmYuRwBnPGoC9FiOTw==\r\n',18),(187,NULL,'Ig+4n2AQ6B/fDK5dcNYY4w==\r\n',NULL,NULL,'1','Counter','RqtodlekcnTuayKTX+kgGXQzN3mq35vMxIp5buTbsZ/SK2dG7k/bNgGC5nZpYylOTTseIGdqtpGF\r\nrHBYzrUWYFny6X/a+dp6jn4begNJX2ll/jhD7JOxy7+IoUPhG/VJT1G+HXkKYfbh742CALFPABZ/\r\n34Bj1pA6cbT1HHYcoqJ3A9OLevOl9uuu5TP1R53M+rrbMMGdckhLJP4cP1AePIjkmqsZk0uzWgq5\r\nmKc3q/ibs3aIdF2iNcMg5jjzI/CDhdzeA7RF4TEDpgGsVH7TDJJw4jhywvNe7plOdmR1SuQ9D3iY\r\nJY9iel7J1drUE0xloN+pWSgmp2QVmdAb7+TXng==\r\n',4),(188,NULL,'YwVCrsHgOZ7xx9J8Xj54gA==\r\n',NULL,NULL,'1','Counter','fDwK/NSE+3emaHUTrDAqNe0l+FWj+RVmYS5PD824GhStYuBIwW4WZxZzFO5n/bfjjj9FvP0tzchx\r\npX5I1bRZWO0Ddz6TnNlV2IG0GRB9RIJikChXPnp0W5FywynbRvzFoyWHjpcXKQ7UnM1xGCRjus95\r\nVotHuUaetjnXIjKEof00u8L5CzrK5oEGneEBMoL1Q9y2BBeFwA7iumNPziG/d3ya1mcrf0lkZNUb\r\nBOXKegtDi/a04rArk8gFSMPZTUxOzEpyyLOhH+iPpFpr18JM1msR8VYrfyNZ/zkX56BFQvQsdQ8J\r\nMV8vxeqIVu0Wje9mtuSGIB3EdPqdtOB9Vg6jJA==\r\n',5),(189,NULL,'RLSVH6DB0fz9itxCIwl90w==\r\n',NULL,NULL,'1','Counter','DAH7X0qS5kkH/qQPAjAdGqt9yd2TBZhA2/FVfeOiIYjT4Ajmw529qu8ZqWNdpfzubzibxGgKFgyH\r\nHv6/PNvVNX2xQAVUV1L8uGonjZABiRaskO4bwqgpnxLIcxqivbeCAokk+0PFI8d/4IOcf8zUUdOs\r\nwQnDQnS6C8PAOCaHR/0tVRofojZDcttgR2/AfT2ar/oKKPXkACyMKjHUldwJNsUZBKPLIcbVHBQn\r\nRNFUezUsOkhOmZxd/gQ06Nz1F7TFk/Zs7i2b7+nTTdh1DW5HT11B7BwshI1J9NqNXs9X+py+ZApV\r\n9KyMoj4PI7Olm99g/o2QNaYT0pachs3y7gKPWg==\r\n',6),(190,NULL,'eiDjxeHF/Ye6rDwGmZI7Mw==\r\n',NULL,NULL,'1','Counter','Yk//Hq4C5dxisFiF96o/QjDgKEMEIBWBDQBX5hzc/Lq/cvdlCwiut7h8P6thiXdHDAThKPWBVuFO\r\n+Y4dM8TCChahPcCXDTNW0/GsGcHYjxtgF42QGpnjZTF/yR4ZZ3/E+4H1IWbOARNGVlYaxVjmvepP\r\ne0+WpIZTTHwFglMaPsxWtix9+oZ7+IbPLKL2qtBULn8AxGoAm67tkrtYLkJLZYUK9uN5ORm4OFXF\r\nICFV1FbeEP+pWsmEMZ6jLNMYDE9Cc63nf2l9Bcn5/FSND2KzgvhzNmVE28kGg427/1pk9i0+IAc4\r\nMMZFkfWrNpo+z+OnvnvlPqrVMMeUyNjafczu1w==\r\n',7),(191,'Miw1\r\n','Ypadfm5miyB+gBK48ZFQ4Q==\r\n','TB08ierpQ5LN/3RrmIl2lA==\r\n','qprRuDPoK8YSwd5QWkqA8g==\r\n','2','Counter','McWGChOlp1/x0S9Di4SgSy9YOCPS4Tnn9MwB1SwUfDhIQ1xMWES/Id65sbAog4B9KmIQ5/Qu8zSw\r\nD1PD+C5GOyM2BUc4bfnQ2C4X4bkWA6uN7/UF4rzf9uZpc1Xwqkp1HEhhBSEbI7ZsRbfvp7iZ+HD7\r\nP5aBWZaQIsO6B7I2mkhWoWZvXGjQqUyBJ9OCEHLdSlMVKSTcSKPC8G1xZ8Q6YTXEMvXhxFfi1N7J\r\nzXwY9YvexSRpydmDqNfv1xnXuuYglEY3UxHsuSLpN1AraPopREzzK/VbWjKHjh5OVixl0wMm0eZi\r\nypf6hhVCDu5CxUbf4FADYC4vXisUUR2JGzMqwg==\r\n',68),(193,'MCww\r\n','ebqoApIMGd/yQlJsgIhiuQ==\r\n','ZaHv5NQm71mByZ9qz6Vy/Q==\r\n','Miw3\r\n','2','Counter','abh+L4uU7Dz0JBpAIWaN4Jb7WlkBib1aEk3NDP1xiS5VNIxyuyPxeT6sPpwVG2UPPtL9tTBRn/a3\r\nDhCD8xSG5mCmOb0fs83VbgaCwt5AWHVtkhZ7D7fEahrswUp/c3+3ceTAY3DUYL4Uz5b8+GoHf0rQ\r\nxV006jogF23REM3C6oC9EwnMdkqHgXzgOdSDwOPcgbOlNPw4SDUTjv0TbBnrU5NofWStt5K4KEcw\r\nvsJrZ/Z69pUUE13KXhtUoQjTeneI8Zu7Me1iGinZISaXRgZhs5bvfKXDpG6oDSWhje2GOE5IlNQo\r\nsaTYKR2CdWMjbd7ei6Ddx/WDtlCGM/np6PQY/w==\r\n',70),(194,NULL,'A0WmoVMqsLYUzZzQwWiMAg==\r\n',NULL,NULL,'2','Counter','gfaIvML7hOMgFzm7j88LgSmhNXx49rFHI26yvZ5vbNFzv05MLksAic7y3EIvWQTdwDkmA1vrL28v\r\n2Ys/2ArMIezNGrXax8ICyU4RGZqe6krZ6AjB11OR4bLtSmd2Vv8Ln2gduUZ5IczO/eJLVnIxS32S\r\nOCJUmrGxOY7tTqICgrqLOnVxVa7RJRzr3AFqCfSwIF0bnwPXCqcydD9H53gmDkFZVY+1Xa4QHA8m\r\nsfn6FPCOrqowYji5QCWctIfLqfv2mCeZ38RCHOY+r1yFoy2bZYP+EjNn2CsoTN0h1dZy7gPX+xIw\r\nNM+U09LcnnVpko3YvWDmZLOL4B4+DJuYyKE9iQ==\r\n',71),(195,'Miw3\r\n','yDftggg5Q11B7Y3YRjQdvg==\r\n','3B3TRxQ8/EgM/kTVs+y0Cw==\r\n','u5WUL7YtSKL5Qe1qoIqzUw==\r\n','2','Counter','Nmus/j6FyGh64qypnvb1gzWpbDlqArodwIgoMP4E+ftEEQ9aLJC76c3qqjV3hfYkR6AEArRQbVL7\r\nqJMoyyHbDJIvz+ahfGzdT1Lm1kfU8A/Fe+c5cAPeLSxI7Yy6aQusgVR53TTQhiMFZYdeCgtUsF8n\r\nm7niSl5sS8uU76D9u7/Eq0OBMHPEaydb0fYwtu/4Lwd0Pb9htq2U1i9Jhza0Cs0wnFbPtWQ9G5Ls\r\npVN+Z+BVyH6WfUK5iz9ccEj8V3gvspzVZcTAa1ZSEay2rZCOLrqInr/ArjZ2amCX+tF//j2BwAkk\r\npbMcFjDSEgpnJbWQ4boYwte8pHJRIzI7Jh1Ekg==\r\n',72),(196,'Miw3\r\n','YfsXPIo/eqMPDQHrrMI13A==\r\n','QdrRf9fsYG1ODTWVfopjqg==\r\n','GJbyNXPlcsYoN9R0Q33XQA==\r\n','2','Counter','dtSxaiiD4fZ1zfPIWNUb3RtoIbp364VLwqVMcV4FoT7ktWNfetlPAlJ/71IhTsM4lwlsZEUGnjTf\r\nS/Ypi3QDrjsKHIh3xuo/EAAvusv8lIL0J5+n0WQ8gk3JZ5CCCBJH+fwlm9hwG2V3EZrmGc7nZ0ad\r\n6k8bQihgoUmKtaK/Pu+RQX0aBwGJOx8xDfD98nanXsttpFvsw2Ep2VhFI3coaySDheoeyQYWKZxj\r\nD6oo9hJGDa+KWnWYHR3hkki53gughy8F2fhBDsdwhMGHpXWEsXJG/JDVWesWDcviMNOeoXL6gYoA\r\n/U9rJWF/GVjac6m3cA20lf+MX91jVyOQ+OD0dw==\r\n',73),(197,NULL,'+ja0tM3XQoqFMiVPlzpyoA==\r\n',NULL,NULL,'2','Counter','RHh7DCKa6jmdyDtG68w61XRkxC1h5oWPmrcPY1iKzy5tZoAmWX1pZjFE8uRM7AAwbGFts2UzUnUO\r\ntXW1GH9tTAhc9W6no7SZmcJOLlfvEzmaW4MC5CGOnBwXNqIcvBb3p6g4Ua8aJFfrVbZrfwKLI2Ee\r\nmQhrz6PbzUuD/8i/dkLHviq/AJHjLKe7Liruin31Vm/6kgB84Y1dQpw2QX1khNacSoVD8Tyvh/Sv\r\nFlMXNkXJoC6ItqfXhlmn6TWwXBIevGZLELjSRmnxMwjpc0YBelSmNwa8cu8rtQsm8Fq1eEPAFrW2\r\n7kRfES4wA9RcciC/vC+RolFQp5jBb/lrbIfiPg==\r\n',74),(198,'Miw1\r\n','kZhkGW4pdReI/alii+90lw==\r\n','j/j4aOi71/lE5Fw9NNXClQ==\r\n','YUrWgDa3zjvuu6+bIuslBQ==\r\n','2','Counter','DQnN0JPer43gHkgUGSc6HEIsox8eYnO0xYTplltpLM4yKjs6wUZawMV5fvB+rcxqA8f3koObcv3V\r\n133mlt8gQDwBs7Hm/fo7YlZRRo2R37/oXtA4asPpZ1Le7nCJPeq3GiRkQ6m9yyNiSQeeSincDXtP\r\n3E6D9X5P4Uiz0VMpC9KhaH4TQcCydDMB+ZL6UsxfqbMrPeuP0zhyVVYO50yNIXxj4p7pv3C4DCvV\r\nW+j8geUh9rsIIRjcK/K6PX/FGbHzSPU9v9LkEhvB8j34731GUm3nOWqmc54IC1TuvsCgzai+Jh/o\r\nXbiBuz9mGumMHD8Qumv/d/+UCegpq150b45TbA==\r\n',75),(199,'Miw3\r\n','0bCt2dbwM9ZIYaNqzByvRw==\r\n','3GhjcKLGfIAqxw18kwMrSw==\r\n','lj2R28DibtwwCfKyeON4Gg==\r\n','2','Counter','Wrnt3RzKWYLJewcvmk7uJWQanqjRH0aiPGgDAczsYR5SO2o1yvePS1EVI/QA+xvZhp4aJ7NDk9qE\r\nKuQX0sIRlVDCtm2HEV2gvMJN3eHtD0Ph7O81l0BAtmixpYZiKNtJZn6Yanj2yCZAnYvz6oZ/jIZJ\r\nhHkJy9Z8hQUfYIC7PhQa+e2stz6+VQV13lcT+CqDFtdoazVvgFnPQB1w0U71GE1nSfDf8pPaBfsK\r\nrOoQsJ2Cqvtvfc3jaYtXPoGvjBu8fxeTUeEm+T0DYdMWBMsnWIUIic6jUM5KfnUncvsoR23E3voK\r\nm522OfDWEQ6TOi6aXuKGsmayvB6QM9MpDLReyw==\r\n',76),(200,'Miw3\r\n','l+LzafHhNGncC1vGeMChrA==\r\n','AIgAadxm6XLxF82U+mQVYQ==\r\n','d/JUjdAfxRbVslt1hNqd1w==\r\n','2','Counter','f45RsYFFM4cPNw5OHkZlUQdhNve6E7sbovQxyHjYfaYcxLqsge7lT6Z9RIbZk4urVRaGk4q7yESw\r\nho3G8ZfGs8vq9VlVhqYwZCmz1I4D9TPTCfd77trQE3sIIstEG6axKWeuh0DrJ2PliF8QIM7urvvs\r\nKJf6ME3qlClS1VfD8xuPhy3YoOlBgi9ljnRqXGkEv7H0EJHgao9qFcZRTa+Ffkx28Dh4rvhs0iDn\r\nIFN1Jb481a9PxEnynOObhO1UGPhwZedFDmH0QsI+d9vaAgEHXKT3UYJnelheSxXNW3a5q47Hn3AO\r\nOx5mS4J+DWQrKUUjtJzIjd4pHNEHVh1KES8Ogg==\r\n',77),(201,'Miw1\r\n','JqsraVJlCFQ2VYMzJxPRgA==\r\n','Jonh0L19o1CFve+4pP3QyQ==\r\n','rNbuqonqyfmVQA91Lme9jA==\r\n','2','Counter','dj8Dx6+yf98fmzori2sX2QSO/SnG3bS7sBZDt+xBf1g8MRf+siorTqRs66BehFUP2aWI40N38pb3\r\nqB16eETHSyOs5jmAU2Mzpbnj0e++ppBMVocRx1pQ1osTqN5MzelSN5QQSwU7NN6Eh9WDzad/f1ve\r\nE/s7r96mYRjJ3PWeD/LnT4Or3srRuvH1W8/k9Di7eAfmg02hbGdJguHqKMwMXFzp56iJbMJv0/DQ\r\n4FV6YCQl5sEgxSi2wGol47RGwzYa5W/Q0SXIbekb2Z5eyWST7IuZxLNGBmih1rKTDpzIPQYwLkIz\r\nbNYe1xHj7ZRRu2GN/XmSXoNotblsTBkd2wYdmQ==\r\n',78),(202,'Miw1\r\n','WD0pQjBsv+tFXuKXLdoatw==\r\n','TdQN28CAJNjv/1QLKfp0Vw==\r\n','N74movarB03ybCWqfsvhCg==\r\n','2','Counter','W8Ik2Ll1FfRq+8eXfFb8JRnC1GI+h5gndDWVimFqLg8YMGvvraHfRaBzTGBpkvk/+BcuC6PKodKx\r\nckTWTk4yMDXbYtTvsgiv9mhhuAoejM6DqbXgOId6N3SnN0iFjJsXxohV9MR9hkXDbYef2znL+lUu\r\nDbXZBTPAT2VYQl22GaS0MyoTPNF27DGhNjSN/7RPwvnjpxZNhio1Uc3Wn28gM3sP+5p1v4sUNBt5\r\n2F+LrtfddzwGDbap7ZwTczw5VNxRZ9nJJmXU4iQGWmZm9/lTP0ONOVl+k6DGze65g/E1VLxN0u8t\r\n1eNeY8KcmyjxJaANcFd5jX+SzOeJC3tAAC+nmQ==\r\n',79),(203,'Miw1\r\n','Y2eZld8j3UUp2hLtSWxnrA==\r\n','48LTaAqYK2MpTYKC2MpQwQ==\r\n','TnGc4hSoP5bKaSaoPW02zQ==\r\n','2','Counter','RRAHIGrd4X/XvqyhnNQP5QvTGhro+aNznNYOb9b13Bqm1eSOJP919SkVsrze1hb4XP8CwfrG+wKO\r\nrqGqMf6BwY6+yfP59pNlj2HaIQ0B3ft/4rAnI3TRmRW0nhdPnFolfCSmCLASKj9kqT2vvlQKwzZA\r\n2QD805O966WDYkTse61sl6J/unnocqAziN1HaImZMwNZqSwmOqpOVqlyeLBRlcagNhl4j4Ccb2FN\r\n72PcUFhzc/nZ0cP2MnySO382igQy6QOxTIGY2olScbBLod3qoCDoNjSSidZ3hsZoETa9rpnW5uDc\r\nmAZ7M8ptKKe8+/20rgMEtQERdC7d9Jgs5W5kDg==\r\n',80),(204,'Miw1\r\n','rcB5eQONXXkPCzjkvDs+Xw==\r\n','HRnyqlS0yIAdsP/Jp1Z26A==\r\n','EWSjSvgkEiMeFmA2AYX64Q==\r\n','2','Counter','CQe5IrZ/RmnFADJOByKiMwJNI6kXyUzD365sismM9E4Q0vmmW2nSdnbIWF8/VQvWQfvNNaDustPb\r\nqnauoMq/6t/QCMeLn7UcKzhwCMh8w2/dR3QTCCjFSFTBAgqhrXWiYJbamFbVl+OyQL8LwuzKTdgK\r\ncfUtfC7yo+NvlWTqyiVAzQBM7RSwZg+FySOTZ7NugasSDe4/KPaZzmYuz3FzZPNh8d0MI7Hb3NVY\r\nZFprjEw8EGLPhRJUAu5ZuxbwJ9Ma08hUjXC5YT+DYvXBA6LrNzyWIbsj42q2JKHDCbC5VdRYogMc\r\nTGOHtWx+Nnv8y15Rw7Hjv84g3JyhQUEFFLQX9g==\r\n',81),(205,'Miw3\r\n','yUZBJ5KT4Xb6FdrqD87GBw==\r\n','RmCnIcGCNevZF2zIUcw+1Q==\r\n','GIheBw3Qah4j8OygsNYkrA==\r\n','2','Counter','EmJ7w96EvNiUtNj/HvC0HHpPi/u/rIs+hTP55pynGXoZM62LKpw8Cg+zguSxx/6HcOI6rKWpJAuv\r\n+FskWRfatBlG8fquqIiAscMJlYDb2K/d3CVkFUSeakklkYAXfUG6o9XSGm+7/q8haAmfT3FgO72/\r\n08kVQqLxaTxiHYyKCq13iDKn2KLn/MmdHDsEXSJlUkD9Otevbuty2XZD3BuXfVTWJOtYPkVQqr6/\r\nC5OKbOlcEavoD/KHkBQKaABt0emQCc04IuaG33ucg30IdjKqKcdjYGnnhYS7KjtwUoHD9TXp3SPT\r\nAnnI6tPxWceVbzVoU5QqUGmfAJIg9DAb6LmqKw==\r\n',82),(206,'Miw3\r\n','tK9QooGpknviujHek/nPTg==\r\n','7TfPZ+TETy/Kplljhc1EFQ==\r\n','GcHgr5E+3IHJdvbicwEtWw==\r\n','2','Counter','M/oD7sKJNbFJ2yDUCtZnuSh+gJOR6irR6NJZoVNJEEIOcfMNaE1sVlOG7xabRq2MUWmpbs3xejkS\r\nVvevSptyNYN0C0K1JIpfooEf1+VMuIB0KeYPPsJvcnMioNpLnS4AfQ195sI5TRxGqIIErrYRoeC/\r\n3TnLJJWxzoiTTXMjoEhKvF2cNTE2xSBL0OdxC0c0GvODeLN4NmI3OI+hIwnVlT4Wri4E8TyIPvMf\r\nSGYCP7XD+Wl4Ogqki95Tzmv/AQhjRTHN9qwIGHblVV4B1QLFRUJY19o14+ib/9xZxuhk0Gx1KYhZ\r\nqhks+u9GjnCM9Cie6iiRWtha9oKoDk8GLACXJg==\r\n',83),(207,'Miw3\r\n','O1Gnk4CWBR8/nsw4VrbLAw==\r\n','cbZeeAGc6JbOXU576F8EUw==\r\n','th5EdPuy4S/mdoSzmc27QA==\r\n','2','Counter','c0ovBe23rEWyGF32jcRz+QRJAUlBodo4QmTfcxMAGHtKNSrXJmYwMcIOv/5/KNevlO9NvtHIMJ6N\r\ni1qjYBKL7KM51xKjELTbZmaHpIgDbDZEFIdY2kpyDrKhsJQAI79Vn4MFKk7QgGbYs+qfbwPydlfE\r\nxqASH/KbwdfH09MVMEYApVI2R9WBEtrJoFJKfnMyosO2nr9HSNeUKc2TWoo6STl5559eVlHOoEYz\r\nkGNidR8TfUP1Y03a3AJGWlSYvlChhJ38khjzPmihq9k9qZY7EnpO0H6CNLIESy+GFoJT9dcRhXHq\r\nQjItCSsEQ/MkG/NtpcseAA0V++zIOQg3udbilg==\r\n',84),(208,'Miw1\r\n','RjvnKxnSsed2rtr8rmHIWg==\r\n','PPtaPtEA/h6AJzo81cPinA==\r\n','/YYWXVCrtPD32PO1VrlYTg==\r\n','2','Counter','dPd0k9udx7BIZREE1Z2wDTZpgSJifQVEY+ZpN9TDNlgQyyEOR1JTzx5wDzeKerYq5lVMX/iMxkep\r\nTNnAEZqV/dfoX/9fJU2D2XF5x3uAWBsKU8Adek94301MjOVYq7fpLZQS6Ss3ynOZaSeCzUs74CR8\r\nuq81Bz3wZeJQcCbcXn6vuEhe0ImAMwnMtXCvxmBR50BeKFE1XQzg4coHG9ZUc61SaWm9CrbommQr\r\n6W4m5PG7ugkvCVzS/JlgsEVu9tlajJHi5LBCbk5wGCEu8xd93Ou9gKkBcjluzhFBbgQfPj+DRa+I\r\nFEt7sxjDhy2rYDC7R7b8AyJ/VZdT1EQV+N5fBw==\r\n',85),(209,NULL,'tK9QooGpknviujHek/nPTg==\r\n',NULL,NULL,'2','Counter','M/oD7sKJNbFJ2yDUCtZnuSh+gJOR6irR6NJZoVNJEEIOcfMNaE1sVlOG7xabRq2MUWmpbs3xejkS\r\nVvevSptyNYN0C0K1JIpfooEf1+VMuIB0KeYPPsJvcnMioNpLnS4AfQ195sI5TRxGqIIErrYRoeC/\r\n3TnLJJWxzoiTTXMjoEhKvF2cNTE2xSBL0OdxC0c0GvODeLN4NmI3OI+hIwnVlT4Wri4E8TyIPvMf\r\nSGYCP7XD+Wl4Ogqki95Tzmv/AQhjRTHN9qwIGHblVV4B1QLFRUJY19o14+ib/9xZxuhk0Gx1KYhZ\r\nqhks+u9GjnCM9Cie6iiRWtha9oKoDk8GLACXJg==\r\n',83),(210,NULL,'O1Gnk4CWBR8/nsw4VrbLAw==\r\n',NULL,NULL,'2','Counter','c0ovBe23rEWyGF32jcRz+QRJAUlBodo4QmTfcxMAGHtKNSrXJmYwMcIOv/5/KNevlO9NvtHIMJ6N\r\ni1qjYBKL7KM51xKjELTbZmaHpIgDbDZEFIdY2kpyDrKhsJQAI79Vn4MFKk7QgGbYs+qfbwPydlfE\r\nxqASH/KbwdfH09MVMEYApVI2R9WBEtrJoFJKfnMyosO2nr9HSNeUKc2TWoo6STl5559eVlHOoEYz\r\nkGNidR8TfUP1Y03a3AJGWlSYvlChhJ38khjzPmihq9k9qZY7EnpO0H6CNLIESy+GFoJT9dcRhXHq\r\nQjItCSsEQ/MkG/NtpcseAA0V++zIOQg3udbilg==\r\n',84),(211,NULL,'RjvnKxnSsed2rtr8rmHIWg==\r\n',NULL,NULL,'2','Counter','dPd0k9udx7BIZREE1Z2wDTZpgSJifQVEY+ZpN9TDNlgQyyEOR1JTzx5wDzeKerYq5lVMX/iMxkep\r\nTNnAEZqV/dfoX/9fJU2D2XF5x3uAWBsKU8Adek94301MjOVYq7fpLZQS6Ss3ynOZaSeCzUs74CR8\r\nuq81Bz3wZeJQcCbcXn6vuEhe0ImAMwnMtXCvxmBR50BeKFE1XQzg4coHG9ZUc61SaWm9CrbommQr\r\n6W4m5PG7ugkvCVzS/JlgsEVu9tlajJHi5LBCbk5wGCEu8xd93Ou9gKkBcjluzhFBbgQfPj+DRa+I\r\nFEt7sxjDhy2rYDC7R7b8AyJ/VZdT1EQV+N5fBw==\r\n',85),(212,NULL,'O1Gnk4CWBR8/nsw4VrbLAw==\r\n',NULL,NULL,'2','Counter','c0ovBe23rEWyGF32jcRz+QRJAUlBodo4QmTfcxMAGHtKNSrXJmYwMcIOv/5/KNevlO9NvtHIMJ6N\r\ni1qjYBKL7KM51xKjELTbZmaHpIgDbDZEFIdY2kpyDrKhsJQAI79Vn4MFKk7QgGbYs+qfbwPydlfE\r\nxqASH/KbwdfH09MVMEYApVI2R9WBEtrJoFJKfnMyosO2nr9HSNeUKc2TWoo6STl5559eVlHOoEYz\r\nkGNidR8TfUP1Y03a3AJGWlSYvlChhJ38khjzPmihq9k9qZY7EnpO0H6CNLIESy+GFoJT9dcRhXHq\r\nQjItCSsEQ/MkG/NtpcseAA0V++zIOQg3udbilg==\r\n',84),(213,NULL,'RjvnKxnSsed2rtr8rmHIWg==\r\n',NULL,NULL,'2','Counter','dPd0k9udx7BIZREE1Z2wDTZpgSJifQVEY+ZpN9TDNlgQyyEOR1JTzx5wDzeKerYq5lVMX/iMxkep\r\nTNnAEZqV/dfoX/9fJU2D2XF5x3uAWBsKU8Adek94301MjOVYq7fpLZQS6Ss3ynOZaSeCzUs74CR8\r\nuq81Bz3wZeJQcCbcXn6vuEhe0ImAMwnMtXCvxmBR50BeKFE1XQzg4coHG9ZUc61SaWm9CrbommQr\r\n6W4m5PG7ugkvCVzS/JlgsEVu9tlajJHi5LBCbk5wGCEu8xd93Ou9gKkBcjluzhFBbgQfPj+DRa+I\r\nFEt7sxjDhy2rYDC7R7b8AyJ/VZdT1EQV+N5fBw==\r\n',85),(214,NULL,'O1Gnk4CWBR8/nsw4VrbLAw==\r\n',NULL,NULL,'2','Counter','c0ovBe23rEWyGF32jcRz+QRJAUlBodo4QmTfcxMAGHtKNSrXJmYwMcIOv/5/KNevlO9NvtHIMJ6N\r\ni1qjYBKL7KM51xKjELTbZmaHpIgDbDZEFIdY2kpyDrKhsJQAI79Vn4MFKk7QgGbYs+qfbwPydlfE\r\nxqASH/KbwdfH09MVMEYApVI2R9WBEtrJoFJKfnMyosO2nr9HSNeUKc2TWoo6STl5559eVlHOoEYz\r\nkGNidR8TfUP1Y03a3AJGWlSYvlChhJ38khjzPmihq9k9qZY7EnpO0H6CNLIESy+GFoJT9dcRhXHq\r\nQjItCSsEQ/MkG/NtpcseAA0V++zIOQg3udbilg==\r\n',84),(215,NULL,'RjvnKxnSsed2rtr8rmHIWg==\r\n',NULL,NULL,'2','Counter','dPd0k9udx7BIZREE1Z2wDTZpgSJifQVEY+ZpN9TDNlgQyyEOR1JTzx5wDzeKerYq5lVMX/iMxkep\r\nTNnAEZqV/dfoX/9fJU2D2XF5x3uAWBsKU8Adek94301MjOVYq7fpLZQS6Ss3ynOZaSeCzUs74CR8\r\nuq81Bz3wZeJQcCbcXn6vuEhe0ImAMwnMtXCvxmBR50BeKFE1XQzg4coHG9ZUc61SaWm9CrbommQr\r\n6W4m5PG7ugkvCVzS/JlgsEVu9tlajJHi5LBCbk5wGCEu8xd93Ou9gKkBcjluzhFBbgQfPj+DRa+I\r\nFEt7sxjDhy2rYDC7R7b8AyJ/VZdT1EQV+N5fBw==\r\n',85),(216,NULL,'O1Gnk4CWBR8/nsw4VrbLAw==\r\n',NULL,NULL,'2','Counter','c0ovBe23rEWyGF32jcRz+QRJAUlBodo4QmTfcxMAGHtKNSrXJmYwMcIOv/5/KNevlO9NvtHIMJ6N\r\ni1qjYBKL7KM51xKjELTbZmaHpIgDbDZEFIdY2kpyDrKhsJQAI79Vn4MFKk7QgGbYs+qfbwPydlfE\r\nxqASH/KbwdfH09MVMEYApVI2R9WBEtrJoFJKfnMyosO2nr9HSNeUKc2TWoo6STl5559eVlHOoEYz\r\nkGNidR8TfUP1Y03a3AJGWlSYvlChhJ38khjzPmihq9k9qZY7EnpO0H6CNLIESy+GFoJT9dcRhXHq\r\nQjItCSsEQ/MkG/NtpcseAA0V++zIOQg3udbilg==\r\n',84),(217,NULL,'RjvnKxnSsed2rtr8rmHIWg==\r\n',NULL,NULL,'2','Counter','dPd0k9udx7BIZREE1Z2wDTZpgSJifQVEY+ZpN9TDNlgQyyEOR1JTzx5wDzeKerYq5lVMX/iMxkep\r\nTNnAEZqV/dfoX/9fJU2D2XF5x3uAWBsKU8Adek94301MjOVYq7fpLZQS6Ss3ynOZaSeCzUs74CR8\r\nuq81Bz3wZeJQcCbcXn6vuEhe0ImAMwnMtXCvxmBR50BeKFE1XQzg4coHG9ZUc61SaWm9CrbommQr\r\n6W4m5PG7ugkvCVzS/JlgsEVu9tlajJHi5LBCbk5wGCEu8xd93Ou9gKkBcjluzhFBbgQfPj+DRa+I\r\nFEt7sxjDhy2rYDC7R7b8AyJ/VZdT1EQV+N5fBw==\r\n',85),(218,NULL,'O1Gnk4CWBR8/nsw4VrbLAw==\r\n',NULL,NULL,'2','Counter','c0ovBe23rEWyGF32jcRz+QRJAUlBodo4QmTfcxMAGHtKNSrXJmYwMcIOv/5/KNevlO9NvtHIMJ6N\r\ni1qjYBKL7KM51xKjELTbZmaHpIgDbDZEFIdY2kpyDrKhsJQAI79Vn4MFKk7QgGbYs+qfbwPydlfE\r\nxqASH/KbwdfH09MVMEYApVI2R9WBEtrJoFJKfnMyosO2nr9HSNeUKc2TWoo6STl5559eVlHOoEYz\r\nkGNidR8TfUP1Y03a3AJGWlSYvlChhJ38khjzPmihq9k9qZY7EnpO0H6CNLIESy+GFoJT9dcRhXHq\r\nQjItCSsEQ/MkG/NtpcseAA0V++zIOQg3udbilg==\r\n',84),(219,NULL,'RjvnKxnSsed2rtr8rmHIWg==\r\n',NULL,NULL,'2','Counter','dPd0k9udx7BIZREE1Z2wDTZpgSJifQVEY+ZpN9TDNlgQyyEOR1JTzx5wDzeKerYq5lVMX/iMxkep\r\nTNnAEZqV/dfoX/9fJU2D2XF5x3uAWBsKU8Adek94301MjOVYq7fpLZQS6Ss3ynOZaSeCzUs74CR8\r\nuq81Bz3wZeJQcCbcXn6vuEhe0ImAMwnMtXCvxmBR50BeKFE1XQzg4coHG9ZUc61SaWm9CrbommQr\r\n6W4m5PG7ugkvCVzS/JlgsEVu9tlajJHi5LBCbk5wGCEu8xd93Ou9gKkBcjluzhFBbgQfPj+DRa+I\r\nFEt7sxjDhy2rYDC7R7b8AyJ/VZdT1EQV+N5fBw==\r\n',85),(220,NULL,'O1Gnk4CWBR8/nsw4VrbLAw==\r\n',NULL,NULL,'2','Counter','c0ovBe23rEWyGF32jcRz+QRJAUlBodo4QmTfcxMAGHtKNSrXJmYwMcIOv/5/KNevlO9NvtHIMJ6N\r\ni1qjYBKL7KM51xKjELTbZmaHpIgDbDZEFIdY2kpyDrKhsJQAI79Vn4MFKk7QgGbYs+qfbwPydlfE\r\nxqASH/KbwdfH09MVMEYApVI2R9WBEtrJoFJKfnMyosO2nr9HSNeUKc2TWoo6STl5559eVlHOoEYz\r\nkGNidR8TfUP1Y03a3AJGWlSYvlChhJ38khjzPmihq9k9qZY7EnpO0H6CNLIESy+GFoJT9dcRhXHq\r\nQjItCSsEQ/MkG/NtpcseAA0V++zIOQg3udbilg==\r\n',84),(221,NULL,'RjvnKxnSsed2rtr8rmHIWg==\r\n',NULL,NULL,'2','Counter','dPd0k9udx7BIZREE1Z2wDTZpgSJifQVEY+ZpN9TDNlgQyyEOR1JTzx5wDzeKerYq5lVMX/iMxkep\r\nTNnAEZqV/dfoX/9fJU2D2XF5x3uAWBsKU8Adek94301MjOVYq7fpLZQS6Ss3ynOZaSeCzUs74CR8\r\nuq81Bz3wZeJQcCbcXn6vuEhe0ImAMwnMtXCvxmBR50BeKFE1XQzg4coHG9ZUc61SaWm9CrbommQr\r\n6W4m5PG7ugkvCVzS/JlgsEVu9tlajJHi5LBCbk5wGCEu8xd93Ou9gKkBcjluzhFBbgQfPj+DRa+I\r\nFEt7sxjDhy2rYDC7R7b8AyJ/VZdT1EQV+N5fBw==\r\n',85),(222,NULL,'O1Gnk4CWBR8/nsw4VrbLAw==\r\n',NULL,NULL,'2','Counter','c0ovBe23rEWyGF32jcRz+QRJAUlBodo4QmTfcxMAGHtKNSrXJmYwMcIOv/5/KNevlO9NvtHIMJ6N\r\ni1qjYBKL7KM51xKjELTbZmaHpIgDbDZEFIdY2kpyDrKhsJQAI79Vn4MFKk7QgGbYs+qfbwPydlfE\r\nxqASH/KbwdfH09MVMEYApVI2R9WBEtrJoFJKfnMyosO2nr9HSNeUKc2TWoo6STl5559eVlHOoEYz\r\nkGNidR8TfUP1Y03a3AJGWlSYvlChhJ38khjzPmihq9k9qZY7EnpO0H6CNLIESy+GFoJT9dcRhXHq\r\nQjItCSsEQ/MkG/NtpcseAA0V++zIOQg3udbilg==\r\n',84),(223,NULL,'RjvnKxnSsed2rtr8rmHIWg==\r\n',NULL,NULL,'2','Counter','dPd0k9udx7BIZREE1Z2wDTZpgSJifQVEY+ZpN9TDNlgQyyEOR1JTzx5wDzeKerYq5lVMX/iMxkep\r\nTNnAEZqV/dfoX/9fJU2D2XF5x3uAWBsKU8Adek94301MjOVYq7fpLZQS6Ss3ynOZaSeCzUs74CR8\r\nuq81Bz3wZeJQcCbcXn6vuEhe0ImAMwnMtXCvxmBR50BeKFE1XQzg4coHG9ZUc61SaWm9CrbommQr\r\n6W4m5PG7ugkvCVzS/JlgsEVu9tlajJHi5LBCbk5wGCEu8xd93Ou9gKkBcjluzhFBbgQfPj+DRa+I\r\nFEt7sxjDhy2rYDC7R7b8AyJ/VZdT1EQV+N5fBw==\r\n',85),(224,NULL,'O1Gnk4CWBR8/nsw4VrbLAw==\r\n',NULL,NULL,'2','Counter','c0ovBe23rEWyGF32jcRz+QRJAUlBodo4QmTfcxMAGHtKNSrXJmYwMcIOv/5/KNevlO9NvtHIMJ6N\r\ni1qjYBKL7KM51xKjELTbZmaHpIgDbDZEFIdY2kpyDrKhsJQAI79Vn4MFKk7QgGbYs+qfbwPydlfE\r\nxqASH/KbwdfH09MVMEYApVI2R9WBEtrJoFJKfnMyosO2nr9HSNeUKc2TWoo6STl5559eVlHOoEYz\r\nkGNidR8TfUP1Y03a3AJGWlSYvlChhJ38khjzPmihq9k9qZY7EnpO0H6CNLIESy+GFoJT9dcRhXHq\r\nQjItCSsEQ/MkG/NtpcseAA0V++zIOQg3udbilg==\r\n',84),(225,NULL,'RjvnKxnSsed2rtr8rmHIWg==\r\n',NULL,NULL,'2','Counter','dPd0k9udx7BIZREE1Z2wDTZpgSJifQVEY+ZpN9TDNlgQyyEOR1JTzx5wDzeKerYq5lVMX/iMxkep\r\nTNnAEZqV/dfoX/9fJU2D2XF5x3uAWBsKU8Adek94301MjOVYq7fpLZQS6Ss3ynOZaSeCzUs74CR8\r\nuq81Bz3wZeJQcCbcXn6vuEhe0ImAMwnMtXCvxmBR50BeKFE1XQzg4coHG9ZUc61SaWm9CrbommQr\r\n6W4m5PG7ugkvCVzS/JlgsEVu9tlajJHi5LBCbk5wGCEu8xd93Ou9gKkBcjluzhFBbgQfPj+DRa+I\r\nFEt7sxjDhy2rYDC7R7b8AyJ/VZdT1EQV+N5fBw==\r\n',85),(226,NULL,'RjvnKxnSsed2rtr8rmHIWg==\r\n',NULL,NULL,'2','Counter','dPd0k9udx7BIZREE1Z2wDTZpgSJifQVEY+ZpN9TDNlgQyyEOR1JTzx5wDzeKerYq5lVMX/iMxkep\r\nTNnAEZqV/dfoX/9fJU2D2XF5x3uAWBsKU8Adek94301MjOVYq7fpLZQS6Ss3ynOZaSeCzUs74CR8\r\nuq81Bz3wZeJQcCbcXn6vuEhe0ImAMwnMtXCvxmBR50BeKFE1XQzg4coHG9ZUc61SaWm9CrbommQr\r\n6W4m5PG7ugkvCVzS/JlgsEVu9tlajJHi5LBCbk5wGCEu8xd93Ou9gKkBcjluzhFBbgQfPj+DRa+I\r\nFEt7sxjDhy2rYDC7R7b8AyJ/VZdT1EQV+N5fBw==\r\n',85),(227,'Miw1\r\n','yaKzazutA7vuOiDcMo/HGQ==\r\n','0QJgBFBu5tKcaogNNtqbcw==\r\n','16mpUJ+CE4g3IGFnHG1pKQ==\r\n','2','Counter','JNNN6AV/k1NVSHoTY6hQOWb0sywi1l4C5+GEZfUhxAtFucuDyBvSW7uLqoOoPtjZ0l6OPXRhN7Hk\r\nYoJ7uOo1h+ytbVVxTrEdhxhWIEpkFQOyzNdbkJ28GEULyyDXRt1njxru+1zdfMOrAlzLtgjWYPZl\r\nt+xHReY1SFQ+zsCXfv4PQVE+NgWJmUZYcsj5y9X+WZsk1LLDqaXajJnTmkEGCzb1Isb7Sc6k7aGw\r\n3PiaixohldC0uXQCFMjQi0vZ13nDZ5xiExhqW/JHW/2VbyzemsbFaZ8UBitBab0wpdCHqOjTOXgh\r\nRvK9TIlO9A4156p38OTrPgkLJDvbPlcsyGFfUA==\r\n',86);
/*!40000 ALTER TABLE `ballot` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blind_ballot`
--

DROP TABLE IF EXISTS `blind_ballot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blind_ballot` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BLIND_BALLOT` varchar(512) NOT NULL,
  `VOTER_SIG` varchar(512) NOT NULL,
  `USER_ID` int(11) NOT NULL,
  `ELECTION_ID` int(11) NOT NULL,
  `VOTED` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=220 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blind_ballot`
--

LOCK TABLES `blind_ballot` WRITE;
/*!40000 ALTER TABLE `blind_ballot` DISABLE KEYS */;
INSERT INTO `blind_ballot` VALUES (219,'XAEMKp+Yw+uVqac5tQdMA3K1bqaheJ6xfjhO70sCHSteQzyhz0nyul+lXn7PIDHJuuTbbQcp8buj\r\nJYX+I3Snnzz21j8kKt9Jn0YEShVXu4klaCVLvO9IZ1IcfAvTHIx6xEUa5P6fn5rBUwQD6JtrDXpR\r\nj2pK5zOpzx3ctycZCGNOGSscaqKPydKXkNGOQlrUnEThIH+r976D6O9qKUyYT5+LWe2xsrBafPEe\r\nKVycTT70ba6x2sYu/eTX9q86XTNs5X3zL2Py1FdPlDvsZP7ZdTPJ/rKDubnWPy60YSlkC7WEcj2h\r\nVdRED03O6OfQLWNVM9haSH7ukHLrSpNnqBw9Mw==\r\n','ORYaTLpW+eWFH5GquwziCpQPUv9WYUOEiD3wNX5khWGgp7w3GOhIQwCPRMK6boP690yRk1pAd5oK\r\n+zuhphEwEaYPBDx5fOxH3utw50bslw+ywFHSCOIi7Kpva+qILwKSmY6qQdvd994RMKVLxbNqr55U\r\nYGjVhARb4U/VXL+G0LxOXwsUWK9zybneFsVlfLl6RvVvGbwsOFZKe3EqkhaOkh2yKjKcHbA8iAuL\r\njBj8dxpmsmxKnddTzK/yOg0RJA6pgVLUlcEA6v7cZdE2EAvoHH34L97FBzrKGXTzA0jRq5Z7QSwb\r\nx/IaM1HRQJtdi0dBxsgFlt6H54FarYutQ/NoEQ==\r\n',2,2,1);
/*!40000 ALTER TABLE `blind_ballot` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `candidate`
--

DROP TABLE IF EXISTS `candidate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `candidate` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `USER_ID` (`USER_ID`),
  CONSTRAINT `CANDIDATE_ibfk_1` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `candidate`
--

LOCK TABLES `candidate` WRITE;
/*!40000 ALTER TABLE `candidate` DISABLE KEYS */;
INSERT INTO `candidate` VALUES (1,5),(2,6),(3,7);
/*!40000 ALTER TABLE `candidate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `candidate_election`
--

DROP TABLE IF EXISTS `candidate_election`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `candidate_election` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CANDIDATE_ID` int(11) NOT NULL,
  `ELECTION_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `CANDIDATE_ID` (`CANDIDATE_ID`),
  KEY `ELECTION_ID` (`ELECTION_ID`),
  CONSTRAINT `CANDIDATE_ELECTION_ibfk_1` FOREIGN KEY (`CANDIDATE_ID`) REFERENCES `user` (`ID`),
  CONSTRAINT `CANDIDATE_ELECTION_ibfk_2` FOREIGN KEY (`ELECTION_ID`) REFERENCES `election` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `candidate_election`
--

LOCK TABLES `candidate_election` WRITE;
/*!40000 ALTER TABLE `candidate_election` DISABLE KEYS */;
INSERT INTO `candidate_election` VALUES (1,5,1),(2,6,1),(3,5,2),(4,7,2),(5,6,3),(6,7,3),(7,2,4),(8,2,5),(9,2,6),(22,3,27),(23,2,27),(24,1,27),(25,2,28),(26,2,29),(27,1,29),(28,3,30),(29,2,30),(30,1,30),(31,2,31),(32,1,31),(33,2,32),(34,1,32),(35,10,33),(36,2,33),(37,1,33),(38,3,33),(39,7,33),(40,5,33),(41,9,33);
/*!40000 ALTER TABLE `candidate_election` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `counter`
--

DROP TABLE IF EXISTS `counter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `counter` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `USERNAME` varchar(45) NOT NULL,
  `PASSWORD` varchar(64) NOT NULL,
  `SALT` varchar(45) NOT NULL,
  `EMAIL` varchar(45) NOT NULL,
  `IS_OTP` int(2) NOT NULL DEFAULT '0',
  `LOGIN_ATTEMPTS` int(8) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `counter`
--

LOCK TABLES `counter` WRITE;
/*!40000 ALTER TABLE `counter` DISABLE KEYS */;
INSERT INTO `counter` VALUES (1,'counter','740cd7381f09e94e2bee3f71f5f700c21fc5748f3b11a16f812561d0b40333a6','9d6ac273','jipoom@gmail.com',0,0),(2,'counter1','ff7120d49acb353f7e87c2c36af159f38257e7ad19317400abbbbdbdf14afb48','bd46ba3a','jipoom@gmail.com',0,0);
/*!40000 ALTER TABLE `counter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `election`
--

DROP TABLE IF EXISTS `election`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `election` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ELECTION_NAME` varchar(50) NOT NULL,
  `START_DATE` datetime DEFAULT NULL,
  `END_DATE` datetime DEFAULT NULL,
  `WINNER` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `election`
--

LOCK TABLES `election` WRITE;
/*!40000 ALTER TABLE `election` DISABLE KEYS */;
INSERT INTO `election` VALUES (1,'CEO ELECTION','2013-11-01 00:00:00','2013-11-13 00:00:00',NULL),(2,'CTO ELECTION','2013-11-01 00:00:00','2013-11-13 00:00:00',NULL),(3,'CHIEF DIRECTOR ELECTION','2013-11-01 00:00:00','2013-11-13 00:00:00',NULL),(4,'TestCreateDB','2013-11-14 00:00:00','2013-11-14 00:00:00',NULL),(5,'','2013-11-23 00:00:00','2013-11-24 00:00:00',NULL),(6,'123','2013-11-23 00:00:00','2013-11-24 00:00:00',NULL),(27,'Test Yunfan Bug','2013-11-27 00:00:00','2013-11-27 00:00:00',NULL),(28,'Test New','2013-11-27 00:00:00','2013-11-27 00:00:00',NULL),(29,'asd','2013-11-27 00:00:00','2013-11-27 00:00:00',NULL),(30,'Test Before Commit','2013-11-27 00:00:00','2013-11-27 00:00:00',NULL),(31,'Test Again','2013-11-27 00:00:00','2013-11-28 00:00:00',NULL),(32,'teet','2013-11-27 00:00:00','2013-11-27 00:00:00',NULL),(33,'Test Election clone','2013-11-27 00:00:00','2013-11-27 00:00:00',NULL);
/*!40000 ALTER TABLE `election` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `election_extended_permission`
--

DROP TABLE IF EXISTS `election_extended_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `election_extended_permission` (
  `ELECTION_ID` int(11) NOT NULL,
  `USER_ID` int(11) NOT NULL,
  `IS_ALLOWED` int(11) NOT NULL,
  PRIMARY KEY (`ELECTION_ID`,`USER_ID`,`IS_ALLOWED`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `election_extended_permission`
--

LOCK TABLES `election_extended_permission` WRITE;
/*!40000 ALTER TABLE `election_extended_permission` DISABLE KEYS */;
INSERT INTO `election_extended_permission` VALUES (2,1,1),(2,2,1),(2,3,1),(3,2,1),(23,3,1),(23,11,0),(27,3,1),(27,10,0),(28,4,0),(28,10,1),(29,4,1),(30,1,1),(30,2,0),(30,3,1),(30,4,0),(30,11,0),(31,1,0),(31,2,0),(31,3,1),(32,1,0),(32,1,1),(32,2,0),(33,2,1),(33,3,0),(33,10,1);
/*!40000 ALTER TABLE `election_extended_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `election_title`
--

DROP TABLE IF EXISTS `election_title`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `election_title` (
  `ELECTION_ID` int(11) NOT NULL,
  `TITLE_ID` int(11) NOT NULL,
  PRIMARY KEY (`ELECTION_ID`,`TITLE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `election_title`
--

LOCK TABLES `election_title` WRITE;
/*!40000 ALTER TABLE `election_title` DISABLE KEYS */;
INSERT INTO `election_title` VALUES (1,1),(23,3),(27,2),(28,3),(29,3),(30,1),(31,2),(32,1),(33,3);
/*!40000 ALTER TABLE `election_title` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `superuser`
--

DROP TABLE IF EXISTS `superuser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `superuser` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USERNAME` varchar(45) NOT NULL,
  `PASSWORD` varchar(64) NOT NULL,
  `SALT` varchar(45) NOT NULL,
  `EMAIL` varchar(45) NOT NULL,
  `IS_OTP` int(2) NOT NULL DEFAULT '0',
  `LOGIN_ATTEMPTS` int(8) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `superuser`
--

LOCK TABLES `superuser` WRITE;
/*!40000 ALTER TABLE `superuser` DISABLE KEYS */;
INSERT INTO `superuser` VALUES (1,'root','ca1ab92411a80117f594b96e2f1c35198f6a1d6e9b172204c74568be9fd964b2','582c1843','jipoom@gmail.com',0,0);
/*!40000 ALTER TABLE `superuser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supervisor`
--

DROP TABLE IF EXISTS `supervisor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supervisor` (
  `ID` int(11) NOT NULL,
  `USER_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `USER_ID` (`USER_ID`),
  CONSTRAINT `SUPERVISOR_ibfk_1` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supervisor`
--

LOCK TABLES `supervisor` WRITE;
/*!40000 ALTER TABLE `supervisor` DISABLE KEYS */;
/*!40000 ALTER TABLE `supervisor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `title`
--

DROP TABLE IF EXISTS `title`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `title` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TITLE_NAME` varchar(45) NOT NULL,
  `PERMISSION` int(2) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `title`
--

LOCK TABLES `title` WRITE;
/*!40000 ALTER TABLE `title` DISABLE KEYS */;
INSERT INTO `title` VALUES (1,'MANAGER',7),(2,'DIRECTOR',5),(3,'ENGINEER',1),(4,'SENIOR ENGINEER',3),(5,'INTERN',0),(6,'COUNTER',0);
/*!40000 ALTER TABLE `title` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `FIRST_NAME` varchar(50) NOT NULL,
  `LAST_NAME` varchar(50) NOT NULL,
  `TITLE_ID` int(11) NOT NULL,
  `DOB` varchar(64) NOT NULL,
  `PASSWORD` varchar(64) NOT NULL,
  `USERNAME` varchar(50) NOT NULL,
  `PUBLIC_KEY` varchar(512) DEFAULT NULL,
  `SALT` varchar(8) NOT NULL,
  `IS_OTP` int(2) DEFAULT '0',
  `LOGIN_ATTEMPTS` int(8) DEFAULT '0',
  `EMAIL` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Yunfan','Zhang',1,'8bdd64eba00d4e0f8e07ddb1e6a8f16908a254b8c56eac7c450be384cc8b587c','52302fd4e83f8700c7c171a86fab7771a94608ea39d0717f41ea0209e4b6e926','zhang','MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAm5QQYdw2nXqpcLerhgAtnHRaujBsNtcB\r\nTciOGcPoF+XEiev4/OQq0Ps3JMK+GGoNFPQ87UqxwyZWREjeB+L9tfD/QjK1/cynaK0ZKkxLbVKj\r\nHuW9P/tNLa42pur5pMnLZWx3x4U+wrosuWWC7YkF+PQPrkPM036eF5mfvLND9qPvCBWNXEvvRE9F\r\nmxcWPA0CY/+Xo6aI6fSx1aVphDizOSOgs81/JESgzCpWT0IZePaK7BhOCY4TKYKIB/MP7u+hvnCI\r\nJy/mQZlxD67zTe3/UCXdjCxkn+1VMaI4DLSpyI7v9m20HRC30C1M/uTkhsfkR3arFGXfLZB6S/BU\r\ncKXs9wIDAQAB\r\n','1234',0,0,'jipoom@gmail.com'),(2,'Jirapat','Temvuttirojn',1,'3c45977094377a684bbcba3794cba6ba048b97f281da54aec27cca6863b50f96','52302fd4e83f8700c7c171a86fab7771a94608ea39d0717f41ea0209e4b6e926','poom','MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAhWhLatjbhyJqfTDH7Qc7u4ac1zuXA7QM\r\nnEJJPxXg5l8fGrrLlsWXH4pfv86ApA9EgcFD5O0xRwpy/tyfyX55CrJBvsXOJms2pu/rMnAvS/if\r\nmNAnwWU7vysqG52szsP7BpnS6g3TSMoVgoiPhJrLnuWwqRN1f00SMkxTg/+iDqGDKIxdVuAapGZt\r\nY5idt7gQ7Lr1+aeF1F7c1MRNQcChAYvElMUble43z9x9AmZIer+gt5z4FacJlrYHc7+F2UNYPesH\r\nUzH3itv1WbtGEtdaIeSead1AIzVuaMQfi/2OClmRA8XiDnZBWR825m/DvhNXJp7bE1fIMVh/u6ai\r\nsrLAKQIDAQAB\r\n','1234',0,0,'jipoom@gmail.com'),(3,'Alemberhan','Getahun',1,'c1c2d2ac12d24ea46eedfafea0eb10b6012a2e56f0f2b06d121a46e92760c28a','52302fd4e83f8700c7c171a86fab7771a94608ea39d0717f41ea0209e4b6e926','alem','MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnqND5YDPZJYFUj4JNNLDzrsQ2Tz88qaE\r\nhe6RkT3wMMTALGv2w7UiE7KNTpEW/5R72VmdyhNz3drq97effv90XsiaTGWDjnltUcQWBurW4VkC\r\nCDL7DKo+yjM5dncOg8TcKMP66d1zPHyUtjoUop94CCZGv5zEBqUEtPptIdRQlGz5UqxrjmTYzlsb\r\nWiZrmiOza73i09fnfbGuZmtOXC03v1v6HUUcGrOEI0bUzVEaf+aWI+rw0R5EE3/+tBV1M0NZvWeU\r\nOYzs70Dup2BH6BJdJV5F9iCu8YKlqwsUPe/W/zKEm4tlUPJCO74p37g3QzaiLIx403zUM1OzpHo2\r\nm43/LwIDAQAB\r\n','1234',0,0,'jipoom@gmail.com'),(4,'John','Ordway',3,'70187d8ab21c68db97962f728b750c0a5264bf36ef607a29a0cd0d0f672c8800','ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f','ordway','MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlSZlrXecizcH63WS5bFNmB7a0m7Q9pC3\nJukMiNmQZJnca7JdNWdLI75k60PzCCfsRFpS5lkqNz3igItEZyN7yJsG9cJJu46ZPc9nzM4ireJv\n/AZruxGiuZBRrEJlCdl90oETmbVMWl9sIS5z/mNiSgVv9a94Yp7WbCKaQfvMqXRXaCVzJxVgXbuS\nxfGeqoew/BaGijuSXr8N2TdB1ICPsqMBWjHNFg/1TGggFOEqZQ8H4zvnZpLcU/tIUJk7OhWITQdJ\nU23NyZxu1Plx1yiQEr4JSYsy/e+A42oil7NymBmTnDpyGE1THx99hG+2BO5+tNLrd1rDI7XFIvkL\nWPL5vQIDAQAB\n','1234',0,0,'test@email.com'),(5,'Tom','Leykis',4,'0ea9ac2b8855b5b4babc0ebfa91e2c3f84450e5ad8c0e2a97e184608561ef876','ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f','tom','MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlADbXFCclOb0HdpFDK8aSnTKSL00ZN99\r\ndxrfXxqzu7KvG9KCDn1RWgIt1UEePLQJ7aamFsgfpcnNGCkqDaRQe03G8YpQY3irJNXQLTn1ro5x\r\n7ONI/0S+UzMtUAcdya9q1tpDIZ7/K06cg6zP/ZeXAo72GFL4XwQHPvCoXXWYdKtGcR0swDJZFOWK\r\nsH047vpUOe6yxi54eDpXtK9ViugBQPSQeAVXhenGNZS4BJdKNkuMUbtudxyin/vK53RGh+o7EdeE\r\nEG8bqW0UW/JcUIC04ecW5Rm9sc1p38RkdnGr6eo92LhdvjhmTLB84ZvMI/vIzK8Uj8aSbjnT6q7T\r\nVLGBAQIDAQAB\r\n','1234',0,0,'test@email.com'),(6,'Linus','Pauling',5,'9a0867d8fcfaf0be8bfc1e2247cef4baaaa0722a7cc24d2ae7d400b72131b260','ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f','pauling','MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAjcY8KWYkFjQJV4BBggvhzTMOwJmsQTXJ\nThpWWXqciPmdIy96UOG3WohK5j0uXVYgnAGzl8Lc6Kgmz4cPxQlveTYz4QB+7ozD9L3QcH3v62mS\nkP2K+oKk5Mxb4P6ECst0zKopjVHz/ku2I0C2JS9lPC4VYQJnR5nMWs2KBvnpxJCGK9+N4Opbp1nG\njAZcQSyfQeS7Tlr+OLAdc70YP9TWGQfObOKmY8k1lRWIEJjrb26fabJqdf5uMufDoP9qRnSdWrCg\nDyFd7uvzrUa+/g2dcrICVvRi9VKhqtcmp1FZl0skZJCfYB3INEAU9Ui6N0gm79Ajj5+zGM54os6I\nvbWYOwIDAQAB\n','1234',0,0,'test@email.com'),(7,'Henry','Ford',4,'9e0b38b2d6b1067084868fac3f1749c18735ce918a277cc4a907d9347ebd9c6f','ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f','ford','MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtIBEd9pEDv18SGLWQOWl3/nblHa/AC6a\nkmHsxpa4tNoi74SChFWqlzQaWdRbV06ex6ZxWCbn4PpWHtw2oYwDamurHd8L4h0eMQa9a3RXwG1z\ncPmoGCgJiqTyLfOhmnVn/t21eqmSHHLFX5Fl+mFsd2EEZlxINzQVIxRfRIZfgsbjKy1BUfy7wNnc\n2A9coCRoPnnvC0266btyHOCdG6KRBGmGXbZOrz6Aehgy9mqrFayePQAwbRvE5LWkj7b9C7CjXpjD\nN8yQXSk+xl9ZayUDmqd7BSwCnZhVnAdqjKK26Sx99IGnQiQPyqigbtvarqdBYw27BAO1SMbiW9An\n71M/mQIDAQAB\n','1234',0,0,'test@email.com'),(8,'Test','Test',6,'9e0b38b2d6b1067084868fac3f1749c18735ce918a277cc4a907d9347ebd9c6f','ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f','Test','MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAjokvzFLr34HWiekTwwKyaKhbrTQ5Ikt2\r\nNyZqLLFJwa4sEVYmfYlHAuqNDpPXvLh/RetWOwCX85lrCUPz7wdPNdTaBdt2yDNWevawoF3eZzbz\r\n4akZTvzQiR+3yWFZwGUxEYjgyCLPvKSCQAax3SbkyeG/kg4cZnNhwnwzasckMT59Fnk5yNgZUgdi\r\n7uK9CGUXIJ+CBGSGT0r/X2TAVOKu4mEiE3dhL7lGyMEunnAuWA/FZLctJmkIUG/xGDoAWFeAaB7i\r\nYLct+DfG1XUK9YZl2PjYaMddvR6cT34HKkkIEnH9I5QeJofwPRIDKVRhc3RfEv+o97xcFRR3P6xJ\r\nL6yCBwIDAQAB\r\n','1234',0,0,'test@hotmail.com'),(9,'pom','pom',4,'88f725d0b6822d7896ec5a9fd41bda5edadee21df27be4a8e6b692f07c8445ac','03b683271e0cece5d86329cdf41955d979d2dd1fecbebcad8ba394bcb835c6c1','pom','MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnnLxX9EpX6LIARBsEuyrNrEo78gwWrfM\r\nFJT3eoiS+tbz4Je+Y7fBbwbSCNBGu+xGbASApKimMlYaf8DvB83j3PklhwG/tkeyoedZ+v6Xuku2\r\nKUSSuoGI5j6CjH4CMPXDLKf70ndWEotou6pEuV79j4Oltsp3jGeOb63j1ZLJfn2lYVs2wfzLxiz9\r\nqVBEcO3WnFkomw4YLQyBcNHpH/29hrxW+skomhXjOzZDQR37xlc/EPRiEjjBIqiJeY8TqCfgPPkK\r\njUj0YUk43TAT6eHRFnm70Ihle4AM53nYFMukznogy2bwMqTPw7imcgruhfiPTiCJzYzsCcbZPmdM\r\nvPytVQIDAQAB\r\n','1234',1,0,'jipoom@gmail.com'),(10,'poomtest','poomtest',2,'3c45977094377a684bbcba3794cba6ba048b97f281da54aec27cca6863b50f96','f9ffbbddeb6000b63ba259e21ccfb10d6da914167672c44690cdc1a58461baf2','poomtest','MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAzfyDMNa+UhXOLOoDGqBAGGbDq9Id7P9X\r\nWJNFIEn3ZgJmAJs95vj7VAu/J1wyu1UYcYPaHpdDPY1ZFlM8I0DPp7ofoF11/Ye78HvZXOKQ+++p\r\nEF1MtZ5Je6ixFZpYdk63ejor9RWtwq4n+lWom94NOOhAkVRJ7DpP0dvcUKQ6GaaN04rmT+1osR5P\r\nS2ikRUAboVHYIyWM89VGV8r2/0Se6J90064wYVfT88Akd/LuPD0Oh7Rnsyl3867mz6k/Ubm/9PhR\r\nOR79eQiOeJy5xXcRvMg/3V2W8JmDLtPs9q6r15RaZYhCl+V8KHKUO9WQtFYQErz5as2ygRe7n9yn\r\nmEWQAwIDAQAB\r\n','1234',1,0,'jipoom@gmail.com'),(11,'Charle','Lee',3,'bd8b8a2e40976ec987e6eeb2a835f7137606b8809ed577d9e184e296a98c1c30','418b4c25f4f55506bddef4a065527051655dbf5bb9e1ea029c9bf80c8dd251fc','lee','MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAknrCXGA5uaOyPcE02pTT4lKDIGSux8JK\r\nLYWAx5k+emKltLzN+r2Q2yG60el1xPx4ww4rmn4MmR5bmrtROIM1ELQO+MIv3TDSbbB21Bx1suDw\r\nTWwcI59ZAyqNyLhTn6LqpAW80jvmdn/Zo1F2O4SE0x9OPVv99IXBAz6r5pQSEtguC9G3WetBen+6\r\n4wWAcLnuiwYyIvXN6m/azeKC08AZZcmmo2M5LGeELME0E1EaajjDqNb05yLWiyDhFsY21gaIQZEU\r\nnS8DvX0hXmPNa1XwAwC2qYlsYHnN/lQxORCj/q9PBjR8CI5Set/7ba0ohbaujklWS+2Z/8n0VFlD\r\nImREEQIDAQAB\r\n','c715b547',1,0,'zhangyunfan@gwmail.gwu.edu'),(12,'ji','ji',1,'88f725d0b6822d7896ec5a9fd41bda5edadee21df27be4a8e6b692f07c8445ac','73c1d8dd160d8a6d11fc11f42a32161cb19c304e5530e887b72ddd54d3e39cd1','testagain','MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAjBTNOUDcbwHEWrl/VjT7Wc8I4MTTAcxG\r\nyyu99m85jSMxqdQ9WKsRU1QiIY+Pd9Kx8Zae4GWoGdw8dR0UScaI/d5ostY6tFDOJBz32xQI6gHn\r\nHrUnRrKN31M9K54T6+9OWLF/PscLDQHPwyO4qo30MVRyU5l8H27sO9rUi8wDtOs7hlbmAweY6XW7\r\nQ3kOvY2MUZnGmhVxKkIXNLUZ/1+aAaQ4wFnHP8IRnd6tuyS1USBVnElahZvQWFR1zByHngKakQNw\r\nG/P14pfgflVb14gVH/jvM3YDCy9WOImiUa4Geg3OK87Q2UwPfbLgNPSqXIr+zFG3CvVPOPRD5aVZ\r\nXiGRSwIDAQAB\r\n','51ad5a17',1,0,'ji_poom@hotmail.com'),(13,'jiji','jiji',2,'b6e1b4fc9f04db0b3d83485d469136816a884fd8446ec8d1433fe8d1c04fe530','d298acf918c394d90728bafff40345e34a1645dc4513470edce7d20c4d02ae22','jiji','MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkS3ySZGLMPB+F3+0BP+z8ti8OaJt9JxT\r\nBHEOiWqQqli5f04V12UpzwSCEOqNHWnJPcey4lnIdLyUGcMjkBDgKmHiEKiOpXrrFzZw6wyZYFXT\r\nbrCS7S7b80SxClHQoWVjBz2mCZ3/A2B7GOHlZ8foXkfmCSwrzwYSo9q2vJTdTSwGYgLDaBlWixBu\r\nfJVB0w6Ij1YUEu3KY7yVZFx/Lm2eELie0UY5p8h5E9GkDeFD3nsDkV4iK2kfY69hbrnBZ6A7a2gL\r\nusxKWTXCLNxPDEN2iRNrroDK/snfzZFRHcpmvVmIVHIK4brh2BrGquWKwrnu70r9H8oUIkTETE3R\r\nLAYLxwIDAQAB\r\n','80947d92',1,0,'ji_poom@hotmail.com');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_election`
--

DROP TABLE IF EXISTS `user_election`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_election` (
  `USER_ID` int(11) NOT NULL,
  `ELECTION_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`USER_ID`,`ELECTION_ID`),
  KEY `ELECTION_ID` (`ELECTION_ID`),
  CONSTRAINT `USER_ELECTION_ibfk_1` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`ID`),
  CONSTRAINT `USER_ELECTION_ibfk_2` FOREIGN KEY (`ELECTION_ID`) REFERENCES `election` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_election`
--

LOCK TABLES `user_election` WRITE;
/*!40000 ALTER TABLE `user_election` DISABLE KEYS */;
INSERT INTO `user_election` VALUES (2,1),(1,2),(2,2),(3,2),(2,3),(2,33);
/*!40000 ALTER TABLE `user_election` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `voter`
--

DROP TABLE IF EXISTS `voter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `voter` (
  `ID` int(11) NOT NULL,
  `USER_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `USER_ID` (`USER_ID`),
  CONSTRAINT `VOTER_ibfk_1` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `voter`
--

LOCK TABLES `voter` WRITE;
/*!40000 ALTER TABLE `voter` DISABLE KEYS */;
/*!40000 ALTER TABLE `voter` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-11-28  2:46:51
