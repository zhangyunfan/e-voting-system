-- MySQL dump 10.13  Distrib 5.6.10, for Win64 (x86_64)
--
-- Host: localhost    Database: evoting
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `evoting`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `evoting` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `evoting`;

--
-- Table structure for table `ballot`
--

DROP TABLE IF EXISTS `ballot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ballot` (
  `ID` bigint(20) NOT NULL,
  `BALLOT` varchar(20) DEFAULT NULL,
  `BC_BALLOT` varchar(20) NOT NULL,
  `BC_BALLOT_KEY` blob,
  `BC_BALLOT_IV` blob,
  `ELECTION_ID` varchar(20) DEFAULT NULL,
  `COUNTER_ID` varchar(20) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ballot`
--

LOCK TABLES `ballot` WRITE;
/*!40000 ALTER TABLE `ballot` DISABLE KEYS */;
/*!40000 ALTER TABLE `ballot` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `candidate`
--

DROP TABLE IF EXISTS `candidate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `candidate` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `USER_ID` (`USER_ID`),
  CONSTRAINT `CANDIDATE_ibfk_1` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `candidate`
--

LOCK TABLES `candidate` WRITE;
/*!40000 ALTER TABLE `candidate` DISABLE KEYS */;
INSERT INTO `candidate` VALUES (1,5),(2,6),(3,7);
/*!40000 ALTER TABLE `candidate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `candidate_election`
--

DROP TABLE IF EXISTS `candidate_election`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `candidate_election` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CANDIDATE_ID` int(11) NOT NULL,
  `ELECTION_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `CANDIDATE_ID` (`CANDIDATE_ID`),
  KEY `ELECTION_ID` (`ELECTION_ID`),
  CONSTRAINT `CANDIDATE_ELECTION_ibfk_1` FOREIGN KEY (`CANDIDATE_ID`) REFERENCES `user` (`ID`),
  CONSTRAINT `CANDIDATE_ELECTION_ibfk_2` FOREIGN KEY (`ELECTION_ID`) REFERENCES `election` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `candidate_election`
--

LOCK TABLES `candidate_election` WRITE;
/*!40000 ALTER TABLE `candidate_election` DISABLE KEYS */;
INSERT INTO `candidate_election` VALUES (1,5,1),(2,6,1),(3,5,2),(4,7,2),(5,6,3),(6,7,3);
/*!40000 ALTER TABLE `candidate_election` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `counter`
--

DROP TABLE IF EXISTS `counter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `counter` (
  `ID` bigint(20) NOT NULL,
  `USER_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `USER_ID` (`USER_ID`),
  CONSTRAINT `COUNTER_ibfk_1` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `counter`
--

LOCK TABLES `counter` WRITE;
/*!40000 ALTER TABLE `counter` DISABLE KEYS */;
/*!40000 ALTER TABLE `counter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `election`
--

DROP TABLE IF EXISTS `election`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `election` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ELECTION_NAME` varchar(50) NOT NULL,
  `START_DATE` datetime DEFAULT NULL,
  `END_DATE` datetime DEFAULT NULL,
  `WINNER` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `election`
--

LOCK TABLES `election` WRITE;
/*!40000 ALTER TABLE `election` DISABLE KEYS */;
INSERT INTO `election` VALUES (1,'CEO ELECTION','2013-11-01 00:00:00','2013-11-13 00:00:00',NULL),(2,'CTO ELECTION','2013-11-01 00:00:00','2013-11-13 00:00:00',NULL),(3,'CHIEF DIRECTOR ELECTION','2013-11-01 00:00:00','2013-11-13 00:00:00',NULL);
/*!40000 ALTER TABLE `election` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supervisor`
--

DROP TABLE IF EXISTS `supervisor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supervisor` (
  `ID` int(11) NOT NULL,
  `USER_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `USER_ID` (`USER_ID`),
  CONSTRAINT `SUPERVISOR_ibfk_1` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supervisor`
--

LOCK TABLES `supervisor` WRITE;
/*!40000 ALTER TABLE `supervisor` DISABLE KEYS */;
/*!40000 ALTER TABLE `supervisor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `FIRST_NAME` varchar(50) NOT NULL,
  `LAST_NAME` varchar(50) NOT NULL,
  `TITLE` varchar(80) NOT NULL,
  `DOB` varchar(64) NOT NULL,
  `PASSWORD` varchar(64) NOT NULL,
  `USERNAME` varchar(50) NOT NULL,
  `PUBLIC_KEY` varchar(512) DEFAULT NULL,
  `SALT` varchar(8) NOT NULL,
  `IS_OTP` int(2) DEFAULT '0',
  `LOGIN_ATTEMPTS` int(8) DEFAULT '0',
  `EMAIL` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Yunfan','Zhang','Voter','8bdd64eba00d4e0f8e07ddb1e6a8f16908a254b8c56eac7c450be384cc8b587c','ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f','zhang','MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA3nA05mwCDlF6RhSHiD7Z6XeKoxzWEP9A\nEKE4pnS/tzvi1+nGJt1Lh+ZjFgasj/woscFFvAII2is0EnMGDxgnyakHIM53pGZsBFUsaNCJjXja\nxThNFFZy0PlIomCGI7c2tsJeGAW7Hy0mgpWRoj0kd+GG6WKu53uD4X77XK60UhQ8TzquREBvxkHe\nR0EKUFY/0bn93t9mGZT6vEZS047CbOa4jmJLnnfWNTCm1yAwWL7iG9x3RM1hccqlO61LBqyMeDvM\nXI7lZjbk7KTTRjoySi5bwJVEZ7cNnpD++qa4gXkwM1vGrCRpLBj0kF4dtl7qvpyIDNzJRhdvmIAO\n86D9BQIDAQAB\n','1234',0,0,'test@email.com'),(2,'Jirapat','Temvuttirojn','Voter','3c45977094377a684bbcba3794cba6ba048b97f281da54aec27cca6863b50f96','ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f','poom','MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAqHrzt0uJNFmt5+DEshoTYSjXvOt7m9EE\nwBdqmDXXk+8ljhfY/Gr5t93KzPZ0rfz/5MiWyCvauB0P7zhMhg8n2igreQ54xljuE4EYz/oRxqLn\nlHJKEhEzYuFiY3Kq6ndUxuIpik7S234rvupomR5+FzE1JCYjpyy2CxCyv8uaJCE1gYZS86gnHs4U\ngWFkkAt1Wphrxfj4nJkFI2kfeZmNSw8kKx6h+fVAC4gCLYXpp3nAQIaGiuoK81MpPEXgSrJVsol5\nhrambhg/Ri2jMrGHTIwmnI5ihdV9GRgNl3AwK7Eo5sOBQGLSEAb3L+Md18UEMm1wcuXacou4Kr3h\nhfysVwIDAQAB\n','1234',0,0,'test@email.com'),(3,'Alemberhan','Getahun','Voter','c1c2d2ac12d24ea46eedfafea0eb10b6012a2e56f0f2b06d121a46e92760c28a','ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f','alem','MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAqLIP2xwEQ6xqDvjvVz/AEf8srmRZi4yS\nRzIXmpWcvQLWrkKROo0vPVzOg8C3VHhuvGE0XbwSHHj5p00GnCbKJAkYXkQGeXWFQeKuoY3MGh1m\nh1Y3ftXQXhmz2ei6qXbbYzk+lsH0coMGMSJ5Xs+GZkg+MZgZsrcyVqcJH0turSemG1CqdOSdHPmi\n/WK2vfabWR2gC+PkTVdN9wiesDTddI/Dcsh+fdFrHeTkCU+rfbLnVzTdhMaWWV+MjakOqTG3+9X8\n8Qgpq4qe9TL6pkwyypgeqdDlSKIOOMwRiO45pqK0ShyIQ61B7cuEoycT8WhBIf+LjZ0f1+W7r5aT\nLUYJJQIDAQAB\n','1234',0,0,'test@email.com'),(4,'John','Ordway','Candidate','70187d8ab21c68db97962f728b750c0a5264bf36ef607a29a0cd0d0f672c8800','ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f','ordway','MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlSZlrXecizcH63WS5bFNmB7a0m7Q9pC3\nJukMiNmQZJnca7JdNWdLI75k60PzCCfsRFpS5lkqNz3igItEZyN7yJsG9cJJu46ZPc9nzM4ireJv\n/AZruxGiuZBRrEJlCdl90oETmbVMWl9sIS5z/mNiSgVv9a94Yp7WbCKaQfvMqXRXaCVzJxVgXbuS\nxfGeqoew/BaGijuSXr8N2TdB1ICPsqMBWjHNFg/1TGggFOEqZQ8H4zvnZpLcU/tIUJk7OhWITQdJ\nU23NyZxu1Plx1yiQEr4JSYsy/e+A42oil7NymBmTnDpyGE1THx99hG+2BO5+tNLrd1rDI7XFIvkL\nWPL5vQIDAQAB\n','1234',0,0,'test@email.com'),(5,'Tom','Leykis','Supervisor','0ea9ac2b8855b5b4babc0ebfa91e2c3f84450e5ad8c0e2a97e184608561ef876','ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f','tom','MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAvmuH0GpxROUAesKTrgYf2CB2mN5l8kiP\nEfGOliVg/VYAeUwVRghlO6vn2qwhqBO4lKHRjvYkb+xZ7jPFnLPplf90RzpT94TKEf+o6B3ygg9P\n4Wmqn38nEmpWcRGNvxnveqKfAYZAxKelIm5P4s7JAJNUofDjW5/EUunf0IXgDGHR3CVh7CMzvJop\n+Y9QIv/EfxcBBc1MuXq6ZLkQnQud/FKeaZaGFeUk/mSJmVcndD4WVfeYNpDA+QNs0+Jv+R6UJIve\n4XBiJmK7mTvhmarsdOLOJrEhoVmS888gOwobm/j8McSw97kFHiKg92HkQiiWjpUEomzkj4UrGsm5\nQ8J7ewIDAQAB\n','1234',0,0,'test@email.com'),(6,'Linus','Pauling','Candidate','9a0867d8fcfaf0be8bfc1e2247cef4baaaa0722a7cc24d2ae7d400b72131b260','ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f','pauling','MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAjcY8KWYkFjQJV4BBggvhzTMOwJmsQTXJ\nThpWWXqciPmdIy96UOG3WohK5j0uXVYgnAGzl8Lc6Kgmz4cPxQlveTYz4QB+7ozD9L3QcH3v62mS\nkP2K+oKk5Mxb4P6ECst0zKopjVHz/ku2I0C2JS9lPC4VYQJnR5nMWs2KBvnpxJCGK9+N4Opbp1nG\njAZcQSyfQeS7Tlr+OLAdc70YP9TWGQfObOKmY8k1lRWIEJjrb26fabJqdf5uMufDoP9qRnSdWrCg\nDyFd7uvzrUa+/g2dcrICVvRi9VKhqtcmp1FZl0skZJCfYB3INEAU9Ui6N0gm79Ajj5+zGM54os6I\nvbWYOwIDAQAB\n','1234',0,0,'test@email.com'),(7,'Henry','Ford','Candidate','9e0b38b2d6b1067084868fac3f1749c18735ce918a277cc4a907d9347ebd9c6f','ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f','ford','MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtIBEd9pEDv18SGLWQOWl3/nblHa/AC6a\nkmHsxpa4tNoi74SChFWqlzQaWdRbV06ex6ZxWCbn4PpWHtw2oYwDamurHd8L4h0eMQa9a3RXwG1z\ncPmoGCgJiqTyLfOhmnVn/t21eqmSHHLFX5Fl+mFsd2EEZlxINzQVIxRfRIZfgsbjKy1BUfy7wNnc\n2A9coCRoPnnvC0266btyHOCdG6KRBGmGXbZOrz6Aehgy9mqrFayePQAwbRvE5LWkj7b9C7CjXpjD\nN8yQXSk+xl9ZayUDmqd7BSwCnZhVnAdqjKK26Sx99IGnQiQPyqigbtvarqdBYw27BAO1SMbiW9An\n71M/mQIDAQAB\n','1234',0,0,'test@email.com'),(8,'Test','Test','Candidate','9e0b38b2d6b1067084868fac3f1749c18735ce918a277cc4a907d9347ebd9c6f','ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f','Test','MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAizvzQRFNdn4DMGCYbyoUg8nzmjAw9dGn\r\nhhefzKn4gRCMdiIo7PIF9djU82dLBIg6n7xqsde8jFuagPdoGRurlovGX4oX4r292D/Q+R/StVcr\r\nO4dGuca3BUv4QnUA2sTgxbDw1BFw6URoP4LbL/a++YXEjV7Gky10KZNsFS2ikqAvCgklIaTpRxBv\r\nVur68URlS8LyMPf/287qDQJZNjgm2iG+DBaok8mSSzYAotp8cClSmLZeqMJFIsZk6Sv6eQlRKF9D\r\n/UdewARNuTakfyrtZiklaApmsZh59md3/lTR8JK3et6MwR+j2I4u/qP6cleveAS+7HnlnRVpdFjN\r\nmQ+21wIDAQAB\r\n','1234',0,0,'test@hotmail.com');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_election`
--

DROP TABLE IF EXISTS `user_election`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_election` (
  `USER_ID` int(11) NOT NULL,
  `ELECTION_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`USER_ID`,`ELECTION_ID`),
  KEY `ELECTION_ID` (`ELECTION_ID`),
  CONSTRAINT `USER_ELECTION_ibfk_1` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`ID`),
  CONSTRAINT `USER_ELECTION_ibfk_2` FOREIGN KEY (`ELECTION_ID`) REFERENCES `election` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_election`
--

LOCK TABLES `user_election` WRITE;
/*!40000 ALTER TABLE `user_election` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_election` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `voter`
--

DROP TABLE IF EXISTS `voter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `voter` (
  `ID` int(11) NOT NULL,
  `USER_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `USER_ID` (`USER_ID`),
  CONSTRAINT `VOTER_ibfk_1` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `voter`
--

LOCK TABLES `voter` WRITE;
/*!40000 ALTER TABLE `voter` DISABLE KEYS */;
/*!40000 ALTER TABLE `voter` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-11-13 20:16:15
