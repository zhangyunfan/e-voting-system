-- MySQL dump 10.13  Distrib 5.6.10, for Win64 (x86_64)
--
-- Host: localhost    Database: EVOTING
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `EVOTING`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `EVOTING` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `EVOTING`;

--
-- Table structure for table `ballot`
--

DROP TABLE IF EXISTS `ballot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ballot` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `BALLOT` varchar(512) DEFAULT NULL,
  `BC_BALLOT` varchar(512) NOT NULL,
  `BC_BALLOT_KEY` varchar(512) DEFAULT NULL,
  `BC_BALLOT_IV` varchar(512) DEFAULT NULL,
  `ELECTION_ID` varchar(20) DEFAULT NULL,
  `COUNTER_ID` varchar(20) NOT NULL,
  `SUP_SIG` varchar(512) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ballot`
--

LOCK TABLES `ballot` WRITE;
/*!40000 ALTER TABLE `ballot` DISABLE KEYS */;
INSERT INTO `ballot` VALUES (1,NULL,'HfQFjeNgN7aQhslpZCoKCQ==\r\n',NULL,NULL,'4','poom',''),(2,'MSwxMjM0\r\n','a09OxLVIOSBJRqOClXKDIA==\r\n','1ck4a4Ht+g7IYCWvXJhQzQ==\r\n','1ck4a4Ht+g7IYCWvXJhQzQ==\r\n','4','poom',''),(3,'MSwxMjM0\r\n','a09OxLVIOSBJRqOClXKDIA==\r\n','1ck4a4Ht+g7IYCWvXJhQzQ==\r\n','1ck4a4Ht+g7IYCWvXJhQzQ==\r\n','3','poom','');
/*!40000 ALTER TABLE `ballot` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blind_ballot`
--

DROP TABLE IF EXISTS `blind_ballot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blind_ballot` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BLIND_BALLOT` varchar(512) NOT NULL,
  `VOTER_SIG` varchar(512) NOT NULL,
  `USER_ID` int(11) NOT NULL,
  `ELECTION_ID` int(11) NOT NULL,
  `VOTED` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blind_ballot`
--

LOCK TABLES `blind_ballot` WRITE;
/*!40000 ALTER TABLE `blind_ballot` DISABLE KEYS */;
INSERT INTO `blind_ballot` VALUES (29,'H0EFLxvcWcqiJg5UrU7sX4S/ZU019qQa7vyAaCjVqji9Uu8DcI5NIg3iDXpb2IGod1u3nPYTEY3f\r\ng7Z60ds0Hw3QEKYM3ClKUEh9IoTa9MrBOkaAgUYVG+DcaSCDlhjBXKag0kc3KBSwSAxbWBdnLqMw\r\ncbIrMJe82B0E6E4RLqPGMMz5m6QZJeolsYHwbE2OcNneV6pAeOc4ceWNr8pmjfJ7BqqW8zVKOVQA\r\nR6bPZUQ3hru6GmrovSmkJbTCQ5g5m8JC42JnVIYdQosezPvp3pd7EGxTJbVnV3K0fun0G+M60SNH\r\ncDC9PDh1mgUWPtUEAvOozBuisso5LLDd+y5l3w==\r\n','JO7+xOltQboJiWw+kknXT5YTVmrz51gZUnOanrtEIFDjrze+J+A28CjfRHSLWdL4PPgfjxEr1heS\r\n+MuzDGjIdHQIkXctuZQygV6CktkUFGzs6gd3F2tOQuGA9rmAr2TzrCEeeZPb5WFtJH6DWA4hNYSs\r\nkZNsy3K5owFy84YA1TjblrPnUP9eKLdr0m2zUUf24Dd9blO25MI69ZcT1uj3WIbDgehcQVAdXtxw\r\n3EmmWuAkOYQe5ByWtsiIOQ8zHUoBo2Gri9VLrUuw7iMoAdCwqwnQn4IOMq9CoeFgVVdbFMZXoxxP\r\nOhpo+7PZybaeODp7In9iEpBrLeVU0M7tH2nczA==\r\n',2,2,0),(30,'QhXGV3pCHXEwRocK+jjWFp0FsS6AmuQ3d8LT952x8wLVAWJzPpGxax6UXIR4UQSWy8cGftFmMFM6\r\nqFwzUZDQLPpSDZQuXUJkWPVmivFLnoTyPcAhrdz66pM3VQBTTcOJuP0K3bOp8S7pLe+YT25oaeNN\r\nV9/2yCRHITwhZBwymgr4EQpzVcvJw5StR7/BKF234G3/+8Om8XtuXzu4Eez5biTbmp3AOCImSQqq\r\nLQAIV4FK1RcNtNo9M1X7S1Ils2DyRv8RCM9PIBsmohVZ9hOCdI++0EYabpXNdbfLQbpNgDSt/7tQ\r\ncV0vQXVPaLrw1q67Nbv6k2RCVkjSFjFFxnTIbw==\r\n','GBbAMVN8Eoc09Wb//tUH+zg5qBVn5Bf+XS7HSwJK5EszyauSLXUH+C4Zx/nIjxb4a/S802dGEJHj\r\nGykZydwEhHut453uhef1M2ZHYsPJBf3KJcdyZGb0L9oQnKktz0EcAYxy1qQ7jKxR9hSfPqP7+dcS\r\nWSGCspQzzft0MIMsj/bIO3FKEf30JOmuPeOalgTjtDH9JEKXtuAaVxBEECVwrA/DgyGAtyNrSds2\r\nn3GP2LEJt+O/a8IRnis7eeZBgu4yWeVNO0HWcNghUPILMMsKm3FdMG3cxn/EHS6bDCv5TIPNygQQ\r\nSpyTg/nqTsnvuC4vDiDV7x/wUUYEMY/0crc3qw==\r\n',2,2,0),(31,'QpxJ7BC9Tqt73OqoDTLdtng9uUvttN4aXf9oOVxPTOQ9ZdvGhB/iJl8kTswF+hHk1BPg9ZfcgPQJ\r\nDDUMQsqRUatR8pwVOAo8P/Q6SH8467Ac8w+QunL+LR4Se6rxM1OLOuI9/q2VZujep9Q+fURAbJFn\r\nWBNtogcWvy2kA7aoq1UOBR9D/cMfz0fedgRFjvHd04iTE+1L0OcEZfLHBMBQDLRYRDh9YalB+Olp\r\n/MR5EK2shR0VmJNKroRhCPVJ9yw9PcqN/+ah+zI0SJMlICDAlyugh47EOBB8su/GhBArsaUAgDlG\r\nIEv8Bhr6aFLO3C9rm/FADxLuQVpix6sm3IuKrA==\r\n','Q8r2cbM1FQZPLBNDp9rkjwjT42PAdiFEozhL6Thav7+BktWDEml1PIrycMJpURBx/Au3Xls0I0Fp\r\n0PvyqVzRrL4rUBDjHc+qjk6SF63tRJUVOS+Bs3LY9SX+jY/b4RL1RnMzcUMXwb15FlR2JSLsAZ3x\r\nzB+YSOFExRTUr2imUj6TO+Ej9lDR9xLeCAPSZA4yH27OSu+YwvzqWfURimRbDyJw0fPuSMqxI5Ti\r\n2HmrrzBe4DQAptvte6xFqnxoQNdeKfGtVcHHBcXIci7up9itdY/7XwftLuZiMlMiApFbV1xYsD/N\r\nUjrPmqBORfETZpsf54ZDdA4ruHADVz5/fFiM+w==\r\n',2,2,0),(40,'JxHgWyM0XggMILfQzYxlGkRUGHlv12i0Q3SflEdAMJ5jZgma949apNUpVmr6Ez04YQX1HvfErbBg\r\nPGu2TegEdRNYwzKhEXaX2py7oA6WhgU+FhY1XyhLPlBpFdXPLhnj0Cuad4vNl43DKF+f7jl3zNGe\r\nxk68Zhs+7vsbGX84/1Jmas6p/RwVzcTO36fRpjsz+XIi19UFx0L5bwUfCW5ED6VGFBLsM89HXhFj\r\nMBZX0j/zm5tW4UcpPAxFyWR+a85Bk4G0HV3591JG2YOVEQlmKgfYVJZ6BKUKLnkUg8mnuG0WNhlg\r\n5E5PYPf9nzRnEvXPtauXzg0O+Hk9k078FKFJxQ==\r\n','CFeayQ3WjLtBY0HNplD+2L6cH/O0RSFpfqI22jkT1PSG46pSD0bjbRqccsjUpLjAmRQHaJ8Npmg4\r\n3Ozy8Nfdirgy8sq06jDmMHgTMQHeaNq71jAPpPv3tXf5K/SrrYZLQcDuJivjtvqL0O8wfjQrMOxk\r\nimHMbTrJr2Fs4/QAfLgX4FtXnMT2OSiRZWPEFS/7u7IafXRv9vWTc+D89idHnbs4Rd+c6SkwrnhC\r\n1jSkysxdJzYqxs1whne0Q22cAhUjFCsj+A5EXbrXtvLZgXFYzz8qSso59sCoUeT4XRRGjV3/GoJF\r\nqoovsL2Ke2tLRXodHLVpj6XVKUOkOnfnmM5QCg==\r\n',2,3,0),(41,'bZ0/l6Nay2qUWTL6BK87m/cCGQ0FQK5/MsAS04vdpDkvtKWAK+3qmIw0wpEp9R/VlOtLrRqQB3u6\r\nD/kHGh+4PKHFVlFhdJuFjhn3VJW18bOqQetIywvi3tr63ZoIy20rkpU8z5M1SLoeiBbf3fHlPc36\r\nRnPgZQ/8G4nQsvEQWZuHXEhW4QNCJ8OEHMJxCY4FVVzgrojpkY4bHIbv/G07laA1i3SdpksvLqY+\r\nxBPoZLHwYwNgi0689X+pvEeF4eioT7h8YByfW8Jrbfk8azQwh8YlSjFjgvkTqzMyfY2dj2/AS9Wc\r\ndDFQyxecn8VNSmTfbdhHubu8QnpNdPvyD4KjBQ==\r\n','TyBJQuXvr9iTHOrOcDUZDvN0jKcATppYpT2GXFAH0QAP69UG7qe7vq0Z5mE88/RuEWzGGcl/NdHm\r\n6Q/lqCAxq107p6U0DW1j8urjE1ZZcaIqbR6KM742bmkOIuEiU3iWGh1+TJmLfbdpNobmJItqYlwo\r\nTq1P/hEEHzh1PbIAkPQIhbHGRBCqIe2YVPinWxFOROX3He3nPKlfLqEAr8jQBJvN1zj5xqnMIwt5\r\nh6uh3OeDbYBMqn9AdWiK84zYCJW1+cBhhdjyjhf2ZhmtO94TJbA1rbG6usSDbKxHRalOqli00IK2\r\n2tko2exVWZH5db2H8GCTT9TXiQ5nUmgv6we2Tg==\r\n',2,3,1);
/*!40000 ALTER TABLE `blind_ballot` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `candidate`
--

DROP TABLE IF EXISTS `candidate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `candidate` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `USER_ID` (`USER_ID`),
  CONSTRAINT `CANDIDATE_ibfk_1` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `candidate`
--

LOCK TABLES `candidate` WRITE;
/*!40000 ALTER TABLE `candidate` DISABLE KEYS */;
INSERT INTO `candidate` VALUES (1,5),(2,6),(3,7);
/*!40000 ALTER TABLE `candidate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `candidate_election`
--

DROP TABLE IF EXISTS `candidate_election`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `candidate_election` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CANDIDATE_ID` int(11) NOT NULL,
  `ELECTION_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `CANDIDATE_ID` (`CANDIDATE_ID`),
  KEY `ELECTION_ID` (`ELECTION_ID`),
  CONSTRAINT `CANDIDATE_ELECTION_ibfk_1` FOREIGN KEY (`CANDIDATE_ID`) REFERENCES `user` (`ID`),
  CONSTRAINT `CANDIDATE_ELECTION_ibfk_2` FOREIGN KEY (`ELECTION_ID`) REFERENCES `election` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `candidate_election`
--

LOCK TABLES `candidate_election` WRITE;
/*!40000 ALTER TABLE `candidate_election` DISABLE KEYS */;
INSERT INTO `candidate_election` VALUES (1,5,1),(2,6,1),(3,5,2),(4,7,2),(5,6,3),(6,7,3),(7,2,4);
/*!40000 ALTER TABLE `candidate_election` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `counter`
--

DROP TABLE IF EXISTS `counter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `counter` (
  `ID` bigint(20) NOT NULL,
  `USER_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `USER_ID` (`USER_ID`),
  CONSTRAINT `COUNTER_ibfk_1` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `counter`
--

LOCK TABLES `counter` WRITE;
/*!40000 ALTER TABLE `counter` DISABLE KEYS */;
/*!40000 ALTER TABLE `counter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `election`
--

DROP TABLE IF EXISTS `election`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `election` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ELECTION_NAME` varchar(50) NOT NULL,
  `START_DATE` datetime DEFAULT NULL,
  `END_DATE` datetime DEFAULT NULL,
  `WINNER` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `election`
--

LOCK TABLES `election` WRITE;
/*!40000 ALTER TABLE `election` DISABLE KEYS */;
INSERT INTO `election` VALUES (1,'CEO ELECTION','2013-11-01 00:00:00','2013-11-13 00:00:00',NULL),(2,'CTO ELECTION','2013-11-01 00:00:00','2013-11-13 00:00:00',NULL),(3,'CHIEF DIRECTOR ELECTION','2013-11-01 00:00:00','2013-11-13 00:00:00',NULL),(4,'TestCreateDB','2013-11-14 00:00:00','2013-11-14 00:00:00',NULL);
/*!40000 ALTER TABLE `election` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `election_extended_permission`
--

DROP TABLE IF EXISTS `election_extended_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `election_extended_permission` (
  `ELECTION_ID` int(11) NOT NULL,
  `USER_ID` int(11) NOT NULL,
  `IS_ALLOWED` int(11) NOT NULL,
  PRIMARY KEY (`ELECTION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `election_extended_permission`
--

LOCK TABLES `election_extended_permission` WRITE;
/*!40000 ALTER TABLE `election_extended_permission` DISABLE KEYS */;
INSERT INTO `election_extended_permission` VALUES (2,2,1),(3,2,1);
/*!40000 ALTER TABLE `election_extended_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `election_title`
--

DROP TABLE IF EXISTS `election_title`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `election_title` (
  `ELECTION_ID` int(11) NOT NULL,
  `TITLE_ID` int(11) NOT NULL,
  PRIMARY KEY (`ELECTION_ID`,`TITLE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `election_title`
--

LOCK TABLES `election_title` WRITE;
/*!40000 ALTER TABLE `election_title` DISABLE KEYS */;
INSERT INTO `election_title` VALUES (1,1);
/*!40000 ALTER TABLE `election_title` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supervisor`
--

DROP TABLE IF EXISTS `supervisor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supervisor` (
  `ID` int(11) NOT NULL,
  `USER_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `USER_ID` (`USER_ID`),
  CONSTRAINT `SUPERVISOR_ibfk_1` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supervisor`
--

LOCK TABLES `supervisor` WRITE;
/*!40000 ALTER TABLE `supervisor` DISABLE KEYS */;
/*!40000 ALTER TABLE `supervisor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `title`
--

DROP TABLE IF EXISTS `title`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `title` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TITLE_NAME` varchar(45) NOT NULL,
  `PERMISSION` int(2) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `title`
--

LOCK TABLES `title` WRITE;
/*!40000 ALTER TABLE `title` DISABLE KEYS */;
INSERT INTO `title` VALUES (1,'MANAGER',7),(2,'DIRECTOR',7),(3,'ENGINEER',3),(4,'SENIOR ENGINEER',3),(5,'INTERN',1),(6,'COUNTER',0);
/*!40000 ALTER TABLE `title` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `FIRST_NAME` varchar(50) NOT NULL,
  `LAST_NAME` varchar(50) NOT NULL,
  `TITLE_ID` int(11) NOT NULL,
  `DOB` varchar(64) NOT NULL,
  `PASSWORD` varchar(64) NOT NULL,
  `USERNAME` varchar(50) NOT NULL,
  `PUBLIC_KEY` varchar(512) DEFAULT NULL,
  `SALT` varchar(8) NOT NULL,
  `IS_OTP` int(2) DEFAULT '0',
  `LOGIN_ATTEMPTS` int(8) DEFAULT '0',
  `EMAIL` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Yunfan','Zhang',1,'8bdd64eba00d4e0f8e07ddb1e6a8f16908a254b8c56eac7c450be384cc8b587c','9c60ed79f8f7f126f2ec73de556264c91ddf45fb7b687ecf7a7ac27a56d9d38b','zhang','MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA3nA05mwCDlF6RhSHiD7Z6XeKoxzWEP9A\nEKE4pnS/tzvi1+nGJt1Lh+ZjFgasj/woscFFvAII2is0EnMGDxgnyakHIM53pGZsBFUsaNCJjXja\nxThNFFZy0PlIomCGI7c2tsJeGAW7Hy0mgpWRoj0kd+GG6WKu53uD4X77XK60UhQ8TzquREBvxkHe\nR0EKUFY/0bn93t9mGZT6vEZS047CbOa4jmJLnnfWNTCm1yAwWL7iG9x3RM1hccqlO61LBqyMeDvM\nXI7lZjbk7KTTRjoySi5bwJVEZ7cNnpD++qa4gXkwM1vGrCRpLBj0kF4dtl7qvpyIDNzJRhdvmIAO\n86D9BQIDAQAB\n','1234',0,0,'test@email.com'),(2,'Jirapat','Temvuttirojn',3,'3c45977094377a684bbcba3794cba6ba048b97f281da54aec27cca6863b50f96','ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f','poom','MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmLje6E5FTXM2HiyWIaA5hb4pVMAXpvEt\r\n0pZEzF3/dkUp+hXSoMlI2L/KmxHrUZBfheQzBoZk3R0eVhE1Zw0kLa8uKM5BimMoYsybJyoIsiCR\r\nlyMbJnGWmvbbtYc5qBvVrtyae0lDQWvIfxtXFE4mfUlnQXU3ahl/spbSuO33KSKRHk655WzoxhBa\r\nuG6qyVDlkrKuho1N0vFUw7v+S6x7YjSBT7eM84xNm3E1+JYtFJ70SxZI/qCcMiMBUXsdWhWYQbWB\r\npw2bUI6F6rve9LVMG9Cd8067V9gdCgp1YFUa4S/cmAujeI/aJ0URSq/tRdZUodjcQZRQWfSa3TO0\r\nZr9DcwIDAQAB\r\n','1234',0,1,'jipoom@gmail.com'),(3,'Alemberhan','Getahun',1,'c1c2d2ac12d24ea46eedfafea0eb10b6012a2e56f0f2b06d121a46e92760c28a','ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f','alem','MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAqLIP2xwEQ6xqDvjvVz/AEf8srmRZi4yS\nRzIXmpWcvQLWrkKROo0vPVzOg8C3VHhuvGE0XbwSHHj5p00GnCbKJAkYXkQGeXWFQeKuoY3MGh1m\nh1Y3ftXQXhmz2ei6qXbbYzk+lsH0coMGMSJ5Xs+GZkg+MZgZsrcyVqcJH0turSemG1CqdOSdHPmi\n/WK2vfabWR2gC+PkTVdN9wiesDTddI/Dcsh+fdFrHeTkCU+rfbLnVzTdhMaWWV+MjakOqTG3+9X8\n8Qgpq4qe9TL6pkwyypgeqdDlSKIOOMwRiO45pqK0ShyIQ61B7cuEoycT8WhBIf+LjZ0f1+W7r5aT\nLUYJJQIDAQAB\n','1234',0,0,'test@email.com'),(4,'John','Ordway',3,'70187d8ab21c68db97962f728b750c0a5264bf36ef607a29a0cd0d0f672c8800','ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f','ordway','MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlSZlrXecizcH63WS5bFNmB7a0m7Q9pC3\nJukMiNmQZJnca7JdNWdLI75k60PzCCfsRFpS5lkqNz3igItEZyN7yJsG9cJJu46ZPc9nzM4ireJv\n/AZruxGiuZBRrEJlCdl90oETmbVMWl9sIS5z/mNiSgVv9a94Yp7WbCKaQfvMqXRXaCVzJxVgXbuS\nxfGeqoew/BaGijuSXr8N2TdB1ICPsqMBWjHNFg/1TGggFOEqZQ8H4zvnZpLcU/tIUJk7OhWITQdJ\nU23NyZxu1Plx1yiQEr4JSYsy/e+A42oil7NymBmTnDpyGE1THx99hG+2BO5+tNLrd1rDI7XFIvkL\nWPL5vQIDAQAB\n','1234',0,0,'test@email.com'),(5,'Tom','Leykis',4,'0ea9ac2b8855b5b4babc0ebfa91e2c3f84450e5ad8c0e2a97e184608561ef876','ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f','tom','MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlADbXFCclOb0HdpFDK8aSnTKSL00ZN99\r\ndxrfXxqzu7KvG9KCDn1RWgIt1UEePLQJ7aamFsgfpcnNGCkqDaRQe03G8YpQY3irJNXQLTn1ro5x\r\n7ONI/0S+UzMtUAcdya9q1tpDIZ7/K06cg6zP/ZeXAo72GFL4XwQHPvCoXXWYdKtGcR0swDJZFOWK\r\nsH047vpUOe6yxi54eDpXtK9ViugBQPSQeAVXhenGNZS4BJdKNkuMUbtudxyin/vK53RGh+o7EdeE\r\nEG8bqW0UW/JcUIC04ecW5Rm9sc1p38RkdnGr6eo92LhdvjhmTLB84ZvMI/vIzK8Uj8aSbjnT6q7T\r\nVLGBAQIDAQAB\r\n','1234',0,0,'test@email.com'),(6,'Linus','Pauling',5,'9a0867d8fcfaf0be8bfc1e2247cef4baaaa0722a7cc24d2ae7d400b72131b260','ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f','pauling','MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAjcY8KWYkFjQJV4BBggvhzTMOwJmsQTXJ\nThpWWXqciPmdIy96UOG3WohK5j0uXVYgnAGzl8Lc6Kgmz4cPxQlveTYz4QB+7ozD9L3QcH3v62mS\nkP2K+oKk5Mxb4P6ECst0zKopjVHz/ku2I0C2JS9lPC4VYQJnR5nMWs2KBvnpxJCGK9+N4Opbp1nG\njAZcQSyfQeS7Tlr+OLAdc70YP9TWGQfObOKmY8k1lRWIEJjrb26fabJqdf5uMufDoP9qRnSdWrCg\nDyFd7uvzrUa+/g2dcrICVvRi9VKhqtcmp1FZl0skZJCfYB3INEAU9Ui6N0gm79Ajj5+zGM54os6I\nvbWYOwIDAQAB\n','1234',0,0,'test@email.com'),(7,'Henry','Ford',4,'9e0b38b2d6b1067084868fac3f1749c18735ce918a277cc4a907d9347ebd9c6f','ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f','ford','MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtIBEd9pEDv18SGLWQOWl3/nblHa/AC6a\nkmHsxpa4tNoi74SChFWqlzQaWdRbV06ex6ZxWCbn4PpWHtw2oYwDamurHd8L4h0eMQa9a3RXwG1z\ncPmoGCgJiqTyLfOhmnVn/t21eqmSHHLFX5Fl+mFsd2EEZlxINzQVIxRfRIZfgsbjKy1BUfy7wNnc\n2A9coCRoPnnvC0266btyHOCdG6KRBGmGXbZOrz6Aehgy9mqrFayePQAwbRvE5LWkj7b9C7CjXpjD\nN8yQXSk+xl9ZayUDmqd7BSwCnZhVnAdqjKK26Sx99IGnQiQPyqigbtvarqdBYw27BAO1SMbiW9An\n71M/mQIDAQAB\n','1234',0,0,'test@email.com'),(8,'Test','Test',6,'9e0b38b2d6b1067084868fac3f1749c18735ce918a277cc4a907d9347ebd9c6f','ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f','Test','MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAjokvzFLr34HWiekTwwKyaKhbrTQ5Ikt2\r\nNyZqLLFJwa4sEVYmfYlHAuqNDpPXvLh/RetWOwCX85lrCUPz7wdPNdTaBdt2yDNWevawoF3eZzbz\r\n4akZTvzQiR+3yWFZwGUxEYjgyCLPvKSCQAax3SbkyeG/kg4cZnNhwnwzasckMT59Fnk5yNgZUgdi\r\n7uK9CGUXIJ+CBGSGT0r/X2TAVOKu4mEiE3dhL7lGyMEunnAuWA/FZLctJmkIUG/xGDoAWFeAaB7i\r\nYLct+DfG1XUK9YZl2PjYaMddvR6cT34HKkkIEnH9I5QeJofwPRIDKVRhc3RfEv+o97xcFRR3P6xJ\r\nL6yCBwIDAQAB\r\n','1234',0,0,'test@hotmail.com'),(9,'pom','pom',4,'88f725d0b6822d7896ec5a9fd41bda5edadee21df27be4a8e6b692f07c8445ac','03b683271e0cece5d86329cdf41955d979d2dd1fecbebcad8ba394bcb835c6c1','pom','MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnnLxX9EpX6LIARBsEuyrNrEo78gwWrfM\r\nFJT3eoiS+tbz4Je+Y7fBbwbSCNBGu+xGbASApKimMlYaf8DvB83j3PklhwG/tkeyoedZ+v6Xuku2\r\nKUSSuoGI5j6CjH4CMPXDLKf70ndWEotou6pEuV79j4Oltsp3jGeOb63j1ZLJfn2lYVs2wfzLxiz9\r\nqVBEcO3WnFkomw4YLQyBcNHpH/29hrxW+skomhXjOzZDQR37xlc/EPRiEjjBIqiJeY8TqCfgPPkK\r\njUj0YUk43TAT6eHRFnm70Ihle4AM53nYFMukznogy2bwMqTPw7imcgruhfiPTiCJzYzsCcbZPmdM\r\nvPytVQIDAQAB\r\n','1234',1,0,'jipoom@gmail.com'),(10,'poomtest','poomtest',2,'3c45977094377a684bbcba3794cba6ba048b97f281da54aec27cca6863b50f96','f9ffbbddeb6000b63ba259e21ccfb10d6da914167672c44690cdc1a58461baf2','poomtest','MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAzfyDMNa+UhXOLOoDGqBAGGbDq9Id7P9X\r\nWJNFIEn3ZgJmAJs95vj7VAu/J1wyu1UYcYPaHpdDPY1ZFlM8I0DPp7ofoF11/Ye78HvZXOKQ+++p\r\nEF1MtZ5Je6ixFZpYdk63ejor9RWtwq4n+lWom94NOOhAkVRJ7DpP0dvcUKQ6GaaN04rmT+1osR5P\r\nS2ikRUAboVHYIyWM89VGV8r2/0Se6J90064wYVfT88Akd/LuPD0Oh7Rnsyl3867mz6k/Ubm/9PhR\r\nOR79eQiOeJy5xXcRvMg/3V2W8JmDLtPs9q6r15RaZYhCl+V8KHKUO9WQtFYQErz5as2ygRe7n9yn\r\nmEWQAwIDAQAB\r\n','1234',1,0,'jipoom@gmail.com'),(11,'Charle','Lee',3,'bd8b8a2e40976ec987e6eeb2a835f7137606b8809ed577d9e184e296a98c1c30','418b4c25f4f55506bddef4a065527051655dbf5bb9e1ea029c9bf80c8dd251fc','lee','MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAknrCXGA5uaOyPcE02pTT4lKDIGSux8JK\r\nLYWAx5k+emKltLzN+r2Q2yG60el1xPx4ww4rmn4MmR5bmrtROIM1ELQO+MIv3TDSbbB21Bx1suDw\r\nTWwcI59ZAyqNyLhTn6LqpAW80jvmdn/Zo1F2O4SE0x9OPVv99IXBAz6r5pQSEtguC9G3WetBen+6\r\n4wWAcLnuiwYyIvXN6m/azeKC08AZZcmmo2M5LGeELME0E1EaajjDqNb05yLWiyDhFsY21gaIQZEU\r\nnS8DvX0hXmPNa1XwAwC2qYlsYHnN/lQxORCj/q9PBjR8CI5Set/7ba0ohbaujklWS+2Z/8n0VFlD\r\nImREEQIDAQAB\r\n','c715b547',1,3,'zhangyunfan@gwmail.gwu.edu');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_election`
--

DROP TABLE IF EXISTS `user_election`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_election` (
  `USER_ID` int(11) NOT NULL,
  `ELECTION_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`USER_ID`,`ELECTION_ID`),
  KEY `ELECTION_ID` (`ELECTION_ID`),
  CONSTRAINT `USER_ELECTION_ibfk_1` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`ID`),
  CONSTRAINT `USER_ELECTION_ibfk_2` FOREIGN KEY (`ELECTION_ID`) REFERENCES `election` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_election`
--

LOCK TABLES `user_election` WRITE;
/*!40000 ALTER TABLE `user_election` DISABLE KEYS */;
INSERT INTO `user_election` VALUES (2,2),(2,3);
/*!40000 ALTER TABLE `user_election` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `voter`
--

DROP TABLE IF EXISTS `voter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `voter` (
  `ID` int(11) NOT NULL,
  `USER_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `USER_ID` (`USER_ID`),
  CONSTRAINT `VOTER_ibfk_1` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `voter`
--

LOCK TABLES `voter` WRITE;
/*!40000 ALTER TABLE `voter` DISABLE KEYS */;
/*!40000 ALTER TABLE `voter` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-11-22 17:39:22
